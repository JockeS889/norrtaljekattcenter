<?php header('Content-type: text/plain; charset=utf-8');

$servername = "mysql531.loopia.se";
$username = "develop@k173002";
$password = "uhJo10_f";
$dbname = "kattcenter_com";

$conn = new mysqli($servername, $username, $password, $dbname);

if($conn->connect_error){
	die("Connection failed: " . $conn->connect_error);
}

/* change character set to utf8 */
if (!$conn->set_charset("utf8")) {
  die("Error loading character set utf8");
}

/* change character set to utf8 */
if (!$conn->set_charset("utf8")) {
  die("Error loading character set utf8");
}


$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
$form_action = isset($_POST['form_action']) ? $_POST['form_action'] : null;
$action_upload = "UPLOAD";
$action_upload_private_cat = "UPLOAD_PRIVATE_CAT";
$action_upload_vetdoc = "UPLOAD_VETDOC";
$action_download = "DOWNLOAD";
$action_download_private_cat = "DOWNLOAD_PRIVATE_CAT";
$action_download_names = "DOWNLOAD_NAMES";
$action_update_family = "UPDATE_FAMILY";
$action_get_family_connections = "GET_FAMILY_CONNECTIONS";



function testUpload($connection){
	
	//$stmt2 = $connection->prepare("SELECT * FROM personel");
	
	$stmt2 = $connection->prepare("INSERT INTO catform_vetdoc_outergenitalias (Övrigt, Onormal_testikelstorlek) VALUES (?, ?);");
	$stmt2->bind_param('ss', $p1, $p2);
	$p1 = "false";
	$p2 = "false";
	$result = $stmt2->execute();
	if($result){
		echo "TEST UPLOAD SUCCESS";
	}
	else{
		echo "TEST UPLOAD FAILED";
	}
	$stmt2->close();
	
}

function cats($connection, $catid, $owner, $isPrivateCat){
	$select_query = "SELECT catcenter_id FROM cats;";
	$stmt = $connection->prepare($select_query);
	$exists = false;
	$success = true;
	if($stmt->execute()){
		$stmt->bind_result($res);
		while($stmt->fetch()){
			//echo $res;
			//echo $catid;
			if(strcmp($res, $catid) == 0){
				//EXISTS
				$exists = true;
				break;
			}
		}
		//echo $exists;
		$stmt->close();
		if($exists == false){
			//echo "NOT EXISTS";
			
			$insert_query = "INSERT INTO cats (catcenter_id, onCatHome, owner, isPrivateCat) VALUES (?,?,?,?);";
			$stmt2 = $connection->prepare($insert_query);
			$stmt2->bind_param('ssss', $p, $p0, $p1, $p2);
			$p0 = 1;
			$p = $catid;
			$p1 = $owner;
			$p2 = $isPrivateCat;
			if($stmt2->execute()){
				$success = true;
			}
			else{
				$success = false;
			}
			$stmt2->close();
		}
	}
	return $success;
}


function uploadForm($connection, $catid, $table, $keys, $values, $placeHolders){
	$select_query = "SELECT * FROM $table WHERE catcenter_id = ?;";
	$stmt = $connection->prepare($select_query);
	$stmt->bind_param('s', $cid);
	$cid = $catid;
	if($stmt->execute()){
		$stmt->store_result();
		$rows = $stmt->num_rows;
		$params = array();
		foreach($values as &$v){
			$params[] = &$v;
		}
		$query = null;
		if($rows == 1){
			//UPDATE
			$stmt->close();
			$parameters = implode(', ', array_map(function($p){return "$p=?";}, array_values($keys)));
			$query = "UPDATE $table SET " . $parameters . " WHERE catcenter_id = ?;";
			$params[] = $catcenter_id;
		}
		elseif($rows == 0){
			//INSERT
			$stmt->close();
			$parameters = implode(', ', $keys);
			$query = "INSERT INTO $table ". '(' . $parameters . ') values ' .
					'(' . implode(', ', $placeHolders) . '); ';
		}
		else{
			//ERROR
			echo $rows;
			$stmt->close();
			die("ERROR UPLOAD");
		}
		
	}	
}



function getNames($connection, $substring){
	$query = "SELECT catcenter_name FROM catform_basic";

	$stmt = $connection->prepare($query);
	$arr = array();
	
	if($stmt->execute()){
		$stmt->bind_result($res);
		while($stmt->fetch()){
			//echo "RESULT ";
			//echo $res;
			if(strcmp($substring, substr($res, 0, strlen($substring))) == 0){
				$arr[] = $res;
			}
		}
	}
	return "autocomplete_name:" . base64_encode(implode(":", $arr));

}


function updateCatStatusCategories($conn, $values, $catid){
	//print_r($keys);
	//print_r($values);
	$arr = array();
	foreach($values as $key => $value){
		$exploded = explode(" ", $value);
		switch($exploded[0]){
			case "Katthem":
				$arr['onCatHome'] = count($exploded) == 2 ? 1 : 0;
				break;
			case "Bokad":
				$arr['waiting'] = count($exploded) == 2 ? 1 : 0;
				break;
			case "Jourhem":
				$arr['onCallCenter'] = count($exploded) == 2 ? 1 : 0;
				break;
			case "TNR":
				$arr['onTNR'] = count($exploded) == 2 ? 1 : 0;
				break;
			case "Adopterad":
				$arr['adopted'] = count($exploded) == 2 ? 1 : 0;
				break;
			case "Avliden":
				$arr['deceased'] = count($exploded) == 2 ? 1 : 0;
				break;
			case "Åter":
				$arr['returnedOwner'] = count($exploded) == 3 ? 1 : 0;
				break;
			case "Bortsprungen/saknad":
				$arr['missing'] = count($exploded) == 2 ? 1 : 0;
				break;
			default:break;
		}
	}
	$keys = array_keys($arr);
	$values = array_values($arr);
	$params = array();
	foreach($values as &$v){
		$params[] = &$v;
	}
	$parameters = implode(', ', array_map(function($p){return "$p=?";}, array_values($keys)));
	$query = "UPDATE cats SET " . $parameters . " WHERE catcenter_id = ?;";
	$params[] = $catid;
	$stmt = $conn->prepare($query);
	$types = array(str_repeat('s', count($params)));
	$values = array_merge($types, $params);
	call_user_func_array(array($stmt, 'bind_param'), $values);
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}


function selectImgSrcFromCat($conn, $catid){
	$query = "SELECT img_src FROM cats WHERE catcenter_id=?;";
	
	$stmt = $conn->prepare($query);
	$stmt->bind_param('s', $catid);
	$stmt->execute();
	$stmt->bind_result($img_src);
	$stmt->fetch();
	$stmt->close();
	return $img_src;
}
function privateCatExist($connection, $owner, $param, $value){
	$query = "SELECT photo_src FROM catform_basic WHERE owner=?;";
	$stmt = $connection->prepare($query);
	$stmt->bind_param('s', $owner);
	$exists = false;
	if($stmt->execute()){
		$stmt->bind_result($photo_src);
		while($stmt->fetch()){
			if(strcmp($photo_src, $current_photo_src) == 0){
				$exists = true;
				break;
			}
		}
	}
	$stmt->close();
	return $exists;
}


/*
	$conn = connection
	$parent_type = "mother|father"
	$catid = catid
	$value = B64(parentname:parentid)
*/
function createParentConnection($conn, $parent_type, $catid, $value){
	
	$query = "UPDATE cats SET $parent_type=? WHERE catcenter_id=?;";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('ss', $p1, $p2);
	$p1 = $value;
	$p2 = $catid;
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}

function getParentConnection($conn, $parent_type, $catid){
	
	$query = "SELECT $parent_type FROM cats WHERE catcenter_id=?;";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('s', $catid);
	$stmt->execute();
	$stmt->bind_result($parent);
	$stmt->fetch();
	$stmt->close();
	$blub = "";
	if(strcmp($parent, "") == 0){
		
	}
	else{
		$expl = explode(":", base64_decode($parent));
		$img_src = selectImgSrcFromCat($conn, $expl[1]);
		$blub = $parent . ":" . $img_src;
		/*
		$query2 = "SELECT img_src FROM cats WHERE catcenter_id=?;";
		$expl = explode(":", base64_decode($parent));
		//echo "SSTS " . $expl[0] . " " . $expl[1];
		$stmt2 = $conn->prepare($query2);
		$stmt2->bind_param('s', $expl[1]);
		$stmt2->execute();
		$stmt2->bind_result($img_src);
		$stmt2->fetch();
		$stmt2->close();
		//echo "SRC: " . $img_src;
		$blub = $parent . ":" . $img_src;
			*/
	}
	return $blub;

}

/*
	$conn = connection
	$parentid = parentid
	$child = B64(catname:catid)
	$children = B64(catname2:catid2):...:B64(catnameN:catidN)
	$allChildren = B64(catname1:catid1):...:B64(catnameN:catidN)
*/
function createChildConnection($conn, $parentid, $child){
	$result = null;
	
	
	//$child_id = explode(":", $child)[1];
	//$img_src = selectImgSrcFromCat($conn, $child_id);
	
	$children = getChildConnection($conn, $parentid);
	$allChildren = "";
	
	if($children == null || strcmp($children, "") == 0){
		//$allChildren = base64_encode($child . ":" . $img_src);
		$allChildren = base64_encode($child);
	}
	else{
		
		
		//echo "IMAGE SORUCE: " . $img_src;
		
		//$allChildren = base64_encode($child . ":" . $img_src) . ":" . $children;
		$allChildren = base64_encode($child) . ":" . $children;
	}
	$query = "UPDATE cats SET children=? WHERE catcenter_id=?;";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('ss', $allChildren, $parentid);
	$result = $stmt->execute();
	$stmt->close();	
	
	
	/*
	//$cat = $catname . ":" . $catid;
	if(hasChildConnection($children, $child) == 0){
		//$children[] = $value;
		$query = "UPDATE cats SET children=? WHERE catcenter_id=?;";
		$stmt = $conn->prepare($query);
		$stmt->bind_param('ss', $p1, $p2);
		$p1 = $children;
		//$p1 = arrayToString($children);
		$p2 = $catid;
		$result = $stmt->execute();
		$stmt->close();
	}
	*/
	return $result;	
}

/*
$children = B64(catname:catid):B64(catname:catid)....
*/
function getChildConnection($conn, $catid){
	$query = "SELECT children FROM cats WHERE catcenter_id=?;";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('s', $catid);
	$stmt->execute();
	$stmt->bind_result($children);
	$stmt->fetch();
	$stmt->close();
	//if(strcmp($children, "") == 0) return null;
	//$children = explode(":", $children);
	return $children;
}

/*
 determines if $cat exists among $children 
 0 = not exists
 1 = exists 
*/
function hasChildConnection($children, $cat){
	$exists = 0;
	foreach($children as $child){
		if(strcmp($child, "") == 0) continue;
		if(strcmp($cat, $child) == 0){
			$exists = 1;
			break;
		}
	}
	return $exists;
}


//	B64(catname:catid):B64(catname:catid)....

function getSiblingConnections($conn, $catid){
	$query = "SELECT siblings FROM cats WHERE catcenter_id=?;";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('s', $catid);
	$stmt->execute();
	$stmt->bind_result($siblings);
	$stmt->fetch();
	$stmt->close();
	//if(strcmp($siblings, "") == 0) return null;
	//$siblings = explode(":", $siblings);
	return $siblings;
}

/*
	determines if $cat exists among $siblings
	0 = not exists
	1 = exists
	
	$sibling = B64(catname:catid)
	$cat = catname:catid
*/
function hasSiblingConnection($siblings, $cat){
	$exists = 0;
	$siblings_array = explode(":", $siblings);
	foreach($siblings_array as $sibling){
		if(strcmp($sibling, "") == 0) continue;
		if(strcmp(base64_encode($cat), $sibling) == 0){
			$exists = 1;
			break;
		}
	}
	return $exists;
}


function createSiblingConnection2($conn, $catid, $sibling, $b64encoded){
	if($b64encoded){
		$sibling = base64_decode($sibling);
	}
	return createSiblingConnection($conn, $catid, $sibling);
}

/*
	$sibling = B64(catname:catid:catimgsrc)
*/
function createSiblingConnection($conn, $catid, $sibling){
	$result = null;
	$siblings = getSiblingConnections($conn, $catid);
	$allSiblings = "";
	if($siblings == null || strcmp($siblings, "") == 0){
		$allSiblings = base64_encode($sibling);
	}
	else{
		$allSiblings = base64_encode($sibling) . ":" . $siblings;
	}
	
	$query = "UPDATE cats SET siblings=? WHERE catcenter_id=?;";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('ss', $allSiblings, $catid);
	$result = $stmt->execute();
	$stmt->close();
	/*
	//$cat = $catname . ":" . $catid;
	if(hasSiblingConnection($siblings, $value) == 0){
		//$siblings[] = $value;
		$query = "UPDATE cats SET siblings=? WHERE catcenter_id=?;";
		$stmt = $conn->prepare($query);
		$stmt->bind_param('ss', $p1, $p2);
		$p1 = $siblings;
		//$p1 = arrayToString($siblings);
		$p2 = $catid;
		$result = $stmt->execute();
		$stmt->close();
	}
	*/
	return $result;
}



function stringToArray($string_list){
	$arr = str_replace("[", "", $string_list);
	$arr = str_replace("]", "", $arr);
	if(strcmp($arr, "") == 0) return null;
	$arr = explode(", ", $arr);
	return $arr;
}

function arrayToString($arr){
	if(count($arr) == 1){
		return $arr[0];
	}
	else if(count($arr) == 0){
		return "";
	}
	return implode(",", $arr);
}


function getCatSex($conn, $catid){
	$query = "SELECT sex FROM catform_basic WHERE catcenter_id=?;";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('s', $catid);
	$stmt->execute();
	$stmt->bind_result($sex);
	$stmt->fetch();
	$stmt->close();
	return $sex;
}

/*
$conn = connection
$json = data
$catid = catid
$key = "mother|father|siblings|children"
if $key = "mother|father":
	$value = B64(catname:catid)


*/

function update_cat_family_members($conn, $catid, $catname, $json){
	$total_updates = 0;
	$cat = $catname . ":" . $catid;
	foreach($json as $key => $value){
		switch($key){
			case "mother":
			case "father":
				if(strcmp($value, "") != 0){
					//$ee = explode("   ", $value);
					//$value = $ee[0] . ":" . $ee[1];
					
					createParentConnection($conn, $key, $catid, $value);
					$parent = base64_decode($value);
					$parent_info = explode(":", $parent);
					$parent_id = $parent_info[1];
					//$cat = $catname . ":" . $catid;
					$img_src = selectImgSrcFromCat($conn, $catid);
					$cat_info = $cat . ":" . $img_src;
					createChildConnection($conn, $parent_id, $cat_info);
					
					if(strcmp($key, "mother")==0){
						$siblings = getSiblingConnections($conn, $catid);
						if(strcmp($siblings, "") != 0){
							$siblings_array = explode(":", $siblings);
							foreach($siblings_array as $sibling){
								$sibling_decoded = base64_decode($sibling);
								$sibling_info = explode(":", $sibling_decoded);
								$sibling_id = $sibling_info[1];
								createParentConnection($conn, $key, $sibling_id, $value);
								createChildConnection($conn, $parent_id, $sibling_decoded);
							}
						}
					}	

					

					
					
				}


				break;
			case "sibling":
				//$siblings = stringToArray($value);
				if($value != null && strcmp($value, "") != 0){
					$sibling = $value;
					$sibling_decoded = base64_decode($sibling);
					$sibling_id = explode(":", $sibling_decoded)[1];
					$sibling_imgsrc = selectImgSrcFromCat($conn, $sibling_id);
					$sibling_info = $sibling_decoded . ":" . $sibling_imgsrc;
					$parent_info = getParentConnection($conn, "mother", $catid);
					if(strcmp($parent_info, "") != 0){
						$parent = explode(":", $parent_info)[0];
						$parent_decoded = base64_decode($parent);
						$parent_info = explode(":", $parent_decoded);
						$parent_id = $parent_info[1];
						createParentConnection($conn, "mother", $sibling_id, $parent);
						createChildConnection($conn, $parent_id, $sibling_info);
					}
					
					$siblings = getSiblingConnections($conn, $catid);
					$siblings_array = explode(":", $siblings);
					foreach($siblings_array as $current_sibling){
						$current_sibling_decoded = base64_decode($current_sibling);
						$current_sibling_id = explode(":", $current_sibling_decoded)[1];
						createSiblingConnection($conn, $sibling_id, $current_sibling_decoded);
						createSiblingConnection($conn, $current_sibling_id, $sibling_info);
					}
					
					
					
					createSiblingConnection($conn, $catid, $sibling_info);
					$img_src = selectImgSrcFromCat($conn, $catid);
					$cat_info = $cat . ":" . $img_src;
					createSiblingConnection($conn, $sibling_id, $cat_info);
					

					

					
				}
				
				break;
			case "child":
				//$children = stringToArray($value);
				if($value != null && strcmp($value, "") != 0){
					$child_encoded = $value;
					$child_decoded = base64_decode($child_encoded);
					$child_id = explode(":", $child_decoded)[1];
					$child_img = selectImgSrcFromCat($conn, $child_id);
					$child_info = $child_decoded . ":" . $child_img;
					//$cat = $catname . ":" . $catid;
					$sex = getCatSex($conn, $catid); //Hämta Kattens kön
					$parent_type = null;
					switch($sex){
						case "HONA":
							$parent_type = "mother";
							break;
						case "HANE":
							$parent_type = "father";
							break;
						default: break;
					}
					if($parent_type != null){
						createParentConnection($conn, $parent_type, $child_id, base64_encode($cat));
						
						$children = getChildConnection($conn, $catid);
						if($children == null || strcmp($children, "") == 0){
							
						}
						else{
							$children_array = explode(":", $children);
							//echo "CHILDREN ARRAY\n";
							//print_r($children_array);
							foreach($children_array as $current_child){
								$current_child_decoded = base64_decode($current_child);
								$current_child_id = explode(":", $current_child_decoded)[1];
								createSiblingConnection($conn, $child_id, $current_child_decoded);
								createSiblingConnection($conn, $current_child_id, $child_info);
							}
						}

						
						createChildConnection($conn, $catid, $child_info);
						
								
					}
					
				}

				break;
			default: break;
		}
	}
}




if($form_action != null && (strcmp($form_action, $action_upload) == 0 || strcmp($form_action, $action_upload_vetdoc) == 0)){

	
	$decoded_blub = base64_decode($blub);
	$exploded_blub = explode(":",$decoded_blub);
	
	//print_r(array_values($exploded_blub));
	
	
	$formType = $exploded_blub[0];
	$blub = $exploded_blub[1];

	$catcenter_id = $exploded_blub[2];
	$owner = $exploded_blub[4];
	$isPrivateCat = $exploded_blub[5];
	
	if(cats($conn, $catcenter_id, $owner, $isPrivateCat)){	//ADD (IF NOT EXISTS) AN ENTRY TO TABLE cats ALWAYS
	
		$rest_type = $exploded_blub[3];
		$json_type = "json";
		$blub = base64_decode($blub);
		$placeHolders = null;
		if($rest_type != null && strcmp($rest_type, $json_type) == 0){
			$json = json_decode($blub, true);
			$keys = array_keys($json);
			$values = array_values($json);
			if(strcmp($form_action, $action_upload) == 0){
				switch ($formType){
					case "catform_medical":
					case "catform_vaccination":
					case "catform_deworming":
					case "catform_verm":
					case "catform_claws":
					case "catform_weight":
						$keys[] = "table_name";
						$values[] = $formType;
						break;
					default: break;
				}

			}
			//echo $keys;
			//echo $values;
			$placeHolders = array_fill(0, count($values), '?');
			//return;
		}
		else{
			if(strcmp(substr($blub, -1), ":") == 0){
				$blub = substr($blub, 0, -1);
			}
			$parent = explode(":", $blub);  //[---,---,---,..]
			$keys = array();
			$placeHolders = array_fill(0, count($parent), '?');
			$values = array();
			foreach($parent as $form){
				$form = base64_decode($form); //form = key:value
				$exploded_form = explode(":", $form); //e_form = [key, value]
				$keys[] = $exploded_form[0];
				$values[] = !empty($exploded_form[1]) ? $exploded_form[1] : "";
			}
		}
		
		
		
		
		$select_query = "SELECT * FROM $formType WHERE catcenter_id = ?;";
		$stmt = $conn->prepare($select_query);
		$stmt->bind_param('s', $cid);
		$cid = $catcenter_id;
		
		
		if($stmt->execute()){
			
			$stmt->store_result();
			$rows = $stmt->num_rows;
			$params = array();
			foreach($values as &$v){
				$params[] = &$v;
			}
			$query = null;
			
			if(strcmp($formType, "catform_basic") == 0 || strcmp($formType, "catform_regskk") == 0){
				//echo "ROWS: " . $rows;
				if($rows == 1){
					//UPDATE
					$stmt->close();
					$parameters = implode(', ', array_map(function($p){return "$p=?";}, array_values($keys)));
					$query = "UPDATE $formType SET " . $parameters . " WHERE catcenter_id = ?;";
					$params[] = $catcenter_id;
				}
				elseif($rows == 0){
					//INSERT
					$stmt->close();
					$parameters = implode(', ', $keys);
					$query = "INSERT INTO $formType ". '(' . $parameters . ') values ' .
							'(' . implode(', ', $placeHolders) . '); ';
				
				}
				else{
					//ERROR
					$stmt->close();
					die("ERROR UPLOAD");
				}
			}
			else{
				//INSERT
				$stmt->close();
				$parameters = implode(', ', $keys);
				$query = "INSERT INTO $formType ". '(' . $parameters . ') values ' .
						'(' . implode(', ', $placeHolders) . '); ';
			}
			
			//echo "QUERY: " . $query;

			$stmt2 = $conn->prepare($query);
			
			$types = array(str_repeat('s', count($params)));
			$values = array_merge($types, $params);
			
			//print_r($values);
			
			call_user_func_array(array($stmt2, 'bind_param'), $values);
			
			$result = $stmt2->execute();
			

			
			
			
			$upload_status = null;
			$upload_status2 = null;
			$upload_status_failed = null;
			$upload_status2_failed = null;
			if(strcmp($form_action, $action_upload) == 0){
				$upload_status = "FORM_UPLOAD_SUCCESS";
				$upload_status2 = "UPLOADED_FORM";
				$upload_status_failed = "FORM_UPLOAD_FAILED";
				$upload_status2_failed = "UPLOADED_FORM_FAILED";
			}
			elseif(strcmp($form_action, $action_upload_vetdoc) == 0){
				$upload_status = "FORM_VETDOC_UPLOAD_SUCCESS";
				$upload_status2 = "UPLOADED_FORM_VETDOC";
				$upload_status_failed = "FORM_VETDOC_UPLOAD_FAILED";
				$upload_status2_failed = "UPLOADED_FORM_VETDOC_FAILED";
			}
			
			if($result){
				$stmt2->close();
				if(strcmp($formType, "catform_status") == 0){
					if(updateCatStatusCategories($conn, $values, $catcenter_id)){
						echo $upload_status . ":" . base64_encode($upload_status2 . ":" . base64_encode($formType));
					}
					else{
						echo $upload_status_failed . ":" . base64_encode($upload_status2_failed . ":" . base64_encode($formType));
					}
				}
				else{
					echo $upload_status . ":" . base64_encode($upload_status2 . ":" . base64_encode($formType));
				}
			}
			else{
				echo $upload_status_failed . ":" . base64_encode($upload_status2_failed . ":" . base64_encode($formType));
			}
			
			
		}
		
	}
	else{
		die("ERROR UPLOAD");
	}
	
}
elseif($form_action != null && strcmp($form_action, $action_upload_private_cat) == 0){
	
	$decoded_blub = base64_decode($blub);
	$json = json_decode($decoded_blub, true);
	
	$keys = array_keys($json);
	$parameters = implode(', ', $keys);
	
	$values = array_values($json);
	$placeHolders = array_fill(0, count($values), '?');
	$params = array();
	foreach($values as &$v){
		$params[] = &$v;
	}
	
	
	$exists = privateCatExist($conn, $json['owner'], "name", $json['name']);
	if($exists == false){
		$exists = privateCatExist($conn, $json['owner'], "photo_src", $json['photo_src']);
	}
	if($exists == false){
		$query = "INSERT INTO catform_basic ". '(' . $parameters . ') values ' .
		'(' . implode(', ', $placeHolders) . '); ';	
	}
	else{
		//$parameters = implode(', ', array_map(function($p){return "$p=?";}, array_values($keys)));
		//$query = "UPDATE $formType SET " . $parameters . " WHERE catcenter_id = ?;";
		//$params[] = $catcenter_id;
		$parameters = implode(', ', array_map(function($p){return "$p=?";}, array_values($keys)));
		$query = "UPDATE catform_basic SET " . $parameters . " WHERE owner=?;";
		$params[] = $json['owner'];
	}

	
	
	$stmt = $conn->prepare($query);

	$types = array(str_repeat('s', count($params)));
	$values = array_merge($types, $params);
	call_user_func_array(array($stmt, 'bind_param'), $values);
	$result = $stmt->execute();
	
	$result = true;
	if($result){
		echo "ADDED_PRIVATE_CAT_SUCCESS";
	}
	else{
		echo "ADDED_PRIVATE_CAT_FAILED";
	}
}

elseif($form_action != null && strcmp($form_action, $action_download) == 0){
	$decoded_blub = base64_decode($blub);
	$param = explode(":", $decoded_blub);
	$form_table = $param[0];
	$cat_id = $param[1];
	$stmt = $conn->prepare("SELECT * FROM $form_table WHERE catcenter_id = ?");
	$stmt->bind_param('s', $cat_id_param);
	$cat_id_param = $cat_id;
	if($stmt->execute()){
		$res = $stmt->get_result();
		//$data_arr = $res_fetch_assoc();
		$rows = "";
		while($data_arr = $res->fetch_assoc()){
			$rows .= json_encode($data_arr) . "\n";
		}
		$encoded_rows = base64_encode($rows);
		//$encoded_json = "DOWNLOADED_FORM:" . base64_encode($form_table . ":" . base64_encode(json_encode($data_arr)));
		//$encoded_blub = "DOWNLOAD_SUCCESS:" . base64_encode($encoded_json);
		$encoded_form = "DOWNLOADED_FORM:" . base64_encode($form_table . ":" . $encoded_rows);
		$encoded_blub = "DOWNLOAD_SUCCESS:" . base64_encode($encoded_form);
		echo $encoded_blub;
	}
	else{
		echo "DOWNLOAD_FAILED";
	}
}

elseif($form_action != null && strcmp($form_action, $action_download_private_cat) == 0){
	$decoded_blub = base64_decode($blub);
	$param = explode(":", $decoded_blub);
	$form_table = $param[0];
	$catname = $param[1];
	$owner = $param[2];
	$stmt = $conn->prepare("SELECT * FROM catform_basic WHERE name=? AND owner=?;");
	$stmt->bind_param('ss', $catname, $owner);
	if($stmt->execute()){
		$res = $stmt->get_result();
		//$data_arr = $res_fetch_assoc();
		$data_arr = $res->fetch_assoc();
		$json = json_encode($data_arr);
		
		$encoded = base64_encode($json);
		//$encoded_json = "DOWNLOADED_FORM:" . base64_encode($form_table . ":" . base64_encode(json_encode($data_arr)));
		//$encoded_blub = "DOWNLOAD_SUCCESS:" . base64_encode($encoded_json);
		$encoded_form = "DOWNLOADED_FORM:" . base64_encode($form_table . ":" . $encoded);
		$encoded_blub = "DOWNLOAD_SUCCESS:" . base64_encode($encoded_form);
		echo $encoded_blub;
	}
	else{
		echo "DOWNLOAD_FAILED";
	}
}


elseif($form_action != null && strcmp($form_action, $action_download_names) == 0){
	$substr = base64_decode($blub);
	$names = getNames($conn, $substr);
	$blub = "DOWNLOADED_NAMES:" . base64_encode($names);
	//echo $blub;
	echo "DOWNLOAD_SUCCESS:" . base64_encode($blub);
	//echo "HEJ";
	//echo $substr;
}
elseif($form_action != null && strcmp($form_action, $action_update_family) == 0){
	$substr = base64_decode($blub);
	//FORMAT = JSON
	//keys = catcenter_id, mother, father, siblings, children
	//values:
	//[catcenter_id] = text
	//[mother] = text
	//[father] = text
	//[siblings] = B64 text (list of id:name)
	//[children] = B64 text (list of id:name)
	$exploded = explode(":", $substr);
	$table_name = $exploded[0];
	$json_encoded = $exploded[1];
	$cat_id = $exploded[2];
	$cat_name = $exploded[3];
	$json_decoded = base64_decode($json_encoded);
	$json = json_decode($json_decoded, true);
	//print_r($json);
	update_cat_family_members($conn, $cat_id, $cat_name, $json);
	$upload_status = "FORM_UPLOAD_SUCCESS";
	$upload_status2 = "UPLOADED_FORM";
	echo $upload_status . ":" . base64_encode($upload_status2 . ":" . base64_encode($table_name));
	//update_cat_family_members($conn, $json['catcenter_id'], $json['mother'], $json['father'], $json['siblings'], $json['children']);
}
elseif($form_action != null && strcmp($form_action, $action_get_family_connections) == 0){
	$decoded_blub = base64_decode($blub);
	$form_table = explode(":", $decoded_blub)[0];
	$catid = explode(":", $decoded_blub)[1];
	
	$mother = getParentConnection($conn, "mother", $catid);
	$father = getParentConnection($conn, "father", $catid);
	$siblings = getSiblingConnections($conn, $catid);
	$children = getChildConnection($conn, $catid);
	$return_val = base64_encode($mother);
	$return_val .= ":" . base64_encode($father);
	$return_val .= ":" . base64_encode($siblings);
	$return_val .= ":" . base64_encode($children);
	
	//$return_val .= ":" . base64_encode(json_encode($siblings, JSON_UNESCAPED_UNICODE));
	//$return_val .= ":" . base64_encode(json_encode($children, JSON_UNESCAPED_UNICODE));
	
	$return_val = base64_encode($return_val);
	$blub = "DOWNLOADED_FORM:" . base64_encode($form_table . ":" . $return_val);
	$encoded_blub = "DOWNLOAD_SUCCESS:" . base64_encode($blub);
	echo $encoded_blub;
}

/*
elseif($form_action != null && strcmp($form_action, $action_download) == 0){
	$decoded_blub = base64_decode($blub);
	$param = explode(":", $decoded_blub);
	$form_table = $param[0];
	$cat_id = $param[1];
	
	$stmt = $conn->prepare("SELECT * FROM $form_table WHERE catcenter_id = ?");
	
	
	$stmt->bind_param('s', $cat_id_param);
	$cat_id_param = $cat_id;
	$data_arr = null;
	$data_values = array();
	if($stmt->execute()){
		$res = $stmt->get_result();
		$data_arr = $res->fetch_assoc();
		
		foreach($data_arr as $key => $value){
			$data_values[] = $value;
		}
	}
	$stmt->close();
	
	$stmt2 = $conn->prepare("SHOW COLUMNS FROM $form_table FROM $dbname");
	$stmt2->execute();
	$res = $stmt2->get_result();
	
	$arr = array();
	while($data = $res->fetch_assoc()){
		foreach($data as $key => $value){
			$arr[] = $value;
			break;
		}
	}
	$col_data = "";
	$combined = array_combine($arr, $data_values);
	foreach($combined as $colname => $colvalue){
		$col_data .= base64_encode($colname . ":" . $colvalue) . ":";
	}
	
	$form = $form_table . ":". base64_encode($col_data);
	$blub = "DOWNLOADED_FORM:" . base64_encode($form);
	$encoded_blub = "DOWNLOAD_SUCCESS:" . base64_encode($blub);
	
	echo $encoded_blub;
	
	$stmt2->close();

}
*/

?>