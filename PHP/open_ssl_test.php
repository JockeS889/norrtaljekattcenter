<?php

function readKeyFromFile(){
	$keyFile = fopen("/keys/key.txt", "r");
	$key = fread($keyfile, filesize("/keys/key.txt"));
	fclose($keyfile);
	return $key;
}

function encryptText($text){
	$enc_key = readKeyFromFile();
	$enc_method = 'AES-128-CTR';
	//$enc_key = openssl_digest(gethostname() . "|" . ip2long($_SERVER['SERVER_ADDR'], 'SHA256', true));
	$enc_iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($enc_method));
	$crypted_token = openssl_encrypt($text, $enc_method, $enc_key, 0, $enc_iv) . "::" . bin2hex($enc_iv);
	unset($text, $enc_method, $enc_key, $enc_iv);
	return $crypted_token;
}

function readKey(){
	$keyFile = fopen("/keys/key.txt", "r");
	$key = fread($keyfile, filesize("/keys/key.txt"));
	fclose($keyfile);
	return $key;
}

function decryptText($text){
	if(preg_match("/^(.*)::(.*)$/", $text, $regs)) {
		$enc_key = readKey();
		list(, $text, $enc_iv) = $regs;
		$enc_method = 'AES-128-CTR';
		//$enc_key = openssl_digest(gethostname() . "|" . ip2long($_SERVER['SERVER_ADDR'], 'SHA256', true));
		$decrypted_token = openssl_decrypt($text, $enc_method, $enc_key, 0, hex2bin($enc_iv));
		unset($text, $enc_method, $enc_key, $enc_iv, $regs);
		return $decrypted_token;
	}
	return null;
}

function ee($text){
	return base64_encode(encryptText($text));
}

function dd($crypto){
	return decryptText(base64_decode($crypto));
}

function test($text){
	$e = ee($text);
	$d = dd($e);
	echo "ENCRYPTED: " . $e;
	
	echo "\n";
	echo "DECRYPTED: " . $d;
	echo "<br></br>";
}

if (isset($_SERVER['HTTPS']) )
{
	/*
	$text = $_GET['e'];
	$e = ee($text);
	$d = dd($e);
	echo "ENCRYPTED: " . $e;
	
	echo "\n";
	echo "DECRYPTED: " . $d;
	*/
	test("Grovstavägen 2");
	test("Norrtälje");
	test("Västernäsvägen 615");
	test("Upplands Väsby");
	test("Hägersten");
	test("Viks Byväg 40");
	test("Engelbrektsvägen 65");
	test("Källgren 6");
	
}
else
{
    echo "UNSECURE: This page is being access through an unsecure connection.<br><br>";
}




/*
$enc_key = openssl_digest(gethostname() . "|" . ip2long($_SERVER['SERVER_ADDR']), 'SHA256', true);
$myfile = fopen("key.txt", "w") or die("Unable to open file");
fwrite($myfile, $enc_key);
fclose($myfile);
*/

/*

// Create the keypair
$res=openssl_pkey_new();

// Get private key
openssl_pkey_export($res, $privatekey);

// Get public key
$publickey=openssl_pkey_get_details($res);
$publickey=$publickey["key"];

echo "Private Key:<BR>$privatekey<br><br>Public Key:<BR>$publickey<BR><BR>";

$cleartext = '1234 5678 9012 3456';

echo "Clear text:<br>$cleartext<BR><BR>";

openssl_public_encrypt($cleartext, $crypttext, $publickey);

echo "Crypt text:<br>$crypttext<BR><BR>";

openssl_private_decrypt($crypttext, $decrypted, $privatekey);

echo "Decrypted text:<BR>$decrypted<br><br>";
*/
?>