<?php header('Content-type: text/plain; charset=utf-8');

//$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
$misc_action = isset($_POST['misc_action']) ? $_POST['misc_action'] : null;

$check_id = "check_id";
$get_names_and_id = "names_and_id";
$get_name_id_catowner = "names_id_catowner";
$get_name_id_imgsrc_catowner = "names_id_imgsrc_catowner";
$update_user_info = "update_user_info";
$update_cat_owner = "set_cat_owner";
$delete_catowner = "delete_catowner";
$delete_personel = "delete_personel";
$update_open_hours = "update_open_hours";
$get_open_hours = "get_open_hours";
$set_catowners = "set_catowners";
$checkin = "checkin";
$delete_timeline_entry = "delete_timeline_entry";

/*
if($blub == null || $misc_action == null){
	die("Not authorized");
}
*/
if($misc_action == null){
	die("Not authorized");
}

$servername = "mysql531.loopia.se";
$username = "develop@k173002";
$password = "uhJo10_f";
$dbname = "kattcenter_com";

$conn = new mysqli($servername, $username, $password, $dbname);

if($conn->connect_error){
	die("Connection failed: " . $conn->connect_error);
}
else{
	//echo "CONNECTION SUCCESS";
}


/* change character set to utf8 */
if (!$conn->set_charset("utf8")) {
  die("Error loading character set utf8");
}



function updateCatowner($conn, $owner, $catid){
	$query = null;
	$stmt = null;
	
	if($catid == null){
		$query = "UPDATE cats SET owner='kattcenter' WHERE owner = ?;";
		$stmt = $conn->prepare($query);
		$stmt->bind_param('s', $p1);
		$p1 = $owner;
	}
	else{
		$query = "UPDATE cats SET owner=? WHERE catcenter_id = ?;";
		$stmt = $conn->prepare($query);
		$stmt->bind_param('ss', $p1, $p2);
		$p1 = $owner;
		$p2 = $catid;
	}
	if($stmt->execute()){
		$stmt->close();
		return "UPDATE_CAT_OWNER_SUCCESS";
	}
	else{
		$stmt->close();
		return "UPDATE_CAT_OWNER_FALED";
	}
	
	//return "UPDATE_CAT_OWNER_SUCCESS";
}

function getOwnerAndDateByCatid($conn, $id){
	
	$query = "SELECT owner, owner_date FROM cats WHERE catcenter_id=?";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('s', $id);
	$return_value = null;
	if($stmt->execute()){
		$stmt->bind_result($owner, $owner_date);
		$stmt->fetch();
		if($owner != null && strcmp($owner, "") != 0 && $owner_date != null && strcmp($owner_date, "") != 0){
			$return_value = $owner . ":" . $owner_date;
		}
	}
	else{
		$return_value = "FAIL";
	}
	$stmt->close();
	return $return_value;
	
	//echo "ID " . $id;
}

function getPreviousOwners($conn, $id){
	$query = "SELECT previous_owner FROM cats WHERE catcenter_id=?";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('s', $id);
	$return_value = null;
	if($stmt->execute()){
		$stmt->bind_result($previous_owner);
		$stmt->fetch();
		$return_value = $previous_owner;
	}
	else{
		$return_value = "FAIL";
	}
	$stmt->close();
	return $return_value;
}

function updatePreviousOwners($conn, $catid, $owners){
	//echo "SET: " . $owners . "  " . $catid . " ";
	$query = "UPDATE cats SET previous_owner=? WHERE catcenter_id=?;";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('ss', $p1, $p2);
	$p1 = $owners;
	$p2 = $catid;
	$return_value = null;
	if($stmt->execute()){
		$stmt->close();
		$return_value = "SUCCESS";
	}
	else{
		$return_value = "FAIL";
	}
	$stmt->close();
	return $return_value;	
}

function isAddedCatownerNewer($addCatownerDate, $currentCatownerDate){
	//echo $addCatownerDate;
	//echo $currentCatownerDate;
	
	$decoded_date1 = base64_decode($addCatownerDate);
	$decoded_date2 = base64_decode($currentCatownerDate);
	$time_1 = strtotime($decoded_date1);
	$time_2 = strtotime($decoded_date2);
	$diff = $time_1 - $time_2;
	if($diff > 0){
		return True;
	}
	return False;
}

function updateCatowner2($conn, $owner, $catid, $date){
	$query = null;
	$stmt = null;
	
	$ownerAndDate = getOwnerAndDateByCatid($conn, $catid);
	$previous_owners = getPreviousOwners($conn, $catid);
	if(strcmp($previous_owners, "FAIL") == 0 || strcmp($ownerAndDate, "FAIL") == 0){
		return "UPDATE_CAT_OWNER_FALED";
	}
	else{
		$decoded_previous_owners = null;
		if($previous_owners != null && strcmp($previous_owners, "") != 0 && strcmp($previous_owners, "FAIL") != 0)
			$decoded_previous_owners = base64_decode($previous_owners);
		//echo "PREV: " . $decoded_previous_owners;
		//echo "OD: " . $ownerAndDate . " PREV: " . $decoded_previous_owners;
		
		$isNewer = False;
		
		if($ownerAndDate != null){
			$expl = explode(":", $ownerAndDate);
			
			if(isAddedCatownerNewer($date, $expl[1])){
				$isNewer = True;
				if($decoded_previous_owners == null)
					$decoded_previous_owners = base64_encode($ownerAndDate);
				else
					$decoded_previous_owners = base64_encode($ownerAndDate) . ":" . $decoded_previous_owners;
			}
			else{
				if($decoded_previous_owners == null)
					$decoded_previous_owners = base64_encode($owner . ":" . $date);
				else
					$decoded_previous_owners = base64_encode($owner . ":" . $date) . ":" . $decoded_previous_owners;
			}				
			

		}
			
		//echo "ADD PREV: " . $decoded_previous_owners;
		$encoded_updated_previous_owners = null;
		if($decoded_previous_owners != null)
			$encoded_updated_previous_owners = base64_encode($decoded_previous_owners);
		$result = null;
		if($encoded_updated_previous_owners != null)
			$result = updatePreviousOwners($conn, $catid, $encoded_updated_previous_owners);
		//echo "RESULT: " . $result;
		if($result == null || strcmp($result, "SUCCESS") == 0){
			if($isNewer == False && $ownerAndDate != null){
				
				
				
				return "UPDATE_CAT_OWNER_SUCCESS";
			}
			else{
				//echo "OWNER: " . $owner . "  DATE: " . $date . "  CATID: " . $catid;
				if($catid == null){
					$query = "UPDATE cats SET owner='Kattcenter', owner_date=? WHERE owner = ?;";
					$stmt = $conn->prepare($query);
					$stmt->bind_param('ss', $p1, $p2);
					$p1 = $date;
					$p2 = $owner;
				}
				else{
					
					$query = "UPDATE cats SET owner=?, owner_date=? WHERE catcenter_id = ?;";
					$stmt = $conn->prepare($query);
					$stmt->bind_param('sss', $p1, $p2, $p3);
					$p1 = $owner;
					$p2 = $date;
					$p3 = $catid;
				}
				if($stmt->execute()){
					$stmt->close();
					return "UPDATE_CAT_OWNER_SUCCESS";
				}
				else{
					$stmt->close();
					return "UPDATE_CAT_OWNER_FALED";
				}
			}
		}
		else{
			return "UPDATE_CAT_OWNER_FALED";
		}
	}
	
	
	
	//return "UPDATE_CAT_OWNER_SUCCESS";
}

function getOwnerByCatid($conn, $id){
	
	$query = "SELECT owner FROM cats WHERE catcenter_id=?";
	$stmt = $conn->prepare($query);
	$stmt->bind_param('s', $id);
	$return_value = null;
	if($stmt->execute()){
		$stmt->bind_result($res);
		$stmt->fetch();
		$return_value = $res;
	}
	$stmt->close();
	return $return_value;
	
	//echo "ID " . $id;
}

function update_hours($conn, $day, $keys, $values){
	$params = array();
	foreach($values as &$v){
		$params[] = &$v;
	}
	$parameters = implode(', ', array_map(function($p){return "$p=?";}, array_values($keys)));
	$query = "UPDATE center_open_hours SET " . $parameters . " WHERE day = ?;";
	$params[] = $day;
	$stmt = $conn->prepare($query);
	$types = array(str_repeat('s', count($params)));
	$values = array_merge($types, $params);
	call_user_func_array(array($stmt, 'bind_param'), $values);
	if($stmt->execute()){
		return "UPDATE_HOURS_SUCCESS";
	}
	else{
		return "UPDATE_HOURS_FAILED";
	}
	$stmt->close();
}

function get_hours($conn){
	$query = "SELECT * FROM center_open_hours;";
	$stmt = $conn->prepare($query);
	if($stmt->execute()){
		$res = $stmt->get_result();
		$jsonRows = "";
		while($arr = $res->fetch_assoc()){
			$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
			$jsonRows .= $json . "\n";
		}
		//$arr = $res->fetch_assoc();
		//$json = json_encode($arr, JSON_UNESCAPED_UNICODE); 
		echo "GET_HOURS_SUCCESS:" . base64_encode("DOWNLOADED_OPEN_HOURS:" . base64_encode($jsonRows));
	}
	else{
		echo "GET_HOURS_FAILED";
	}
	
}


//echo "HEEEEEEEEEEE: ". $misc_action;


if(strcmp($misc_action, $get_names_and_id) == 0){
	
	$query = "SELECT catcenter_id, catcenter_name FROM catform_basic";
	$stmt = $conn->prepare($query);
	if($stmt->execute()){
		$res = $stmt->get_result();
		$jsonRows = "";
		while($arr = $res->fetch_assoc()){
			$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
			$jsonRows .= $json . "\n";
		}
		/*
		$res = $stmt->get_result();
		$blub_string = null;
		while($data_arr = $res->fetch_assoc()){
			$json = json_encode($data_arr, JSON_UNESCAPED_UNICODE);
			$blub_string .= $json . "\n";
		}
		*/
		$blub = "NAMES_AND_ID:" . base64_encode($jsonRows);
		echo "DOWNLOAD_SUCCESS:" . base64_encode($blub);
	}
	else{
		echo "DOWNLOAD_FAILED";
	}
}
elseif(strcmp($misc_action, $check_id) == 0){
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	if($blub == null) echo "CAT_ID_CHECK_ERROR";
	else{
		$catid = base64_decode($blub);
		$query = "SELECT catcenter_id FROM catform_basic WHERE catcenter_id=?;";
		$stmt = $conn->prepare($query);
		$stmt->bind_param('s', $catid);
		if($stmt->execute()){
			$res = $stmt->get_result();
			$rows = mysqli_num_rows($res);
			if($rows == 1){
				echo "CAT_ID_UNAVAILABLE";
			}else{
				echo "CAT_ID_AVAILABLE";
			}
		}		
	}

}
elseif(strcmp($misc_action, $get_name_id_imgsrc_catowner) == 0){
	$query = "SELECT photo_src, catcenter_id, catcenter_name FROM catform_basic";
	$stmt = $conn->prepare($query);
	$array = array();
	if($stmt->execute()){
		$res = $stmt->get_result();
		while($arr = $res->fetch_assoc()){
			$array[] = $arr;
		}
		$stmt->close();
		foreach($array as $arr){
			$owner = getOwnerByCatid($conn, $arr['catcenter_id']);
			$arr['owner'] = $owner;
			$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
			$jsonRows .= $json . "\n";
		}
		$blub = "NAMES_ID_IMGSRC_CATOWNER:" . base64_encode($jsonRows);
		echo "DOWNLOAD_SUCCESS:" . base64_encode($blub);
	}
}
elseif(strcmp($misc_action, $get_name_id_catowner) == 0){
	$query = "SELECT catcenter_id, catcenter_name FROM catform_basic";
	$stmt = $conn->prepare($query);
	$jsonRows = "";
	$array = array();
	if($stmt->execute()){
		$res = $stmt->get_result();
		while($arr = $res->fetch_assoc()){
			//$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
			//$jsonRows .= $json . "\n";
			$array[] = $arr;
		}
		//$blub = "NAMES_ID_CATOWNER:" . base64_encode($jsonRows);
		//echo "DOWNLOAD_SUCCESS:" . base64_encode($blub);
		$stmt->close();
		foreach($array as $arr){
			//echo "CATID " . $arr['catcenter_id'];
			//print_r($arr);
			$owner = getOwnerByCatid($conn, $arr['catcenter_id']);
			$arr['owner'] = $owner;
			$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
			$jsonRows .= $json . "\n";
			//echo "OWNER  " . $owner;
		}
		$blub = "NAMES_ID_CATOWNER:" . base64_encode($jsonRows);
		echo "DOWNLOAD_SUCCESS:" . base64_encode($blub);
	}
	else{
		$stmt->close();
		echo "DOWNLOAD_FAILED";
	}

	
}
elseif(strcmp($misc_action, $update_user_info) == 0){
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	if($blub == null) echo "UPDATE_USER_INFO_FAILED";
	else{
		$exploded = explode(":", base64_decode($blub));
		$form_table = $exploded[0];
		$type = $exploded[1];
		$email = $exploded[2];
		$blub = base64_decode($exploded[3]);
		$json = json_decode($blub, true);
		foreach($json as $key => $value){
			if(strcmp($key, "password") == 0){
				$pass_unhashed = $value;
				$pass_hashed = password_hash($pass_unhashed, PASSWORD_DEFAULT);
				$json['password'] = $pass_hashed;
				break;
			}
		}
		$keys = array_keys($json);
		$values = array_values($json);
		$parameters = implode(', ', array_map(function($p){return "$p=?";}, array_values($keys)));
		$params = array();
		/*
		print_r($json);

		for($i = 0; $i < count($values); $i++){
			echo $values[i] . " " . $keys[i];
			if(strcmp($keys[i], "password") == 0){
				$pass_unhashed = $values[i];
				$pass_hashed = password_hash($pass_unhashed, PASSWORD_DEFAULT);
				$values[i] = $pass_hashed;
			}
		}
		*/
		foreach($values as &$v){
			$params[] = &$v;
		}
		$params[] = $email;
		$query = "UPDATE $form_table SET " . $parameters . " WHERE email = ?;";
		$stmt = $conn->prepare($query);
		$types = array(str_repeat('s', count($params)));
		$values = array_merge($types, $params);
		call_user_func_array(array($stmt, 'bind_param'), $values);
		if($stmt->execute()){
			echo "UPDATE_USER_INFO_SUCCESS:".base64_encode("Type:".$type);
		}
		else{
			echo "UPDATE_USER_INFO_FALED";
		}
	}
}
elseif(strcmp($misc_action, $update_cat_owner) == 0){
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	if($blub == null) echo "SET_CATOWNER_FAILED";
	else{
		$exploded = explode(":", base64_decode($blub));
		$owner = $exploded[0];
		$catid = $exploded[1];
		echo updateCatowner($conn, $owner, $catid);
		/*
		$query = "UPDATE cats SET owner=? WHERE catcenter_id = ?;";
		$stmt = $conn->prepare($query);
		$stmt->bind_param('ss', $p1, $p2);
		$p1 = $owner;
		$p2 = $catid;
		if($stmt->execute()){
			echo "UPDATE_CAT_OWNER_SUCCESS";
		}
		else{
			echo "UPDATE_CAT_OWNER_FALED";
		}
		*/
	}
}
elseif(strcmp($misc_action, $set_catowners) == 0){
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	if($blub == null) echo "SET_CATOWNER_FAILED";
	else{
		$exploded = explode(":", base64_decode($blub));
		$owner_name = $exploded[0];
		$owner_mail = $exploded[1];
		$owner = base64_encode($owner_name . ":" . $owner_mail);
		$catid = $exploded[2];
		$date = $exploded[3];
		echo updateCatowner2($conn, $owner, $catid, $date);
		//echo "HEEEEE";
	}
}
elseif(strcmp($misc_action, $delete_catowner) == 0){
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	if($blub == null) echo "DELETE_CAT_OWNER_FAILED";
	else{
		
		//$exploded = explode(":", base64_decode($blub));
		//$mail = $exploded[0];
		$mail = base64_decode($blub);
		$query = "DELETE FROM catowner WHERE email=?";
		$stmt = $conn->prepare($query);
		$stmt->bind_param('s', $mail);
		
		$delete_success = False;
		if($stmt->execute()){
			$delete_success = True;
			//echo "DELETE_CAT_OWNER_SUCCESS";
		}
		else{
			//echo "DELETE_CAT_OWNER_FALED";
		}
		$stmt->close();
		
		if($delete_success){
			//$return_value = updateCatowner($conn, $mail, null);
			
			if(strcmp(updateCatowner($conn, $mail, null), "UPDATE_CAT_OWNER_SUCCESS") == 0){
				echo "DELETE_CAT_OWNER_SUCCESS";
			}
			else{
				echo "DELETE_CAT_OWNER_FAILED";
			}
			
			//echo $return_value;
		}
		else{
			echo "DELETE_CAT_OWNER_FAILED";
		}
		
		//echo "UNDER WORK";
		
	}
	
}
elseif(strcmp($misc_action, $delete_personel) == 0){
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	if($blub == null) echo "DELETE_PERSONEL_FAILED";
	else{
		
		//$exploded = explode(":", base64_decode($blub));
		//$mail = $exploded[0];
		$mail = base64_decode($blub);
		$query = "DELETE FROM personel WHERE email=?";
		$stmt = $conn->prepare($query);
		$stmt->bind_param('s', $mail);
		
		$delete_success = False;
		if($stmt->execute()){
			$delete_success = True;
			//echo "DELETE_CAT_OWNER_SUCCESS";
		}
		else{
			//echo "DELETE_CAT_OWNER_FALED";
		}
		$stmt->close();
		
		if($delete_success){
			//$return_value = updateCatowner($conn, $mail, null);
			/*
			if(strcmp(updateCatowner($conn, $mail, null), "UPDATE_CAT_OWNER_SUCCESS") == 0){
				echo "DELETE_CAT_OWNER_SUCCESS";
			}
			else{
				echo "DELETE_CAT_OWNER_FAILED";
			}*/
			echo "DELETE_PERSONEL_SUCCESS";
			//echo $return_value;
		}
		else{
			echo "DELETE_PERSONEL_FAILED";
		}
		
		//echo "UNDER WORK";
		
	}
	
}
elseif(strcmp($misc_action, $checkin) == 0){
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	if($blub == null) echo "CHECKIN_FAILED";
	else{
		$blub = base64_decode($blub);
		$json = json_decode($blub, true);
		$query = "INSERT INTO checkin (email, date, note) VALUES (?,?,?);";
		$stmt = $conn->prepare($query);
		$stmt->bind_param('sss', $p1, $p2, $p3);
		$p1 = $json['email'];
		$p2 = date("Y-m-d_H-i-s");
		$p3 = $json['note'];
		if($stmt->execute()){
			echo "CHECKIN_SUCCESS";
		}
		else{
			echo "CHECKIN_FAILED";
		}
	}
}
elseif(strcmp($misc_action, $delete_timeline_entry) == 0){
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	if($blub == null) echo "DELETE_TIMELINE_ENTRY_FAILED";
	else{
		$dec = base64_decode($blub);
		$json = json_decode($dec, true);
		$table = $json['table'];
		$regdate = $json['reg_date'];
		$catid = $json['catid'];
		$query = "DELETE FROM $table WHERE reg_date=? AND catcenter_id=?";
		$stmt = $conn->prepare($query);
		$stmt->bind_param('ss', $regdate, $catid);
		
		$delete_success = False;
		if($stmt->execute()){
			$delete_success = True;
		}
		else{
		}
		$stmt->close();
		if($delete_success){
			echo "DELETE_TIMELINE_ENTRY_SUCCESS";
		}else{
			echo "DELETE_TIMELINE_ENTRY_FAILED";
		}
	}
}
elseif(strcmp($misc_action, $update_open_hours) == 0){
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	if($blub == null) echo "UPDATE_OPEN_HOURS_FAILED";
	else{
		$json = base64_decode($blub);
		$decoded_json = json_decode($json, true);
		$monday_open_hour = $decoded_json['monday_open'];
		$monday_close_hour = $decoded_json['monday_close'];
		$monday_is_closed = $decoded_json['monday_is_closed'];
		$tuesday_open_hour = $decoded_json['tuesday_open'];
		$tuesday_close_hour = $decoded_json['tuesday_close'];
		$tuesday_is_closed = $decoded_json['tuesday_is_closed'];
		$wednesday_open_hour = $decoded_json['wednesday_open'];
		$wednesday_close_hour = $decoded_json['wednesday_close'];
		$wednesday_is_closed = $decoded_json['wednesday_is_closed'];
		$thursday_open_hour = $decoded_json['thursday_open'];
		$thursday_close_hour = $decoded_json['thursday_close'];
		$thursday_is_closed = $decoded_json['thursday_is_closed'];
		$friday_open_hour = $decoded_json['friday_open'];
		$friday_close_hour = $decoded_json['friday_close'];
		$friday_is_closed = $decoded_json['friday_is_closed'];
		$saturday_open_hour = $decoded_json['saturday_open'];
		$saturday_close_hour = $decoded_json['saturday_close'];
		$saturday_is_closed = $decoded_json['saturday_is_closed'];
		$sunday_open_hour = $decoded_json['sunday_open'];
		$sunday_close_hour = $decoded_json['sunday_close'];
		$sunday_is_closed = $decoded_json['sunday_is_closed'];
		
		$divergent_times = $decoded_json['divergent_hours'];
		$comment = $decoded_json['comment'];
		
		$keys = array("open_time", "close_time", "closed");
		$mon_values = array($monday_open_hour, $monday_close_hour, $monday_is_closed);
		$tue_values = array($tuesday_open_hour, $tuesday_close_hour, $tuesday_is_closed);
		$wed_values = array($wednesday_open_hour, $wednesday_close_hour, $wednesday_is_closed);
		$thu_values = array($thursday_open_hour, $thursday_close_hour, $thursday_is_closed);
		$fri_values = array($friday_open_hour, $friday_close_hour, $friday_is_closed);
		$sat_values = array($saturday_open_hour, $saturday_close_hour, $saturday_is_closed);
		$sun_values = array($sunday_open_hour, $sunday_close_hour, $sunday_is_closed);
		$mon_res = update_hours($conn, "monday", $keys, $mon_values);
		$tue_res = update_hours($conn, "tuesday", $keys, $tue_values);
		$wed_res = update_hours($conn, "wednesday", $keys, $wed_values);
		$thu_res = update_hours($conn, "thursday", $keys, $thu_values);
		$fri_res = update_hours($conn, "friday", $keys, $fri_values);
		$sat_res = update_hours($conn, "saturday", $keys, $sat_values);
		$sun_res = update_hours($conn, "sunday", $keys, $sun_values);
		
		$general_keys = array("divergent", "info");
		$general_values = array($divergent_times, $comment);
		$gen_res = update_hours($conn, "general", $general_keys, $general_values);
		
		$total_res = 0;
		if(strcmp($mon_res, "UPDATE_HOURS_SUCCESS") == 0){
			$total_res++;
		}
		if(strcmp($tue_res, "UPDATE_HOURS_SUCCESS") == 0){
			$total_res++;
		}		
		if(strcmp($wed_res, "UPDATE_HOURS_SUCCESS") == 0){
			$total_res++;
		}
		if(strcmp($thu_res, "UPDATE_HOURS_SUCCESS") == 0){
			$total_res++;
		}
		if(strcmp($fri_res, "UPDATE_HOURS_SUCCESS") == 0){
			$total_res++;
		}
		if(strcmp($sat_res, "UPDATE_HOURS_SUCCESS") == 0){
			$total_res++;
		}
		if(strcmp($sun_res, "UPDATE_HOURS_SUCCESS") == 0){
			$total_res++;
		}
		if(strcmp($gen_res, "UPDATE_HOURS_SUCCESS") == 0){
			$total_res++;
		}
		if($total_res == 8){
			echo "UPDATE_OPEN_HOURS_SUCCESS";
		}
		else{
			echo "UPDATE_OPEN_HOURS_FAILED-$total_res";
		}
	}
}
elseif(strcmp($misc_action, $get_open_hours) == 0){
	get_hours($conn);
}






?>