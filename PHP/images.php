<?php

$image_action = isset($_POST['image_action']) ? $_POST['image_action'] : null;
$download = "download";
$upload = "upload";
$upload_private = "upload_private";


$servername = "mysql531.loopia.se";
$username = "develop@k173002";
$password = "uhJo10_f";
$dbname = "kattcenter_com";
$conn = new mysqli($servername, $username, $password, $dbname);
if($conn->connect_error){
	die("Connection failed: " . $conn->connect_error);
}
if (!$conn->set_charset("utf8")) {
  die("Error loading character set utf8");
}
function addSrcToCatsTable($connection, $catid, $img_src){
	/*
	$servername = "mysql531.loopia.se";
	$username = "develop@k173002";
	$password = "uhJo10_f";
	$dbname = "kattcenter_com";
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	if($conn->connect_error){
		die("Connection failed: " . $conn->connect_error);
	}
	*/
	/* change character set to utf8 */
	/*
	if (!$conn->set_charset("utf8")) {
	  die("Error loading character set utf8");
	}
	*/
	$select_query = "SELECT catcenter_id FROM cats;";
	$stmt = $connection->prepare($select_query);
	$exists = false;
	$success = true;
	
	if($stmt->execute()){
		$stmt->bind_result($res);
		while($stmt->fetch()){
			//echo $res;
			//echo $catid;
			if(strcmp($res, $catid) == 0){
				//EXISTS
				$exists = true;
				break;
			}
		}
		//echo $exists;
		$stmt->close();
		if($exists == true){
			$update_query = "UPDATE cats SET img_src = ? WHERE catcenter_id = ?;";
			$stmt2 = $connection->prepare($update_query);
			$stmt2->bind_param('ss', $p1, $p2);
			$p1 = $img_src;
			$p2 = $catid;
			if($stmt2->execute()){
				$success = true;
			}
			else{
				$success = false;
			}
			$stmt2->close();
		}
		else{
			//echo "NOT EXISTS";
			
			$insert_query = "INSERT INTO cats (catcenter_id, img_src) VALUES (?, ?);";
			$stmt2 = $connection->prepare($insert_query);
			$stmt2->bind_param('ss', $p1, $p2);
			$p1 = $catid;
			$p2 = $img_src;
			if($stmt2->execute()){
				$success = true;
			}
			else{
				$success = false;
			}
			$stmt2->close();
		}
		
	}
	
	return $success;
}

function catExist($connection, $owner, $current_photo_src){
	$query = "SELECT photo_src FROM catform_basic WHERE owner=?;";
	$stmt = $connection->prepare($query);
	$stmt->bind_param('s', $owner);
	$exists = false;
	if($stmt->execute()){
		$stmt->bind_result($photo_src);
		while($stmt->fetch()){
			if(strcmp($photo_src, $current_photo_src) == 0){
				$exists = true;
				break;
			}
		}
	}
	$stmt->close();
	return $exists;
}

function addPrivateSrcToTable($connection, $img_src, $owner){
	if(catExist($connection, $owner, $img_src)==false){
		$query = "INSERT INTO catform_basic (photo_src, owner) VALUES (?,?);";
		$stmt = $connection->prepare($query);
		$stmt->bind_param('ss', $img_src, $owner);
		if($stmt->execute()){
			return true;
		}
		return false;
	}
	return true;
}


/*
function getImageSrcFromDB(){
	$servername = "mysql531.loopia.se";
	$username = "develop@k173002";
	$password = "uhJo10_f";
	$dbname = "kattcenter_com";
	$conn = new mysqli($servername, $username, $password, $dbname);
	if($conn->connect_error){
		die("Connection failed: " . $conn->connect_error);
	}
	if (!$conn->set_charset("utf8")) {
	  die("Error loading character set utf8");
	}
	$stmt = $conn->prepare("SELECT catcenter_id, img_src FROM cats");
	if($stmt->execute()){
		$res = $stmt->get_result();
		$string_blub = "";
		while($arr = $res->fetch_assoc()){
			$json = json_encode($arr);
			$string_blub .= $json . "\n";
		}
		
		return "DOWNLOAD_IMAGE_SRC_SUCCESSFUL:" . base64_encode("IMAGES_SRC:" . $string_blub);
	}
	else{
		return "DOWNLOAD_IMAGES_FAILED";
	}
	
}
*/

if($image_action != null && strcmp($image_action, $download) == 0){
	//DOWNLOAD
	//return getImageSrcFromDB();
}
elseif($image_action != null && strcmp($image_action, $upload) == 0){
	//UPLOAD IMAGE SRC AND REMOVE PREVIOUS IMAGE BY PREVIOUS IMAGE SRC 
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	$decoded_blub = base64_decode($blub);
	$exploded = explode(":", $decoded_blub);
	$catid = $exploded[0];
	$img_src = $exploded[1];
	$prev_img_src = $exploded[2];
	$none = "NONE";

	
	if(addSrcToCatsTable($conn, $catid, $img_src) == true){
		if(strcmp($prev_img_src, $none) != 0){
			$target_dir = "im/";
			$prev_img = $target_dir . $prev_img_src;
			unlink($prev_img);
		}
		echo "FILE_SRC_UPLOAD_SUCCESS:" . base64_encode("FILE_SRC_UPLOAD_SUCCESS:catform_basic");
	}
	else{
		echo "FILE_SRC_UPLOAD_FAILED";
	}
	
	//echo "TEST_UPLOAD_SRC";
}
elseif($image_action != null && strcmp($image_action, $upload_private) == 0){
	$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
	$decoded = base64_decode($blub);
	$param = explode(":", $decoded);
	$img_src = $param[0];
	$owner = $param[1];
	
	if(addPrivateSrcToTable($conn, $img_src, $owner) == true){
		echo "FILE_SRC_UPLOAD_SUCCESS:" . base64_encode("PRIVATE_FILE_SRC_UPLOAD_SUCCESS:catform_basic");
	}
	else{
		echo "FILE_SRC_UPLOAD_FAILED";
	}
	/*
	if(addSrcToCatsTable($catid, $img_src) == true){
		if(strcmp($prev_img_src, $none) != 0){
			$target_dir = "im/";
			$prev_img = $target_dir . $prev_img_src;
			unlink($prev_img);
		}
		echo "FILE_SRC_UPLOAD_SUCCESS:" . base64_encode("FILE_SRC_UPLOAD_SUCCESS:catform_basic");
	}
	else{
		echo "FILE_SRC_UPLOAD_FAILED";
	}
	*/
	
	//echo "TEST_UPLOAD_SRC";
}
elseif($image_action == null){
	//UPLOAD ACTUAL IMAGE
	if (is_uploaded_file($_FILES['filedata']['tmp_name'])) {
		$target_dir = "im/";
		$target_file = $target_dir . basename($_FILES['filedata']['name']);
		if(move_uploaded_file($_FILES['filedata']['tmp_name'], $target_file)){
			chmod("/im/" . $_FILES['filedata']['name'], 0644);
			echo "FILE_UPLOAD_SUCCESS:" . base64_encode("FILE_UPLOADED:IMAGE:catform_basic");
		}
		else{
			echo "FILE UPLOADED BUT DON'T KNOW WHERE";
		}
	}
	elseif(is_uploaded_file($_FILES['filedata']['name'])){
		echo "WHY HERE";
	}
	else{
		echo "FILE NOT UPLOADED";
	}
}
?>