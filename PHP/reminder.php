<?php header('Content-type: text/plain; charset=utf-8');

$blub = isset($_POST['blub']) ? $_POST['blub'] : null;

$year = "YEARLY_REMINDERS";
$month = "MONTHLY_REMINDERS";


if($blub == null){
	die("ACTION ERROR");
}

$servername = "mysql531.loopia.se";
$username = "develop@k173002";
$password = "uhJo10_f";
$dbname = "kattcenter_com";

$conn = new mysqli($servername, $username, $password, $dbname);


if($conn->connect_error){
	die("Connection failed: " . $conn->connect_error);
}

/* change character set to utf8 */
if (!$conn->set_charset("utf8")) {
  die("Error loading character set utf8");
}

$decoded_blub = base64_decode($blub);




function yearlyReminders($connection){
	$return_value = null;
	$query = "SELECT cell, last_login, yearly_reminded FROM catowner";
	$stmt = $connection->prepare($query);
	if($stmt->execute()){
		$res = $stmt->get_result();
		$string_blub = "";
		while($arr = $res->fetch_assoc()){
			if(strcmp($arr['cell'], "") != 0){
				$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
				$string_blub .= $json . "\n";
			}
		}
		$return_value = "yearly_reminder:" . base64_encode($string_blub);
	}
	$stmt->close();
	
	return $return_value;
}

function monthtlyReminders($connection){
	
	$query = "SELECT table_name, catcenter_id, start_date, repeat_date, start_date_reminded, repeat_date_reminded " .
			 "FROM catform_medical UNION ALL " .
			 "SELECT table_name, catcenter_id, start_date, repeat_date, start_date_reminded, repeat_date_reminded " .
			 "FROM catform_vaccination UNION ALL " .
			 "SELECT table_name, catcenter_id, start_date, repeat_date, start_date_reminded, repeat_date_reminded " .
			 "FROM catform_deworming UNION ALL " .
			 "SELECT table_name, catcenter_id, start_date, repeat_date, start_date_reminded, repeat_date_reminded " .
			 "FROM catform_verm UNION ALL " .
			 "SELECT table_name, catcenter_id, start_date, repeat_date, start_date_reminded, repeat_date_reminded " .
			 "FROM catform_claws UNION ALL " .
			 "SELECT table_name, catcenter_id, start_date, repeat_date, start_date_reminded, repeat_date_reminded " .
			 "FROM catform_weight;";
	//echo $query;
	$stmt = $connection->prepare($query);
	//echo "QUERY PREPARED";
	$return_value = null;
	$reminders = array();
	if($stmt->execute()){
		$res = $stmt->get_result();
		$result_string = "";
		while($arr = $res->fetch_assoc()){
			//echo "JOCKE";
			$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
			//$result_string .= $json . "\n";
			$reminders[] = $json;
		}
	}
	$stmt->close();
	$rows = array();
	foreach(array_values($reminders) as $rem){
		$json = json_decode($rem, true);
		//print_r($json);
		$st_date = $json['start_date'];
		$re_date = $json['repeat_date'];
		$difference = (strtotime($st_date) - time());
		$monthLimit = 2332800;
		$customLimit = 700000;
		if($difference > 0 && $difference < $customLimit){
			 $rows[] = array($json['catcenter_id'], $json['table_name'], "startdate");
		}
		elseif($re_date != null && strcmp($re_date, "") != 0){
			$re_difference = (strtotime($st_date) - time());
			$re_monthLimit = 2332800;
			$re_customLimit = 700000;
			if($re_difference > 0 && $re_difference < $re_customLimit){
				$rows[] =  array($json['catcenter_id'], $json['table_name'], "repeatdate");
			}
		}
	}
	$query = "SELECT catcenter_id, owner FROM cats;";
	$stmt = $connection->prepare($query);
	$catowners = array();
	if($stmt->execute()){
		$res = $stmt->get_result();
		while($arr = $res->fetch_assoc()){
			$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
			$catowners[] = $json;
		}
	}
	$stmt->close();
	$result_array = array();
	foreach(array_values($catowners) as $cow){
		$json = json_decode($cow, true);
		//print_r($json);
		$owner = $json['owner'];
		$catid = $json['catcenter_id'];
		foreach(array_values($rows) as $row){
			//print_r($row);
			//echo $catid . " ";
			//echo $row[0] . "\n";
			//echo $catid . " " . $row;
			if(strcmp($catid, $row[0]) == 0){
				if($result_array[$owner] == null){
					$result_array[$owner] = array("catid" => $catid, "reminders" => array($row[1]));
				}
				else{
					$reminder_array = $result_array[$owner]["reminders"];
					$reminder_array[] = $row[1];
					$result_array[$owner]["reminders"] = $reminder_array;
				}
				//print_r($row);
				//echo $owner . " " . $catid . " " . " " . $row[1] . " " . $row[2];
			}
		}
	}
	return "monthly_reminders:" . base64_encode(json_encode($result_array, JSON_UNESCAPED_UNICODE));
}


function allTableContents($connection, $table_name){
	$return_value = null;
	$query = "SELECT * FROM $table_name;";
	$stmt = $connection->prepare($query);
	//echo $table_name;
	if($stmt->execute()){
		
		$res = $stmt->get_result();
		$string_blub = "";
		if($res){
			//echo $res;
		}
		else{
			//echo "NULL";
		}
		
		while($arr = $res->fetch_assoc()){
			//print_r(array_values($arr));
			$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
			//echo "JSON\n";
			//echo $json;
			$string_blub .= $json . "\n";
		}
		$return_value = "$table_name:" . base64_encode($string_blub);
		
		
		
	}
	$stmt->close();
	
	return $return_value;
}


function tableContents($connection, $table_name, $parameter, $value){
	$return_value = null;
	$query = "SELECT * FROM $table_name WHERE $parameter=?;";
	//echo $query;
	$stmt = $connection->prepare($query);
	if(strcmp($table_name, "catowner") == 0){
		$stmt->bind_param('s', $p);
		$p = $value;
	}
	else{
		$stmt->bind_param('d', $p);
		$p = $value;
	}
	//echo $table_name;
	if($stmt->execute()){
		
		$res = $stmt->get_result();
		$string_blub = "";
		if($res){
			//echo $res;
		}
		else{
			//echo "NULL";
		}
		
		while($arr = $res->fetch_assoc()){
			//print_r(array_values($arr));
			$json = json_encode($arr);
			//echo "JSON\n";
			//echo $json;
			$string_blub .= $json . "\n";
		}
		$return_value = "$table_name:" . base64_encode($string_blub);
		$blub = null;
		if(strcmp($table_name, "catowner") == 0 && strcmp($parameter, "email") == 0){
			$blub = "DOWNLOAD_CATOWNER:" . base64_encode($return_value);
		}
		else{
			$blub = "DOWNLOAD_LIST: " . base64_encode($return_value);
		}
		$return_value = "DOWNLOAD_SUCCESS:" . base64_encode($blub);
		
	}
	$stmt->close();
	
	return $return_value;
}





if(strcmp($decoded_blub, $year) == 0){
	
	$result = yearlyReminders($conn);
	$blub = "DOWNLOAD_REMINDERS:" . base64_encode($result);
	echo "DOWNLOAD_SUCCESS:" . base64_encode($blub);
}
elseif(strcmp($decoded_blub, $month) == 0){
	$result = monthtlyReminders($conn);
	$blub = "DOWNLOAD_REMINDERS:" . base64_encode($result);
	echo "DOWNLOAD_SUCCESS:" . base64_encode($blub);
}
?>