<?php header('Content-type: text/plain; charset=utf-8');

$blub = isset($_POST['blub']) ? $_POST['blub'] : null;
$history_action = isset($_POST['history_action']) ? $_POST['history_action'] : null;

$add = "add_event";
$get = "get_events";

if($blub == null || $history_action == null){
	die("Not authorized");
}

$servername = "mysql531.loopia.se";
$username = "develop@k173002";
$password = "uhJo10_f";
$dbname = "kattcenter_com";

$conn = new mysqli($servername, $username, $password, $dbname);

if($conn->connect_error){
	die("Connection failed: " . $conn->connect_error);
}
else{
	//echo "CONNECTION SUCCESS";
}


/* change character set to utf8 */
if (!$conn->set_charset("utf8")) {
  die("Error loading character set utf8");
}






function addToHistory($connection, $catid, $date, $event){

	$query = "INSERT INTO catform_history (catcenter_id, date, comment) VALUES (?,?,?)";
	$stmt = $connection->prepare($query);
	$stmt->bind_param('sss', $p1, $p2, $p3);
	
	
	$p1 = $catid;
	$p2 = $date;
	$p3 = $event;
	if($stmt->execute()){
		return "UPLOAD_SUCCESS:" . base64_encode("uploaded_history:$table_name");
		//return "UPLOAD_SUCCESS";
	}
	else{
		return "UPLOAD_FAILED:" . base64_encode("uploaded_history:$table_name");
		//return "UPLOAD_FAILED";
	}
	
	//return "JOCKE";
	
}


function retreiveHistoryByID($connection, $catid){
	$query = "SELECT date, comment FROM catform_history WHERE catcenter_id = ?";
	$stmt = $connection->prepare($query);
	$stmt->bind_param('s', $p1);
	$p1 = $catid;
	if($stmt->execute()){
		$res = $stmt->get_result();
		$blub_string = null;
		while($data_arr = $res->fetch_assoc()){
			$json = json_encode($data_arr, JSON_UNESCAPED_UNICODE);
			$blub_string .= $json . "\n";
		}
		return $catid . ":" . base64_encode($blub_string);
	}
	else{
		return $catid . ":";
	}
}


$decoded_blub = base64_decode($blub);

if(strcmp($history_action, $add) == 0){
	$exploded = explode(":", $decoded_blub);
	$catid = $exploded[0];
	$date = $exploded[1];
	$event = $exploded[2];
	$result = addToHistory($conn, $catid, $date, $event);
	echo $result;
}

elseif(strcmp($history_action, $get) == 0){
	$catid = $decoded_blub;
	$result = "HISTORY:" . base64_encode(retreiveHistoryByID($conn, $catid));
	echo "DOWNLOAD_SUCCESS:" . base64_encode($result);
}
else{
	echo "ERROR_HISTORY";
}




?>