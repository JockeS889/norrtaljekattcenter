package kattcenter.norrtljekattcenter.init;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by Joakim on 2017-09-11.
 */

public class Initializer {

    public static boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        }
        catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
