package kattcenter.norrtljekattcenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.dialog.BottomDialog;
import kattcenter.norrtljekattcenter.network.DataPacket;
import kattcenter.norrtljekattcenter.network.DatabaseConnection;

/**
 * Created by Jocke on 2017-06-17.
 */


public class CatownerListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> catOwnerNamesCopy, catOwnerEmailsCopy, catOwnerStatusesCopy;
    private ArrayList<String> catOwnerNames, catOwnerEmails, catOwnerStatuses;
    //private Type privileged;

    /*
    public ImageAdapter(Context context, ConnectionType connectionType){
        this.context = context;
        this.connectionType = connectionType;
    }
    */

    public CatownerListAdapter(Context context, Type privileged){
        this.context = context;
        //this.privileged = privileged;
    }

    public void setCatOwnerNamesCopy(ArrayList<String> catOwnerNamesCopy){
        this.catOwnerNamesCopy = catOwnerNamesCopy;
    }

    public void setCatOwnerEmailsCopy(ArrayList<String> catOwnerEmailsCopy){
        this.catOwnerEmailsCopy = catOwnerEmailsCopy;
    }

    public void setCatOwnerStatusesCopy(ArrayList<String> catOwnerStatusesCopy){
        this.catOwnerStatusesCopy = catOwnerStatusesCopy;
    }

    public ArrayList<String> getCatOwnerNamesCopy(){
        return catOwnerNamesCopy;
    }

    public ArrayList<String> getCatOwnerEmailsCopy(){
        return catOwnerEmailsCopy;
    }

    public ArrayList<String> getCatOwnerStatusesCopy(){
        return catOwnerStatusesCopy;
    }

    public void setCatOwnerNames(ArrayList<String> catOwnerNames){
        this.catOwnerNames = catOwnerNames;
    }

    public void setCatOwnerEmails(ArrayList<String> catOwnerEmails){
        this.catOwnerEmails = catOwnerEmails;
    }

    public void setCatOwnerStatuses(ArrayList<String> catOwnerStatuses){
        this.catOwnerStatuses = catOwnerStatuses;
    }

    public ArrayList<String> getCatOwnerNames(){
        return catOwnerNames;
    }

    public ArrayList<String> getCatOwnerEmails(){
        return catOwnerEmails;
    }

    public ArrayList<String> getCatOwnerStatuses(){
        return catOwnerStatuses;
    }

    @Override
    public int getCount() {
        return catOwnerEmails.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //GridView.LayoutParams layoutParams = new GridView.LayoutParams(200,200);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(250, 250+200);

        View view;
        if(convertView == null){
            //view = ((Activity) context).getLayoutInflater().inflate(R.layout.catlist_item, null);
            //view = layoutInflater.inflate(R.layout.catowner_listentry, null);
            view = layoutInflater.inflate(R.layout.new_catowner_listentry, null);
        }
        else{
            view = convertView;
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Log.i("CATOWNERLISTADAPTER", "BACKGROUND ALREADY SET");
        }
        else{
            Log.i("CATOWNERLISTADAPTER", "RESETTING BACKGROUND");
            view.setBackground(ContextCompat.getDrawable(context, android.R.drawable.dialog_holo_light_frame));
        }
        TextView nameView = (TextView) view.findViewById(R.id.short_catowner_name);
        nameView.setText(catOwnerNames.get(position));
        ((TextView)view.findViewById(R.id.short_catowner_email)).setText(catOwnerEmails.get(position));
        ((TextView)view.findViewById(R.id.short_catowner_status)).setText(catOwnerStatuses.get(position));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(catOwnerEmails.get(position).equals("")){
                    new BottomDialog(context).popup("Kan ej ladda information om: 'Kattcenter'");
                }
                else{
                    ((HomePage)context).setLoadCatownerCats(true);
                    String encodedOwner = Base64.encodeToString((catOwnerNames.get(position)+":"+catOwnerEmails.get(position)).getBytes(), Base64.NO_WRAP);
                    ((HomePage)context).getHomePageHandler().getCatownerManager().selectCatOwner(encodedOwner);
                }

                //selectCatowner(catOwnerEmails.get(position));
            }
        });
        return view;
    }

    private void selectCatowner(String email){
        ((HomePage)context).getHomePageHandler().getCatownerManager().initCatListDialog();
        String formTable = "catowner";
        String blub = formTable+":"+email;
        String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("form_action", "DOWNLOAD");
        data.put("blub", encoded_blub);
        HashMap<String, String> data2 = new HashMap<>();
        String blub2 = "CATOWNER_CATS:"+email;
        String encoded_blub2 = Base64.encodeToString(blub2.getBytes(), Base64.NO_WRAP);
        data2.put("blub", encoded_blub2);
        try {
            ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_LIST));
            ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data2, Type.DOWNLOAD_LIST));
            ((HomePage)context).signalQueueToSend();
            //((HomePage)context).showLoadingDialog("Laddar information...");
            ((HomePage)context).getHomePageHandler().showStandardDialog("Laddar information...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //new DatabaseConnection(context, data, Type.DOWNLOAD_LIST).execute();

    }
}

