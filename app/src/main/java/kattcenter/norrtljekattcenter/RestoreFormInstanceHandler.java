package kattcenter.norrtljekattcenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.SynchronousQueue;

import kattcenter.norrtljekattcenter.file.FileManager;

/**
 * Created by Jocke on 2017-06-08.
 */

public class RestoreFormInstanceHandler {

    private Context context;
    private HomePage homePage;
    private HashMap<String, Bundle> savedInstances;

    public RestoreFormInstanceHandler(Context context, HashMap<String, Bundle> savedInstances){
        this.context = context;
        homePage = (HomePage) context;
        this.savedInstances = savedInstances;
        restoreBasicForms();
        restoreMedicalForms();
        restoreVaccinationForms();
        restoreParasitesForms();
        //restoreDewormingForms();
        //restoreVerminForms();
        restoreClawForms();
        restoreWeightForms();
        restoreVetDocForms();
        restoreRegSKKForms();
        restoreVetForms();
        restoreInsuranceForms();
        restoreBackgroundForms();
        restorePreviousOwnerForms();
        restoreStatusForms();
        restoreCatownerForm();
    }

    private void restoreBasicForms(){
        Bundle basicBundle = savedInstances.get("BASIC_FORM");
        if(basicBundle != null){
            Set<String> keySet = basicBundle.keySet();
            //for(String s: keySet) System.out.println("[DEBUG]  Key: "+s);
            //System.out.println("[HOME]: "+((TextView)homePage.findViewById(R.id.catbasic_catname_text)));
            /*
            byte[] array = basicBundle.getByteArray("CATPHOTO");

            if(array != null && array.length != 0){
                Bitmap bitmap = BitmapFactory.decodeByteArray(array, 0, array.length);
                ((ImageView)homePage.findViewById(R.id.info_cat_photo)).setImageBitmap(bitmap);

            }
            else{
                System.out.println("SETTING TO NULL");
                ((ImageView)homePage.findViewById(R.id.info_cat_photo)).setImageResource(android.R.color.transparent);
            }
            */
            if(basicBundle.getBoolean("CATPHOTO")){
                Bitmap bitmap = BitmapFactory.decodeFile(new FileManager(homePage.getPrivileged()).getLatestTakenPicture().getAbsolutePath(), null);
                ((ImageView)homePage.findViewById(R.id.info_cat_photo)).setImageBitmap(bitmap);
                (homePage.findViewById(R.id.info_cat_photo)).setTag("TAKEN_PHOTO");
            }
            else{
                (homePage.findViewById(R.id.info_cat_photo)).setBackground(ContextCompat.getDrawable(context, R.drawable.gray_dash_round_border));
                (homePage.findViewById(R.id.info_cat_photo)).setTag("DEFAULT_PHOTO");
            }

            ((AutoCompleteTextView)homePage.findViewById(R.id.catbasic_catname_autocomplete_text)).setText(basicBundle.getString("CATHOME_NAME"));
            //((TextView)homePage.findViewById(R.id.catbasic_present_date_text)).setText(basicBundle.getString("PRESENT_DATE"));
            ((TextView)homePage.findViewById(R.id.catbasic_arrived_date_text)).setText(basicBundle.getString("ARRIVED_DATE"));
            ((TextView)homePage.findViewById(R.id.catbasic_neuter_date_text)).setText(basicBundle.getString("NEUTER_DATE"));
            ((TextView)homePage.findViewById(R.id.catbasic_tatoo_date_text)).setText(basicBundle.getString("TATOO_DATE"));
            ((TextView)homePage.findViewById(R.id.catbasic_tatoo_text)).setText(basicBundle.getString("TATOO_STRING"));
            ((TextView)homePage.findViewById(R.id.catbasic_chip_date_text)).setText(basicBundle.getString("CHIP_DATE"));
            ((TextView)homePage.findViewById(R.id.catbasic_chip_code_text)).setText(basicBundle.getString("CHIP_CODE"));
            ((TextView)homePage.findViewById(R.id.catbasic_birthday_date_text)).setText(basicBundle.getString("BIRTHDAY"));
            ((TextView)homePage.findViewById(R.id.catbasic_birthday_guess)).setText(basicBundle.getString("BIRTHDAY_GUESS"));
            ((TextView)homePage.findViewById(R.id.catbasic_cathome_id_text)).setText(basicBundle.getString("CATHOME_ID"));
            ((TextView)homePage.findViewById(R.id.catbasic_sex_type_text)).setText(basicBundle.getString("SEX"));
            ((TextView)homePage.findViewById(R.id.catbasic_race_text)).setText(basicBundle.getString("RACE"));
            ((TextView)homePage.findViewById(R.id.catbasic_color_text)).setText(basicBundle.getString("COLOR"));
            ((TextView)homePage.findViewById(R.id.catbasic_hair_type)).setText(basicBundle.getString("HAIR"));

            //((EditText)homePage.findViewById(R.id.catbasic_box_text)).setText(basicBundle.getString("BOX"));
            ((TextView)homePage.findViewById(R.id.catbasic_moved_date_text)).setText(basicBundle.getString("MOVED"));
            //((EditText)homePage.findViewById(R.id.catbasic_connection_text)).setText(basicBundle.getString("CONNECTION"));
            //((TextView)homePage.findViewById(R.id.catbasic_tatoo_text)).setText(basicBundle.getString("TATOO_STRING"));
            //((CheckBox)homePage.findViewById(R.id.catbasic_temper_tame)).setChecked(basicBundle.getBoolean("TEMPER_TAME"));
            //((CheckBox)homePage.findViewById(R.id.catbasic_temper_shy)).setChecked(basicBundle.getBoolean("TEMPER_SHY"));
            //((CheckBox)homePage.findViewById(R.id.tatooCheckbox)).setChecked(basicBundle.getBoolean("TATOO"));
            //((CheckBox)homePage.findViewById(R.id.tatooCheckbox)).setText(basicBundle.getString("TATOO_DATE"));
            //((CheckBox)homePage.findViewById(R.id.neuterCheckbox)).setChecked(basicBundle.getBoolean("NEUTER"));
            //((CheckBox)homePage.findViewById(R.id.neuterCheckbox)).setText(basicBundle.getString("NEUTER_DATE"));
            //((CheckBox)homePage.findViewById(R.id.chipCheckbox)).setChecked(basicBundle.getBoolean("CHIP"));
            ((TextView)homePage.findViewById(R.id.catbasic_cathome_name_text)).setText(basicBundle.getString("CATNAME"));
            //((EditText)homePage.findViewById(R.id.catbasic_cathome_id_text)).setText(basicBundle.getString("CATHOME_ID"));
            //((TextView)homePage.findViewById(R.id.catbasic_birthday_text)).setText(basicBundle.getString("BIRTHDAY"));
            //String b_guess = basicBundle.getString("BIRTHDAY_GUESS");
            //((Spinner)homePage.findViewById(R.id.catbasic_birthday_guess)).setPrompt(b_guess);
            //((Spinner)homePage.findViewById(R.id.catbasic_sex_type)).setPrompt(basicBundle.getString("SEX"));
            //((EditText)homePage.findViewById(R.id.catbasic_rase_text)).setText(basicBundle.getString("RACE"));
            //((EditText)homePage.findViewById(R.id.catbasic_color_text)).setText(basicBundle.getString("COLOR"));
            //((Spinner)homePage.findViewById(R.id.catbasic_hair_type)).setPrompt(basicBundle.getString("HAIR"));
        }
    }

    private void restoreMedicalForms(){
        Bundle medicalBundle = savedInstances.get("MEDICAL_FORM");
        if(medicalBundle != null){
            ((TextView)homePage.findViewById(R.id.catmed_start_date_text)).setText(medicalBundle.getString("START_DATE"));
            ((TextView)homePage.findViewById(R.id.catmed_end_date_text)).setText(medicalBundle.getString("END_DATE"));
            //((TextView)homePage.findViewById(R.id.catmed_type_text)).setText(medicalBundle.getString("MEDICATION"));
            ((TextView)homePage.findViewById(R.id.catmed_dose_amount_text)).setText(medicalBundle.getString("DOSE"));
            //((TextView)homePage.findViewById(R.id.catmed_administration_text)).setText(medicalBundle.getString("ADMINISTRATION"));
            ((TextView)homePage.findViewById(R.id.catmed_treatment_type_text)).setText(medicalBundle.getString("TREATMENT_TYPE"));
            ((TextView)homePage.findViewById(R.id.catmed_comment_text)).setText(medicalBundle.getString("COMMENT"));
            ((TextView)homePage.findViewById(R.id.catmed_repeat_date_text)).setText(medicalBundle.getString("REPEAT_DATE"));
        }
    }

    private void restoreVaccinationForms(){
        Bundle vaccinBundle = savedInstances.get("VACCIN_FORM");
        //System.out.println("RESTORING VACCIN FORM----------------------------------");
        if(vaccinBundle != null){
           // System.out.println("EXISTS---");
            ((TextView)homePage.findViewById(R.id.catvaccin_start_date_text)).setText(vaccinBundle.getString("START_DATE"));
            ((TextView)homePage.findViewById(R.id.catvaccin_type_text)).setText(vaccinBundle.getString("VACCIN_TYPE"));
            ((RadioGroup)homePage.findViewById(R.id.catvaccin_vaccinTime)).check(vaccinBundle.getInt("TIME"));
            ((TextView)homePage.findViewById(R.id.catvaccin_comment_text)).setText(vaccinBundle.getString("COMMENT"));
            ((TextView)homePage.findViewById(R.id.catvaccin_repeat_date_text)).setText(vaccinBundle.getString("REPEAT_DATE"));
        }
    }

    private void restoreParasitesForms(){
        Bundle parasitesBundle = savedInstances.get("PARASITES_FORM");
        if(parasitesBundle != null){

        }
    }

    /*
    private void restoreDewormingForms(){
        Bundle dewormingBundle = savedInstances.get("DEWORMING_FORM");
        if(dewormingBundle != null){
            ((TextView)homePage.findViewById(R.id.catdeworming_start_date_text)).setText(dewormingBundle.getString("START_DATE"));
            ((TextView)homePage.findViewById(R.id.catdeworming_type_text)).setText(dewormingBundle.getString("DEWORMING_TYPE"));
            ((EditText)homePage.findViewById(R.id.catdeworming_dose_text)).setText(dewormingBundle.getString("DOSE"));
            ((TextView)homePage.findViewById(R.id.catdeworming_comment_text)).setText(dewormingBundle.getString("COMMENT"));
            ((TextView)homePage.findViewById(R.id.catdeworming_repeat_date_text)).setText(dewormingBundle.getString("REPEAT_DATE"));
        }
    }

    private void restoreVerminForms(){
        Bundle verminBundle = savedInstances.get("VERMIN_FORM");
        if(verminBundle != null){
            ((TextView)homePage.findViewById(R.id.catvermin_start_date_text)).setText(verminBundle.getString("START_DATE"));
            ((TextView)homePage.findViewById(R.id.catvermin_type_text)).setText(verminBundle.getString("VERMIN_TYPE"));
            ((TextView)homePage.findViewById(R.id.catvermin_treatment_text)).setText(verminBundle.getString("TREATMENT_TYPE"));
            ((TextView)homePage.findViewById(R.id.catvermin_comment_text)).setText(verminBundle.getString("COMMENT"));
            ((TextView)homePage.findViewById(R.id.catvermin_repeat_date_text)).setText(verminBundle.getString("REPEAT_DATE"));
        }
    }
    */

    private void restoreClawForms(){
        Bundle clawBundle = savedInstances.get("CLAW_FORM");
        if(clawBundle != null){
            ((TextView)homePage.findViewById(R.id.catclaw_start_date_text)).setText(clawBundle.getString("START_DATE"));
            ((TextView)homePage.findViewById(R.id.catclaw_comment_text)).setText(clawBundle.getString("COMMENT"));
            ((TextView)homePage.findViewById(R.id.catclaw_repeat_date_text)).setText(clawBundle.getString("REPEAT_DATE"));
        }
    }

    private void restoreWeightForms(){
        Bundle weightBundle = savedInstances.get("WEIGHT_FORM");
        if(weightBundle != null){
            ((TextView)homePage.findViewById(R.id.catweight_start_date_text)).setText(weightBundle.getString("START_DATE"));
            ((TextView)homePage.findViewById(R.id.catweight_weight_text)).setText(weightBundle.getString("WEIGHT"));
            ((TextView)homePage.findViewById(R.id.catweight_comment_text)).setText(weightBundle.getString("COMMENT"));
            ((TextView)homePage.findViewById(R.id.catweight_check_weight_date_text)).setText(weightBundle.getString("CHECK_WEIGHT"));
        }
    }

    private void restoreVetDocForms(){
        Bundle vetDocBundle = savedInstances.get("VETDOC_FORM");
        if(vetDocBundle != null) {
            ((TextView) homePage.findViewById(R.id.catvet_reg_date_text)).setText(vetDocBundle.getString("DATE"));
            HashMap<String, HashMap<String, Boolean>> record = (HashMap<String, HashMap<String, Boolean>>) vetDocBundle.getSerializable("VETDOCCHECKBOXES");
            if (record != null) {
                Set<String> keySet = record.keySet();
                for (String s : keySet) {
                    System.out.println("RESTORING:   RECORDKEY = " + s);
                    HashMap<String, Boolean> subRecord = record.get(s);
                    if (subRecord != null) {
                        Set<String> subKeySet = subRecord.keySet();
                        for (String subKey : subKeySet) {
                            //System.out.println("RESTORING:   SUBKEY = " + subKey);
                            homePage.updateRecord(s, subKey, subRecord.get(subKey));
                        }
                    }
                }
            }
            HashMap<String, String> notes = (HashMap<String, String>) vetDocBundle.getSerializable("VETDOCNOTES");
            if(notes != null){
                Set<String> keySet = notes.keySet();
                for(String s: keySet){
                    homePage.updateCatvetregComment(s, notes.get(s));
                }
            }
        }
    }

    private void restoreRegSKKForms(){
        Bundle regSKKBundle = savedInstances.get("REGSKK_FORM");
        if(regSKKBundle != null){
            ((RadioGroup)homePage.findViewById(R.id.catreg_skk_rg)).check(regSKKBundle.getInt("REG_SKK"));
            ((RadioGroup)homePage.findViewById(R.id.catreg_svekatt_rg)).check(regSKKBundle.getInt("REG_SVE"));
            ((RadioButton)homePage.findViewById(R.id.catreg_skk_yes_rb)).setText(regSKKBundle.getString("REG_SKK_DATE"));
            ((RadioButton)homePage.findViewById(R.id.catreg_svekatt_yes_rb)).setText(regSKKBundle.getString("REG_SVE_DATE"));
        }
    }

    private void restoreVetForms(){
        Bundle vetBundle = savedInstances.get("VET_FORM");
        if(vetBundle != null){
            ((CheckBox)homePage.findViewById(R.id.catvet_no_known)).setChecked(vetBundle.getBoolean("NO_KNOWN"));
            ((TextView)homePage.findViewById(R.id.catvet_no_known)).setText(vetBundle.getString("NO_KNOWN_DATE"));
            ((TextView)homePage.findViewById(R.id.catvet_bloodsample_date_text)).setText(vetBundle.getString("SAMPLE_DATE"));
            ((TextView)homePage.findViewById(R.id.catvet_bloodsample_result_text)).setText(vetBundle.getString("SAMPLE_RESULT"));
            ((TextView)homePage.findViewById(R.id.catvet_known_events_text)).setText(vetBundle.getString("KNOWN_EVENTS"));
            ((TextView)homePage.findViewById(R.id.catvet_comment_text)).setText(vetBundle.getString("COMMENT"));
        }
    }

    private void restoreInsuranceForms(){
        Bundle insuranceBundle = savedInstances.get("INSURANCE_FORM");
        if(insuranceBundle != null){
            ((TextView)homePage.findViewById(R.id.catinsurance_taken_date_text)).setText(insuranceBundle.getString("TAKEN_DATE"));
            ((TextView)homePage.findViewById(R.id.catinsurance_taken_company_text)).setText(insuranceBundle.getString("OLD_COMPANY"));
            ((EditText)homePage.findViewById(R.id.catinsurance_old_number)).setText(insuranceBundle.getString("OLD_NUMBER"));
            ((TextView)homePage.findViewById(R.id.catinsurance_signed_date_text)).setText(insuranceBundle.getString("SIGNED_DATE"));
            ((TextView)homePage.findViewById(R.id.catinsurance_signed_company_text)).setText(insuranceBundle.getString("NEW_COMPANY"));
            ((EditText)homePage.findViewById(R.id.catinsurance_new_number)).setText(insuranceBundle.getString("NEW_NUMBER"));
            ((TextView)homePage.findViewById(R.id.catinsurance_transfered_date_text)).setText(insuranceBundle.getString("TRANSFER_DATE"));
        }
    }

    private void restoreBackgroundForms(){
        Bundle backgroundBundle = savedInstances.get("BACKGROUND_FORM");
        if(backgroundBundle != null){
            String background_born_outside = backgroundBundle.getString("BORN_OUTSIDE");
            restoreView((homePage.findViewById(R.id.catbackground_born_outside)), background_born_outside, (background_born_outside.split(" ").length == 3));
            String background_homeless = backgroundBundle.getString("HOMELESS");
            restoreView((homePage.findViewById(R.id.catbackground_homeless)), background_homeless, (background_homeless.split(" ").length == 2));
            String background_dumped = backgroundBundle.getString("DUMPED");
            restoreView((homePage.findViewById(R.id.catbackground_dumped)), background_dumped, (background_dumped.split(" ").length == 2));
            String background_lost = backgroundBundle.getString("LOST");
            restoreView((homePage.findViewById(R.id.catbackground_lost)), background_lost, (background_lost.split(" ").length == 2));
            String background_left_behind = backgroundBundle.getString("LEFT_BEHIND");
            restoreView((homePage.findViewById(R.id.catbackground_left)), background_left_behind, (background_left_behind.split(" ").length == 2));
            String background_estate = backgroundBundle.getString("ESTATE");
            restoreView((homePage.findViewById(R.id.catbackground_estate)), background_estate, (background_estate.split(" ").length == 2));
            String background_relocate = backgroundBundle.getString("RELOCATE");
            restoreView((homePage.findViewById(R.id.catbackground_replacing)), background_relocate, (background_relocate.split(" ").length == 2));
            String background_via_vet = backgroundBundle.getString("VIA_VET");
            restoreView((homePage.findViewById(R.id.catbackground_via_vet)), background_via_vet, (background_via_vet.split(" ").length == 3));
            String background_comment = backgroundBundle.getString("COMMENT");
            ((TextView)homePage.findViewById(R.id.catbackground_comment_text)).setText(background_comment);
        }
    }

    private void restorePreviousOwnerForms(){

    }

    private void restoreStatusForms(){
        //System.out.println("||||||||||||||||||||||ABCDE|||||||||||||||||||||||||||||||||");
        Bundle statusBundle = savedInstances.get("STATUS_FORM");
        if(statusBundle != null){
            String status_not_bought = statusBundle.getString("NOT_BOUGHT");
            restoreView((homePage.findViewById(R.id.catstatus_not_bought)), status_not_bought, (status_not_bought.split(" ").length == 3));
            String status_new_arrived = statusBundle.getString("NEWLY_ARRIVED");
            restoreView((homePage.findViewById(R.id.catstatus_new_arrived)), status_new_arrived, (status_new_arrived.split(" ").length == 2));
            String status_adoptable = statusBundle.getString("ADOPTABLE");
            restoreView((homePage.findViewById(R.id.catstatus_adoptable)), status_adoptable, (status_adoptable.split(" ").length == 2));
            String status_cathome = statusBundle.getString("CATHOME");
            restoreView((homePage.findViewById(R.id.catstatus_cathome)), status_cathome, (status_cathome.split(" ").length == 2));
            String status_booked = statusBundle.getString("BOOKED");
            restoreView((homePage.findViewById(R.id.catstatus_booked)), status_booked, (status_booked.split(" ").length == 2));
            String status_adopted = statusBundle.getString("ADOPTED");
            restoreView((homePage.findViewById(R.id.catstatus_adopted)), status_adopted, (status_adopted.split(" ").length == 2));
            String status_callcenter = statusBundle.getString("CALLCENTER");
            restoreView((homePage.findViewById(R.id.catstatus_callcenter)), status_callcenter, (status_callcenter.split(" ").length == 2));
            String status_tnr = statusBundle.getString("TNR");
            restoreView((homePage.findViewById(R.id.catstatus_tnr)), status_tnr, (status_tnr.split(" ").length == 2));
            String external_cathome = statusBundle.getString("MOVED");
            restoreView((homePage.findViewById(R.id.catstatus_external_cathome)), external_cathome, (external_cathome.split(" ").length == 5));
           // String external_cathome_text = statusBundle.getString("EXTERNAL_CATHOME");
            String returned_owner = statusBundle.getString("RETURNEDOWNER");
            restoreView((homePage.findViewById(R.id.catstatus_returned)), returned_owner, (returned_owner.split(" ").length == 3));
            String status_deceased = statusBundle.getString("DECEASED");
            restoreView((homePage.findViewById(R.id.catstatus_deceased)), status_deceased, (status_deceased.split(" ").length == 2));
            /*

            ((CheckBox)homePage.findViewById(R.id.catstatus_booked)).setChecked(statusBundle.getBoolean("BOOKED"));
            ((CheckBox)homePage.findViewById(R.id.catstatus_adopted)).setChecked(statusBundle.getBoolean("ADOPTED"));
            ((CheckBox)homePage.findViewById(R.id.catstatus_callcenter)).setChecked(statusBundle.getBoolean("CALLCENTER"));
            ((CheckBox)homePage.findViewById(R.id.catstatus_tnr)).setChecked(statusBundle.getBoolean("TNR"));
            ((CheckBox)homePage.findViewById(R.id.catstatus_external_cathome)).setChecked(statusBundle.getBoolean("MOVED"));
            ((EditText)homePage.findViewById(R.id.catstatus_external_cathome_text)).setText(statusBundle.getString("EXTERNAL_CATHOME"));
            ((CheckBox)homePage.findViewById(R.id.catstatus_missing)).setChecked(statusBundle.getBoolean("MISSING"));
            ((CheckBox)homePage.findViewById(R.id.catstatus_returned)).setChecked(statusBundle.getBoolean("RETURNEDOWNER"));
            ((CheckBox)homePage.findViewById(R.id.catstatus_deceased)).setChecked(statusBundle.getBoolean("DECEASED"));
            */
        }
    }

    private void restoreCatownerForm(){
        Bundle catownerBundle = savedInstances.get("CATOWNER_FORM");
        if(catownerBundle != null){
            String date = catownerBundle.getString("DATE");
            String time = catownerBundle.getString("TIME");
            ((TextView)homePage.findViewById(R.id.catform_catowner_date_text)).setText(date);
            ((TextView)homePage.findViewById(R.id.catform_catowner_time_text)).setText(time);
            ArrayList<CharSequence> arrayList = catownerBundle.getCharSequenceArrayList("SELECTED_OWNERS");
            if(arrayList == null){
                ((LinearLayout)homePage.findViewById(R.id.catform_new_catowner_list)).removeAllViews();
                homePage.findViewById(R.id.cat_add_catowner).setEnabled(true);
            }
            else{


                for(CharSequence c: arrayList){
                    final RelativeLayout entry = (RelativeLayout) homePage.getLayoutInflater().inflate(R.layout.catform_catowner_list_entry, null);
                    String element = new String(Base64.decode(c.toString().getBytes(), Base64.NO_WRAP));
                    ((TextView)entry.findViewById(R.id.chosenCatOwnerName)).setText(element.split(":")[0]);
                    ((TextView)entry.findViewById(R.id.chosenCatownerEmail)).setText(element.split(":")[1]);
                    ((LinearLayout)homePage.findViewById(R.id.catform_new_catowner_list)).addView(entry);
                    entry.findViewById(R.id.catform_catowner_remove_entry).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((LinearLayout)homePage.findViewById(R.id.catform_new_catowner_list)).removeView(entry);
                        }
                    });
                }
            }
        }
    }

    private void restoreView(View view, String string, boolean status){
        if(view instanceof RadioButton){
            ((RadioButton)view).setText(string);
            ((RadioButton)view).setChecked(status);
        }
        else if (view instanceof CheckBox){
            ((CheckBox)view).setText(string);
            ((CheckBox)view).setChecked(status);
            if (view.getId() == R.id.catstatus_external_cathome) {
                (homePage.findViewById(R.id.catstatus_external_cathome_text)).setVisibility(status ? View.VISIBLE : View.GONE);
            }
        }

    }

}
