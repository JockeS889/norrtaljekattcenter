package kattcenter.norrtljekattcenter.network;

import android.content.Context;
import android.util.Base64;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.Type;

/**
 * Created by Jocke on 2017-06-12.
 */

public class UploadProduct {

    private Context context;
    private HashMap<String, String> data = new HashMap<>();
    private boolean error = false;
    public UploadProduct(Context context, String table_name, String action, String product){
        this.context = context;
        String content = null;
        try {
            content = table_name+":"+action+":"+ Base64.encodeToString(product.getBytes("UTF-8"), Base64.NO_WRAP);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            error = true;
        }
        String blub = Base64.encodeToString(content.getBytes(), Base64.NO_WRAP);
        System.out.println("UPLOAD: "+blub);
        data.put("blub", blub);
    }

    public void upload() throws InterruptedException {
        if(error){
            Toast.makeText(context, "Kodningsfel", Toast.LENGTH_SHORT).show();
            return;
        }

        ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.UPLOAD_PRODUCT));
        ((HomePage)context).signalQueueToSend();

        //new DatabaseConnection(context, data, Type.UPLOAD_PRODUCT).execute();
    }
}
