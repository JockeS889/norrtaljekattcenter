package kattcenter.norrtljekattcenter.network;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import kattcenter.norrtljekattcenter.R;
import okhttp3.OkHttpClient;

/**
 * Created by Jocke on 2017-06-18.
 */

public class ImageFetcher{

    private Context context;
    public ImageFetcher(Context context){
        this.context = context;
    }

    public void fetch(String url, ImageView imageView){
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(context.getString(R.string.serverauthname), context.getString(R.string.serverauthpass)))
                .build();

        new OkHttpUrlLoader.Factory(httpClient);
    }
}
