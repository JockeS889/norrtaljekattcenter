package kattcenter.norrtljekattcenter.network;

import android.content.Context;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Jocke on 2017-06-20.
 */

public class NetworkThread extends Thread {

    private Context context;
    private LinkedBlockingQueue<DataPacket> preQueue = new LinkedBlockingQueue<>();
    private LinkedBlockingQueue<DataPacket> dataQueue = new LinkedBlockingQueue<>();
    private Boolean clearToSend = false;


    public NetworkThread(Context context){
        this.context = context;
    }


    public void addDataToQueue(DataPacket dataPacket) throws InterruptedException {
        if(clearToSend){
            dataQueue.put(dataPacket);
        }
        else {
            clearToSend = false;
            preQueue.put(dataPacket);
        }
    }

    /**
     * SEND NEXT PREPARED DATA PACKET IF EXISTS
     * IF NOT EXISTS, GIVE A CLEAR SIGNAL FOR NEXT
     * DATA TO BE SENT DIRECTLY INSTEAD OF PREPARED
     * @return
     * @throws InterruptedException
     */
    public boolean nextInQueue() throws InterruptedException {
        DataPacket dataPacket = preQueue.poll();
        System.out.println("HAS SOMETHING TO SEND? "+(dataPacket != null));
        if(dataPacket == null){
            clearToSend = true;
            return false;
        }
        else{
            dataQueue.put(dataPacket);
            return true;
        }
    }

    public void prioritizedDatapacket(DataPacket dataPacket) throws InterruptedException {
        dataQueue.put(dataPacket);
    }

    @Override
    public void run(){
        while(true){
            try {
                DataPacket dataPacket = dataQueue.take();
                new DatabaseConnection(context, dataPacket).execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}
