package kattcenter.norrtljekattcenter.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import net.hockeyapp.android.metrics.model.Base;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.LoginPage;
import kattcenter.norrtljekattcenter.PriviligeHolder;
import kattcenter.norrtljekattcenter.RestoreFormInstanceHandler;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.WelcomePage;
import kattcenter.norrtljekattcenter.dialog.BottomDialog;
import kattcenter.norrtljekattcenter.file.FileManager;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import kattcenter.norrtljekattcenter.R;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.TlsVersion;

/**
 * Created by Jocke on 2017-06-10.
 */

public class DatabaseConnection extends AsyncTask<Void, Void, String>{

    private Context context;
    private DataPacket dataPacket;
    private HashMap<String, String> data;
    private String post_data = "";
    private Type connectionType;
    private Type privilige;
    private Type fileType;
    private FileManager fileManager;
    private boolean uploadedMultipleFiles = false;

    public DatabaseConnection(Context context, DataPacket dataPacket){
        this.context = context;
        this.dataPacket = dataPacket;
        this.data = dataPacket.getData();
        this.connectionType = dataPacket.getConnectionType();
        if(this.connectionType == Type.LOGIN){

        }else{
            //this.privilige = Type.ADMIN;
            this.privilige = PriviligeHolder.getType();
            fileManager = new FileManager(privilige);
            //System.out.println("//////////////////////// CONNECTION TYPE: "+connectionType.toString()+"..............");
            //System.out.println("POST_DATA: "+post_data);
        }
        System.out.println("DATABASECONNECTION: contains form_action = "+data.containsKey("form_action"));

        if(data.containsKey("form_action") && (data.get("form_action").equals("UPLOAD_IMAGE") ||
                data.get("form_action").equals("UPLOAD_FILE") || data.get("form_action").equals("UPLOAD_SAMPLE_FILE") || data.get("form_action").equals(Type.VETDOC_FILE.toString()) || data.get("form_action").equals(Type.SKKDOC_FILE.toString()))){
            System.out.println("DATABASECONNECTION: form_action = "+data.get("form_action"));
            switch (data.get("form_action")){
                case "UPLOAD_IMAGE":
                    this.connectionType = Type.UPLOAD_IMAGE;
                    break;
                case "UPLOAD_SAMPLE_FILE":
                    this.connectionType = Type.UPLOAD_FILE;
                    this.fileType = Type.SAMPLE_FILE;
                case "UPLOAD_FILE":
                    this.connectionType = Type.UPLOAD_FILE;
                    break;
                case "VETDOC_FILE":
                    this.connectionType = Type.UPLOAD_MULT_FILES;
                    fileType = Type.VETDOC_FILE;
                    break;
                case "SKKDOC_FILE":
                    this.connectionType = Type.UPLOAD_MULT_FILES;
                    fileType = Type.SKKDOC_FILE;
                    break;
                default:break;
            }
            post_data += "upload=true";
        }
        else if(connectionType != Type.DOWNLOAD_IMAGE){
            if(data.containsKey("form_action") && data.get("form_action").equals("UPLOAD_IMAGE_SRC"))
                this.connectionType = Type.UPLOAD_IMAGE_SRC;
            else if(data.containsKey("form_action") && data.get("form_action").equals("UPLOAD_PRIVATE_IMAGE_SRC"))
                this.connectionType = Type.UPLOAD_PRIVATE_IMAGE_SRC;
            else {
                Set<String> keys = data.keySet();
                for (String key : keys) {
                    post_data += key + "=" + data.get(key) + "&";
                }
            }
        }
    }

    /*
    public DatabaseConnection(Context context, HashMap<String, String> data, Type connectionType){
        this.context = context;
        this.data = data;
        this.connectionType = connectionType;
        this.privilige = Type.ADMIN;
        fileManager = new FileManager(privilige);
        System.out.println("//////////////////////// CONNECTION TYPE: "+connectionType.toString()+"..............");
        if(data.containsKey("form_action") && data.get("form_action").equals("UPLOAD_IMAGE")){
            this.connectionType = Type.UPLOAD_IMAGE;
            post_data += "upload=true";
        }
        else if(connectionType != Type.DOWNLOAD_IMAGE){
            if(data.containsKey("form_action") && data.get("form_action").equals("UPLOAD_IMAGE_SRC"))
                this.connectionType = Type.UPLOAD_IMAGE_SRC;
            else {
                Set<String> keys = data.keySet();
                for (String key : keys) {
                    post_data += key + "=" + data.get(key) + "&";
                }
            }
        }
        System.out.println("POST_DATA: "+post_data);
    }
    */


    /*
    public void setPrivilige(Type privilige){
        this.privilige = privilige;
        fileManager = new FileManager(privilige);
    }
    */

    @Override
    protected String doInBackground(Void... params) {
        Response response = null;
        String responseContent = null;
        if(post_data == null){
            Toast.makeText(context, "Felaktig/Ingen data", Toast.LENGTH_SHORT).show();
        }
        else{
            try {
                //System.out.println("BLUB: "+ new String(Base64.decode(data.get("blub").getBytes(), Base64.NO_WRAP)));
                System.out.println("CONNECTIONTYPE: "+connectionType);
                switch (connectionType){
                    case LOGIN:
                        /*
                        String loginType = data.get("logintype");
                        switch (loginType) {
                            case "login":
                                response = login();
                                break;
                            default:
                                break;
                        }
                        */
                        response = login();
                        break;
                    case UPLOAD:
                        response = uploadForm();
                        break;
                    case UPLOAD_MISC:
                    case DOWNLOAD_MISC:
                        response = handleMisc();
                        break;
                    case UPLOAD_HISTORY:
                        response = handleHistory();
                        break;
                    case DOWNLOAD_HISTORY:
                        response = handleHistory();
                        break;
                    case UPLOAD_IMAGE:
                        response = handleImage();
                        break;
                    case UPLOAD_IMAGE_SRC:
                    case UPLOAD_PRIVATE_IMAGE_SRC:
                        response = handleImage();
                        break;
                    case UPLOAD_FILE:
                    case UPLOAD_MULT_FILES:
                    case DOWNLOAD_FILE:
                        response = handleFile();
                        break;
                    case UPLOAD_PRODUCT:
                    case DOWNLOAD_PRODUCT:
                    case DELETE_PRODUCT:
                        response = handleProduct();
                        break;
                    /*
                    case UPLOAD_MED_VAC:
                        response = uploadMedications();
                        break;
                    case DOWNLOAD_MED_VAC:
                        response = downloadMedications();
                        break;
                        */
                    case DOWNLOAD_LIST:
                        response = downloadList();
                        break;
                    /*
                    case DOWNLOAD_IMAGE:
                        downloadImage();
                        break;
                        */
                    case DOWNLOAD_FORM:
                        response = downloadForm();
                        break;
                    case DOWNLOAD_REMINDERS:
                        response = handleReminders();
                        break;
                    case DOWNLOAD_ALL:
                        response = downloadList();
                        break;
                    default:break;
                }
                if(response != null) {
                    if (response.code() == 200) {
                        switch (connectionType){
                            case DOWNLOAD_FILE:
                                responseContent = storeDownloadedFile(response);
                                break;
                            default:
                                responseContent = response.body().string();
                                break;
                        }

                    }
                    else {
                        responseContent = "CODE:" + response.code();
                    }
                }
                else{

                }
            } catch (IOException ioe){
                ioe.printStackTrace();
            }
        }

        return responseContent;
    }

    @Override
    protected void onPostExecute(String responseText){
        System.out.println("RESPONSETEXT: "+responseText);
        if(responseText != null) {
            String status = responseText.split(":")[0];
            if(status.equals("CODE")){
                //Toast.makeText(context, "Error code: [" +responseText.split(":")[1] + "]", Toast.LENGTH_LONG).show();
                System.out.println("Error code: [" +responseText.split(":")[1] + "]");
                System.out.println("DATA: "+data);
                /*
                if(data.get("form_action").equals("UPLOAD_VETDOC")){

                    System.out.println("FAILED UPLOAD VETDOC");
                }
                */
                if(context instanceof HomePage){
                    if(ConnectionHolder.getDownloadManager() == null){
                        ConnectionHolder.setDownloadManager(new DownloadManager(context));
                    }
                    ConnectionHolder.getDownloadManager().retryConnection(dataPacket);
                }
                    //new DownloadManager(context).retryConnection(dataPacket);
                    //((HomePage)context).retryConnection(dataPacket);
                //((HomePage)context).retryConnection(data, connectionType);
            }
            else {
                System.out.println("[onPostExecute] "+responseText);
                //String responseText = response.body().string();
                switch (responseText) {
                    /*
                    case "LOGIN_SUCCESS":
                        ((LoginPage) context).loginSuccess();
                        break;
                        */
                    case "LOGIN_FAILED":
                        Toast.makeText(context, responseText, Toast.LENGTH_LONG).show();
                        if(context instanceof LoginPage)
                            ((LoginPage) context).loginFailed();
                        else if(context instanceof WelcomePage)
                            ((WelcomePage) context).loginResult(false);
                        break;
                    case "REGISTER_CREATED_SUCCESS":
                        String decodedData = new String(Base64.decode(data.get("blub").getBytes(), Base64.NO_WRAP));
                        String[] pieces = decodedData.split(":");
                        //((HomePage)context).sendLogin(pieces[1], pieces[7], pieces[8], pieces[10]);
                        ((HomePage)context).sendLoginExtApp(pieces[1], pieces[7], pieces[8], pieces[10]);
                        break;
                    case "REGISTER_CREATED_FAILED":
                        new BottomDialog(context).popup("Ett fel uppstod vid skapande av login");
                        break;
                    case "CAT_ID_CHECK_ERROR":

                        break;
                    case "CAT_ID_UNAVAILABLE":
                        ((HomePage)context).getController().unavailableCatID();
                        break;
                    case "CAT_ID_AVAILABLE":
                        ((HomePage)context).getController().availableCatID();
                        break;
                    case "UPDATE_USER_INFO_FAILED":
                        new BottomDialog(context).popup("Kunde inte uppdatera databasen\nFörsök igen senare");
                        break;
                    case "UPDATE_CAT_OWNER_SUCCESS":
                        ((HomePage)context).removeCatFromChooser(((HomePage)context).getChosenCatFromListAdapter().getChosenPosition());
                        Log.i("DATABASECONNECTION", ((HomePage)context).getChosenCatFromListAdapter().getChosenPosition()+" BE REMOVED");
                        //new BottomDialog(context).popup("Byte av kattägare lyckad");  MUST FIX
                        break;
                    case "DELETE_CAT_OWNER_SUCCESS":
                        new BottomDialog(context).popup("Lyckad avregistrering av kattägare");
                        break;
                    case "DELETE_PRODUCT_SUCCESS":
                        new BottomDialog(context).popup("Produkt borttagen från listan");
                        break;
                    case "DELETE_PRODUCT_FAILED":
                        new BottomDialog(context).popup("Fel uppstod vid borttagning från servern");
                        break;
                    case "DELETE_TIMELINE_ENTRY_SUCCESS":
                        new BottomDialog(context).popup("Borttagning lyckades");
                        ((HomePage)context).removeTimeLineSuccess();
                        break;
                    case "DELETE_TIMELINE_ENTRY_FALED":
                        new BottomDialog(context).popup("Fel uppstod vid borttagning från servern");
                        break;
                    case "UPDATE_CAT_OWNER_FAILED":
                        new BottomDialog(context).popup("Misslyckat byte av kattägare");
                        break;
                    case "UPDATE_OPEN_HOURS_SUCCESS":
                        new BottomDialog(context).popup("Uppdatering av öppettider lyckad");
                        break;
                    case "REGISTER_SUCCESS":
                        ((LoginPage) context).registerSuccess();
                        break;
                    case "REGISTER_FAILED":
                        ((LoginPage) context).registerFailed();
                        break;
                    case "UPLOAD_SUCCESS":
                        Toast.makeText(context, "Uppladdning lyckad", Toast.LENGTH_SHORT).show();
                        break;
                    case "UPLOAD_FAILED":
                        Toast.makeText(context, "Kunde inte ladda upp information", Toast.LENGTH_LONG).show();
                        break;
                    case "DOWNLOAD_FAILED":
                    case "DOWNLOAD_IMAGES_FAILED":
                        Toast.makeText(context, "Kunde inte hämta information", Toast.LENGTH_LONG).show();
                        break;
                    case "SERVER_ERROR":
                        Toast.makeText(context, "Fel", Toast.LENGTH_LONG).show();
                        break;
                    case "FILE_DOWNLOADED":
                        //((HomePage)context).setFileDownloadComplete();
                        System.out.println("FILE DOWNLOADED: "+data.get("file_name_decoded"));
                        ((HomePage)context).handleDownloadedFile(data.get("file_name_decoded"));
                        break;
                    case "FILE_NOT_DOWNLOADED":

                        break;
                    default:
                        break;
                }
                String partResponse = responseText.split(":")[0];
                if(partResponse.equals("LOGIN_SUCCESS")){
                    String priv = responseText.split(":")[1];
                    switch (priv){
                        case "ADMIN":
                            PriviligeHolder.setType(Type.ADMIN);
                            break;
                        case "PERSONEL":
                            PriviligeHolder.setType(Type.PERSONAL);
                            break;
                        case "CATOWNER":
                            PriviligeHolder.setType(Type.CATOWNER);
                            break;
                        default:break;
                    }
                    if(context instanceof LoginPage)
                        ((LoginPage)context).loginSuccess();
                    else if(context instanceof WelcomePage)
                        ((WelcomePage)context).loginResult(true);
                }
                else {
                    switch (partResponse) {
                        case "DOWNLOAD_SUCCESS":
                        case "DOWNLOAD_IMAGE_SRC_SUCCESSFUL":
                        case "FILE_UPLOAD_SUCCESS":
                        case "FILE_SRC_UPLOAD_SUCCESS":
                        case "FORM_UPLOAD_SUCCESS":
                        case "FORM_UPLOAD_FAILED":
                        case "FORM_VETDOC_UPLOAD_SUCCESS":
                        case "FORM_VETDOC_UPLOAD_FAILED":
                        case "GET_HOURS_SUCCESS":
                            String blub = responseText.split(":")[1];
                            //((HomePage) context).handleDownloadedContent(blub);
                            new DownloadManager(context).handleDownloadedContent(blub);
                            break;
                        case "UPDATE_USER_INFO_SUCCESS":
                            ((HomePage)context).userInfoUpdateSuccess(responseText.split(":")[1]);
                            break;
                        case "UPDATE_OPEN_HOURS_FAILED":
                            new BottomDialog(context).popup("Misslyckad uppdatering [Antal lyckande: "+responseText.split("-")[1]+"/8]");
                            break;
                        case "ADDED_PRIVATE_CAT_SUCCESS":
                            ((HomePage)context).uploadComplete();
                            break;
                        case "ADDED_PRIVATE_CAT_FAILED":
                            new BottomDialog(context).popup("Server fel! Kunde inte lägga till privat katt");
                            break;
                        default:
                            break;

                    }
                }
                /*
                if (responseText.split(":")[0].equals("DOWNLOAD_SUCCESS") || responseText.split(":")[0].equals("FILE_UPLOAD_SUCCESS") || responseText.split(":")[0].equals("FILE_SRC_UPLOAD_SUCCESS") ||responseText.split(":")[0].equals("FORM_UPLOAD_SUCCESS") || responseText.split(":")[0].equals("FORM_UPLOAD_FAILED")
                        || responseText.split(":")[0].equals("FORM_VETDOC_UPLOAD_SUCCESS") || responseText.split(":")[0].equals("FORM_VETDOC_UPLOAD_FAILED")) {
                    String blub = responseText.split(":")[1];
                    ((HomePage) context).handleDownloadedContent(blub);
                }
                */
                //System.out.println("RESPONSE: " + responseText);
            }
        }
        else{
            if(uploadedMultipleFiles){
                Log.i("DATABASECONNECTION", "UPLOADED MULTIPLE FILES-----------------------------");
                uploadedMultipleFiles = false;
                String blub = Base64.encodeToString("FILE_UPLOADED:DOCUMENT".getBytes(), Base64.NO_WRAP);
                new DownloadManager(context).handleDownloadedContent(blub);
            }
            else {
                if (context instanceof LoginPage) {
                    Toast.makeText(context, "Kunde inte ansluta, försök igen senare", Toast.LENGTH_LONG).show();
                    ((LoginPage) context).findViewById(R.id.loginProgressbar).setVisibility(View.INVISIBLE);
                } else if (context instanceof HomePage) {
                    ((HomePage) context).errorLoadingData();
                }
                if (connectionType == Type.DOWNLOAD_IMAGE) {
                    //Toast.makeText(context, "Bilder hämtade", Toast.LENGTH_SHORT).show();

                }
            }
        }

    }

    private Response login() throws IOException{
        return getResponse(context.getString(R.string.serverloginurl));
    }

    private Response uploadForm() throws IOException{
        return getResponse(context.getString(R.string.serverformurl));
    }

    private Response downloadForm() throws IOException{
        return getResponse(context.getString(R.string.serverformurl));
    }

    private Response downloadList() throws IOException{
        return getResponse(context.getString(R.string.serverlisturl));
    }

    private Response handleProduct() throws IOException{
        return getResponse(context.getString(R.string.serverproducturl));
    }

    private Response handleImage() throws IOException{
        return getImageResponse(context.getString(R.string.serverimagesurl));
    }

    private Response handleFile() throws IOException{
        Response response = null;
        switch (connectionType){
            case UPLOAD_FILE:
            case UPLOAD_MULT_FILES:
                response = getFileResponse(context.getString(R.string.serverfilesurl));
                break;
            case DOWNLOAD_FILE:
                response = getFileResponse(context.getString(R.string.serverfilesrc)+"/"+data.get("file_name"));
                break;
            default: break;
        }
        return response;
    }

    private Response handleReminders() throws IOException{
        return getResponse(context.getString(R.string.serverremindersrc));
    }

    private Response handleHistory() throws IOException{
        return getResponse(context.getString(R.string.serverhistoryurl));
    }

    private Response handleMisc() throws IOException{
        return getResponse(context.getString(R.string.servermiscurl));
    }

    /*
    private Response downloadMedications() throws IOException{
        return getResponse(context.getString(R.string.servermedicationsurl));
    }

    private Response uploadMedications() throws IOException{
        return getResponse(context.getString(R.string.servermedicationsurl));
    }
    */

    private ConnectionSpec MyConnectionSpec(){
        ConnectionSpec connectionSpec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_DHE_RSA_WITH_AES_256_CBC_SHA256,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256
                )
                .build();
        return connectionSpec;
    }

    private Response getResponse(String url) throws IOException{

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(context.getString(R.string.serverauthname), context.getString(R.string.serverauthpass)))
                .connectionSpecs(Collections.singletonList(MyConnectionSpec()))
                .build();

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody requestBody = RequestBody.create(mediaType, post_data);

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        System.out.println("REQUEST: "+request.url() + " "+post_data.toString());

        Response response = null;
        try{
            response = httpClient.newCall(request).execute();
        } catch (SocketTimeoutException timeoutexp){
            timeoutexp.printStackTrace();
        }
        return response;
    }

    private Response getFileResponse(String url) throws IOException{
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(context.getString(R.string.serverauthname), context.getString(R.string.serverauthpass)))
                .connectionSpecs(Collections.singletonList(MyConnectionSpec()))
                .build();

        File file = null;
        File[] files = null;
        if(connectionType != Type.DOWNLOAD_FILE) {
            switch (fileType) {
                case SAMPLE_FILE:
                    file = fileManager.getSampleFile();
                    break;
                case VETDOC_FILE:
                    files = fileManager.getDocuments(fileType);
                    break;
                case SKKDOC_FILE:
                    files = fileManager.getDocuments(fileType);
                    break;
            }
        }
        //File file = fileManager.getSampleFile();
        Response response = null;
        switch (connectionType){
            case UPLOAD_FILE:
            case UPLOAD_MULT_FILES:
                if(files != null){
                    int total_files = files.length;
                    int total_ok = 0;
                    //int file_index = Integer.parseInt(data.get("file_index"));
                    String file_names = data.get("file_names");
                    String[] names = file_names.split(":");
                    for(int i = 0; i < files.length; i++){
                        Log.i("DATABASECONNECTION", "FILE PATH------------- "+files[i].getAbsolutePath());
                        response = uploadFile(httpClient, url, files[i], new String(Base64.decode(names[i].getBytes(), Base64.NO_WRAP)));
                        /* SHOULD CHECK RESPONSE = FILE_UPLOADED:DOCUMENT INSTEAD */
                        if(response.code() == 200){

                            total_ok++;
                        }
                    }
                    if(total_files == total_ok){
                        uploadedMultipleFiles = true;
                    }
                    return null;
                }
                else
                    response = uploadFile(httpClient, url, file, data.get("file_name"));
                break;
            case DOWNLOAD_FILE:
                response = downloadFile(httpClient, url);
                break;
            default:break;
        }
        return response;

    }

    private Response uploadFile(OkHttpClient httpClient, String url, File file, String fileName) throws IOException{
        String contentType = file.toURI().toURL().openConnection().getContentType();
        //String fileName = data.get("file_name");
        //MediaType MEDIA_TYPE_MARKDOWN = MediaType.parse("text/x-markdown; charset=utf-8");
        //RequestBody body1 = RequestBody.create(MEDIA_TYPE_MARKDOWN, file);
        System.out.println("SSS: "+connectionType.toString()+"  "+url);
        System.out.println("SSS: "+file.getAbsolutePath()+"  "+fileName);
        if(connectionType == Type.UPLOAD_FILE_SRC){
            String catid = data.get("catid");
            String blub = Base64.encodeToString(new String(catid+":"+fileName).getBytes(), Base64.NO_WRAP);
            post_data += "sample_action=upload&blub="+blub;
            return getResponse(url);
        }

        RequestBody body = MultipartBody.create(MediaType.parse(contentType), file);
        RequestBody body1 = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("fileUploadType", "1")
                .addFormDataPart("miniType", contentType)
                .addFormDataPart("ext", file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(".")))
                .addFormDataPart("fileTypeName", "pdf")
                .addFormDataPart("clientFilePath", Uri.fromFile(file).getPath())
                .addFormDataPart("filedata", fileName, body)
                .build();


        Request request = new Request.Builder()
                .url(url)
                .post(body1)
                .build();

        return httpClient.newCall(request).execute();
    }

    private Response downloadFile(OkHttpClient httpClient, String url) throws IOException{
        Log.i("DATABASE", "TRYING TO FETCH: "+url);
        Request request = new Request.Builder()
                .url(url)
                .build();
        return httpClient.newCall(request).execute();
    }

    private String storeDownloadedFile(Response response) throws IOException{
        InputStream is = response.body().byteStream();

        BufferedInputStream input = new BufferedInputStream(is);
        //String currentFileName = ((HomePage)context).getCurrentSampleFilename();
        String currentFileName = data.get("file_name_decoded");
        File downloadDir = fileManager.getDownloadFolder();
        String path = downloadDir.getPath()+"/"+currentFileName;
        Log.i("DATABASE", "STORING FILE TO "+path);
        OutputStream output = new FileOutputStream(path);

        byte[] data = new byte[1024];

        long total = 0;
        int count = 0;
        while ((count = input.read(data)) != -1) {
            total += count;
            output.write(data, 0, count);
        }

        output.flush();
        output.close();
        input.close();
        if(total > 0){
            return "FILE_DOWNLOADED";
        }
        else{
            return "FILE_NOT_DOWNLOADED";
        }
    }

    private Response getImageResponse(String url) throws IOException{
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(context.getString(R.string.serverauthname), context.getString(R.string.serverauthpass)))
                .connectionSpecs(Collections.singletonList(MyConnectionSpec()))
                .build();
        //MediaType mediaType = MediaType.parse("image/jpg");
        //System.out.println("PARAMETERS: "+url_data);
        //RequestBody body = RequestBody.create(mediaType, url_data);
        File file = fileManager.getLatestTakenPicture();
        System.out.println(file);
        if(file != null) {

            String contentType = file.toURI().toURL().openConnection().getContentType();
            System.out.println("CONTENTTYPE: " + contentType);
            /*
            String fileName = Base64.encodeToString((new String((new Random()).nextInt(15000) + "")).getBytes(), Base64.NO_WRAP);
            Long ts = System.currentTimeMillis() / 1000;
            String timestamp = ts.toString();
            fileName += ":" + Base64.encodeToString(timestamp.getBytes(), Base64.NO_WRAP);
            fileName = Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
            */
            String fileName = data.get("file_name");
            //fileName += ".jpg";
            String previousFileName = data.get("previous_file_name");
            System.out.println("FILENAME: " + fileName);
            System.out.println("PREVIOUS: " + previousFileName);

            if(connectionType == Type.UPLOAD_IMAGE_SRC){
                String catid = data.get("catid");


                String blub = Base64.encodeToString(new String(catid+":"+fileName+":"+previousFileName).getBytes(), Base64.NO_WRAP);
                post_data += "image_action=upload&blub="+blub;
                return getResponse(url);
            }
            else if(connectionType == Type.UPLOAD_PRIVATE_IMAGE_SRC){
                String owner = data.get("owner");
                String blub = Base64.encodeToString(new String(fileName+":"+owner).getBytes(), Base64.NO_WRAP);
                post_data += "image_action=upload_private&blub="+blub;
                return getResponse(url);
            }
            RequestBody body = MultipartBody.create(MediaType.parse(contentType), file);
            RequestBody body1 = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("fileUploadType", "1")
                    .addFormDataPart("miniType", contentType)
                    .addFormDataPart("ext", file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(".")))
                    .addFormDataPart("fileTypeName", "img")
                    .addFormDataPart("clientFilePath", Uri.fromFile(file).getPath())
                    .addFormDataPart("filedata", fileName, body)
                    .build();

            Request request = new Request.Builder()
                    .url(url)
                    .post(body1)
                    .build();

            Response response = httpClient.newCall(request).execute();
            return response;
        }
        else{
            return null;
        }

    }

    /*
    private void downloadImage() throws IOException{
        System.out.println("DOWNLOAD IMAGE --------------");
        String catid = data.get("catid");
        String src = data.get("src");
        if(src != null && !src.equals("")) {
            HashMap<String, Bitmap> bitmapHashmap = new HashMap<>();
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(new BasicAuthInterceptor(context.getString(R.string.serverauthname), context.getString(R.string.serverauthpass)))
                    .build();

            String url = context.getString(R.string.serverimagesrc) + src;
            Request request2 = new Request.Builder()
                    .url(url)
                    .build();
            System.out.println("IMAGE DOWNLOAD URL: " + url);
            final Response response = httpClient.newCall(request2).execute();
            int response_code = response.code();
            if (response_code == 200) {
                System.out.println("IMAGE ACCESSIBLE");
                InputStream inputStream = response.body().byteStream();
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                //bitmapHashmap.put(catid, bitmap);
                addImageToList(catid, bitmap);
                //bitmaps.add(bitmap);
                //fileManager.saveImageToLocalStorage(imageURL, bitmap, false);
            } else {
                System.out.println("IMAGE DOWNLOAD RESPONSE CODE: " + response_code);
            }
        }
        else{

            if(src == null){
                System.out.println("SRC IS NULL");
            }
            else
                System.out.println("SRC: "+src);
            noCatImage(catid);

        }
    }
    */

    /*
    private void noCatImage(final String catid){
        ((HomePage)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((HomePage)context).noImageWithCat(catid);
            }
        });
    }

    private void addImageToList(final String catid, final Bitmap bitmap){
        ((HomePage)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((HomePage)context).addCatImageToList(catid, bitmap);
            }
        });
    }
    */
}
