package kattcenter.norrtljekattcenter.network;

import java.util.HashMap;

import kattcenter.norrtljekattcenter.Type;

/**
 * Created by Jocke on 2017-06-20.
 */

public class DataPacket {

    private HashMap<String, String> data;
    private Type connectionType;

    public DataPacket(HashMap<String, String> data, Type connectionType){
        this.data = data;
        this.connectionType = connectionType;
    }

    public HashMap<String, String> getData(){
        return data;
    }

    public Type getConnectionType(){
        return connectionType;
    }
}
