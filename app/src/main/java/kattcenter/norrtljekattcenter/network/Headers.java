package kattcenter.norrtljekattcenter.network;

import android.content.Context;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;

import okhttp3.Credentials;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-18.
 */

public class Headers {


    public static GlideUrl getUrlWithHeaders(Context context, String url){
        final String AUTHORIZATION = Credentials.basic(context.getString(R.string.serverauthname), context.getString(R.string.serverauthpass));
        return new GlideUrl(url, new LazyHeaders.Builder()
                .addHeader("Authorization", AUTHORIZATION)
                .build());
    }
}
