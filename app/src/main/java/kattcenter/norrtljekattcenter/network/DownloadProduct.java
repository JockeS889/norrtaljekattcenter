package kattcenter.norrtljekattcenter.network;

import android.content.Context;
import android.util.Base64;

import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.Type;

/**
 * Created by Jocke on 2017-06-12.
 */

public class DownloadProduct {

    private Context context;
    private HashMap<String, String> data = new HashMap<>();

    public DownloadProduct(Context context, String currentForm, String table_name, String action){
        this.context = context;
        String content = table_name+":"+action+":"+currentForm;
        String blub = Base64.encodeToString(content.getBytes(), Base64.NO_WRAP);
        data.put("blub", blub);
    }

    public void download() throws InterruptedException {
        ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_PRODUCT));
        ((HomePage)context).signalQueueToSend();
        //new DatabaseConnection(context, data, Type.DOWNLOAD_PRODUCT).execute();
    }
}
