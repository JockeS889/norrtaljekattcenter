package kattcenter.norrtljekattcenter.network;

/**
 * Created by Joakim on 2017-07-24.
 */

public class ConnectionHolder {
    //public static ConnectionHolder connectionHolder;
    private static DownloadManager downloadManager;

    /*
    public static synchronized ConnectionHolder getConnectionHolder(){
        return ConnectionHolder.connectionHolder;
    }

    public static synchronized void setConnectionHolder(ConnectionHolder connectionHolder){
        ConnectionHolder.connectionHolder = connectionHolder;
    }
    */

    public static synchronized DownloadManager getDownloadManager(){
        return ConnectionHolder.downloadManager;
    }

    public static synchronized void setDownloadManager(DownloadManager downloadManager){
        ConnectionHolder.downloadManager = downloadManager;
    }
}
