package kattcenter.norrtljekattcenter.network;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.LoadFormHandler;
import kattcenter.norrtljekattcenter.LoadPreviewHandler;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.dialog.BottomDialog;

/**
 * Created by Joakim on 2017-06-25.
 */

public class DownloadManager {

    private Context context;
    private HomePage homePage;
    private int connectionAttempts = 3;

    public DownloadManager(Context context){
        this.context = context;
        homePage = (HomePage) this.context;
    }


    public void handleDownloadedContent(String blub){
        String decoded_blub = new String(Base64.decode(blub.getBytes(), Base64.NO_WRAP));
        String[] decoded = decoded_blub.split(":");
        String type = decoded[0];
        System.out.println("TYPE==="+type);
        switch (type){
            case "CATEGORIES":
                homePage.getHomePageHandler().reloadPersonalHomepage(decoded);
                break;
            case "NAMES_AND_ID":
                String jsonObj = null;
                if(decoded.length == 2)
                    jsonObj = new String(Base64.decode(decoded[1].getBytes(), Base64.NO_WRAP));
                //catListDialog.addCatsToPicker(jsonObj);
                //homePage.addCatInfoToPicker(jsonObj);
                homePage.checkDownloadedNamesAndID(jsonObj);
                break;
            case "NAMES_ID_IMGSRC_CATOWNER":
                String jsonObj1 = null;
                if(decoded.length == 2)
                    jsonObj1 = new String(Base64.decode(decoded[1].getBytes(), Base64.NO_WRAP));
                homePage.checkDownloadedNamesAndID(jsonObj1);
                break;
            case "NAMES_ID_CATOWNER":
                String jsonObj2 = null;
                if(decoded.length == 2)
                    jsonObj1 = new String(Base64.decode(decoded[1].getBytes(), Base64.NO_WRAP));
                homePage.checkDownloadedNamesAndID(jsonObj2);
                break;
            /*
            case "HISTORY":
                String result = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                homePage.updateHistoryLogView(result);
                homePage.signalQueueToSend();
                break;
                */
            case "DOWNLOAD_REMINDERS":
                String reminder_result = new String(Base64.decode(decoded[1].getBytes(), Base64.NO_WRAP));
                //reminderHandler.addReminder(reminder_result);
                homePage.addReminder(reminder_result);
                homePage.signalQueueToSend();
                break;
            case "DOWNLOADED_NAMES":
                String blub_names = new String(Base64.decode(decoded[1].getBytes(), Base64.NO_WRAP));
                String[] exploded_blub_names = blub_names.split(":");
                if(exploded_blub_names.length == 2) {
                    String names = exploded_blub_names[1];
                    String decoded_names = new String(Base64.decode(names.getBytes(), Base64.NO_WRAP));
                    homePage.setAutoCompleteNames(decoded_names);
                }
                homePage.setReceivedNames(true);
                break;
            case "DOWNLOAD_CATINFO":
                String json_encoded = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                System.out.println("ENCODED: "+json_encoded);
                homePage.getHomePageHandler().getListHandler().addPreviousCatowners(json_encoded);

                break;
            case "DOWNLOAD_LIST":
                String value = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                System.out.println("VALUE========="+value);
                String r_table_name = value.split(":")[0];
                if(value.split(":").length == 2) {
                    String listjson = new String(Base64.decode(value.split(":")[1].getBytes(), Base64.NO_WRAP));
                    System.out.println("TABLE_NAME: " + r_table_name);
                    if (r_table_name.equals("catowner")) {
                        //addCatownersToList(listjson);
                        homePage.getHomePageHandler().getListHandler().addCatownersToList(listjson);
                    } else if(r_table_name.equals("cats")){
                        //System.out.println("LJ: " + listjson);
                        //addCatsToList(listjson);
                        homePage.getHomePageHandler().getListHandler().addCatsToList(listjson);
                    } else if(r_table_name.equals("private_cats")){
                        homePage.getHomePageHandler().getListHandler().addPrivateCatsToList(listjson);
                    } else if(r_table_name.equals("personel")){
                        homePage.getHomePageHandler().getListHandler().addPersonelsToList(listjson);
                    }
                }
                else{
                    if (r_table_name.equals("catowner")) {
                        //addCatownersToList(listjson);
                        homePage.getHomePageHandler().getListHandler().addCatownersToList("");
                    } else if(r_table_name.equals("cats")){
                        //System.out.println("LJ: " + listjson);
                        //addCatsToList(listjson);
                        homePage.getHomePageHandler().getListHandler().addCatsToList("");
                    }else if(r_table_name.equals("personel")){
                        homePage.getHomePageHandler().getListHandler().addPersonelsToList("");
                    }
                    homePage.getHomePageHandler().closeStandardDialog();
                }
                break;
            /*
            case "DOWNLOAD_CATOWNER_CATLIST":
                String catowner_catlist_encoded = decoded[1];
                //String rec1 = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                //String catowner_catlist_encoded = rec1.split(":")[1];
                String catowner_catlist_decoded = new String(Base64.decode(catowner_catlist_encoded.getBytes(), Base64.NO_WRAP));
                //String json_rows_encoded = catowner_catlist_decoded.split(":")[1];
                //String json_rows_decoded = new String(Base64.decode(json_rows_encoded.getBytes(), Base64.NO_WRAP));
                //Log.i("DOWNLOADMANAGER", "MY CATS: "+catowner_catlist_decoded);
                //homePage.getHomePageHandler().getListHandler().addCatsToList(catowner_catlist_decoded);
                homePage.getHomePageHandler().handleDownloadedCatownerCatsInfo(catowner_catlist_decoded);

                break;
                */
            case "FILE_UPLOADED":
                String filetype = decoded[1];
                if(filetype.equals("IMAGE")){
                    System.out.println("IMAGE");
                    String table_name_recieved = decoded[2];
                    if(table_name_recieved.equals("catform_basic")){
                        System.out.println("basic form");
                        //uploadedBasicInfo++;
                        int uploadedBasicInfo = homePage.incrementUploadedBasicInfo();
                        if(homePage.getMaxBasicInfo() == uploadedBasicInfo){
                            LinearLayout dialog_row = homePage.getUploadingForm().get(table_name_recieved);
                            (dialog_row.findViewById(R.id.form_locker_progressbar)).setVisibility(View.INVISIBLE);
                            ((TextView)dialog_row.findViewById(R.id.form_locker_text_status)).setText("LÅST");
                            ((TextView)dialog_row.findViewById(R.id.form_locker_text_status)).setTextColor(Color.parseColor("#000000"));
                            homePage.setChangedCatPhoto(false);
                            homePage.resetUploadedBasicInfo();
                        }
                    }
                }
                homePage.getController().startUpload(); //CHECK IF MORE UPLOAD IS QUEUED
                homePage.signalQueueToSend();
                break;
            case "FILE_SRC_UPLOAD_SUCCESS":
                String table = decoded[1];
                if(table.equals("catform_basic")){
                    System.out.println("basic form");
                    int uploadedBasicInfo = homePage.incrementUploadedBasicInfo();
                    if(homePage.getMaxBasicInfo() == uploadedBasicInfo){
                        LinearLayout dialog_row = homePage.getUploadingForm().get(table);
                        (dialog_row.findViewById(R.id.form_locker_progressbar)).setVisibility(View.INVISIBLE);
                        ((TextView)dialog_row.findViewById(R.id.form_locker_text_status)).setText("LÅST");
                        ((TextView)dialog_row.findViewById(R.id.form_locker_text_status)).setTextColor(Color.parseColor("#000000"));
                        homePage.setChangedCatPhoto(false);
                        homePage.resetUploadedBasicInfo();
                    }
                    else{
                        homePage.getController().startUpload();
                        homePage.signalQueueToSend();
                    }
                }
                break;
            case "PRIVATE_FILE_SRC_UPLOAD_SUCCESS":

                if(decoded[1].equals("catform_basic")){
                    System.out.println("basic form");
                    int uploadedBasicInfo = homePage.incrementUploadedBasicInfo();
                    if(homePage.getMaxBasicInfo() == uploadedBasicInfo){
                        LinearLayout dialog_row = homePage.getUploadingForm().get(decoded[1]);
                        (dialog_row.findViewById(R.id.form_locker_progressbar)).setVisibility(View.INVISIBLE);
                        ((TextView)dialog_row.findViewById(R.id.form_locker_text_status)).setText("LÅST");
                        ((TextView)dialog_row.findViewById(R.id.form_locker_text_status)).setTextColor(Color.parseColor("#000000"));
                        homePage.setChangedCatPhoto(false);
                        homePage.resetUploadedBasicInfo();
                    }
                    else{
                        homePage.getController().startUpload();
                        homePage.signalQueueToSend();
                    }
                }
                break;
            case "UPLOADED_FORM":
                String table_name_received = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                if(table_name_received.equals("catform_basic")){
                    int uploadedBasicInfo = homePage.incrementUploadedBasicInfo();
                    System.out.println("UPLOADED BASIC FORM INFO: "+uploadedBasicInfo);
                    if(homePage.isChangedCatPhoto() && homePage.getMaxBasicInfo() != uploadedBasicInfo){
                        System.out.println("REMAINING BASIC FORM INFO");
                        homePage.getController().startUpload(); //CHECK IF MORE UPLOAD IS QUEUED
                        break;
                    }
                    else{
                        homePage.resetUploadedBasicInfo();
                    }
                }
                homePage.setChangedCatPhoto(false);
                homePage.getController().startUpload(); //CHECK IF MORE UPLOAD IS QUEUED
                homePage.signalQueueToSend();
                break;
            /*
            case "UPLOADED_FORM":
                String table_name_received = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                //System.out.println("TABLE_UPLOADED: "+table_name_received);
                //System.out.println("UPLOADING FORM AFTER: "+uploadingForm.toString());
                LinearLayout dialog_row = homePage.getUploadingForm().get(table_name_received);
                System.out.println("ROW CHILDREN: "+dialog_row.getChildCount());
                if(table_name_received.equals("catform_basic")){
                    int uploadedBasicInfo = homePage.incrementUploadedBasicInfo();
                    System.out.println("UPLOADED BASIC FORM INFO: "+uploadedBasicInfo);
                    if(homePage.isChangedCatPhoto() && homePage.getMaxBasicInfo() != uploadedBasicInfo){
                        System.out.println("REMAINING BASIC FORM INFO");
                        homePage.getController().startUpload(); //CHECK IF MORE UPLOAD IS QUEUED
                        break;
                    }
                    else{
                        homePage.resetUploadedBasicInfo();
                    }
                }
                homePage.setChangedCatPhoto(false);
                (dialog_row.findViewById(R.id.form_locker_progressbar)).setVisibility(View.INVISIBLE);
                ((TextView)dialog_row.findViewById(R.id.form_locker_text_status)).setText("LÅST");
                ((TextView)dialog_row.findViewById(R.id.form_locker_text_status)).setTextColor(Color.parseColor("#000000"));
                //setLockForm(table_name_received, false);
                homePage.getController().startUpload(); //CHECK IF MORE UPLOAD IS QUEUED
                homePage.signalQueueToSend();
                break;
                */
            case "UPLOADED_FORM_VETDOC":
                if(homePage.getUploaded_catform_docs_failed() == false) {
                    int uploaded_catform_docs = homePage.incrementUploaded_catform_docs();
                    //uploadStatusQueue.add("UPLOAD_CLEAR");
                    if (uploaded_catform_docs == 12) {
                        homePage.resetUploaded_catform_docs();
                        //uploadStatusQueue.clear();
                        LinearLayout dialog_row1 = homePage.getUploadingForm().get("catform_vetdoc");
                        //System.out.println("ROW CHILDREN: "+dialog_row1.getChildCount());
                        //(dialog_row1.findViewById(R.id.form_locker_progressbar)).setVisibility(View.INVISIBLE);
                        //((TextView) dialog_row1.findViewById(R.id.form_locker_text_status)).setText("LÅST");
                        //((TextView) dialog_row1.findViewById(R.id.form_locker_text_status)).setTextColor(Color.parseColor("#000000"));
                        //setLockForm("catform_vetdoc", false);
                        System.out.println("TABLE_UPLOADED: "+"catform_vetregdoc");
                    }
                }
                homePage.getController().startUpload(); // CHECK IF MORE UPLOAD IS QUEUED
                homePage.signalQueueToSend();
                break;
            case "UPLOADED_FORM_FAILED":
                String table_name_received1 = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                Log.i("DOWNLOADMANAGER", "TABLE NAME: "+table_name_received1);
                //LinearLayout dialog_row2 = homePage.getUploadingForm().get(table_name_received1);
                //(dialog_row2.findViewById(R.id.form_locker_progressbar)).setVisibility(View.INVISIBLE);
                //((TextView)dialog_row2.findViewById(R.id.form_locker_text_status)).setText("FEL");
                //((TextView)dialog_row2.findViewById(R.id.form_locker_text_status)).setTextColor(Color.parseColor("#FF5555"));
                break;
            case "UPLOADED_FORM_VETDOC_FAILED":
                homePage.setUploaded_catform_docs_failed(true);
                homePage.resetUploaded_catform_docs();
                //LinearLayout dialog_row3 = homePage.getUploadingForm().get("catform_vetdoc");
                //(dialog_row3.findViewById(R.id.form_locker_progressbar)).setVisibility(View.INVISIBLE);
                //((TextView)dialog_row3.findViewById(R.id.form_locker_text_status)).setText("FEL");
                //((TextView)dialog_row3.findViewById(R.id.form_locker_text_status)).setTextColor(Color.parseColor("#FF5555"));
                break;
            case "DOWNLOADED_FORM":
                String content_type = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                System.out.println("CONTENT-TYPE: "+content_type);
                String form_table = content_type.split(":")[0];
                LoadPreviewHandler loadPreviewHandler = new LoadPreviewHandler(context);
                if(content_type.split(":").length == 2) {
                    String form_content = content_type.split(":")[1];
                    //setLockForm(form_table, false);
                    if(homePage.getLoadPreview()) {
                        if (form_table.equals("catform_basic") || form_table.equals("reg_skk") || form_table.equals("catform_connection")) {
                            LoadFormHandler loadFormHandler = new LoadFormHandler(context);
                            loadFormHandler.loadForm(form_table, form_content);

                        }

                        loadPreviewHandler.loadPreview(form_table, form_content);
                    }
                    else if(homePage.getLoadPdfGenerator()){
                        Log.i("DOWNLOADMANAGER", "Load to pdf generator");
                        //homePage.addContentToPdfGenerator(form_table, form_content);
                        homePage.getHomePageHandler().addContentToPDfGenerator(form_table, form_content);
                    }
                }
                else{
                    loadPreviewHandler.clearPreview();
                }
                //homePage.setLockForm(form_table, false);
                if(homePage.getPrivileged() != Type.CATOWNER)
                    homePage.getController().startDownloadForms(); //CHECK IF MORE DOWNLOAD IS QUEUED
                break;
            case "DOWNLOADED_MED":
                String content = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                String[] type_list = content.split(":");
                String table_type = type_list[0];
                switch (table_type){
                    case "MEDICATIONS":

                        break;
                    case "VACCINATIONS":
                        homePage.getController().updateVaccinList(type_list[1]);
                        break;
                    default:break;
                }
                break;
            case "DOWNLOADED_PRODUCT":
                String content1 = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                System.out.println("R_CONT: "+content1);
                String[] elements = content1.split(":");
                String table_name = elements[0];
                String encoded_blub = elements[1];
                homePage.getController().updateProductList(table_name, encoded_blub);
                break;
            case "DOWNLOAD_PERSONEL":
                String value1 = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                String personel_info_json= new String(Base64.decode(value1.split(":")[1].getBytes(), Base64.NO_WRAP));
                personel_info_json = personel_info_json.split("\n")[0];
                System.out.println(personel_info_json);
                //homePage.getHomePageHandler().receivedPersonelSettings(personel_info_json);
                homePage.getHomePageHandler().recievedPersonelInfo(personel_info_json);
                //homePage.getHomePageHandler().getCatownerManager().loadCatownerInfo(personel_info_json);
                //homePage.getHomePageHandler().closeStandardDialog();
                break;
            case "DOWNLOAD_CATOWNER":
                String value2 = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                //String r_table_name1 = value1.split(":")[0];
                String catowner_info_json= new String(Base64.decode(value2.split(":")[1].getBytes(), Base64.NO_WRAP));
                catowner_info_json = catowner_info_json.split("\n")[0];
                System.out.println(catowner_info_json);
                homePage.getHomePageHandler().getCatownerManager().loadCatownerInfo(catowner_info_json);
                homePage.getHomePageHandler().closeStandardDialog();
                break;
            case "DOWNLOADED_OPEN_HOURS":
                String hours = new String(Base64.decode(decoded[1], Base64.NO_WRAP));
                homePage.getHours(hours);
                break;
            default:break;
        }
    }

    public void retryConnection(final DataPacket dataPacket){
        if(connectionAttempts > 0) {
            Handler handler = new Handler();
            Runnable retryRunnable = new Runnable() {
                @Override
                public void run() {
                    System.out.println("RETRY");
                    try {
                        homePage.addPrioritizedDatapacketToNetworkThread(dataPacket);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //new DatabaseConnection(HomePage.this, data, connectionType).execute();
                }
            };
            handler.postDelayed(retryRunnable, 5000);
            connectionAttempts--;
        }
        else{
            /*
            if(homePage.getHomePageHandler().isSaveformShowing()) {
                //String blub = dataPacket.getData().get("blub");
                //String table_name_received = new String(Base64.decode(blub.getBytes(), Base64.NO_WRAP)).split(":")[0];
                //LinearLayout dialog_row = homePage.getUploadingForm().get(table_name_received);
                //(dialog_row.findViewById(R.id.form_locker_progressbar)).setVisibility(View.INVISIBLE);
                //((TextView) dialog_row.findViewById(R.id.form_locker_text_status)).setText("FEL");
                //((TextView) dialog_row.findViewById(R.id.form_locker_text_status)).setTextColor(Color.parseColor("#FF5454"));
                homePage.getController().startUpload();
            }
            */
            ConnectionHolder.setDownloadManager(null);
            new BottomDialog(context).popup("Fel vid anslutning till server");
            connectionAttempts = 3;
        }
    }


}
