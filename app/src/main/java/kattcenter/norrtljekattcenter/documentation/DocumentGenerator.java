package kattcenter.norrtljekattcenter.documentation;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DrawInterface;
import com.itextpdf.text.pdf.fonts.otf.TableHeader;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.BufferUnderflowException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.SaveFormInstanceHandler;
import kattcenter.norrtljekattcenter.catform.BundleResourceMapping;
import kattcenter.norrtljekattcenter.catform.FormFieldMapper;

/**
 * Created by Jocke on 2017-06-03.
 */

public class DocumentGenerator {


    //private HashMap<String, String> bundleTranslationTable = new HashMap<>();

    private FormFieldMapper formFieldMapper;
    private Context context;
    private SaveFormInstanceHandler saveFormInstanceHandler;
    //private Document document;
    //private PdfPTable pdfPTable;
    private ArrayList<PdfPTable> rows = new ArrayList<>();
    private BundleResourceMapping higherCaseMapping;
    private FormFieldMapper mapping;
    private HashMap<String, PdfPTable> tableHashMap = new HashMap<>();
    private Image headerImage;

    public DocumentGenerator(Context context){
        this.context = context;
        saveFormInstanceHandler = new SaveFormInstanceHandler((HomePage)context);
        formFieldMapper = new FormFieldMapper();
        higherCaseMapping = new BundleResourceMapping();
        mapping = new FormFieldMapper();
        prepHeader();
        //initTranslationTable();
    }

    private void prepHeader(){
        generateHeader();
    }

    /*
    public Uri generatePDF(HashMap<String, Boolean> checkedMailForms){

        Set<String> set = checkedMailForms.keySet();
        for(String key: set){
            System.out.println(key+"  "+checkedMailForms.get(key));
        }






        String fileName = "testPdf.pdf";
        String directory = "Test";
        Document document = new Document();
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + directory;
        File dir = new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }
        File pdfFile = new File(dir, fileName);
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(pdfFile);
            PdfWriter.getInstance(document, fileOutputStream);
            document.open();

            PdfPTable pdfPTable = new PdfPTable(1);
            //pdfPTable.setWidths(new int[]{1,1,1});
            pdfPTable.setWidthPercentage(100);
            if(checkedMailForms.get("Allmän info"))
                pdfPTable.addCell(pdfCell("Allmän info", saveFormInstanceHandler.saveBasicFormInstance()));
            if(checkedMailForms.get("Temperament"))
                pdfPTable.addCell(pdfCell("Temperament", saveFormInstanceHandler.saveTemperFormInstance()));
            if(checkedMailForms.get("Medicinering"))
                pdfPTable.addCell(pdfCell("Medicinering", saveFormInstanceHandler.saveMedicationFormInstance()));
            if(checkedMailForms.get("Vaccinering"))
                pdfPTable.addCell(pdfCell("Vaccinering", saveFormInstanceHandler.saveVaccinFormInstance()));
            if(checkedMailForms.get("Parasiter"))
                pdfPTable.addCell(pdfCell("Parasiter", saveFormInstanceHandler.saveParasitesFormInstance()));

            if(checkedMailForms.get("Vikt"))
                pdfPTable.addCell(pdfCell("Vikt", saveFormInstanceHandler.saveClawFormInstance()));
            if(checkedMailForms.get("Veterinärintyg"))
                pdfPTable.addCell(pdfTableVetDoc("Veterinärintyg", saveFormInstanceHandler.saveVetDocFormInstance()));
            document.add(pdfPTable);



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } finally {
            document.close();
        }


                    /*
            Paragraph paragraph = new Paragraph("Detta är en test rad");
            Font font = new Font(Font.FontFamily.COURIER);
            paragraph.setAlignment(Paragraph.ALIGN_CENTER);
            paragraph.setFont(font);
            PdfPTable pdfPTable = new PdfPTable(2);
            pdfPTable.setWidthPercentage(100);

            pdfPTable.addCell(new PdfPCell(paragraph));
            document.add(pdfPTable);
            */
/*
        Uri fileURI = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/" + directory + "/" + fileName));
        return fileURI;
    }
    */


    private PdfPCell pdfCell(String title, Bundle bundle){
        PdfPCell currentCell = new PdfPCell();
        Paragraph paragraphTitle = new Paragraph(title);
        Font fontTitle = new Font(Font.FontFamily.COURIER);
        fontTitle.setStyle(Font.BOLD);
        paragraphTitle.setFont(fontTitle);
        paragraphTitle.setAlignment(Paragraph.ALIGN_LEFT);
        currentCell.addElement(paragraphTitle);
        Set<String> keySet = bundle.keySet();
        for(String key: keySet){
            Object valueObj = bundle.get(key);
            if(valueObj instanceof String){
                String value = (String) valueObj;
                if(formFieldMapper.containsTranslation(key)){
                    String readableName = formFieldMapper.getTranslation(key);
                    Paragraph paragraph = new Paragraph(readableName+":"+value);
                    Font font = new Font(Font.FontFamily.COURIER);
                    paragraph.setFont(font);
                    paragraph.setAlignment(Paragraph.ALIGN_LEFT);
                    currentCell.addElement(paragraph);
                }
            }
        }
        return currentCell;
    }

    private void pdfTable2(String title, String content){
        PdfPTable entryTable = new PdfPTable(1);
        PdfPCell titleCell = new PdfPCell();
        System.out.println("TITLE: "+title);
        HashMap<String, String> map = higherCaseMapping.getCatVetItemTable();
        Paragraph paragraphTitle;
        if(map.containsKey(title)){
            paragraphTitle = new Paragraph(map.get(title));
        }
        else{
            map = higherCaseMapping.getFormTableTranslated(title);
            paragraphTitle = new Paragraph(((HomePage)context).getDbTablesToTitles().get(title));
        }

        Font fontTitle = new Font(Font.FontFamily.COURIER, 25);
        fontTitle.setStyle(Font.BOLD);
        paragraphTitle.setFont(fontTitle);
        paragraphTitle.setAlignment(Paragraph.ALIGN_LEFT);
        titleCell.setBorder(Rectangle.NO_BORDER);
        titleCell.addElement(paragraphTitle);
        entryTable.addCell(titleCell);
        PdfPCell emptyCell = new PdfPCell();
        emptyCell.setBorder(Rectangle.NO_BORDER);
       // entryTable.addCell(emptyCell);
        entryTable.completeRow();

        System.out.println("---------------------MAPPING--------");

        if(!title.equals("catform_connection")) {
            for (Map.Entry entry : map.entrySet()) {
                System.out.println(entry.getKey() + ", " + entry.getValue());
            }
        }
        System.out.println("------------------------------CONTENT--------------------------");
        System.out.println(content);
        String[] timeLineEntries = content.split("\n");
        try{
            for(String entry: timeLineEntries){
                if(title.equals("catform_connection")){


                    PdfPTable subTable = new PdfPTable(2);
                    boolean dataExists = false;


                    String[] familyComp = content.split(":", -1);
                    String mother = familyComp[0];
                    String father = familyComp[1];
                    String siblings = familyComp[2];
                    String children = familyComp[3];
                    if(mother.isEmpty()){

                    }
                    else{
                        dataExists = true;
                        mother = new String(Base64.decode(mother.getBytes(), Base64.NO_WRAP));
                        mother = mother.split(":")[0];
                        mother = new String(Base64.decode(mother.getBytes(), Base64.NO_WRAP));
                        String mother_desc = mother.split(":")[0] + ", "+mother.split(":")[1];
                        Paragraph state = new Paragraph("Förälder");
                        Paragraph description = new Paragraph(mother_desc);
                        Font font = new Font(Font.FontFamily.COURIER, 12);
                        state.setFont(font);
                        state.setAlignment(Element.ALIGN_LEFT);
                        PdfPCell stateCell = new PdfPCell();
                        stateCell.addElement(state);
                        stateCell.setBorder(Rectangle.NO_BORDER);

                        description.setFont(font);
                        description.setAlignment(Element.ALIGN_RIGHT);
                        PdfPCell descriptionCell = new PdfPCell();
                        descriptionCell.addElement(description);
                        descriptionCell.setBorder(Rectangle.NO_BORDER);

                        subTable.addCell(stateCell);
                        subTable.addCell(descriptionCell);
                        subTable.completeRow();
                    }
                    if(father.isEmpty()){

                    }
                    else{
                        dataExists = true;
                        father = new String(Base64.decode(father.getBytes(), Base64.NO_WRAP));
                        father = father.split(":")[0];
                        father = new String(Base64.decode(father.getBytes(), Base64.NO_WRAP));
                        String father_desc = father.split(":")[0] +","+ father.split(":")[1];
                        Paragraph state = new Paragraph("Förälder");
                        Paragraph description = new Paragraph(father_desc);
                        Font font = new Font(Font.FontFamily.COURIER, 12);
                        state.setFont(font);
                        state.setAlignment(Element.ALIGN_LEFT);
                        PdfPCell stateCell = new PdfPCell();
                        stateCell.addElement(state);
                        stateCell.setBorder(Rectangle.NO_BORDER);

                        description.setFont(font);
                        description.setAlignment(Element.ALIGN_RIGHT);
                        PdfPCell descriptionCell = new PdfPCell();
                        descriptionCell.addElement(description);
                        descriptionCell.setBorder(Rectangle.NO_BORDER);

                        subTable.addCell(stateCell);
                        subTable.addCell(descriptionCell);
                        subTable.completeRow();
                    }
                    if(siblings.isEmpty()){

                    }
                    else{
                        dataExists = true;
                        siblings = new String(Base64.decode(siblings.getBytes(), Base64.NO_WRAP));
                        for(String sibling: siblings.split(":")){
                            String decoded_sibling = new String(Base64.decode(sibling.getBytes(), Base64.NO_WRAP));
                            String[] sibling_info = decoded_sibling.split(":");
                            String sibling_name = sibling_info[0];
                            String sibling_id = sibling_info[1];
                            Paragraph state = new Paragraph("Syskon");
                            Paragraph description = new Paragraph(sibling_name+", "+sibling_id);
                            Font font = new Font(Font.FontFamily.COURIER, 12);
                            state.setFont(font);
                            state.setAlignment(Element.ALIGN_LEFT);
                            PdfPCell stateCell = new PdfPCell();
                            stateCell.addElement(state);
                            stateCell.setBorder(Rectangle.NO_BORDER);

                            description.setFont(font);
                            description.setAlignment(Element.ALIGN_RIGHT);
                            PdfPCell descriptionCell = new PdfPCell();
                            descriptionCell.addElement(description);
                            descriptionCell.setBorder(Rectangle.NO_BORDER);

                            subTable.addCell(stateCell);
                            subTable.addCell(descriptionCell);
                            subTable.completeRow();
                        }
                    }
                    if(children.isEmpty()){

                    }
                    else{
                        dataExists = true;
                        children = new String(Base64.decode(children.getBytes(), Base64.NO_WRAP));
                        for(String child: children.split(":")){
                            String decoded_child = new String(Base64.decode(child.getBytes(), Base64.NO_WRAP));
                            String[] child_info = decoded_child.split(":");
                            String child_name = child_info[0];
                            String child_id = child_info[1];
                            Paragraph state = new Paragraph("Barn");
                            Paragraph description = new Paragraph(child_name+", "+child_id);
                            Font font = new Font(Font.FontFamily.COURIER, 12);
                            state.setFont(font);
                            state.setAlignment(Element.ALIGN_LEFT);
                            PdfPCell stateCell = new PdfPCell();
                            stateCell.addElement(state);
                            stateCell.setBorder(Rectangle.NO_BORDER);

                            description.setFont(font);
                            description.setAlignment(Element.ALIGN_RIGHT);
                            PdfPCell descriptionCell = new PdfPCell();
                            descriptionCell.addElement(description);
                            descriptionCell.setBorder(Rectangle.NO_BORDER);

                            subTable.addCell(stateCell);
                            subTable.addCell(descriptionCell);
                            subTable.completeRow();
                        }
                    }
                    if(dataExists) {
                        entryTable.addCell(subTable);
                        entryTable.completeRow();
                    }

                }
                else {
                    JSONObject jsonObject = new JSONObject(entry);
                    Iterator<String> keys = jsonObject.keys();
                    PdfPTable subTable = new PdfPTable(2);
                    while (keys.hasNext()) {
                        String key = keys.next();

                        if (key.equals("id") ||
                                key.equals("catcenter_id") || key.equals("photo_src") ||
                                key.equals("table_name") || key.equals("start_date_reminded") || key.equals("repeat_date_reminded"))
                            continue;
                        if (!map.containsKey(title)) {
                            if (key.equals("reg_date") || key.equals("date") || key.equals("reg_author"))
                                continue;
                        }
                        String value = jsonObject.getString(key);
                        System.out.println(key + "     " + value);
                        String higher_case = map.get(key);
                        key = (formFieldMapper.containsTranslation(higher_case)) ? formFieldMapper.getTranslation(higher_case) : key;
                        key = (map.containsKey(key)) ? map.get(key) : key;
                        if (key.equals("date")) key = "Datum";
                        if (key.equals("reg_date")) key = "Registrerat datum ";
                        if (key.equals("reg_author")) {
                            key = "Registrerat av";
                            value = new String(Base64.decode(value.getBytes(), Base64.NO_WRAP));
                        }
                        if (value.equals("1")) value = "Ja";
                        else if (value.isEmpty()) value = "";
                        if(key.equals("Dokument") && !value.isEmpty()){
                            value = new String(Base64.decode(value.getBytes(), Base64.NO_WRAP));
                            String contentPart = value.split(".")[0];
                            contentPart = new String(Base64.decode(contentPart.getBytes(), Base64.NO_WRAP));
                            value = new String(Base64.decode(contentPart.split(":")[0].getBytes(), Base64.NO_WRAP));
                        }
                        //Paragraph paragraph = new Paragraph(key+":"+value);
                        Paragraph state = new Paragraph(key);
                        Paragraph description = new Paragraph(value);
                        Font font = new Font(Font.FontFamily.COURIER, 12);
                        state.setFont(font);
                        state.setAlignment(Element.ALIGN_LEFT);
                        PdfPCell stateCell = new PdfPCell();
                        stateCell.addElement(state);
                        stateCell.setBorder(Rectangle.NO_BORDER);

                        description.setFont(font);
                        description.setAlignment(Element.ALIGN_RIGHT);
                        PdfPCell descriptionCell = new PdfPCell();
                        descriptionCell.addElement(description);
                        descriptionCell.setBorder(Rectangle.NO_BORDER);

                        subTable.addCell(stateCell);
                        subTable.addCell(descriptionCell);
                        subTable.completeRow();
                    /*
                    paragraph.setFont(font);
                    paragraph.setAlignment(Paragraph.ALIGN_LEFT);
                    currentCell.addElement(paragraph);
                    */
                    }
                    entryTable.addCell(subTable);
                    entryTable.completeRow();
                }

            }
        } catch (JSONException jsone){
            jsone.printStackTrace();
        }
        //rows.add(entryTable);
        tableHashMap.put(title, entryTable);
        //return currentCell;
    }

    private PdfPCell pdfCellFromHashmap(String parent, HashMap<String, Boolean> hashMap, String comment){
        PdfPCell currentCell = new PdfPCell();
        Paragraph paragraph = new Paragraph(parent);
        Font font = new Font(Font.FontFamily.COURIER);
        font.setSize(13);
        paragraph.setFont(font);
        paragraph.setAlignment(Paragraph.ALIGN_LEFT);
        currentCell.addElement(paragraph);
        Set<String> keySet = hashMap.keySet();
        for(String key: keySet){
            Boolean valueObj = hashMap.get(key);
            String stringValue = valueObj ? "Ja" : "Nej";
            Paragraph paragraph1 = new Paragraph(key +": "+stringValue);
            Font font1 = new Font(Font.FontFamily.COURIER);
            font1.setSize(10);
            paragraph1.setFont(font1);
            paragraph1.setAlignment(Paragraph.ALIGN_LEFT);
            currentCell.addElement(paragraph1);
        }
        Paragraph paragraph2 = new Paragraph("Kommentar: "+comment);
        Font font2 = new Font(Font.FontFamily.COURIER);
        font2.setStyle(Font.BOLD);
        paragraph2.setFont(font2);
        paragraph2.setAlignment(Paragraph.ALIGN_LEFT);
        currentCell.addElement(paragraph2);
        return currentCell;
    }

    private PdfPTable pdfTableVetDoc(String title, Bundle bundle){
        PdfPTable titleTable = new PdfPTable(1);
        PdfPTable currentTable = new PdfPTable(3);
        Set<String> keySet = bundle.keySet();
        String date = bundle.getString("DATE");
        HashMap<String, HashMap<String, Boolean>> records = (HashMap<String, HashMap<String, Boolean>>) bundle.getSerializable("VETDOCCHECKBOXES");
        HashMap<String, String> comments = (HashMap<String, String>) bundle.getSerializable("VETDOCNOTES");
        Set<String> parents = records.keySet();
        for(String parent: parents){
            HashMap<String, Boolean> record = records.get(parent);
            String comment = comments.get(parent);
            currentTable.addCell(pdfCellFromHashmap(parent, record, comment));
        }
        Paragraph paragraph = new Paragraph(title+"         Datum: "+date);
        Font font = new Font(Font.FontFamily.COURIER);
        font.setStyle(Font.BOLD);
        font.setSize(15);
        paragraph.setFont(font);
        paragraph.setAlignment(Paragraph.ALIGN_CENTER);
        titleTable.addCell(new PdfPCell(paragraph));
        titleTable.addCell(currentTable);
        return titleTable;
    }

    /*
    private void getHeaderInfo(){
        ((HomePage)context).setLoadPdfGenerator(true);
    }
    */

    private void orderListEntries(){
        String[] orderedKeys = ((HomePage)context).getDbTables();
        String[] orderedCatVetItemsKeys = ((HomePage)context).getCatvetdoctables();
        Set<String> keySet = tableHashMap.keySet();
        for(String key: keySet){
            //System.out.println("KEY: "+key);
        }
        for(String title: orderedKeys){
            if(tableHashMap.containsKey(title)){
                //System.out.println("ORDER: "+title+" "+tableHashMap.get(title));
                rows.add(tableHashMap.get(title));
            }
        }
        for(String title: orderedCatVetItemsKeys){
            if(tableHashMap.containsKey(title)){
                //System.out.println("ORDER CATVETITEMS: "+title+" "+tableHashMap.get(title));
                rows.add(tableHashMap.get(title));
            }
        }
    }

    public Uri createDocument(RelativeLayout settingsPage){

        String author = ((TextView)settingsPage.findViewById(R.id.user_name_text)).getText().toString();
        /*
        String author = ((TextView)settingsPage.findViewById(R.id.user_surname_text)).getText().toString() + " " +
                ((TextView)settingsPage.findViewById(R.id.user_lastname_text)).getText().toString();
        */
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String currentDate = simpleDateFormat.format(Calendar.getInstance().getTime());

        String fileName = "testPdf.pdf";
        String directory = "Test";
        HeaderTable eventHeader = new HeaderTable();

        Document document = new Document(PageSize.A4, 36, 36, 150 + eventHeader.getTableHeight(), 100);
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + directory;
        File dir = new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }
        File pdfFile = new File(dir, fileName);
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(pdfFile);
            PdfWriter pdfWriter = PdfWriter.getInstance(document, fileOutputStream);
            //PdfPTable tableFooter = new PdfPTable(1);
            //tableFooter.addCell("SIDFOT");
            //FooterTable eventFooter = new FooterTable(tableFooter);
            pdfWriter.setPageEvent(eventHeader);


            PdfPTable table = new PdfPTable(2);
            table.setTotalWidth(523);
            PdfPCell cell = new PdfPCell(new Phrase("Utfärdare: "+author));
            cell.setBorder(Rectangle.NO_BORDER);
            //cell.setBackgroundColor(BaseColor.ORANGE);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Datum: "+currentDate));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            //cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);
            FooterTable event = new FooterTable(table);
            pdfWriter.setPageEvent(event);



            //pdfWriter.setPageEvent(eventFooter);
            document.open();
            PdfPTable pdfPTable = new PdfPTable(1);
            pdfPTable.setWidthPercentage(100);

            //pdfPTable.addCell(generateHeader());

            orderListEntries();
            for(PdfPTable row: rows){
                System.out.println("ROW: "+row.toString());
                pdfPTable.addCell(row);
            }

            document.add(pdfPTable);
            Log.i("DOCUMENTGENERATOR", "TABLE ADDED TO DOCUMENT");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
        Uri fileURI = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/" + directory + "/" + fileName));
        return fileURI;
    }
    public void addContent(String form_table, String content){
        Log.i("DOCUMENTGENERATOR", "ADDING "+content+ " TO PDF WITH TABLE NAME: "+form_table);
        pdfTable2(form_table, new String(Base64.decode(content.getBytes(), Base64.NO_WRAP)));
        //pdfPTable.addCell(pdfCell2(form_table, new String(Base64.decode(content.getBytes(), Base64.NO_WRAP))));
    }

    /*
    public void closeDocument(){
        try{
            document.close();
        } catch (Exception ioe){
            ioe.printStackTrace();
        }
    }
    */


    public class HeaderTable extends PdfPageEventHelper {
        protected PdfPTable table;
        protected float tableHeight;
        public HeaderTable() {
            table = new PdfPTable(1);
            table.setTotalWidth(523);
            table.setLockedWidth(true);
            Font font = new Font(Font.FontFamily.TIMES_ROMAN);
            font.setSize(9);
            table.addCell(noBorderTextCell("Norrtälje Kattcenter", font));
            table.addCell(noBorderTextCell("Grovstavägen 4", font));
            table.addCell(noBorderTextCell("761 93 Norrtälje", font));
            tableHeight = table.getTotalHeight();
        }

        public float getTableHeight() {
            return tableHeight;
        }

        public void onEndPage(PdfWriter writer, Document document) {

            PdfContentByte cb = writer.getDirectContent();

            try {
                cb.addImage(headerImage);
            } catch (DocumentException e) {
                e.printStackTrace();
            }

            table.writeSelectedRows(0, -1,
                    document.left(),
                    document.top() + ((document.topMargin() + tableHeight) / 2),
                    writer.getDirectContent());
        }
    }

    public class FooterTable extends PdfPageEventHelper {
        protected PdfPTable footer;
        public FooterTable(PdfPTable footer) {
            this.footer = footer;
        }
        public void onEndPage(PdfWriter writer, Document document) {
            footer.writeSelectedRows(0, -1, 36, 64, writer.getDirectContent());
        }
    }


    public PdfPCell noBorderTextCell(String text, Font font){
        PdfPCell pdfPCell = new PdfPCell();
        Paragraph paragraph = new Paragraph(text);
        paragraph.setFont(font);
        pdfPCell.setBorder(Rectangle.NO_BORDER);
        pdfPCell.addElement(paragraph);
        return pdfPCell;
    }

    public void generateHeader(){
        PdfPTable pdfPTable = new PdfPTable(1);
        //pdfPTable.setWidthPercentage(20);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Drawable d = context.getResources().getDrawable(R.drawable.catcenter_icon, null);
            Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] bitmapdata = stream.toByteArray();
            try {
                headerImage = Image.getInstance(bitmapdata);
                headerImage.scaleToFit(70,70);
                headerImage.setAbsolutePosition(50, 760);

                headerImage.setBorder(Rectangle.NO_BORDER);
            } catch (BadElementException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //pdfPTable.addCell(image);
        }
        //return pdfPTable;
    }


    /*
    private void initTranslationTable(){
        bundleTranslationTable.put("CATNAME", "Kattnamn");
        bundleTranslationTable.put("PRESENT_DATE", "Inkom datum");
        bundleTranslationTable.put("BOX", "Box");
        bundleTranslationTable.put("MOVED", "Flyttad");
        bundleTranslationTable.put("CONNECTION", "Koppling");
        bundleTranslationTable.put("TATOO", "Tatuerad");
        bundleTranslationTable.put("TATOO_STRING", "Tatuering");
        bundleTranslationTable.put("TATOO_DATE", "Datum");
        bundleTranslationTable.put("TEMPER_TAME", "Tam");
        bundleTranslationTable.put("TEMPER_SHY", "Skygg");
        bundleTranslationTable.put("CHIP", "Chip");
        bundleTranslationTable.put("CHIP_STRING", "Kod");
        bundleTranslationTable.put("NEUTER", "Kastrerad");
        bundleTranslationTable.put("NEUTER_DATE", "Datum");
        bundleTranslationTable.put("CATHOME_NAME", "Katthems namn");
        bundleTranslationTable.put("CATHOME_ID", "Katthems id");
        bundleTranslationTable.put("BIRTHDAY", "Födelsedag");
        bundleTranslationTable.put("SEX", "Kön");
        bundleTranslationTable.put("RACE", "Ras");
        bundleTranslationTable.put("COLOR", "Färg");
        bundleTranslationTable.put("HAIR", "Päls");
        bundleTranslationTable.put("START_DATE", "Startdatum");
        bundleTranslationTable.put("END_DATE", "Slutdatum");
        bundleTranslationTable.put("MEDICATION", "Läkemedel");
        bundleTranslationTable.put("DOSE", "Dos");
        bundleTranslationTable.put("ADMINISTRATION", "Administrering");
        bundleTranslationTable.put("VACCIN_TYPE", "Vaccintyp");
        bundleTranslationTable.put("TIME", "Företeelse");
        bundleTranslationTable.put("COMMENT", "Kommentar");
        bundleTranslationTable.put("REPEAT_DATE", "Upprepas");
        bundleTranslationTable.put("VERMIN_TYPE", "Typ av ohyra");
        bundleTranslationTable.put("TREATMENT_TYPE", "Typ av behandling");
        bundleTranslationTable.put("WEIGHT", "Vikt");
        bundleTranslationTable.put("CHECK_WEIGHT", "Kolla vikt igen");
        bundleTranslationTable.put("AMOUNT", "Antal");
        bundleTranslationTable.put("DEWORMING_TYPE", "Typ av avmaskning");
        bundleTranslationTable.put("REG_SKK", "Registrerad i SKK");
        bundleTranslationTable.put("REG_SVE", "Registrerad i Svekatt");
        bundleTranslationTable.put("TAKEN_DATE", "Datum tagit över försäkring");
        bundleTranslationTable.put("OLD_COMPANY", "Bolag");
        bundleTranslationTable.put("OLD_NUMBER", "Försäkringsnummer");
        bundleTranslationTable.put("SIGNED_DATE", "Datum nytecknad");
        bundleTranslationTable.put("NEW_COMPANY", "Bolag");
        bundleTranslationTable.put("NEW_NUMBER", "Försäkringsnummer");
        bundleTranslationTable.put("TRANSFER_DATE", "Fört över till nya ägaren");

    }
    */




















    public Uri generate4(Bundle medicalForms) throws IOException{
        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(595, 842, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);
        float x = 0, y = 50;
        Typeface courier = Typeface.create("Courier New", Typeface.NORMAL);
        Set<String> keySet = medicalForms.keySet();
        for(String key: keySet){
            //View content = textView;
            //Bitmap bitmap = Bitmap.createBitmap(content.getWidth(), content.getHeight(), Bitmap.Config.ARGB_8888);
            //content.draw(new Canvas(bitmap));
            String text = medicalForms.getString(key);
            String row = key+": "+text;
            Canvas pageCanvas = page.getCanvas();
            Paint paint = new Paint();
            paint.setColor(Color.BLACK);
            paint.setTextSize(20);
            paint.setTypeface(courier);
            pageCanvas.drawText(row, x, y, paint);
            y += 30;
            //content.draw(page.getCanvas());
            //page.getCanvas().translate(0, content.getHeight());
        }
        pdfDocument.finishPage(page);
        String fileName = "generatedMedicalInfo.pdf";
        File outputFile = new File(Environment.getExternalStorageDirectory(), fileName);
        FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
        pdfDocument.writeTo(fileOutputStream);
        pdfDocument.close();
        fileOutputStream.close();
        File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);

        return Uri.fromFile(filelocation);
    }

    public Uri generate3(ArrayList<TextView> textViewArrayList) throws IOException{
        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(595, 842, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);
        float x = 0, y = 50;
        Typeface courier = Typeface.create("Courier New", Typeface.NORMAL);
        for(TextView textView: textViewArrayList){
            //View content = textView;
            //Bitmap bitmap = Bitmap.createBitmap(content.getWidth(), content.getHeight(), Bitmap.Config.ARGB_8888);
            //content.draw(new Canvas(bitmap));
            Canvas pageCanvas = page.getCanvas();
            Paint paint = new Paint();
            paint.setColor(Color.BLACK);
            paint.setTextSize(20);
            paint.setTypeface(courier);
            pageCanvas.drawText(textView.getText().toString(), x, y, paint);
            y += 30;
            //content.draw(page.getCanvas());
            //page.getCanvas().translate(0, content.getHeight());
        }
        pdfDocument.finishPage(page);
        String fileName = "generatedMedicalInfo.pdf";
        File outputFile = new File(Environment.getExternalStorageDirectory(), fileName);
        FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
        pdfDocument.writeTo(fileOutputStream);
        pdfDocument.close();
        fileOutputStream.close();
        File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);

        return Uri.fromFile(filelocation);
    }

    public Uri generate2(Bundle bundle) throws IOException{
        String fileName = "generatedMedicalInfo.txt";
        File outputFile = new File(Environment.getExternalStorageDirectory(), fileName);
        FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
        fileOutputStream.write(new String("---------------------------------").getBytes());
        Set<String> keySet = bundle.keySet();
        for(String key: keySet){
            fileOutputStream.write(key.getBytes());
            fileOutputStream.write(":".getBytes());
            fileOutputStream.write(bundle.getString(key).getBytes());
        }
        fileOutputStream.flush();
        fileOutputStream.close();
        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(595, 842, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);
        File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);
        return Uri.fromFile(filelocation);
    }

    public Uri generate(Bundle bundle) throws IOException{
        String fileName = "generatedMedicalInfo.txt";
        File outputFile = new File(Environment.getExternalStorageDirectory(), fileName);
        FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
        //PrintWriter printWriter = new PrintWriter(outputFile);
        fileOutputStream.write(new String("---------------------------------").getBytes());
        fileOutputStream.write(new String("\n").getBytes());
        fileOutputStream.write(new String("Startdatum: ").getBytes());
        if(bundle.getInt("START_DATE_SET") == 0) fileOutputStream.write(bundle.getString("START_DATE").getBytes());
        else fileOutputStream.write(new String("\n").getBytes());
        fileOutputStream.write(new String("Slutdatum: ").getBytes());
        if(bundle.getInt("END_DATE_SET") == 0) fileOutputStream.write(bundle.getString("END_DATE").getBytes());
        else fileOutputStream.write(new String("\n").getBytes());
        fileOutputStream.write(new String("Medicin: ").getBytes());
        if(bundle.getInt("MEDICATION_SET") == 0) fileOutputStream.write(bundle.getString("MEDICATION").getBytes());
        else fileOutputStream.write(new String("\n").getBytes());
        fileOutputStream.write(new String("Dos: ").getBytes());
        if(bundle.getInt("DOSE_SET") == 0) fileOutputStream.write(bundle.getString("DOSE").getBytes());
        else fileOutputStream.write(new String("\n").getBytes());
        fileOutputStream.write(new String("Administrering: ").getBytes());
        if(bundle.getInt("ADMINISTRATION_SET") == 0) fileOutputStream.write(bundle.getString("ADMINISTRATION").getBytes());
        else fileOutputStream.write(new String("\n").getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);
        return Uri.fromFile(filelocation);

        /*
        Intent send = new Intent(Intent.ACTION_SENDTO);

        String uriText = "mailto:" + Uri.encode("email@gmail.com") +
                "?subject=" + Uri.encode("the subject") +
                "&body=" + Uri.encode("the body of the message");
        Uri uri = Uri.parse(uriText);

        send.setData(uri);
        startActivity(Intent.createChooser(send, "Send mail..."));
        /*
        Set<String> keySet = bundle.keySet();
        ArrayList<String> keyList = new ArrayList<>();
        for(String key: keySet){
            keyList.add(key);
            /*
            Object value = bundle.get(key);
            if(value instanceof String){
                System.out.println(key+" "+bundle.getString(key));
            }
            else if(value instanceof Integer){
                System.out.println(key+" "+bundle.getInt(key));
            }
            */
        /*
        }
        Collections.sort(keyList);
        for(String key: keyList){
            Object value = bundle.get(key);
            if(value instanceof String){
                System.out.println(key+" "+bundle.getString(key));
            }
            else if(value instanceof Integer){
                System.out.println(key+" "+bundle.getInt(key));
            }
        }
        */
    }
}
