package kattcenter.norrtljekattcenter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Jocke on 2017-05-23.
 */

public class CalendarAdapter extends BaseAdapter {

    private Context context;
    private GregorianCalendar calendarPreviousMonth, calendarCurrentMonth, selectedDate, previousMonthMaxSet, currentDateMonth;
    private ArrayList<String> items;
    public static ArrayList<String> dates;
    private String currentViewedDate, currentDate, itemValue, sunday;
    private DateFormat dateFormat;
    private View previousView;
    private int firstDay, maxWeeknumber, monthLength, previousMonthMax, calendarMaxPrevious;
    private static ArrayList<Integer> sundays;

    public CalendarAdapter(Context context, GregorianCalendar gregorianCalendar, GregorianCalendar currentDateMonth){
        this.context = context;
        calendarCurrentMonth = gregorianCalendar;
        this.currentDateMonth = currentDateMonth;
        selectedDate = (GregorianCalendar) calendarCurrentMonth.clone();
        calendarCurrentMonth.set(GregorianCalendar.DAY_OF_MONTH, 1);
        CalendarAdapter.dates = new ArrayList<>();
        CalendarAdapter.sundays = new ArrayList<>();
        items = new ArrayList<>();
        Locale.setDefault(Locale.getDefault());
        //System.out.println("DEFAULT LOCALE: "+Locale.getDefault());
        dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        //System.out.println("DEFAULT DATEFORMAT: "+dateFormat);
        currentViewedDate = dateFormat.format(selectedDate.getTime());
        currentDate = dateFormat.format(currentDateMonth.getTime());
        System.out.println("ADAPTER: "+ currentViewedDate+"    "+currentDate);
        refreshDays();
    }

    public void setItems(ArrayList<String> items){
        for(int i = 0; i < items.size(); i++){
            if(items.get(i).length() == 1){
                items.set(i, "0"+items.get(i));
            }
        }

        this.items = items;
        //System.out.println("SET LENGTH: "+this.items.size());
    }

    public View setSelected(View view) {
        if (previousView != null) {
            //previousView.setBackgroundResource(R.drawable.list_item_background);
            //previousView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                previousView.setBackgroundColor(context.getResources().getColor(R.color.colorCalendar, null));
            }
            else{
                previousView.setBackgroundColor(context.getResources().getColor(R.color.colorCalendar));
            }
        }
        previousView = view;
        //view.setBackgroundResource(R.drawable.calendar_cel_selectl);
        view.setBackgroundColor(Color.parseColor("#652020"));
        return view;
    }

    public void refreshDays() {
        // clear items
        items.clear();
        dates.clear();
        sundays.clear();
        GregorianCalendar nextSunday = (GregorianCalendar) calendarCurrentMonth.clone();
        nextSunday.set(GregorianCalendar.DAY_OF_WEEK, GregorianCalendar.SUNDAY);
        //Locale.setDefault(Locale.US);
        Locale.setDefault(new Locale("sv", "SE"));
        calendarPreviousMonth = (GregorianCalendar) calendarCurrentMonth.clone();
        // month start day. ie; sun, mon, etc
        firstDay = calendarCurrentMonth.get(GregorianCalendar.DAY_OF_WEEK) - 2;
        firstDay = (firstDay < 0) ? firstDay+7 : firstDay;
        //System.out.println("FIRST DAY: "+firstDay);
        // finding number of weeks in current month.
        maxWeeknumber = calendarCurrentMonth.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);
        // allocating maximum row number for the gridview.
        monthLength = maxWeeknumber * 7;
        previousMonthMax = getMaxP(); // previous month maximum day 31,30....
        calendarMaxPrevious = previousMonthMax - (firstDay - 1);// calendar offday starting 24,25 ...
        //System.out.println("MAX PREV: "+calendarMaxPrevious);
        /**
         * Calendar instance for getting a complete gridview including the three
         * month's (previous,current,next) dates.
         */
        previousMonthMaxSet = (GregorianCalendar) calendarPreviousMonth.clone();
        /**
         * setting the start date as previous month's required date.
         */
        previousMonthMaxSet.set(GregorianCalendar.DAY_OF_MONTH, calendarMaxPrevious);

        /**
         * filling calendar gridview.
         */
        for (int n = 0; n < monthLength; n++) {

            itemValue = dateFormat.format(previousMonthMaxSet.getTime());
            //System.out.println("ITEM: "+itemValue);
            previousMonthMaxSet.add(GregorianCalendar.DATE, 1);
            dates.add(itemValue);
        }
        String sundayDate = android.text.format.DateFormat.format("dd", nextSunday).toString();
        int sundayDateValue = Integer.parseInt(sundayDate.replaceFirst("^0*", ""));
        int ml = calendarCurrentMonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
        for(int n = sundayDateValue; n <= ml; n+=7){

            sundays.add(n);
        }
        for(int n = sundayDateValue; n > 0; n-=7){

            if(!sundays.contains(n)) sundays.add(n);
        }
        /*
        for(int i: sundays){
            System.out.println("SUNDAYS: "+i);
        }
        */
        //System.out.println("REFRESHED LENGTH: "+this.items.size());
        /*
                sunday = dateFormat.format(i)
        sundays.add(sunday);
            */
    }

    private int getMaxP() {
        int maxP;
        int a = calendarCurrentMonth.get(GregorianCalendar.MONTH);
        int b = calendarCurrentMonth.getActualMinimum(GregorianCalendar.MONTH);

        if (a == b) {
            calendarPreviousMonth.set((calendarPreviousMonth.get(GregorianCalendar.YEAR) - 1),
                    calendarCurrentMonth.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            calendarPreviousMonth.set(GregorianCalendar.MONTH,
                    calendarCurrentMonth.get(GregorianCalendar.MONTH) - 1);
        }

        maxP = calendarPreviousMonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

        return maxP;
    }





    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public Object getItem(int position) {
        return dates.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        TextView textView;
        if(convertView == null){
            view = ((Activity) context).getLayoutInflater().inflate(R.layout.calendar_item, null);
        }

        textView = (TextView) view.findViewById(R.id.dateText);
        String[] dateformat = dates.get(position).split("-");
        String dayOfDate = dateformat[2];
        //System.out.println("DAY: "+dayOfDate);
        dayOfDate.replaceFirst("^0*", "");
        int dayValue = Integer.parseInt(dayOfDate);
        //System.out.println("VALUES: "+dayValue+" "+position+" "+firstDay);
        if(dayValue > 1 && position < firstDay){
            textView.setTextColor(Color.parseColor("#CECECE"));
            textView.setClickable(false);
            textView.setFocusable(false);
        }
        else if(dayValue < 7 && position > 28){
            textView.setTextColor(Color.parseColor("#CECECE"));
            textView.setClickable(false);
            textView.setFocusable(false);
        }
        else{

            if(dates.get(position).equals(currentViewedDate)){
                textView.setTextColor(Color.parseColor("#FFFFFF"));
            }
            else{
                textView.setTextColor(Color.parseColor("#FFFFFF"));
            }
            for(int s: sundays){

                if(dayValue == s){
                    //System.out.println(s+" "+dayValue);
                    textView.setTextColor(Color.parseColor("#CC6666"));
                }
            }
        }
        if(dates.get(position).equals(currentDate)){
            setSelected(view);
            previousView = view;

        }
        else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                view.setBackgroundColor(context.getResources().getColor(R.color.colorCalendar, null));
            }
            else{
                view.setBackgroundColor(context.getResources().getColor(R.color.colorCalendar));
            }
        }
        textView.setText(dayOfDate);

        String dateString = dates.get(position);
        if(dateString.length() == 1){
            dateString = "0" + dateString;
        }

        String monthString = "" + (calendarCurrentMonth.get(GregorianCalendar.MONTH));
        if(monthString.length() == 1){
            monthString = "0" + monthString;
        }

        TextView textView1 = (TextView) view.findViewById(R.id.eventText);

        if(dateString.length() > 0 && items != null){
            textView1.setVisibility(View.INVISIBLE);
            for(String item: items){
                if(item.split(":")[0].equals(dateString)){
                    textView1.setText(item.split(":")[1]);
                    textView1.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
        else{
            textView1.setVisibility(View.INVISIBLE);
        }

        return view;

    }
}
