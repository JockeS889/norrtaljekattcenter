package kattcenter.norrtljekattcenter;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.Spannable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import kattcenter.norrtljekattcenter.calendar.ReminderHandlerAsync;
import kattcenter.norrtljekattcenter.catform.AutoCompleteAdapter;
import kattcenter.norrtljekattcenter.catform.FormController;
import kattcenter.norrtljekattcenter.catform.IndexHolder;
import kattcenter.norrtljekattcenter.catform.RegSKKInfo;
import kattcenter.norrtljekattcenter.catform.VetRegInfo;
import kattcenter.norrtljekattcenter.catlist.CatListDialog;
import kattcenter.norrtljekattcenter.catlist.ChosenCatFromListAdapter;
import kattcenter.norrtljekattcenter.dialog.BottomDialog;
import kattcenter.norrtljekattcenter.file.FileManager;
import kattcenter.norrtljekattcenter.init.Initializer;
import kattcenter.norrtljekattcenter.layout.personal_homepage.AboutUsSettings;
import kattcenter.norrtljekattcenter.layout.personal_homepage.HomePageHandler;
import kattcenter.norrtljekattcenter.network.DataPacket;
import kattcenter.norrtljekattcenter.network.GMailSender;
import kattcenter.norrtljekattcenter.network.NetworkThread;
import kattcenter.norrtljekattcenter.notification.NotificationHandler;
import kattcenter.norrtljekattcenter.search.SearchEngine;
import kattcenter.norrtljekattcenter.sms.SmsSender;


import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

/**
 *  Mainclass for homepage, handles the activity
 */
public class HomePage extends AppCompatActivity {

    //private static View view;
    private DrawerLayout drawerLayout, appMenuLayout;
    private ViewFlipper viewFlipper;

    //STATE REGISTERS
    private int currentFlippedViewIndex = 0;
    private int currentSettingButtonId = 0;

    private Type privileged;
    //private CatListImageAdapter catListImageAdapter;
    //private CatownerListAdapter catownerListAdapter;
   // private GridView catListView, calendarView;
    private ListView catListView;

    private ListView catownerListView;
    //private RelativeLayout footerLayout;
    private RelativeLayout headerLayout, homePage, catlistPage, catowner_management_page, settingsPage;
    private Boolean isPortraitMode = true;
    //private LinearLayout footerBar;


    //private SearchView searchCatBar, searchCatOwnerBar;

    private RelativeLayout catOwnerInfo, catOwnerCatsInfoPage, catOwnerSavePage;
    private RelativeLayout personalListPage, personalInfoPage, personalHistoryPage, personalSavePage;
    private ListView personalListView;

    private RelativeLayout catInfoParasites, catInfoConnection, catInfoTemperament, catInfoCatowners;
    private RelativeLayout catInfoBasic, catInfoMediciation, catInfoVaccin, catInfoDeworming, catInfoVermin, catInfoClaws, catInfoWeight, catInfoVetDoc, catInfoRegSKK, catInfoVet, catInfoInsurance, catInfoBackground, catInfoStatus;
    private TextView headerPrimaryTitle, headerSecondaryTitle;
    //private String[] catformtitles = {"Allmän info", "Medicinering", "Vaccinering", "Avmaskning", "Ohyra", "Klor", "Vikt", "Veterinärintyg", "Reg.SKK", "Veterinär", "Försäkring", "Bakgrund", "Historik", "Status"};
    private String[] catformtitles = {"Allmän Info", "Temperament", "Medicinering", "Vaccinering", "Parasiter", "Klor", "Vikt", "Veterinärintyg", "Reg.SKK", "Veterinär", "Försäkring", "Bakgrund", "Status", "Koppling", "Kattägare"};

   // private String[] dbTables = {"catform_basic", "catform_medical", "catform_vaccination", "catform_deworming", "catform_verm", "catform_claws", "catform_weight", "catform_vetdoc", "catform_regskk", "catform_vet", "catform_insurance", "catform_background", "catform_history", "catform_status"};

    private String[] dbTables = {"catform_basic", "catform_temperament", "catform_medical", "catform_vaccination", "catform_parasites", "catform_claws", "catform_weight", "catform_vetdoc", "catform_regskk", "catform_vet", "catform_insurance", "catform_background", "catform_status", "catform_connection"};
    private String[] catvetdoctables = {"catform_vetdoc_generalstate", "catform_vetdoc_lynne", "catform_vetdoc_skinpaws", "catform_vetdoc_lymf", "catform_vetdoc_eyes", "catform_vetdoc_ears",
            "catform_vetdoc_mouth", "catform_vetdoc_abdomen", "catform_vetdoc_circulationorgan", "catform_vetdoc_respirationorgan", "catform_vetdoc_outergenitalias", "catform_vetdoc_locomotiveorgan"};

    private HashMap<String, String> titlesToDbtables = new HashMap<>();
    private HashMap<String, String> dbTablesToTitles = new HashMap<>();
    //private HashMap<String, String> categorieToDBColumn = new HashMap<>();

    private Toolbar headerToolbar;
    private ImageView infoSelectedCat;



    //private ListView appMenuList;
    //private ArrayList<HamburgerListItem> menuItemList;
    //private AppMenuListAdapter appMenuListAdapter;



    private ListView hamburgerListCatForm;
    private ArrayList<HamburgerListItem> hamburgerListItemArrayList;
    private HamburgerListAdapter hamburgerListAdapter;



    private EditText searchField;
    private int DEVICE_ORIENTATION = 0;
    private String currentHeaderTitle, currentHeaderSecondaryTitle;
    private int headerPrimaryColorID, headerPrimaryDarkColorID;

    private TextView saveAndLockCatForm;


    private HashMap<String, Bundle> savedInstances = new HashMap<>();

    private HashMap<String, HashMap<String, Boolean>> spinnerRecord = new HashMap<>();
    private HashMap<String, String> catvetreg_comments = new HashMap<>();
    private String currentCatvetregParent = null;



    private FormController controller;

    static final int SCAN_CODE_REQUEST = 1;  // The request code
    static final int PICK_FILE = 22;
    static final int PICK_VETDOC = 23;
    static final int PICK_REGSKKDOC = 24;
    static final int CREATE_FILE = 21;

    private boolean addNewCat = false, addNewCatowner = false, addNewPersonal = false;
    private String currentViewedCatname = null;
    private String currentViewedCatImageFileName = null;
    private String currentCatId = null;
    private String currentCatImgSrc = null;


    private SaveformAdapter formListAdapter;
    private HashMap<String, LinearLayout> uploadingForm = new HashMap<>();

    private int uploaded_catform_docs = 0;
    private boolean uploaded_catform_docs_failed = false;

    private boolean newPrivateCat = false;

    private boolean multiFileDownload = false;


    private boolean showFormOptions = false;

    private boolean loadCatownerCats = false;


    //private HashMap<String, Boolean> enabledForms = new HashMap<>();



    private VetRegInfo vetRegInfo;
    private RegSKKInfo regSKKInfo;



    private FileManager fileManager;
    private static Uri camera_uri;
    static final int CAMERA_INTENT = 37;
    static final int GALLERY_INTENT = 36;


    private int uploadedBasicInfo = 0;
    private int maxBasicInfo = 3; // IMAGE + IMAGE_SRC + FORM DATA


    private boolean changedCatPhoto = false;
    //private ArrayList<String[]> catsInfo = new ArrayList<>();
    //private ArrayList<String> urlImageList = new ArrayList<>();

    private SmsSender smsSender;
    static String SMS_SENT = "sms_constant";

    //private ReminderHandler reminderHandler;
    private ReminderHandlerAsync reminderHandlerAsync;
    private Date current_date;
    private long current_date_long = 0;

    private NetworkThread networkThread;


    private AutoCompleteAdapter autoCompleteAdapter;
    private AutoCompleteTextView autoCompleteTextView;
    private boolean receivedNames = true;


    static final String NETWORK_TAG = "NETWORK";
    //static final String JSON_TAG = "JSON";
    static final String INTENT_TAG = "INTENT";


    private String chosenAccount, password;

    private CatListDialog catListDialog = null;

    private HomePageHandler homePageHandler;

    private NotificationHandler notificationHandler;
    private RelativeLayout privateCatInfoView;
    private boolean checked_reminders;
    private RelativeLayout catformPreview;

    private int currentPreviewedForm = 0;

    private boolean loadPreview = true;
    private boolean savingForm = false;

    private String currentUserId;
    private String currentUserName;

    //private ListView chosenCatownerCats;
    private ChosenCatFromListAdapter chosenCatFromListAdapter, confirmChosenCatFromListAdapter;

    private boolean loadNewCatownerList = false;
    private boolean loadConnectCatownerList = false;
    private boolean loadPreviousCatownerList = false;
    private boolean loadCatVetItems = false;
    private boolean loadPdfGenerator = false;

    private boolean manage_catformConnection_catlist = false;

    private String currentSearchType = null;

    private SearchEngine searchEngine;
    private boolean generateID = false;
    private String currentSampleFileName;
    private String catListTitle = null;
    private String selectedForm = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_home_page);
        setContentView(R.layout.new_home_page);
        isPortraitMode = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) ? true : false;

        SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", 0);

        Bundle extras = getIntent().getExtras();

        if (extras == null) {
            finish();
        } else {

            initFormTitleMap();
            //initLockedFormMap();

            privileged = (Type) extras.get("PRIVILEGED");

            fileManager = new FileManager(privileged);
            fileManager.createDir();

            if (savedInstanceState != null) {
                restoreSavedState(savedInstanceState);
            } else {
                headerPrimaryColorID = R.color.newColorPrimary;
                headerPrimaryDarkColorID = R.color.newColorPrimary;
            }


            networkThread = new NetworkThread(this);
            networkThread.start();
            Log.i(NETWORK_TAG, "thread started");

            if(privileged == Type.ADMIN || privileged == Type.PERSONAL){
                notificationHandler = new NotificationHandler(this);
            }
            if (privileged == Type.ADMIN) {
                smsSender = new SmsSender(this);
                registerReceiver(smsSender, new IntentFilter(SMS_SENT));



                //reminderHandler = new ReminderHandler(this, smsSender);
                //reminderHandler.start();
                String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(Calendar.getInstance().getTime());

                checked_reminders = false;

                try {
                    current_date = new SimpleDateFormat("yyyy-MM-dd_HH-mm").parse(timeStamp);
                    current_date_long = current_date.getTime();
                    long previous_check_time = sharedPreferences.getLong("CHECKED_REMINDERS", 0);
                    long limit = 60000L;   //CHANGE THIS TO 30000000L = 30min WHEN RELEASED
                    checked_reminders = (previous_check_time != 0 && current_date_long - previous_check_time < limit) ? true : false;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (!checked_reminders) {
                    try {

                        notificationHandler.createNotification();
                        HashMap<String, String> data = new HashMap<>();
                        HashMap<String, String> data2 = new HashMap<>();
                        String blub = Base64.encodeToString("YEARLY_REMINDERS".getBytes(), Base64.NO_WRAP);
                        data.put("blub", blub);
                        String blub2 = Base64.encodeToString("MONTHLY_REMINDERS".getBytes(), Base64.NO_WRAP);
                        data2.put("blub", blub2);
                        networkThread.addDataToQueue(new DataPacket(data, Type.DOWNLOAD_REMINDERS));
                        networkThread.addDataToQueue(new DataPacket(data2, Type.DOWNLOAD_REMINDERS));
                        if (networkThread.nextInQueue()) {
                            //showLoadingDialog("Hanterar påminnelser...");
                            Log.i(NETWORK_TAG, "SEND OUT REMINDERS");
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putLong("CHECKED_REMINDERS", current_date_long);
                            editor.commit();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            System.out.println("PRIV: " + privileged.toString());
            initLayouts();
        }
        //LoginManager.register(this, "5226f6d4f50f4c61abdb474a2cff9d03", LoginManager.LOGIN_MODE_EMAIL_PASSWORD);
        //LoginManager.verifyLogin(this, getIntent());

        checkForUpdates();

        searchEngine = new SearchEngine(this);

        System.out.println("CREATED ACTIVITY");
        /* SAVED INSTANCES LOADED COMPLETE */
        savedInstances.clear();
        final String appPackageName = "com.google.zxing.client.android"; // getPackageName() from Context or Activity object
        boolean ZXingFound = Initializer.isAppInstalled(this, appPackageName);
        if(!ZXingFound && privileged != Type.CATOWNER) {
            Dialog installDialog = new Dialog(this);
            installDialog.setTitle("Installera ");
            LinearLayout buttonField = (LinearLayout) getLayoutInflater().inflate(R.layout.text_button_dialog, null);
            TextView textView = (TextView) buttonField.findViewById(R.id.general_dialog_text);
            textView.setText("Appen 'Barcode scanner' är inte installerad på enheten. Vänligen installera den om du vill kunna använda scanningsfunktionen i denna app.");
            Button confirm = (Button) buttonField.findViewById(R.id.confirm);
            confirm.setText("Google Play");
            Button abort = (Button) buttonField.findViewById(R.id.abort);
            abort.setText("Avbryt / Senare");
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            });
            installDialog.setContentView(buttonField);
            installDialog.show();
        }
        checkPermission();
    }



    private final static int REQUEST_PERMISSION = 55;
    private void checkPermission(){

        boolean hasPermission = (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if(hasPermission){

        }else{
            // ask the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.GET_ACCOUNTS,
                            Manifest.permission.INTERNET,
                            Manifest.permission.READ_SYNC_SETTINGS,
                            Manifest.permission.SEND_SMS,
                            Manifest.permission.CALL_PHONE,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_CALENDAR,
                            Manifest.permission.WRITE_CALENDAR,
                    },
                    REQUEST_PERMISSION);
            // You have to put nothing here (you can't write here since you don't
            // have the permission yet and requestPermissions is called asynchronously)
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // The result of the popup opened with the requestPermissions() method
        // is in that method, you need to check that your application comes here
        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            }
        }
    }




    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("RESUMED ACTIVITY");
        checkForCrashes();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    public String getHeaderTitle(){
        String returnValue;
        switch (privileged){
            case ADMIN: returnValue =  getString(R.string.admin); break;
            case PERSONAL: returnValue = getString(R.string.personal); break;
            case CATOWNER: returnValue = getString(R.string.my_cats); break;
            default:returnValue = null; break;
        }
        return returnValue;
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        if (privileged == Type.ADMIN)
            unregisterReceiver(smsSender);
        super.onDestroy();
        unregisterManagers();
    }

    public void setLoadNewCatownerList(boolean loadNewCatownerList){
        this.loadNewCatownerList = loadNewCatownerList;
    }

    public boolean getLoadNewCatownerList(){
        return loadNewCatownerList;
    }

    public void setLoadConnectCatownerList(boolean loadConnectCatownerList){
        this.loadConnectCatownerList = loadConnectCatownerList;
    }

    public boolean getLoadCatownerCats(){
        System.out.println("GET TTTLOAD : "+loadCatownerCats);
        return loadCatownerCats;
    }

    public void setLoadCatownerCats(boolean loadCatownerCats){
        System.out.println("SET TTTLOAD TO : "+loadCatownerCats);
        this.loadCatownerCats = loadCatownerCats;
    }

    public boolean getLoadConnectCatownerList(){
        return loadConnectCatownerList;
    }

    public void setLoadPreviousCatownerList(boolean loadPreviousCatownerList){
        this.loadPreviousCatownerList = loadPreviousCatownerList;
    }

    public boolean getLoadPreviousCatownerList(){
        return loadPreviousCatownerList;
    }


    public void setLoadCatVetItems(boolean loadCatVetItems){
        this.loadCatVetItems = loadCatVetItems;
    }

    public boolean getLoadCatVetItems(){
        return loadCatVetItems;
    }

    public void setLoadPdfGenerator(boolean loadPdfGenerator){
        this.loadPdfGenerator = loadPdfGenerator;
        if(loadPdfGenerator){
            homePageHandler.initPDFGenerator();
        }
    }



    public boolean getLoadPdfGenerator(){
        return loadPdfGenerator;
    }

    public void addPrioritizedDatapacketToNetworkThread(DataPacket datapacket) throws InterruptedException{
        networkThread.prioritizedDatapacket(datapacket);
    }

    public void addDatapacketToNetworkThread(DataPacket dataPacket) throws InterruptedException {
        networkThread.addDataToQueue(dataPacket);
    }

    public void signalQueueToSend() {
        try {

            while(networkThread.nextInQueue() == true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setAutoCompleteNames(String result) {
        if (result != null && !result.equals("")) {
            String[] names = result.split(":");
            ArrayList<String> arrayList = new ArrayList<>();
            for (String name : names) arrayList.add(name);
            autoCompleteAdapter = new AutoCompleteAdapter(this, arrayList);
            autoCompleteTextView.setAdapter(autoCompleteAdapter);
            autoCompleteTextView.showDropDown();
        } else {
            autoCompleteTextView.dismissDropDown();
        }
    }
    public void setReceivedNames(boolean receivedNames){
        this.receivedNames = receivedNames;
    }

    public int getMaxBasicInfo(){
        return maxBasicInfo;
    }

    public synchronized int incrementUploadedBasicInfo(){
        uploadedBasicInfo++;
        return uploadedBasicInfo;
    }

    public synchronized void resetUploadedBasicInfo(){
        uploadedBasicInfo = 0;
    }

    public AutoCompleteAdapter getAutoCompleteAdapter(){
        return autoCompleteAdapter;
    }


    public FileManager getFileManager() {
        return fileManager;
    }

    public void setCurrentViewedCatImageFileName(String currentViewedCatImageFileName) {
        this.currentViewedCatImageFileName = currentViewedCatImageFileName;
    }

    public String getCurrentViewedCatImageFileName() {
        return currentViewedCatImageFileName;
    }

    public boolean isChangedCatPhoto() {
        return changedCatPhoto;
    }

    public void setChangedCatPhoto(boolean changedCatPhoto){
        this.changedCatPhoto = changedCatPhoto;
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        SaveFormInstanceHandler saveFormInstanceHandler = new SaveFormInstanceHandler(this);
        state.putInt("CURRENT_VIEW_INDEX", currentFlippedViewIndex);
        state.putInt("CURRENT_PREVIEWED_FORM", currentPreviewedForm);
        state.putInt("CURRENT_SETTING_BUTTON_ID", currentSettingButtonId);
        state.putString("CURRENT_HEADER_TITLE", currentHeaderTitle);
        state.putString("CURRENT_HEADER_SECONDARY_TITLE", currentHeaderSecondaryTitle);
        state.putInt("CURRENT_HEADER_PRIMARY_COLOR_ID", headerPrimaryColorID);
        state.putInt("CURRENT_HEADER_PRIMARY_DARK_COLOR_ID", headerPrimaryDarkColorID);
        state.putString("CURRENT_CAT_ID", currentCatId);
        //state.putInt("CURRENT_VIEWED_MONTH", currentMonthIndex);
        //state.putInt("CURRENT_VIEWED_YEAR", currentYearIndex);
        state.putInt("ORIENTATION_MODE", DEVICE_ORIENTATION);
        if (privileged == Type.ADMIN || privileged == Type.PERSONAL) {
            state.putBoolean("CHANGED_CAT_PHOTO", changedCatPhoto);
            state.putBoolean("NEWCATFORM", addNewCat);
            state.putBoolean("NEWCATOWNER", addNewCatowner);
            state.putBundle("BASIC_INFO", saveFormInstanceHandler.saveBasicFormInstance());
            state.putBundle("MEDICAL_INFO", saveFormInstanceHandler.saveMedicationFormInstance());
            state.putBundle("VACCIN_INFO", saveFormInstanceHandler.saveVaccinFormInstance());
            state.putBundle("PARASITES_INFO", saveFormInstanceHandler.saveParasitesFormInstance());
            //state.putBundle("DEWORMING_INFO", saveFormInstanceHandler.saveDewormingFormInstance());
            //state.putBundle("VERMIN_INFO", saveFormInstanceHandler.saveVerminFormInstance());
            state.putBundle("CLAW_INFO", saveFormInstanceHandler.saveClawFormInstance());
            state.putBundle("WEIGHT_INFO", saveFormInstanceHandler.saveWeightFormInstance());
            state.putBundle("VETDOC_INFO", saveFormInstanceHandler.saveVetDocFormInstance());
            state.putBundle("REGSKK_INFO", saveFormInstanceHandler.saveRegSKKFormInstance());
            state.putBundle("VET_INFO", saveFormInstanceHandler.saveVetFormInstance());
            state.putBundle("INSURANCE_INFO", saveFormInstanceHandler.saveInsuranceFormInstance());
            state.putBundle("BACKGROUND_INFO", saveFormInstanceHandler.saveBackgroundFormInstance());
            state.putBundle("PREVIOUS_OWNERS_INFO", saveFormInstanceHandler.savePrevOwnersFormInstance());
            state.putBundle("STATUS_INFO", saveFormInstanceHandler.saveStatusFormInstance());
        }
    }


    private void leaveUnsavedFormsToPage(final int viewIndex, final String title, final String secondTitle) {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Spara ändringar?");
        LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.three_button_dialog, null);
        Button confirm = (Button) linearLayout.findViewById(R.id.option1);
        Button exit = (Button) linearLayout.findViewById(R.id.option2);
        Button abort = (Button) linearLayout.findViewById(R.id.option3);
        confirm.setText("Spara");
        exit.setText("Lämna ändå");
        abort.setText("Avbryt");
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkSave();
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewIndex != IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged))
                    clearForms();
                setView(viewIndex, title);
                //setView(viewIndex, title, secondTitle, R.color.newColorPrimary, R.color.newColorPrimary);
                dialog.dismiss();
            }
        });
        //((TextView)relativeLayout.findViewById(R.id.listEntry)).setText("Du har inte låst formulären");
        dialog.setContentView(linearLayout);
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (viewFlipper.getDisplayedChild() >= IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged) && addNewCat) {
            switch (privileged) {
                case ADMIN:
                case PERSONAL:
                    leaveUnsavedFormsToPage(IndexHolder.authenticateIndex(IndexHolder.HOMEPAGE_INDEX, privileged), getString(R.string.admin), null);
                    break;
                default:
                    break;
            }

        } else if (viewFlipper.getDisplayedChild() != 0) {
            int toIndex = IndexHolder.authenticateIndex(IndexHolder.HOMEPAGE_INDEX, privileged);
            String title;
            final int owner_page = IndexHolder.authenticateIndex(IndexHolder.OWNER_INFO_PAGE, privileged);
            final int preview_page = IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged);
            final int owner_list = IndexHolder.authenticateIndex(IndexHolder.OWNER_LIST_PAGE, privileged);
            if(currentFlippedViewIndex == owner_page){
                title = getString(R.string.user);
                toIndex = owner_list;
                setView(toIndex, title);
                //setView(toIndex, title, null, R.color.newColorPrimary, R.color.newColorPrimary);
            }
            else if(currentFlippedViewIndex == owner_list){
                Intent homeScreen = new Intent(Intent.ACTION_MAIN);
                homeScreen.addCategory(Intent.CATEGORY_HOME);
                homeScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(homeScreen);
            }
            else if(currentFlippedViewIndex == preview_page){
                setView(IndexHolder.authenticateIndex(IndexHolder.CATLIST_PAGE, privileged), "Katter");
                //setView(IndexHolder.authenticateIndex(IndexHolder.CATLIST_PAGE, privileged), "Sök...", null, R.color.newColorPrimary, R.color.newColorPrimary);
            }
            else{
                title = getHeaderTitle();
                //title = (privileged == Type.ADMIN) ? getString(R.string.admin) : getString(R.string.my_cats);
                setView(toIndex, title);
                //setView(toIndex, title, null, R.color.newColorPrimary, R.color.newColorPrimary);
            }
            /*
            switch (currentFlippedViewIndex) {
                case IndexHolder.OWNER_INFO_PAGE:
                    title = getString(R.string.user);
                    toIndex = IndexHolder.OWNER_LIST_PAGE;
                    setView(toIndex, title, null, R.color.newColorPrimary, R.color.newColorPrimary);
                    break;
                case IndexHolder.FORM_PREVIEW_PAGE:
                    setView(IndexHolder.CATLIST_PAGE, "Sök...", null, R.color.newColorPrimary, R.color.newColorPrimary);
                    break;
                case IndexHolder.OWNER_LIST_PAGE:
                    Intent homeScreen = new Intent(Intent.ACTION_MAIN);
                    homeScreen.addCategory(Intent.CATEGORY_HOME);
                    homeScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(homeScreen);
                    break;
                default:
                    title = getHeaderTitle();
                    //title = (privileged == Type.ADMIN) ? getString(R.string.admin) : getString(R.string.my_cats);
                    setView(toIndex, title, null, R.color.newColorPrimary, R.color.newColorPrimary);
                    break;
            }
            */

        } else {
            Intent homeScreen = new Intent(Intent.ACTION_MAIN);
            homeScreen.addCategory(Intent.CATEGORY_HOME);
            homeScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(homeScreen);
        }
    }


    public Type getPrivileged() {
        return privileged;
    }

    private void initFormTitleMap() {
        for (int i = 0; i < dbTables.length; i++) {
            String formTitle = catformtitles[i];
            String table = dbTables[i];
            titlesToDbtables.put(formTitle, table);
            dbTablesToTitles.put(table, formTitle);
        }
    }


    /*
    private void initLockedFormMap() {
        for (String s : dbTables) {
            enabledForms.put(s, true);
        }
    }
    */

    public void setManage_catformConnection_catlist(boolean manage_catformConnection_catlist){
        this.manage_catformConnection_catlist = manage_catformConnection_catlist;
    }

    public boolean getManage_catformConnection_catlist(){
        return manage_catformConnection_catlist;
    }


    public int getCurrentFlippedViewIndex() {
        return currentFlippedViewIndex;
    }


    private void restoreSavedState(Bundle savedInstanceState) {
        currentFlippedViewIndex = savedInstanceState.getInt("CURRENT_VIEW_INDEX", 0);
        currentPreviewedForm = savedInstanceState.getInt("CURRENT_PREVIEWED_FORM", IndexHolder.GENERAL_FORM_INDEX);
        currentSettingButtonId = savedInstanceState.getInt("CURRENT_SETTING_BUTTON_ID", 0);
        currentHeaderTitle = savedInstanceState.getString("CURRENT_HEADER_TITLE", getHeaderTitle());
        currentHeaderSecondaryTitle = savedInstanceState.getString("CURRENT_HEADER_SECONDARY_TITLE", null);
        headerPrimaryColorID = savedInstanceState.getInt("CURRENT_HEADER_PRIMARY_COLOR_ID");
        headerPrimaryDarkColorID = savedInstanceState.getInt("CURRENT_HEADER_PRIMARY_DARK_COLOR_ID");
        currentCatId = savedInstanceState.getString("CURRENT_CAT_ID", null);
        //currentMonthIndex = savedInstanceState.getInt("CURRENT_VIEWED_MONTH", -1);
        //currentYearIndex = savedInstanceState.getInt("CURRENT_VIEWED_YEAR", -1);
        DEVICE_ORIENTATION = savedInstanceState.getInt("ORIENTATION_MODE", 0);

        if (privileged == Type.ADMIN || privileged == Type.PERSONAL) {
            changedCatPhoto = savedInstanceState.getBoolean("CHANGED_CAT_PHOTO", false);
            addNewCat = savedInstanceState.getBoolean("NEWCATFORM", false);
            addNewCatowner = savedInstanceState.getBoolean("NEWCATOWNER", false);
            restoreFormBundles(savedInstanceState);
        }
    }

    private void restoreFormBundles(Bundle savedInstanceState) {
        //Bundle medicalBundle = savedInstanceState.getBundle("MEDICAL_INFO");
        savedInstances.put("BASIC_FORM", savedInstanceState.getBundle("BASIC_INFO"));
        savedInstances.put("MEDICAL_FORM", savedInstanceState.getBundle("MEDICAL_INFO"));
        savedInstances.put("VACCIN_FORM", savedInstanceState.getBundle("VACCIN_INFO"));
        savedInstances.put("PARASITES_FORM", savedInstanceState.getBundle("PARASITES_INFO"));
        //savedInstances.put("DEWORMING_FORM", savedInstanceState.getBundle("DEWORMING_INFO"));
        //savedInstances.put("VERMIN_FORM", savedInstanceState.getBundle("VERMIN_INFO"));
        savedInstances.put("CLAW_FORM", savedInstanceState.getBundle("CLAW_INFO"));
        savedInstances.put("WEIGHT_FORM", savedInstanceState.getBundle("WEIGHT_INFO"));
        savedInstances.put("VETDOC_FORM", savedInstanceState.getBundle("VETDOC_INFO"));
        savedInstances.put("REGSKK_FORM", savedInstanceState.getBundle("REGSKK_INFO"));
        savedInstances.put("VET_FORM", savedInstanceState.getBundle("VET_INFO"));
        savedInstances.put("INSURANCE_FORM", savedInstanceState.getBundle("INSURANCE_INFO"));
        savedInstances.put("BACKGROUND_FORM", savedInstanceState.getBundle("BACKGROUND_INFO"));
        savedInstances.put("PREVIOUS_OWNERS_FORM", savedInstanceState.getBundle("PREVIOUS_OWNERS_INFO"));
        savedInstances.put("STATUS_FORM", savedInstanceState.getBundle("STATUS_INFO"));
    }

    public HashMap<String, Bundle> getSavedInstances() {
        return savedInstances;
    }


    public void removeCatFromChooser(int position){
        catListDialog.removeChoosenCat(position);
    }

    public void addCatInfoToPicker(String catInfo){
        catListDialog.addCatsToPicker(catInfo);
    }

    public void setCatListDialog(CatListDialog catListDialog){
        this.catListDialog = catListDialog;
    }

    public CatListDialog getCatListDialog(){
        return catListDialog;
    }

    public RelativeLayout getCatOwnerCatsInfoPage(){
        return catOwnerCatsInfoPage;
    }

    public RelativeLayout getCatOwnerSavePage(){
        return catOwnerSavePage;
    }


    private void initLayouts() {
        drawerLayout = (DrawerLayout) findViewById(R.id.homepage_fullscreen_content);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        appMenuLayout = (DrawerLayout) findViewById(R.id.homepage_fullscreen_content);
        appMenuLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        viewFlipper = (ViewFlipper) findViewById(R.id.homepage_viewflipper);
        //footerLayout = (RelativeLayout) findViewById(R.id.activity_footer_bar);
        //settingsPage = (RelativeLayout) getLayoutInflater().inflate(R.layout.settings_parent, null);
        settingsPage = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_settings_page, null);
        String versionText = "Version: ";

        try {
            versionText += getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            versionText += "Kunde inte läsa in";
        }
        ((TextView) settingsPage.findViewById(R.id.versionTextView)).setText(versionText);
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        String email = settings.getString("Email", null);
        String calendarEmail = settings.getString("CalendarEmail", null);
        boolean isReminderOn = settings.getBoolean("REMINDERS", true);
        ((TextView) settingsPage.findViewById(R.id.user_email_text)).setText(email);
        //((TextView) settingsPage.findViewById(R.id.user_calendar_email_text)).setText(calendarEmail);
        //((TextView)settingsPage.findViewById(R.id.notificationStatusText)).setText(isReminderOn ? "På" : "Av");

        //aboutUsPage = (RelativeLayout) getLayoutInflater().inflate(R.layout.aboutus, null);
        //catformHeader = (RelativeLayout) findViewById(R.id.catformH);
        //initCalendar();

        //addSettingLayouts();
        switch (privileged) {
            case ADMIN:
            case PERSONAL:
                homePage = (RelativeLayout) getLayoutInflater().inflate(R.layout.personal_homepage, null);
                //catlistPage = (RelativeLayout) getLayoutInflater().inflate(R.layout.list_and_search_view, null);
                catlistPage = (RelativeLayout) getLayoutInflater().inflate(R.layout.cat_management, null);
                catowner_management_page = (RelativeLayout) getLayoutInflater().inflate(R.layout.catowner_management, null);
                //catListView = (GridView) catlistPage.findViewById(R.id.list_gridviewCM);
                catListView = (ListView) catlistPage.findViewById(R.id.cat_listview);
                //catListView.setNumColumns(1);
                // searchCatBar = (SearchView) catlistPage.findViewById(R.id.search_list_barCM);
                catownerListView = (ListView) catowner_management_page.findViewById(R.id.catownerListCOM);
                //searchCatOwnerBar = (SearchView) catowner_management_page.findViewById(R.id.search_list_barCOM);
                //formScrollbar = (RelativeLayout) getLayoutInflater().inflate(R.layout.catformheader, null);
                //catformHeader.addView(formScrollbar);

                //catOwnerInfo = (RelativeLayout) getLayoutInflater().inflate(R.layout.catowner_infopage, null);
                catOwnerInfo = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_profile_page, null);

                //catOwnerInfo.findViewById(R.id.catowner_info_buttons).setVisibility(addNewCatowner ? View.GONE : View.VISIBLE);
                //catOwnerInfo.findViewById(R.id.catowners_cats).setVisibility(addNewCatowner ? View.VISIBLE : View.GONE);
                /*
                if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)){
                    catOwnerInfo.findViewById(R.id.call_catowner).setVisibility(View.GONE);
                    catOwnerInfo.findViewById(R.id.sms_catowner).setVisibility(View.GONE);
                }
                */

                catOwnerCatsInfoPage = (RelativeLayout) getLayoutInflater().inflate(R.layout.manage_catowner_cat_page, null);


                catOwnerSavePage = (RelativeLayout) getLayoutInflater().inflate(R.layout.save_catowner_page, null);


                //chosenCatownerCats = (ListView)catOwnerCatsInfoPage.findViewById(R.id.catlist);

                /**
                 * ----------------  2017-08-05   MIGRATING CODE FOR BETTER CODE USAGE - CATDIALOG AND ADDITIONAL CODE -----------------
                 */
                /*
                chosenCatFromListAdapter = new ChosenCatFromListAdapter(this, new ArrayList<String[]>(), true);
                confirmChosenCatFromListAdapter = new ChosenCatFromListAdapter(this, new ArrayList<String[]>(), false);
                //catListDialog = new CatListDialog(this, (ListView)catOwnerCatsInfoPage.findViewById(R.id.catlist));
                catListDialog = new CatListDialog(this, (ListView) catOwnerCatsInfoPage.findViewById(R.id.catlist), (ListView)catOwnerSavePage.findViewById(R.id.pickedCatList), chosenCatFromListAdapter, confirmChosenCatFromListAdapter);
                //catListDialog = new CatListDialog(this, (ListView)catOwnerInfo.findViewById(R.id.catlist));
                //catListDialog = new CatListDialog(this, chosenCatownerCats, chosenCatFromListAdapter);
                (catOwnerCatsInfoPage.findViewById(R.id.pickCat)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        catListDialog.viewPickerDialog();
                        HashMap<String, String> data = new HashMap<>();
                        data.put("misc_action", "names_id_catowner");
                        try {
                            networkThread.addDataToQueue(new DataPacket(data, Type.DOWNLOAD_MISC));
                            networkThread.nextInQueue();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                */

                /**
                 *  -----------------  END OF MIGRATING CODE ----------------------
                 */






                personalHomeView = (GridView) homePage.findViewById(R.id.personalHomepageGridview);
                homepageAdapter = new HomepageAdapter(this);

                homePageHandler = new HomePageHandler(this);

                homePageHandler.initPersonalHomepage();
                homePageHandler.getListHandler().clearList();
                homePageHandler.refreshSettingsPage("personel", email);
                homePageHandler.loadAboutUsPage();


                personalHomeView.setAdapter(homepageAdapter);

                //initAppMenu();
                initHamburgerMenu();

                viewFlipper.addView(homePage);
                //viewFlipper.addView(aboutUsPage);
                viewFlipper.addView(settingsPage);
                viewFlipper.addView(catlistPage);
                viewFlipper.addView(catowner_management_page);
                viewFlipper.addView(catOwnerInfo);
                viewFlipper.addView(catOwnerCatsInfoPage);

                if(privileged == Type.ADMIN){
                    viewFlipper.addView(catOwnerSavePage);
                    personalListPage = (RelativeLayout) getLayoutInflater().inflate(R.layout.personal_list_view, null);
                    //personalInfoPage = (RelativeLayout) getLayoutInflater().inflate(R.layout.personal_info_view, null);
                    personalInfoPage = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_profile_page, null);
                    personalHistoryPage = (RelativeLayout) getLayoutInflater().inflate(R.layout.personal_history_view, null);
                    personalSavePage = (RelativeLayout) getLayoutInflater().inflate(R.layout.save_personal_page, null);

                    personalListView = (ListView)personalListPage.findViewById(R.id.personalList);
                    personalListView.setAdapter(homePageHandler.getListHandler().getPersonelListAdapter());
                    viewFlipper.addView(personalListPage);
                    viewFlipper.addView(personalInfoPage);
                    viewFlipper.addView(personalHistoryPage);
                    viewFlipper.addView(personalSavePage);
                }


                //footerBar = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_footer, footerLayout, false);
                //footerLayout.addView(footerBar);
                controller = new FormController(HomePage.this);
                addCatInfoForms();
                if (currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.OWNER_LIST_PAGE, privileged)) {
                    homePageHandler.getListHandler().viewList(Type.CATOWNER);
                }else {
                    if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged)){
                        if(currentHeaderSecondaryTitle != null && !currentHeaderSecondaryTitle.isEmpty() && !currentHeaderSecondaryTitle.equals("null")) {
                            prepareDataToQueue(titlesToDbtables.get(currentHeaderSecondaryTitle));
                            controller.startDownloadForms();
                        }
                    }
                    setView(currentFlippedViewIndex, "NY KATT");
                    //setView(currentFlippedViewIndex, currentHeaderTitle, currentHeaderSecondaryTitle, headerPrimaryColorID, headerPrimaryDarkColorID);
                }

                break;
            case CATOWNER:
                controller = new FormController(HomePage.this);
                catformPreview = (RelativeLayout) getLayoutInflater().inflate(R.layout.catform_general_preview, null);
                homePage = (RelativeLayout) getLayoutInflater().inflate(R.layout.catowner_homepage, null);
                //catListView = (GridView) homePage.findViewById(R.id.catGridview);
                catListView = (ListView) homePage.findViewById(R.id.cat_listview);

                homePageHandler = new HomePageHandler(this, privileged);
                homePageHandler.getListHandler().clearList();
                homePageHandler.refreshSettingsPage("catowner", email);

                homePageHandler.loadAboutUsPage();

                //searchCatBar = (SearchView) homePage.findViewById(R.id.search_list_bar);

                //privateCatInfoView = (RelativeLayout)getLayoutInflater().inflate(R.layout.catowner_private_cat, null);
                privateCatInfoView = (RelativeLayout)getLayoutInflater().inflate(R.layout.catbasic_form_updated, null);
                privateCatInfoView.findViewById(R.id.catbasic_catname_autocomplete_text).setEnabled(false);
                privateCatInfoView.findViewById(R.id.catbasic_catcenter_name_holder).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catowner_editable_catname).setVisibility(View.VISIBLE);
                privateCatInfoView.findViewById(R.id.catbasic_catcenter_name_text).setVisibility(View.VISIBLE);
                privateCatInfoView.findViewById(R.id.catbasic_catname_autocomplete_text).setVisibility(View.GONE);

                /*
                privateCatInfoView.findViewById(R.id.catbasic_select_arrived_date).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catbasic_select_catid).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catbasic_select_moved_date).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catbasic_select_neuter_date).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catbasic_select_tatoo_date_and_text).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catbasic_select_chip_date).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catbasic_select_birthday_date).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catbasic_select_sex_type).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catbasic_select_race).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catbasic_select_color).setVisibility(View.GONE);
                privateCatInfoView.findViewById(R.id.catbasic_select_hair_type).setVisibility(View.GONE);
                */

                infoSelectedCat = (ImageView) privateCatInfoView.findViewById(R.id.info_cat_photo);
                ArrayList<String> items = new ArrayList<>();
                items.add("Kön");
                items.add("Hane");
                items.add("Hona");
                items.add("Vet ej");
                //initMyGeneralSpinner(items);

                viewFlipper.addView(homePage);
                //viewFlipper.addView(aboutUsPage);
                viewFlipper.addView(settingsPage);
                viewFlipper.addView(privateCatInfoView);
                viewFlipper.addView(catformPreview);

                //footerBar = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_footer, footerLayout, false);
                //footerLayout.addView(footerBar);

                setView(currentFlippedViewIndex, currentHeaderTitle);
                //setView(currentFlippedViewIndex, currentHeaderTitle, currentHeaderSecondaryTitle, headerPrimaryColorID, headerPrimaryDarkColorID);

                break;
            default:
                break;
        }



    }

    public void settingsLoaded(){
        homePageHandler.loadCatownerHomepage(getUserEmail());
    }

    public String getUserEmail(){
        return ((TextView) settingsPage.findViewById(R.id.user_email_text)).getText().toString();
    }

    public String[] getCatvetdoctables(){
        return catvetdoctables;
    }

    public String[] getDbTables(){
        return dbTables;
    }

    public int getHomepageHeight() {
        return homePage.getHeight();
    }


    private GridView personalHomeView;
    private HomepageAdapter homepageAdapter;

    public void setHomepageAdapter(HomepageAdapter homepageAdapter){
        this.homepageAdapter = homepageAdapter;
    }

    public HomepageAdapter getHomepageAdapter(){
        return homepageAdapter;
    }

    public FormController getController(){
        return controller;
    }

    public RelativeLayout getCatOwnerInfo(){
        return catOwnerInfo;
    }

    public RelativeLayout getSettingsPage(){ return settingsPage; }

    public ListView getCatownerListView(){
        return catownerListView;
    }

    public ListView getPersonalListView(){ return personalListView; }

    public RelativeLayout getPersonalInfo(){ return personalInfoPage; }

    public ListView getCatListView(){
        return catListView;
    }


    public ChosenCatFromListAdapter getChosenCatFromListAdapter(){
        //return chosenCatFromListAdapter;
        return homePageHandler.getCatownerManager().getChosenCatFromListAdapter();
    }

    public ChosenCatFromListAdapter getConfirmChosenCatFromListAdapter(){
        return confirmChosenCatFromListAdapter;
    }



    private void initHamburgerMenu() {
        hamburgerListCatForm = (ListView) findViewById(R.id.hamburgerListCatform);
        hamburgerListItemArrayList = new ArrayList<>();

        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[0], R.drawable.ic_description_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[1], R.drawable.ic_mood_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[2], R.drawable.ic_local_hospital_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[3], R.drawable.ic_colorize_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[4], R.drawable.ic_gesture_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[5], R.drawable.ic_pets_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[6], R.drawable.ic_group_black_50dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[7], R.drawable.ic_description_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[8], R.drawable.ic_description_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[9], R.drawable.ic_local_pharmacy_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[10], R.drawable.ic_description_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[11], R.drawable.ic_description_black_24dp));
        //hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[12], R.drawable.history_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[12], R.drawable.ic_info_black_24dp));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[13], R.drawable.ic_compare_arrows_black_24dp));

        /*
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[0], R.drawable.document_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[1], R.drawable.medical_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[2], R.drawable.vaccin_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[3], R.drawable.worm_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[4], R.drawable.bug_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[5], R.drawable.claws_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[6], R.drawable.weightscale_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[7], R.drawable.document_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[8], R.drawable.document_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[9], R.drawable.veterinarian_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[10], R.drawable.document_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[11], R.drawable.document_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[12], R.drawable.history_icon));
        hamburgerListItemArrayList.add(new HamburgerListItem(catformtitles[13], R.drawable.status_icon));
        */
        //hamburgerListAdapter = new HamburgerListAdapter(this, R.layout.hamburger_catform_item, hamburgerListItemArrayList);
        hamburgerListAdapter = new HamburgerListAdapter(this, R.layout.form_menu_list_item, hamburgerListItemArrayList);
        hamburgerListCatForm.setAdapter(hamburgerListAdapter);
        hamburgerListCatForm.setOnItemClickListener(new DrawerItemClickListener());

    }

    public void catownerCheckIn(View view){
        final Dialog dialog = new Dialog(this);
        RelativeLayout content = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_checkin_dialog, null);
        RadioGroup radioGroup = (RadioGroup) content.findViewById(R.id.checkin_options);
        final RadioButton normal = (RadioButton) radioGroup.getChildAt(0);
        final RadioButton deviation = (RadioButton) radioGroup.getChildAt(1);
        final EditText commentField = (EditText) content.findViewById(R.id.commentField);
        final TextView errorTextView = (TextView) content.findViewById(R.id.errorCommentMessage);
        deviation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                commentField.setEnabled(isChecked);
            }
        });

        content.findViewById(R.id.confirmCheckin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(deviation.isChecked()){
                    if(commentField.getText().toString().isEmpty()){
                        errorTextView.setText("Kommentarsfältet får inte vara tomt");
                    }
                    else{
                        sendCheckin(commentField.getText().toString());
                        dialog.dismiss();
                    }
                }
                else if(normal.isChecked()){
                    sendCheckin("");
                    dialog.dismiss();
                }

            }
        });

        content.findViewById(R.id.abortButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(content);
        dialog.show();
    }

    private void sendCheckin(String note){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", getUserEmail());
            jsonObject.put("note", note);
            String blub = Base64.encodeToString(jsonObject.toString().getBytes(), Base64.NO_WRAP);
            HashMap<String, String> data = new HashMap<>();
            data.put("blub", blub);
            data.put("misc_action", "checkin");
            prepareAndSendData(data, Type.UPLOAD_MISC);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void setCurrentCatId(String id) {
        currentCatId = id;
    }

    public String getCurrentCatId() {
        return currentCatId;
    }

    public void setCurrentCatImgSrc(String currentCatImgSrc){
        this.currentCatImgSrc = currentCatImgSrc;
    }

    public String getCurrentCatImgSrc(){
        return currentCatImgSrc;
    }

    private void setInsuranceFieldListener(){
        final EditText insuranceNumberFieldFrom = (EditText)catInfoInsurance.findViewById(R.id.catinsurance_old_number);
        final EditText insuranceNumberFieldTo = (EditText)catInfoInsurance.findViewById(R.id.catinsurance_new_number);
        final LinearLayout parentFrom = (LinearLayout)insuranceNumberFieldFrom.getParent();
        final LinearLayout parentTo = (LinearLayout)insuranceNumberFieldTo.getParent();
        parentFrom.getChildAt(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insuranceNumberFieldFrom.setText("");
            }
        });
        parentTo.getChildAt(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insuranceNumberFieldTo.setText("");
            }
        });
        insuranceNumberFieldFrom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 1){
                    parentFrom.getChildAt(1).setVisibility(View.VISIBLE);
                }
                else if(s.length() == 0){
                    parentFrom.getChildAt(1).setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        insuranceNumberFieldTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 1){
                    parentTo.getChildAt(1).setVisibility(View.VISIBLE);
                }
                else if(s.length() == 0){
                    parentTo.getChildAt(1).setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * ----------------------------------------------------- CATFORMS -------------------------------------------------------------------------
     */
    private void addCatInfoForms() {

        catformPreview = (RelativeLayout) getLayoutInflater().inflate(R.layout.catform_general_preview, null);
        viewFlipper.addView(catformPreview);

        catInfoBasic = (RelativeLayout) getLayoutInflater().inflate(R.layout.catbasic_form_updated, null);

        catInfoTemperament = (RelativeLayout) getLayoutInflater().inflate(R.layout.catform_temperament, null);

        catInfoMediciation = (RelativeLayout) getLayoutInflater().inflate(R.layout.catmedication_form, null);
        catInfoVaccin = (RelativeLayout) getLayoutInflater().inflate(R.layout.catvaccin_form, null);
        //catInfoDeworming = (RelativeLayout) getLayoutInflater().inflate(R.layout.catform_deworming, null);
        //catInfoVermin = (RelativeLayout) getLayoutInflater().inflate(R.layout.catvermin_form, null);
        catInfoParasites = (RelativeLayout) getLayoutInflater().inflate(R.layout.catparasite_form, null);
        catInfoClaws = (RelativeLayout) getLayoutInflater().inflate(R.layout.catclaw_form, null);
        catInfoWeight = (RelativeLayout) getLayoutInflater().inflate(R.layout.catweight_form, null);
        catInfoVetDoc = (RelativeLayout) getLayoutInflater().inflate(R.layout.catvet_reg, null);
        catInfoRegSKK = (RelativeLayout) getLayoutInflater().inflate(R.layout.reg_skk_form, null);
        catInfoVet = (RelativeLayout) getLayoutInflater().inflate(R.layout.catvet_form, null);
        catInfoInsurance = (RelativeLayout) getLayoutInflater().inflate(R.layout.catinsurance_form, null);
        catInfoBackground = (RelativeLayout) getLayoutInflater().inflate(R.layout.catbackground_form, null);
        //catInfoPreviousOwner = (RelativeLayout) getLayoutInflater().inflate(R.layout.catprevious_owner_form, null);
        //catInfoHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.cathistory_form, null);
        catInfoStatus = (RelativeLayout) getLayoutInflater().inflate(R.layout.catstatus_form, null);

        catInfoCatowners = (RelativeLayout) getLayoutInflater().inflate(R.layout.catform_catowner, null);
        catInfoConnection = (RelativeLayout) getLayoutInflater().inflate(R.layout.catform_connection, null);
        //catInfoConnection = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_catconnection_page, null);

        setInsuranceFieldListener();


        /*
        LinearLayout catInfoBasicFrameLayout = (LinearLayout) catInfoBasic.findViewById(R.id.catbasic_framelayout);
        if (!isPortraitMode) {
            LinearLayout upperLayout = (LinearLayout) catInfoBasic.findViewById(R.id.catbasic_upperLayout);
            LinearLayout mainLayout = (LinearLayout) catInfoBasic.findViewById(R.id.mainform);
            catInfoBasicFrameLayout.setOrientation(LinearLayout.HORIZONTAL);
            catInfoBasicFrameLayout.removeAllViews();
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
            upperLayout.setLayoutParams(params);
            mainLayout.setLayoutParams(params);
            catInfoBasicFrameLayout.addView(upperLayout);
            catInfoBasicFrameLayout.addView(mainLayout);
        }
        */
        infoSelectedCat = (ImageView) catInfoBasic.findViewById(R.id.info_cat_photo);
        autoCompleteTextView = (AutoCompleteTextView) catInfoBasic.findViewById(R.id.catbasic_catname_autocomplete_text);

        autoCompleteAdapter = new AutoCompleteAdapter(this, new ArrayList<String>());
        autoCompleteTextView.setAdapter(autoCompleteAdapter);

        autoCompleteTextView.addTextChangedListener(new MyAutocompleteTextWatcher());

        /*
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String subString = s.toString();
                if (!subString.equals("") && subString.length() > 0 && receivedNames) {
                    HashMap<String, String> data = new HashMap<>();
                    data.put("form_action", "DOWNLOAD_NAMES");
                    data.put("blub", Base64.encodeToString(subString.getBytes(), Base64.NO_WRAP));
                    try {

                        networkThread.addDataToQueue(new DataPacket(data, Type.DOWNLOAD_FORM));
                        networkThread.nextInQueue();
                        receivedNames = false;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        */

        //final EditText catIdField = (EditText) catInfoBasic.findViewById(R.id.catbasic_cathome_id_text);
        /*
        catIdField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.equals("")) {
                    String[] parts = s.toString().split("-");
                    if (parts[0].length() == 2 && parts.length > 1 && parts[1].length() == 3 && parts[0].matches("[1-3][0-9]") && parts[1].matches("00[0-9]|0[1-9][0-9]|[1-9][0-9][0-9]")) {
                        Date dateYear = new Date();
                        String cYear = new SimpleDateFormat("yy").format(dateYear);
                        if (!cYear.equals(parts[0])) {
                            catIdField.setText(cYear + "-" + parts[1]);
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

*/



        viewFlipper.addView(catInfoBasic);
        viewFlipper.addView(catInfoTemperament);
        viewFlipper.addView(catInfoMediciation);
        viewFlipper.addView(catInfoVaccin);
        viewFlipper.addView(catInfoParasites);
        //viewFlipper.addView(catInfoDeworming);
        //viewFlipper.addView(catInfoVermin);
        viewFlipper.addView(catInfoClaws);
        viewFlipper.addView(catInfoWeight);
        viewFlipper.addView(catInfoVetDoc);
        viewFlipper.addView(catInfoRegSKK);
        viewFlipper.addView(catInfoVet);
        viewFlipper.addView(catInfoInsurance);
        viewFlipper.addView(catInfoBackground);
       // viewFlipper.addView(catInfoPreviousOwner);
        //viewFlipper.addView(catInfoHistory);
        viewFlipper.addView(catInfoStatus);
        viewFlipper.addView(catInfoConnection);

        viewFlipper.addView(catInfoCatowners);
        //initSpinners();
        initCatVetRegSubforms();
        initRegSSK();
        new RestoreFormInstanceHandler(this, savedInstances);
    }



    public String getCurrentUserName() {
        return currentUserName;
    }

    public void setCurrentUserName(String currentUserName) {
        this.currentUserName = currentUserName;
    }

    public class MyAutocompleteTextWatcher implements TextWatcher{

        public MyAutocompleteTextWatcher(){

        }

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * are about to be replaced by new text with length <code>after</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param count
         * @param after
         */
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * have just replaced old text that had length <code>before</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param before
         * @param count
         */
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String subString = s.toString();
            if (autoCompleteTextView.hasFocus() && !subString.equals("") && subString.length() > 0 && receivedNames) {
                HashMap<String, String> data = new HashMap<>();
                data.put("form_action", "DOWNLOAD_NAMES");
                data.put("blub", Base64.encodeToString(subString.getBytes(), Base64.NO_WRAP));
                try {

                    networkThread.addDataToQueue(new DataPacket(data, Type.DOWNLOAD_FORM));
                    networkThread.nextInQueue();
                    receivedNames = false;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * This method is called to notify you that, somewhere within
         * <code>s</code>, the text has been changed.
         * It is legitimate to make further changes to <code>s</code> from
         * this callback, but be careful not to get yourself into an infinite
         * loop, because any changes you make will cause this method to be
         * called again recursively.
         * (You are not told where the change took place because other
         * afterTextChanged() methods may already have made other changes
         * and invalidated the offsets.  But if you need to know here,
         * you can use {@link Spannable#setSpan} in {@link #onTextChanged}
         * to mark your place and then look up from here where the span
         * ended up.
         *
         * @param s
         */
        @Override
        public void afterTextChanged(Editable s) {

        }
    }


    private void initCatVetRegSubforms() {
        initSpinnerRecord();
        initCatVetSpinner();

    }

    private void initCatVetSpinner() {

        Spinner spinner = (Spinner) catInfoVetDoc.findViewById(R.id.catvet_reg_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.catvet_items, R.layout.dropdown_textview_1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        vetRegInfo = new VetRegInfo(this);
        vetRegInfo.setParent(catInfoVetDoc.findViewById(R.id.catvetreg_container));
        spinner.setOnItemSelectedListener(vetRegInfo);
    }


    public VetRegInfo getVetRegInfo(){
        return vetRegInfo;
    }

    private void initRegSSK(){
        regSKKInfo = new RegSKKInfo(this);
    }

    public RegSKKInfo getRegSKKInfo(){
        return regSKKInfo;
    }


    public void showDropdown(View view) {
        Spinner spinner = (Spinner) catInfoVetDoc.findViewById(R.id.catvet_reg_spinner);
        spinner.performClick();
    }

    public void refreshSpinnerClick() {
        Spinner spinner = (Spinner) catInfoVetDoc.findViewById(R.id.catvet_reg_spinner);
        spinner.setSelection(0);
    }

    public void initSpinnerRecord() {
        String[] parents = getResources().getStringArray(R.array.catvet_items);
        currentCatvetregParent = parents[0];
        for (int i = 0; i < parents.length; i++) {

            catvetreg_comments.put(parents[i], "");

            HashMap<String, Boolean> options = new HashMap<>();
            String[] optionsArray;
            switch (i) {
                case 0:
                    optionsArray = getResources().getStringArray(R.array.generalstate);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 1:
                    optionsArray = getResources().getStringArray(R.array.Lynne);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 2:
                    optionsArray = getResources().getStringArray(R.array.Skin_Paws);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 3:
                    optionsArray = getResources().getStringArray(R.array.Lymf);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 4:
                    optionsArray = getResources().getStringArray(R.array.Eyes);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 5:
                    optionsArray = getResources().getStringArray(R.array.Ears);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 6:
                    optionsArray = getResources().getStringArray(R.array.Mouth_teeth);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 7:
                    optionsArray = getResources().getStringArray(R.array.abdomen);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 8:
                    optionsArray = getResources().getStringArray(R.array.circulation_organ);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 9:
                    optionsArray = getResources().getStringArray(R.array.respiration_organ);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 10:
                    optionsArray = getResources().getStringArray(R.array.Outer_genitalias);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                case 11:
                    optionsArray = getResources().getStringArray(R.array.locomotive_organs);
                    for (String option : optionsArray) options.put(option, false);
                    break;
                default:
                    break;
            }

            spinnerRecord.put(parents[i], options);
        }
    }

    public void updateCurrentCatvetregParent(String currentCatvetregParent) {
        this.currentCatvetregParent = currentCatvetregParent;
    }

    public String getCurrentCatvetregParent() {
        return currentCatvetregParent;
    }


    public HashMap<String, HashMap<String, Boolean>> getRecord() {
        return spinnerRecord;
    }

    public void updateRecord(String parent, String option, Boolean isChecked) {
        HashMap<String, Boolean> options = spinnerRecord.get(parent);
        options.put(option, isChecked);
        spinnerRecord.put(parent, options);
    }

    public HashMap<String, String> getCatvetreg_comments() {
        return catvetreg_comments;
    }

    public void updateCatvetregComment(String parent, String comment) {
        catvetreg_comments.put(parent, comment);
    }


    public void errorLoadingData(){
        homePageHandler.closeStandardDialog();
        new BottomDialog(this).popup("Kunde inte ladda information, försök igen senare");
    }


    public void showCategories(View view){
        //appMenuLayout.openDrawer(Gravity.LEFT);
        //((TextView)findViewById(R.id.add_object_text)).setText("NY KATT");
        findViewById(R.id.add_object).setVisibility(View.VISIBLE);
        setView(IndexHolder.authenticateIndex(IndexHolder.HOMEPAGE_INDEX, privileged), "NY KATT");
    }

    public void showPersonelList(View view){
        setView(IndexHolder.authenticateIndex(IndexHolder.PERSONAL_LIST_PAGE, privileged), "NY PERSONAL");
        //((TextView)findViewById(R.id.add_object_text)).setText("NY PERSONAL");
        findViewById(R.id.add_object).setVisibility(View.VISIBLE);
        homePageHandler.getListHandler().viewList(Type.PERSONAL);
    }

    public void showCatownerList(View view){

        setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_LIST_PAGE, privileged), "NY KATTÄGARE");
        findViewById(R.id.add_object).setVisibility(View.VISIBLE);
        //((TextView)findViewById(R.id.add_object_text)).setText("NY KATTÄGARE");
        homePageHandler.getListHandler().viewList(Type.CATOWNER);
    }

    public void showSettingsPage(View view){
        findViewById(R.id.add_object).setVisibility(View.GONE);
        setView(IndexHolder.authenticateIndex(IndexHolder.SETTINGSPAGE_INDEX, privileged), "Inställningar");
    }


    public void showFormMenu(View view) {
        drawerLayout.openDrawer(Gravity.RIGHT);
    }

    public void handleFormOptionLayout(View view){


        findViewById(R.id.sub_action_bar).setVisibility(Variable.getFormOptions() ? View.GONE : View.VISIBLE);
        Variable.setFormOptions(!Variable.getFormOptions());
    }



    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectForm(position);
            autoCompleteTextView.setAdapter((AutoCompleteAdapter)null);
            //showPreview(position);
        }
    }



    private void showForm(int position, String title){
        //clearForms();

        if(privileged != Type.CATOWNER) {
            autoCompleteTextView.setText(currentViewedCatname, false);
            autoCompleteTextView.setAdapter(autoCompleteAdapter);
        }
        else{
            if(currentViewedCatname != null)
                ((TextView)privateCatInfoView.findViewById(R.id.catbasic_catcenter_name_text)).setText(currentViewedCatname);
        }
        setView(position+IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged), catformtitles[position]);
        //setView(position + IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged), currentViewedCatname, title, R.color.newColorPrimary, R.color.newColorPrimary);
    }

    public void resetFormIndex(){
        hamburgerListAdapter.setSelectedItem(0);
        hamburgerListAdapter.notifyDataSetInvalidated();
    }

    private void selectForm(int position) {
        System.out.println("SELECTED: "+position);
        currentPreviewedForm = position;
        hamburgerListAdapter.setSelectedItem(position);
        hamburgerListAdapter.notifyDataSetInvalidated();
        selectedForm = catformtitles[position];
        currentHeaderSecondaryTitle = selectedForm;
        drawerLayout.closeDrawer(Gravity.RIGHT);

        new LoadPreviewHandler(this).clearPreview();
        //

        if (!addNewCat) {
            setView(IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged), catformtitles[position]);
            //setView(IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged), currentViewedCatname, catformtitles[position], R.color.newColorPrimary, R.color.newColorPrimary);
            String formTable = titlesToDbtables.get(catformtitles[position]);
            if (formTable.equals("catform_vetdoc")) {
                loadCatVetForms();
            }
            else if(formTable.equals("catform_history")){
                HashMap<String, String> data = new HashMap<>();
                data.put("history_action", "get_events");
                String blub = Base64.encodeToString(getCurrentCatId().getBytes(), Base64.NO_WRAP);
                data.put("blub", blub);
                try{
                    networkThread.addDataToQueue(new DataPacket(data, Type.DOWNLOAD_HISTORY));
                    networkThread.nextInQueue();
                } catch (InterruptedException ioe){
                    ioe.printStackTrace();
                }
                return;
            }
            else{
                prepareDataToQueue(formTable);
                /*
                HashMap<String, String> data = new HashMap<>();
                String blub = formTable + ":" + getCurrentCatId();
                String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
                data.put("form_action", "DOWNLOAD");
                data.put("blub", encoded_blub);
                controller.addToDownloadQueue(data);
                */
            }
            controller.startDownloadForms();
            homePageHandler.showStandardDialog("Hämtar formulär...");
            //showLoadingDialog("Hämtar formulär...");
        }
        else{
            setView(position+IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged), currentViewedCatname);
            //setView(position + IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged), currentViewedCatname, catformtitles[position], R.color.newColorPrimary, R.color.newColorPrimary);
        }

    }

    private void loadCatVetForms(){
        setLoadCatVetItems(true);


        for (String docTable : catvetdoctables) {
            prepareDataToQueue(docTable);
        }
    }

    public void prepareDataToQueue(String formTable){
        HashMap<String, String> data = new HashMap<>();
        String blub = formTable + ":" + getCurrentCatId();
        String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        if(formTable.equals("catform_connection"))
            data.put("form_action", "GET_FAMILY_CONNECTIONS");
        else
            data.put("form_action", "DOWNLOAD");
        data.put("blub", encoded_blub);
        controller.addToDownloadQueue(data);
    }



    public void formDownloadComplete(){
        if(!getLoadPreview()) {
            if(getLoadPdfGenerator()){
                Log.i("HOMEPAGE", "THIS SHOULD ONLY APPEAR WHEN PREPARING PDF OR MAIL");
                homePageHandler.closeShareLoadingDialog();
                setLoadPreview(true);
            }

        }
        else{
            setLoadCatVetItems(false);
            /*
            if( currentHeaderSecondaryTitle != null && currentHeaderSecondaryTitle.equals("Reg.SKK") && ((LinearLayout)findViewById(R.id.contentHolder)).getChildCount() > 0){
                headerLayout.findViewById(R.id.newCatform).setEnabled(false);
                ((TextView)headerLayout.findViewById(R.id.newCatformIcon)).setTextColor(Color.parseColor("#cccccc"));
                ((TextView)headerLayout.findViewById(R.id.newCatformText)).setTextColor(Color.parseColor("#cccccc"));
            }
            else{
               headerLayout.findViewById(R.id.newCatform).setEnabled(true);
                ((TextView)headerLayout.findViewById(R.id.newCatformIcon)).setTextColor(Color.parseColor("#ffffff"));
                ((TextView)headerLayout.findViewById(R.id.newCatformText)).setTextColor(Color.parseColor("#ffffff"));
            }
            */
            //Log.i("HOMEPAGE", "FORM DOWNLOADED COMPLETE --------------------");
            homePageHandler.closeStandardDialog();
        }
    }


    public void setLoadPreview(boolean loadPreview){
        this.loadPreview = loadPreview;
    }

    public boolean getLoadPreview(){
        return loadPreview;
    }

    public void setCurrentUserId(String currentUserId){
        Log.i("HOMEPAGE", "USER ID SET: "+currentUserId);
        this.currentUserId = currentUserId;
    }

    public String getCurrentUserId(){
        Log.i("HOMEPAGE", "USER ID GET: "+currentUserId);
        return currentUserId;
    }



    public void handleFormActions(View view) {
        controller = new FormController(this);
        controller.handleAction(view);
    }


    public void handleSettingActions(View view) {
        switch (view.getId()) {
            case R.id.user_name:
                openSettingsDialogForInput(view, "Ändra namn");
                break;
            case R.id.user_email:
            case R.id.user_calendar_email:
                openSettingsDialogForInput(view, "Ändra email");
                break;
            case R.id.user_cell_settings:
                openSettingsDialogForInput(view, "Ändra mobil");
                break;
            case R.id.user_address_settings:
                openSettingsDialogForInput(view, "Ändra adress");
                break;
            case R.id.user_password_settings:
                openSettingsDialogForInput(view, "Ändra lösenord");
                break;
            case R.id.logout:

                break;
            default:
                break;
        }
    }

    public void handleHomepageActions(View view) {
        /*
        switch (view.getId()) {
            case R.id.cat_management:
                setView(IndexHolder.authenticateIndex(IndexHolder.HOMEPAGE_INDEX, privileged), getHeaderTitle(), null, R.color.newColorPrimary, R.color.newColorPrimary);
                break;
            case R.id.catowner_management:

                homePageHandler.getListHandler().viewList(Type.CATOWNER);
                //loadList("Sök kattägare ...", Type.CATOWNER);
                break;
            case R.id.personal_management:
                //setView(IndexHolder.PERSONAL_LIST_PAGE, getHeaderTitle(), null, R.color.newColorPrimary, R.color.newColorPrimary);
                homePageHandler.getListHandler().viewList(Type.PERSONAL);
                break;
            default:
                break;
        }
        */
    }


    public void sendLoginExtApp(String surname, String mail, String token, String table_name){
        if(table_name.equals("catowner")) {
            ArrayList<String> catownerNames = homePageHandler.getListHandler().getCatownerListAdapter().getCatOwnerNames();
            catownerNames.add(surname);
            ArrayList<String> catownerEmails = homePageHandler.getListHandler().getCatownerListAdapter().getCatOwnerEmails();
            catownerEmails.add(mail);
            ArrayList<String> catownerStatuses = homePageHandler.getListHandler().getCatownerListAdapter().getCatOwnerStatuses();
            catownerStatuses.add("Inväntar bekräftelse");
            homePageHandler.getListHandler().getCatownerListAdapter().setCatOwnerNames(catownerNames);
            homePageHandler.getListHandler().getCatownerListAdapter().setCatOwnerEmails(catownerEmails);
            homePageHandler.getListHandler().getCatownerListAdapter().setCatOwnerStatuses(catownerStatuses);
            homePageHandler.getListHandler().getCatownerListAdapter().notifyDataSetChanged();
        }else{
            ArrayList<String> personelNames = homePageHandler.getListHandler().getPersonelListAdapter().getPersonelNames();
            personelNames.add(surname);
            ArrayList<String> personelEmails = homePageHandler.getListHandler().getPersonelListAdapter().getPersonelEmails();
            personelEmails.add(mail);
            ArrayList<String> personelStatuses = homePageHandler.getListHandler().getPersonelListAdapter().getPersonelStatuses();
            personelStatuses.add("Inväntar bekräftelse");
            homePageHandler.getListHandler().getPersonelListAdapter().setPersonelNames(personelNames);
            homePageHandler.getListHandler().getPersonelListAdapter().setPersonelEmails(personelEmails);
            homePageHandler.getListHandler().getPersonelListAdapter().setPersonelStatuses(personelStatuses);
            homePageHandler.getListHandler().getPersonelListAdapter().notifyDataSetChanged();
        }
        //int index = table_name.equals("catowner") ? IndexHolder.OWNER_LIST_PAGE : IndexHolder.PERSONAL_LIST_PAGE;
        //setView(IndexHolder.authenticateIndex(index, privileged), "NY " + user_type.toUpperCase());

        //setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_LIST_PAGE, privileged), "Kattägare", null, R.color.newColorPrimary, R.color.newColorPrimary);
        //new BottomDialog(HomePage.this).popup("Login skickat till " + user_type);

        String centerCell = "Tel: 0176-161 91, Mail: info@kattcenter.se";
        final String user_type = (table_name.equals("catowner")) ? "kattägare" : "personal";
        String loginText = "Välkommen till Norrtälje Kattcenter som registread " + user_type +". \n\n\n\nHär kommer ditt login\n\n" +
                token + "\n\nDu har 14 dagar på dig att aktivera kontot i appen.\n" +
                "Var god kontakta Norrtälje Kattcenter på "+centerCell+" om aktivering sker efter " +
                "denna tid eller om du har några frågor.";
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{ mail});
        //email.putExtra(Intent.EXTRA_CC, new String[]{ to});
        //email.putExtra(Intent.EXTRA_BCC, new String[]{to});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Kontoregistrering");
        emailIntent.putExtra(Intent.EXTRA_TEXT, loginText);
        emailIntent.setType("message/rfc822");
        try {
            startActivity(Intent.createChooser(emailIntent, "Skicka email..."));
            finish();
            Log.i("HOMEPAGE", "Email SENT TO user");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(HomePage.this, "Ingen email klient installerad.", Toast.LENGTH_SHORT).show();
        }
    }


    public void sendLogin(final String surname, final String mail, final String token, final String table_name){
        final GMailSender gMailSender = new GMailSender(chosenAccount, password);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String centerCell = "Tel: 0176-161 91, Mail: info@kattcenter.se";
                    final String user_type = (table_name.equals("catowner")) ? "kattägare" : "personal";
                    String loginText = "Välkommen till Norrtälje Kattcenter som registread " + user_type +". \n\n\n\nHär kommer ditt login\n\n" +
                            token + "\n\nDu har 14 dagar på dig att aktivera kontot i appen.\n" +
                            "Var god kontakta Norrtälje Kattcenter på "+centerCell+" om aktivering sker efter " +
                            "denna tid eller om du har några frågor.";
                    gMailSender.sendMail("Test", loginText, chosenAccount, mail);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if(table_name.equals("catowner")) {
                                ArrayList<String> catownerNames = homePageHandler.getListHandler().getCatownerListAdapter().getCatOwnerNames();
                                catownerNames.add(surname);
                                ArrayList<String> catownerEmails = homePageHandler.getListHandler().getCatownerListAdapter().getCatOwnerEmails();
                                catownerEmails.add(mail);
                                ArrayList<String> catownerStatuses = homePageHandler.getListHandler().getCatownerListAdapter().getCatOwnerStatuses();
                                catownerStatuses.add("Inväntar bekräftelse");
                                homePageHandler.getListHandler().getCatownerListAdapter().setCatOwnerNames(catownerNames);
                                homePageHandler.getListHandler().getCatownerListAdapter().setCatOwnerEmails(catownerEmails);
                                homePageHandler.getListHandler().getCatownerListAdapter().setCatOwnerStatuses(catownerStatuses);
                                homePageHandler.getListHandler().getCatownerListAdapter().notifyDataSetChanged();
                            }else{
                                ArrayList<String> personelNames = homePageHandler.getListHandler().getPersonelListAdapter().getPersonelNames();
                                personelNames.add(surname);
                                ArrayList<String> personelEmails = homePageHandler.getListHandler().getPersonelListAdapter().getPersonelEmails();
                                personelEmails.add(mail);
                                ArrayList<String> personelStatuses = homePageHandler.getListHandler().getPersonelListAdapter().getPersonelStatuses();
                                personelStatuses.add("Inväntar bekräftelse");
                                homePageHandler.getListHandler().getPersonelListAdapter().setPersonelNames(personelNames);
                                homePageHandler.getListHandler().getPersonelListAdapter().setPersonelEmails(personelEmails);
                                homePageHandler.getListHandler().getPersonelListAdapter().setPersonelStatuses(personelStatuses);
                                homePageHandler.getListHandler().getPersonelListAdapter().notifyDataSetChanged();
                            }
                            int index = table_name.equals("catowner") ? IndexHolder.OWNER_LIST_PAGE : IndexHolder.PERSONAL_LIST_PAGE;
                            setView(IndexHolder.authenticateIndex(index, privileged), "NY " + user_type.toUpperCase());
                            //setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_LIST_PAGE, privileged), "Kattägare", null, R.color.newColorPrimary, R.color.newColorPrimary);
                            new BottomDialog(HomePage.this).popup("Login skickat till " + user_type);
                                //catListDialog.confirmCats();

                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Kunde inte skicka mail");
                }
            }
        }).start();
    }

    private String generateToken(){
        SecureRandom secureRandom = new SecureRandom();
        byte[] bytes = new byte[8];
        secureRandom.nextBytes(bytes);
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }



    public void handleUserOptions(View view){
        String cell, mail;
        switch (view.getId()){
            case R.id.call_user:
                cell = ((TextView)((LinearLayout)((LinearLayout)view.getParent()).getChildAt(1)).getChildAt(0)).getText().toString();
                userInterractionDialog("Ring", cell, 1);
                break;
            case R.id.sms_user:
                cell = ((TextView)((LinearLayout)((LinearLayout)view.getParent()).getChildAt(1)).getChildAt(0)).getText().toString();
                userInterractionDialog("Sms", cell, 2);
                break;
            case R.id.mail_user:
                mail = ((TextView)((LinearLayout)((LinearLayout)view.getParent()).getChildAt(1)).getChildAt(0)).getText().toString();
                userInterractionDialog("Maila", mail, 3);
                break;
            case R.id.map_user:
                userInterractionDialog(null, null, 4);
                break;
            case R.id.change_number_option:

                break;
            case R.id.change_mail_option:

                break;
            case R.id.change_address_option:

                break;
            default:break;
        }
    }

    public void userInterractionDialog(String text, final String info, int mode){
        final Dialog dialog = new Dialog(this);
        LinearLayout linearLayout = null;
        if(mode == 1) {
            linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.new_user_call_dialog, null);
            linearLayout.findViewById(R.id.call_option).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+info.trim()));
                    startActivity(callIntent);
                }
            });
            linearLayout.findViewById(R.id.change_number_option).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    String mail = (currentFlippedViewIndex == IndexHolder.OWNER_INFO_PAGE) ? homePageHandler.getCatownerManager().getMail() : homePageHandler.getPersonelManager().getMail();
                    String table = (currentFlippedViewIndex == IndexHolder.OWNER_INFO_PAGE) ? "catowner" : "personel";
                    changeUserInfoDialog(1, table, mail);
                }
            });

        }
        else if(mode == 2) {
            linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.new_user_sms_dialog, null);
            linearLayout.findViewById(R.id.sms_option).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setData(Uri.parse("sms:"+info.trim()));
                    startActivity(smsIntent);
                }
            });
            linearLayout.findViewById(R.id.change_number_option).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    String mail = (currentFlippedViewIndex == IndexHolder.OWNER_INFO_PAGE) ? homePageHandler.getCatownerManager().getMail() : homePageHandler.getPersonelManager().getMail();
                    String table = (currentFlippedViewIndex == IndexHolder.OWNER_INFO_PAGE) ? "catowner" : "personel";
                    changeUserInfoDialog(1, table, mail);
                }
            });
        }
        else if(mode == 3) {
            linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.new_user_mail_dialog, null);
            linearLayout.findViewById(R.id.mail_option).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent mailIntent = new Intent(Intent.ACTION_VIEW);
                    mailIntent.setData(Uri.parse("mailto:"+info.trim()));
                    startActivity(mailIntent);
                }
            });
            linearLayout.findViewById(R.id.change_mail_option).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    String mail = (currentFlippedViewIndex == IndexHolder.OWNER_INFO_PAGE) ? homePageHandler.getCatownerManager().getMail() : homePageHandler.getPersonelManager().getMail();
                    String table = (currentFlippedViewIndex == IndexHolder.OWNER_INFO_PAGE) ? "catowner" : "personel";
                    changeUserInfoDialog(2, table, mail);
                }
            });
        }
        else if(mode == 4){
            linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.new_user_address_dialog, null);
            linearLayout.findViewById(R.id.map_option).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    String address = (currentFlippedViewIndex == IndexHolder.OWNER_INFO_PAGE) ? homePageHandler.getCatownerManager().getAddressInfo() : homePageHandler.getPersonelManager().getAddressInfo();
                    String map = "http://maps.google.co.in/maps?q=" + address;
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                    startActivity(mapIntent);
                }
            });
            linearLayout.findViewById(R.id.change_address_option).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    String mail = (currentFlippedViewIndex == IndexHolder.OWNER_INFO_PAGE) ? homePageHandler.getCatownerManager().getMail() : homePageHandler.getPersonelManager().getMail();
                    String table = (currentFlippedViewIndex == IndexHolder.OWNER_INFO_PAGE) ? "catowner" : "personel";
                    changeUserInfoDialog(3, table, mail);
                }
            });
        }
        if(mode != 4)
            ((TextView)linearLayout.findViewById(R.id.userInfoText)).setText(text+" "+info);
        linearLayout.findViewById(R.id.abort_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(linearLayout);

        dialog.show();
    }

    private void changeUserInfoDialog(int mode, final String table, final String user_mail){
        final Dialog dialog = new Dialog(this);
        LinearLayout linearLayout = null;
        switch (mode){
            case 1:
                final LinearLayout linearLayout1 = (LinearLayout) getLayoutInflater().inflate(R.layout.new_number_field, null);
                linearLayout1.findViewById(R.id.complete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            dialog.dismiss();
                            updateUserNumber(table, user_mail, ((TextView)linearLayout1.findViewById(R.id.user_new_phone_number)).getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                linearLayout = linearLayout1;
                break;
            case 2:
                final LinearLayout linearLayout2 = (LinearLayout) getLayoutInflater().inflate(R.layout.new_mail_field, null);
                linearLayout2.findViewById(R.id.complete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            dialog.dismiss();
                            updateUserEmail(table, user_mail, ((TextView)linearLayout2.findViewById(R.id.user_new_email_address)).getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                linearLayout = linearLayout2;
                break;
            case 3:
                final LinearLayout linearLayout3 = (LinearLayout) getLayoutInflater().inflate(R.layout.new_address_field, null);
                linearLayout3.findViewById(R.id.complete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            dialog.dismiss();
                            updateUserAddress(table, user_mail,
                                    ((TextView)linearLayout3.findViewById(R.id.user_new_street)).getText().toString(),
                                    ((TextView)linearLayout3.findViewById(R.id.user_new_zipcode)).getText().toString(),
                                    ((TextView)linearLayout3.findViewById(R.id.user_new_city)).getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                linearLayout = linearLayout3;
                break;
            default:break;
        }

        linearLayout.findViewById(R.id.abort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(linearLayout);
        dialog.show();
    }

    public void sendLogin(final Dialog dialog, final String table_name){
        RelativeLayout formContent = (RelativeLayout) dialog.findViewById(R.id.new_user_dialog_content);
        final String newUserSSN = Base64.encodeToString(((EditText) formContent.findViewById(R.id.new_user_ssn_text)).getText().toString().getBytes(), Base64.NO_WRAP);
        final String newUserSurName = ((EditText) formContent.findViewById(R.id.new_user_surname_text)).getText().toString();
        final String newUserLastName = ((EditText) formContent.findViewById(R.id.new_user_lastname_text)).getText().toString();
        final String newUserAddress = ((EditText) formContent.findViewById(R.id.new_user_address_text)).getText().toString();
        final String newUserZipCode = ((EditText) formContent.findViewById(R.id.new_user_zipcode_text)).getText().toString();
        final String newUserCity = ((EditText) formContent.findViewById(R.id.new_user_city_text)).getText().toString();
        final String newUserCell = ((EditText) formContent.findViewById(R.id.new_user_cell_text)).getText().toString();
        final String newUserMail = ((EditText) formContent.findViewById(R.id.new_user_mail_text)).getText().toString();
        if (newUserSSN != null && newUserSurName != null && newUserLastName != null && newUserAddress != null &&
                newUserZipCode != null && newUserCity != null && newUserCell != null && newUserMail != null &&
                !newUserSSN.isEmpty() && !newUserSurName.isEmpty() && !newUserLastName.isEmpty() &&
                !newUserAddress.isEmpty() && !newUserZipCode.isEmpty() && !newUserCity.isEmpty() && !newUserCell.isEmpty() &&
                !newUserMail.isEmpty()) {
            /*
            ArrayList<String[]> chosenCats = catListDialog.getChosenCats();
            JSONObject catsJsonObject = new JSONObject();
            for(String[] catInfo: chosenCats){
                Log.i("HOMEPAGE", "Chosen cats: "+catInfo[0] + " " + catInfo[1]);
                try {
                    catsJsonObject.put(catInfo[0], catInfo[1]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            final String catInfoJsonEncoded = Base64.encodeToString(catsJsonObject.toString().getBytes(), Base64.NO_WRAP);
            */
            final ArrayList<String> mailList = new ArrayList<>();
            Account[] accounts = AccountManager.get(this).getAccounts();
            for (Account account : accounts) {
                //System.out.println("Account: "+account.name + "  type: "+account.type);
                if (account.type.equals("com.google")) {
                    mailList.add(account.name);
                }
            }




            final Dialog chooseMailDialog = new Dialog(this);
            chooseMailDialog.setTitle("Konto");
            final RelativeLayout mailDialogContent = (RelativeLayout) getLayoutInflater().inflate(R.layout.mail_picker_dialog, null);
            ListView mailListView = (ListView) mailDialogContent.findViewById(R.id.emailList);
            final EditText passwordField = (EditText) mailDialogContent.findViewById(R.id.password_field);
            final TextView errorStatus = (TextView) mailDialogContent.findViewById(R.id.errorSendStatus);
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.mail_chooser_item, mailList);
            mailListView.setAdapter(arrayAdapter);
            mailListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    System.out.println(mailList.get(position));
                    chosenAccount = mailList.get(position);
                    String token = generateToken();
                    HashMap<String, String> data = new HashMap<>();
                    String blub;
                    blub = newUserSSN+":"+newUserSurName+":"+newUserLastName+":"+newUserAddress+":"+
                            newUserZipCode+":"+newUserCity+":"+newUserCell+":"+newUserMail+":"+token+":"+privileged+":"+table_name;

                    blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
                    data.put("logintype", "register");
                    data.put("blub", blub);
                    try {
                        networkThread.addDataToQueue(new DataPacket(data, Type.LOGIN));
                        networkThread.nextInQueue();
                        chooseMailDialog.dismiss();
                        dialog.dismiss();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    /*
                    if(table_name.equals("catowner")) {
                        ArrayList<String> catownerNames = homePageHandler.getListHandler().getCatownerListAdapter().getCatOwnerNames();
                        catownerNames.add(surname);
                        ArrayList<String> catownerEmails = homePageHandler.getListHandler().getCatownerListAdapter().getCatOwnerEmails();
                        catownerEmails.add(mail);
                        ArrayList<String> catownerStatuses = homePageHandler.getListHandler().getCatownerListAdapter().getCatOwnerStatuses();
                        catownerStatuses.add("Inväntar bekräftelse");
                        homePageHandler.getListHandler().getCatownerListAdapter().setCatOwnerNames(catownerNames);
                        homePageHandler.getListHandler().getCatownerListAdapter().setCatOwnerEmails(catownerEmails);
                        homePageHandler.getListHandler().getCatownerListAdapter().setCatOwnerStatuses(catownerStatuses);
                        homePageHandler.getListHandler().getCatownerListAdapter().notifyDataSetChanged();
                    }else{
                        ArrayList<String> personelNames = homePageHandler.getListHandler().getPersonelListAdapter().getPersonelNames();
                        personelNames.add(surname);
                        ArrayList<String> personelEmails = homePageHandler.getListHandler().getPersonelListAdapter().getPersonelEmails();
                        personelEmails.add(mail);
                        ArrayList<String> personelStatuses = homePageHandler.getListHandler().getPersonelListAdapter().getPersonelStatuses();
                        personelStatuses.add("Inväntar bekräftelse");
                        homePageHandler.getListHandler().getPersonelListAdapter().setPersonelNames(personelNames);
                        homePageHandler.getListHandler().getPersonelListAdapter().setPersonelEmails(personelEmails);
                        homePageHandler.getListHandler().getPersonelListAdapter().setPersonelStatuses(personelStatuses);
                        homePageHandler.getListHandler().getPersonelListAdapter().notifyDataSetChanged();
                    }

                    int index = table_name.equals("catowner") ? IndexHolder.OWNER_LIST_PAGE : IndexHolder.PERSONAL_LIST_PAGE;
                    setView(IndexHolder.authenticateIndex(index, privileged), "NY " + user_type.toUpperCase());
                    //setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_LIST_PAGE, privileged), "Kattägare", null, R.color.newColorPrimary, R.color.newColorPrimary);
                    new BottomDialog(HomePage.this).popup("Login skickat till " + user_type);
                    sendLoginExtApp(newUserMail, token, "catowner");
                    */

                    /*
                    (mailDialogContent.findViewById(R.id.passwordFrame)).setVisibility(View.VISIBLE);
                    (mailDialogContent.findViewById(R.id.emailList)).setVisibility(View.GONE);

                    passwordField.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(passwordField, InputMethodManager.SHOW_IMPLICIT);
                    ((TextView) mailDialogContent.findViewById(R.id.pickedEmail)).setText(chosenAccount);
                    */
                }
            });
            Button confirmSend = (Button) mailDialogContent.findViewById(R.id.confirmSendLogin);
            confirmSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    password = passwordField.getText().toString();
                    if (password != null && !password.equals("")) {
                        //String currentTimeString = new SimpleDateFormat("MM_HH:mm").format(new Date());
                        //String token = Base64.encodeToString(currentTimeString.getBytes(), Base64.NO_WRAP);
                        String token = generateToken();
                        HashMap<String, String> data = new HashMap<>();
                        String blub;
                        blub = newUserSSN+":"+newUserSurName+":"+newUserLastName+":"+newUserAddress+":"+
                                newUserZipCode+":"+newUserCity+":"+newUserCell+":"+newUserMail+":"+token+":"+privileged+":"+table_name;
                        /*
                        if(catInfoJsonEncoded != null)
                            blub = newCatOwnerSSN+":"+newCatOwnerSurName+":"+newCatOwnerLastName+":"+newCatOwnerAddress+":"+
                                    newCatOwnerZipCode+":"+newCatOwnerCity+":"+newCatOwnerCell+":"+newCatOwnerMail+":"+token+":"+privileged+":"+catInfoJsonEncoded;
                        else
                            blub = newCatOwnerSSN+":"+newCatOwnerSurName+":"+newCatOwnerLastName+":"+newCatOwnerAddress+":"+
                                    newCatOwnerZipCode+":"+newCatOwnerCity+":"+newCatOwnerCell+":"+newCatOwnerMail+":"+token+":"+privileged;
                                    */
                        blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
                        data.put("logintype", "register");
                        data.put("blub", blub);
                        try {
                            networkThread.addDataToQueue(new DataPacket(data, Type.LOGIN));
                            networkThread.nextInQueue();
                            chooseMailDialog.dismiss();
                            dialog.dismiss();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        errorStatus.setText("X");
                    }
                }
            });

            chooseMailDialog.setContentView(mailDialogContent);
            chooseMailDialog.show();
        } else {
            Dialog errorDialog = new Dialog(this);
            errorDialog.setTitle("Fel");
            TextView textView = new TextView(this);
            textView.setText("Alla fält måste fyllas i ");
            errorDialog.setContentView(textView);
            errorDialog.show();
        }

    }

    /**
     * Function to handle buttons on catowner's info page
     * @param view
     */
    public void handleCatownerOptions(View view) {

        switch (view.getId()) {
            case R.id.sendLogin:
                final String newCatOwnerSSN = Base64.encodeToString(((EditText) catOwnerInfo.findViewById(R.id.catowner_ssn_text)).getText().toString().getBytes(), Base64.NO_WRAP);
                final String newCatOwnerSurName = ((EditText) catOwnerInfo.findViewById(R.id.catowner_surname_text)).getText().toString();
                final String newCatOwnerLastName = ((EditText) catOwnerInfo.findViewById(R.id.catowner_lastname_text)).getText().toString();
                final String newCatOwnerAddress = ((EditText) catOwnerInfo.findViewById(R.id.catowner_address_text)).getText().toString();
                final String newCatOwnerZipCode = ((EditText) catOwnerInfo.findViewById(R.id.catowner_zipcode_text)).getText().toString();
                final String newCatOwnerCity = ((EditText) catOwnerInfo.findViewById(R.id.catowner_city_text)).getText().toString();
                final String newCatOwnerCell = ((EditText) catOwnerInfo.findViewById(R.id.catowner_cell_text)).getText().toString();
                final String newCatOwnerMail = ((EditText) catOwnerInfo.findViewById(R.id.catowner_mail_text)).getText().toString();
                if (newCatOwnerSSN != null && newCatOwnerSurName != null && newCatOwnerLastName != null && newCatOwnerAddress != null &&
                        newCatOwnerZipCode != null && newCatOwnerCity != null && newCatOwnerCell != null && newCatOwnerMail != null &&
                        !newCatOwnerSSN.isEmpty() && !newCatOwnerSurName.isEmpty() && !newCatOwnerLastName.isEmpty() &&
                        !newCatOwnerAddress.isEmpty() && !newCatOwnerZipCode.isEmpty() && !newCatOwnerCity.isEmpty() && !newCatOwnerCell.isEmpty() &&
                        !newCatOwnerMail.isEmpty()) {

                    ArrayList<String[]> chosenCats = catListDialog.getChosenCats();
                    JSONObject catsJsonObject = new JSONObject();
                    for(String[] catInfo: chosenCats){
                        Log.i("HOMEPAGE", "Chosen cats: "+catInfo[0] + " " + catInfo[1]);
                        try {
                            catsJsonObject.put(catInfo[0], catInfo[1]);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    final String catInfoJsonEncoded = Base64.encodeToString(catsJsonObject.toString().getBytes(), Base64.NO_WRAP);

                    final ArrayList<String> mailList = new ArrayList<>();
                    Account[] accounts = AccountManager.get(this).getAccounts();
                    for (Account account : accounts) {
                        //System.out.println("Account: "+account.name + "  type: "+account.type);
                        if (account.type.equals("com.google")) {
                            mailList.add(account.name);
                        }
                    }

                    final Dialog chooseMailDialog = new Dialog(this);
                    chooseMailDialog.setTitle("Konto");
                    final RelativeLayout content = (RelativeLayout) getLayoutInflater().inflate(R.layout.mail_picker_dialog, null);
                    ListView mailListView = (ListView) content.findViewById(R.id.emailList);
                    final EditText passwordField = (EditText) content.findViewById(R.id.password_field);
                    final TextView errorStatus = (TextView) content.findViewById(R.id.errorSendStatus);
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.mail_chooser_item, mailList);
                    mailListView.setAdapter(arrayAdapter);
                    mailListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            //System.out.println(mailList.get(position));
                            chosenAccount = mailList.get(position);
                            (content.findViewById(R.id.passwordFrame)).setVisibility(View.VISIBLE);
                            (content.findViewById(R.id.emailList)).setVisibility(View.GONE);

                            passwordField.requestFocus();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(passwordField, InputMethodManager.SHOW_IMPLICIT);
                            ((TextView) content.findViewById(R.id.pickedEmail)).setText(chosenAccount);
                        }
                    });
                    Button confirmSend = (Button) content.findViewById(R.id.confirmSendLogin);
                    confirmSend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            password = passwordField.getText().toString();
                            if (password != null && !password.equals("")) {
                                String currentTimeString = new SimpleDateFormat("MM_HH:mm").format(new Date());
                                String token = Base64.encodeToString(currentTimeString.getBytes(), Base64.NO_WRAP);

                                HashMap<String, String> data = new HashMap<>();
                                String blub;
                                if(catInfoJsonEncoded != null)
                                    blub = newCatOwnerSSN+":"+newCatOwnerSurName+":"+newCatOwnerLastName+":"+newCatOwnerAddress+":"+
                                        newCatOwnerZipCode+":"+newCatOwnerCity+":"+newCatOwnerCell+":"+newCatOwnerMail+":"+token+":"+privileged+":"+catInfoJsonEncoded;
                                else
                                    blub = newCatOwnerSSN+":"+newCatOwnerSurName+":"+newCatOwnerLastName+":"+newCatOwnerAddress+":"+
                                            newCatOwnerZipCode+":"+newCatOwnerCity+":"+newCatOwnerCell+":"+newCatOwnerMail+":"+token+":"+privileged;
                                blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
                                data.put("logintype", "register");
                                data.put("blub", blub);
                                try {
                                    networkThread.addDataToQueue(new DataPacket(data, Type.LOGIN));
                                    networkThread.nextInQueue();
                                    chooseMailDialog.dismiss();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                errorStatus.setText("X");
                            }
                        }
                    });

                    chooseMailDialog.setContentView(content);
                    chooseMailDialog.show();
                } else {
                    Dialog dialog = new Dialog(this);
                    dialog.setTitle("Fel");
                    TextView textView = new TextView(this);
                    textView.setText("Alla fält måste fyllas i ");
                    dialog.setContentView(textView);
                    dialog.show();
                }



                /*
                Gmail gmail = new Gmail(this);
                SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", 0);
                String user = sharedPreferences.getString("CalendarEmail", null);
                if(user != null)
                    gmail.sendMail("Test", "Hej, här kommer ditt login", user, gmail.getToken(), newCatOwnerMail);
                    */
                break;
            case R.id.catowner_basic_info_holder:
                setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_INFO_PAGE, privileged), "Kattägare");
                //setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_INFO_PAGE, privileged), "Kattägare", "Hantera katter", R.color.newColorPrimary, R.color.newColorPrimary);
                break;
            case R.id.manageCatownerCatsHolder:
                setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_CATS_INFO_PAGE, privileged), "Kattägare");
                //setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_CATS_INFO_PAGE, privileged), "Kattägare", "Hantera katter", R.color.newColorPrimary, R.color.newColorPrimary);
                break;
            case R.id.pickCat:
                /*
                catListDialog.viewPickerDialog();
                HashMap<String, String> data = new HashMap<>();
                data.put("misc_action", "names_id_catowner");
                prepareAndSendData(data, Type.DOWNLOAD_MISC);
                */
                new BottomDialog(this).popup("Inaktiverad! Koppling mellan katt och kattägare sker vid vald katt");
                break;

            case R.id.send_reminder_holder:
                if(privileged == Type.CATOWNER){
                    handleFormActions(view);
                    /*
                    Dialog dialog = new Dialog(this);
                    RelativeLayout content = (RelativeLayout) getLayoutInflater().inflate(R.layout.catowner_reminder_activities_dialog, null);

                    dialog.setContentView(content);
                    dialog.show();
                    */
                }
                else{
                    final String phoneNumber = ((EditText) catOwnerInfo.findViewById(R.id.catowner_cell_text)).getText().toString();
                    final String reminder = "Hej, detta är en påminnelse från Norrtälje Kattcenter.\n"+
                            "Var vänlig logga in i appen för att kontrollera dina uppgifter";
                    //smsSender.sendSms(phoneNumber, reminder);

                    final Dialog dialog = new Dialog(this);
                    dialog.setTitle("Påminnelse till: "+phoneNumber);
                    LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.general_confirm_dialog, null);
                    //final EditText editText = (EditText) relativeLayout.findViewById(R.id.phoneNumber);
                    Button confirm = (Button) linearLayout.findViewById(R.id.confirm);
                    confirm.setText("Skicka");
                    confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //String phoneNumber = editText.getText().toString();
                            if (phoneNumber != null && !phoneNumber.equals(""))
                                smsSender.sendSms(phoneNumber, reminder);
                            dialog.dismiss();
                        }
                    });
                    ((Button) linearLayout.findViewById(R.id.abort)).setText("Avbryt");
                    (linearLayout.findViewById(R.id.abort)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.setContentView(linearLayout);
                    dialog.show();
                }




                break;
            case R.id.call_catowner_holder:
                String newCatOwnerName1 = ((EditText) catOwnerInfo.findViewById(R.id.catowner_surname_text)).getText().toString();
                final String newCatOwnerCell1 = ((EditText) catOwnerInfo.findViewById(R.id.catowner_cell_text)).getText().toString();
                String dialogTitle = "Ringa " + newCatOwnerName1 + " (" + newCatOwnerCell1 + ") ?";
                final Dialog confirmCallCatOwnerDialog = new Dialog(this);
                confirmCallCatOwnerDialog.setTitle(dialogTitle);
                LinearLayout linearLayout1 = (LinearLayout) getLayoutInflater().inflate(R.layout.general_confirm_dialog, null);
                Button confirm1 = (Button) linearLayout1.findViewById(R.id.confirm);
                confirm1.setText("Ring");
                confirm1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + newCatOwnerCell1));
                        if (ActivityCompat.checkSelfPermission(HomePage.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(callIntent);
                    }
                });
                Button abort1 = (Button) linearLayout1.findViewById(R.id.abort);
                abort1.setText("Avbryt");
                abort1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmCallCatOwnerDialog.dismiss();
                    }
                });
                confirmCallCatOwnerDialog.setContentView(linearLayout1);
                confirmCallCatOwnerDialog.show();
                break;
            case R.id.sms_catowner_holder:
                final String newCatOwnerCell2 = ((EditText) catOwnerInfo.findViewById(R.id.catowner_cell_text)).getText().toString();
                System.out.println("SKICKADE ETT SMS TILL KATTÄGAREN");
                final Dialog smsFormDialog = new Dialog(this);
                smsFormDialog.setTitle("SMS");
                final RelativeLayout smsForm = (RelativeLayout) getLayoutInflater().inflate(R.layout.comment_form, null);
                Button abort = (Button) smsForm.findViewById(R.id.abortButton);
                abort.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        smsFormDialog.dismiss();
                    }
                });
                Button send = (Button) smsForm.findViewById(R.id.addCommentButton);
                send.setText("Skicka");
                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //SKICKA SMS
                        String sms = ((EditText)smsForm.findViewById(R.id.commentField)).getText().toString();
                        if(sms != null && !sms.isEmpty()){
                            smsSender.sendSms(newCatOwnerCell2, sms);
                            smsFormDialog.dismiss();
                        }
                        else{
                            ((TextView)smsForm.findViewById(R.id.errorCommentMessage)).setVisibility(View.VISIBLE);
                        }

                    }
                });
                smsFormDialog.setContentView(smsForm);
                smsFormDialog.show();
                break;
            case R.id.deleteHolder:
                if(viewFlipper.getDisplayedChild() == IndexHolder.OWNER_INFO_PAGE)
                    unregisterCatowner();
                else if(viewFlipper.getDisplayedChild() == IndexHolder.PERSONAL_INFO_PAGE)
                    unregisterPersonel();
                break;
            case R.id.saveCatownerHolder:
                if(privileged == Type.CATOWNER){
                    confirmSave();
                }else{
                    String currentSurName = ((EditText)catOwnerInfo.findViewById(R.id.catowner_surname_text)).getText().toString();
                    String currentLastName = ((EditText)catOwnerInfo.findViewById(R.id.catowner_lastname_text)).getText().toString();
                    ((TextView)catOwnerSavePage.findViewById(R.id.saveCatownerName)).setText(currentSurName + " " + currentLastName);
                    setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_SAVE_PAGE, privileged), "");
                    //setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_SAVE_PAGE, privileged), null, null, R.color.newColorPrimary, R.color.newColorPrimary);
                }

                break;
            default:break;

        }


    }

    public void handleReminderCatowner(View view){
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        switch(view.getId()){
            case R.id.selected_date_catowner_cat:
            case R.id.reminder_date_catowner_cat:
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new MyDateSelecter(view), year, month, day);
                datePickerDialog.show();
                break;
            case R.id.saveCatownerCatActionReminder:

                break;
            default:break;
        }
    }

    private class MyDateSelecter implements DatePickerDialog.OnDateSetListener{

        private View myView;
        public MyDateSelecter(View myView){
            this.myView = myView;
        }
        /**
         * @param view       the picker associated with the dialog
         * @param year       the selected year
         * @param month      the selected month (0-11 for compatibility with
         *                   {@link Calendar#MONTH})
         * @param dayOfMonth th selected day of the month (1-31, depending on
         */
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            month++;
            String monthText = (month < 10) ? "0"+month : ""+month;
            String dayText = (dayOfMonth < 10) ? "0"+dayOfMonth : ""+dayOfMonth;
            ((TextView)myView).setText(year+"-"+monthText+"-"+dayText);
        }
    }

    public void handlePersonalOptions(View view){
        switch (view.getId()){
            case R.id.personal_basic_info:
                setView(IndexHolder.authenticateIndex(IndexHolder.PERSONAL_INFO_PAGE, privileged), "");
                //setView(IndexHolder.authenticateIndex(IndexHolder.PERSONAL_INFO_PAGE, privileged), null, null, R.color.newColorPrimary, R.color.newColorPrimary);
                break;
            case R.id.history_personal:
                setView(IndexHolder.authenticateIndex(IndexHolder.PERSONAL_HISTORY_PAGE, privileged), "");
                //setView(IndexHolder.authenticateIndex(IndexHolder.PERSONAL_HISTORY_PAGE, privileged), null, null, R.color.newColorPrimary, R.color.newColorPrimary);
                break;
            case R.id.call_personal:

                break;
            case R.id.sms_personal:

                break;
            case R.id.deletePersonal:

                break;
            case R.id.savePersonal:
                setView(IndexHolder.authenticateIndex(IndexHolder.PERSONAL_SAVE_PAGE, privileged), "");
                //setView(IndexHolder.authenticateIndex(IndexHolder.PERSONAL_SAVE_PAGE, privileged), null, null, R.color.newColorPrimary, R.color.newColorPrimary);
                break;
            case R.id.sendLoginToPersonal:

                break;
            default:break;
        }
    }

    private void unregisterPersonel(){
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Avregistrera personal");
        LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.new_delete_user_dialog, null);
        RelativeLayout confirm = (RelativeLayout) linearLayout.findViewById(R.id.delete_option);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog1 = new Dialog(HomePage.this);
                LinearLayout linearLayout1 = (LinearLayout) getLayoutInflater().inflate(R.layout.new_delete_user_confirm_dialog, null);
                linearLayout1.findViewById(R.id.delete_option).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mail = getHomePageHandler().getPersonelManager().getMail();
                        HashMap<String, String> data = new HashMap<>();
                        data.put("misc_action", "delete_personel");
                        data.put("blub", Base64.encodeToString(mail.getBytes(), Base64.NO_WRAP));
                        prepareAndSendData(data, Type.UPLOAD_MISC);
                        dialog.dismiss();
                        dialog1.dismiss();
                        homePageHandler.getListHandler().viewList(Type.PERSONAL);
                        setView(IndexHolder.PERSONAL_LIST_PAGE, "Personal");

                    }
                });
                linearLayout1.findViewById(R.id.abort_option).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        dialog.dismiss();
                    }
                });


                dialog1.setContentView(linearLayout1);
                dialog1.show();

            }
        });
        linearLayout.findViewById(R.id.abort_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(linearLayout);
        dialog.show();
    }

    private void unregisterCatowner(){
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Avregistrera kattägare");
        LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.new_delete_user_dialog, null);
        RelativeLayout confirm = (RelativeLayout) linearLayout.findViewById(R.id.delete_option);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog1 = new Dialog(HomePage.this);
                LinearLayout linearLayout1 = (LinearLayout) getLayoutInflater().inflate(R.layout.new_delete_user_confirm_dialog, null);
                linearLayout1.findViewById(R.id.delete_option).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mail = getHomePageHandler().getCatownerManager().getMail();
                        HashMap<String, String> data = new HashMap<>();
                        data.put("misc_action", "delete_catowner");
                        data.put("blub", Base64.encodeToString(mail.getBytes(), Base64.NO_WRAP));
                        prepareAndSendData(data, Type.UPLOAD_MISC);
                        dialog.dismiss();
                        dialog1.dismiss();
                        homePageHandler.getListHandler().viewList(Type.CATOWNER);
                        setView(IndexHolder.OWNER_LIST_PAGE, "Kattägare");

                    }
                });
                linearLayout1.findViewById(R.id.abort_option).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        dialog.dismiss();
                    }
                });


                dialog1.setContentView(linearLayout1);
                dialog1.show();

            }
        });
        linearLayout.findViewById(R.id.abort_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(linearLayout);
        dialog.show();

    }

    public void prepareAndSendData(HashMap<String, String> data, Type type){
        try {
            addDatapacketToNetworkThread(new DataPacket(data, type));
            signalQueueToSend();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void prepareData(HashMap<String, String> data, Type type){
        try {
            addDatapacketToNetworkThread(new DataPacket(data, type));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void openSettingsDialogForInput(final View view, String title){
        final Dialog dialog = new Dialog(this);
        dialog.setTitle(title);
        RelativeLayout relativeLayout = null;
        switch (view.getId()){
            case R.id.user_name:
                relativeLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.dual_input_fields, null);
                ((EditText)relativeLayout.findViewById(R.id.input_text1)).setText(((TextView) view.findViewById(R.id.user_surname_text)).getText().toString());
                ((EditText)relativeLayout.findViewById(R.id.input_text2)).setText(((TextView) view.findViewById(R.id.user_lastname_text)).getText().toString());
                break;
            case R.id.user_email:
                relativeLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.simple_input_dialog_field, null);
                ((EditText)relativeLayout.findViewById(R.id.input_text)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                ((EditText)relativeLayout.findViewById(R.id.input_text)).setText(((TextView) view.findViewById(R.id.user_email_text)).getText().toString());
                break;
            case R.id.user_calendar_email:
                relativeLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.simple_input_dialog_field, null);
                ((EditText)relativeLayout.findViewById(R.id.input_text)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                ((EditText)relativeLayout.findViewById(R.id.input_text)).setText(((TextView) view.findViewById(R.id.user_calendar_email_text)).getText().toString());
                break;
            case R.id.user_cell_settings:
                relativeLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.simple_input_dialog_field, null);
                ((EditText)relativeLayout.findViewById(R.id.input_text)).setInputType(InputType.TYPE_CLASS_NUMBER);
                ((EditText)relativeLayout.findViewById(R.id.input_text)).setText(((TextView) view.findViewById(R.id.user_cell_text)).getText().toString());
                break;
            case R.id.user_address_settings:
                relativeLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.triple_input_fields, null);
                ((EditText)relativeLayout.findViewById(R.id.input_text1)).setText(((TextView) view.findViewById(R.id.user_address_text)).getText().toString());
                ((EditText)relativeLayout.findViewById(R.id.input_text2)).setText(((TextView) view.findViewById(R.id.user_zipcode_text)).getText().toString());
                ((EditText)relativeLayout.findViewById(R.id.input_text3)).setText(((TextView) view.findViewById(R.id.user_city_text)).getText().toString());
                break;
            case R.id.user_password_settings:
                relativeLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.text_input_fields, null);
                ((TextView)relativeLayout.findViewById(R.id.input_field_text_message)).setText("Skriv nytt lösenord");
                ((EditText)relativeLayout.findViewById(R.id.input_text)).setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
            default:break;
        }
        if(relativeLayout != null) {
            final RelativeLayout content = relativeLayout;
            //final EditText editText = (EditText) relativeLayout.findViewById(R.id.input_text);
            Button confirmButton = (Button) relativeLayout.findViewById(R.id.confirm);
            confirmButton.setText("Uppdatera");
            Button abortButton = (Button) relativeLayout.findViewById(R.id.abort);
            abortButton.setText("Avbryt");
            final SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", 0);
            final SharedPreferences.Editor editor = sharedPreferences.edit();


            abortButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            confirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject jsonObject = new JSONObject();
                    boolean upload_to_server = true;
                    String type = null;
                    try {
                        switch (view.getId()) {
                            case R.id.user_email:
                                String updated_email =((EditText) content.findViewById(R.id.input_text)).getText().toString();
                                ((TextView) view.findViewById(R.id.user_email_text)).setText(updated_email);
                                editor.putString("Email", ((EditText) content.findViewById(R.id.input_text)).getText().toString());
                                editor.commit();
                                jsonObject.put("email", updated_email);
                                type = "email";
                                break;
                            case R.id.user_calendar_email:
                                upload_to_server = false;
                                ((TextView) view.findViewById(R.id.user_calendar_email_text)).setText(((EditText) content.findViewById(R.id.input_text)).getText().toString());
                                editor.putString("CalendarEmail", ((EditText) content.findViewById(R.id.input_text)).getText().toString());
                                editor.commit();
                                break;
                            case R.id.user_name:
                                String updated_surname = ((EditText) content.findViewById(R.id.input_text1)).getText().toString();
                                String updated_lastname = ((EditText) content.findViewById(R.id.input_text2)).getText().toString();
                                ((TextView) view.findViewById(R.id.user_surname_text)).setText(updated_surname);
                                ((TextView) view.findViewById(R.id.user_lastname_text)).setText(updated_lastname);
                                jsonObject.put("surname", updated_surname);
                                jsonObject.put("lastname", updated_lastname);
                                type = "name";

                                break;
                            case R.id.user_cell_settings:
                                String updated_cell = ((EditText) content.findViewById(R.id.input_text)).getText().toString();
                                ((TextView) view.findViewById(R.id.user_cell_text)).setText(updated_cell);
                                jsonObject.put("cell", updated_cell);
                                type = "cell";
                                break;
                            case R.id.user_address_settings:
                                String updated_address = ((EditText) content.findViewById(R.id.input_text1)).getText().toString();
                                String updated_zipcode = ((EditText) content.findViewById(R.id.input_text2)).getText().toString();
                                String updated_city = ((EditText) content.findViewById(R.id.input_text3)).getText().toString();
                                ((TextView) view.findViewById(R.id.user_address_text)).setText(updated_address);
                                ((TextView) view.findViewById(R.id.user_zipcode_text)).setText(updated_zipcode);
                                ((TextView) view.findViewById(R.id.user_city_text)).setText(updated_city);
                                jsonObject.put("address", updated_address);
                                jsonObject.put("zipcode", updated_zipcode);
                                jsonObject.put("city", updated_city);
                                type = "address";
                                break;
                            case R.id.user_password_settings:
                                String updated_password = ((EditText) content.findViewById(R.id.input_text)).getText().toString();
                                updated_password = Base64.encodeToString(updated_password.getBytes(), Base64.NO_WRAP);
                                editor.putString("Password", password);
                                editor.commit();
                                jsonObject.put("password", updated_password);
                                type = "password";
                            default:
                                break;
                        }
                    } catch (JSONException jsone){
                        jsone.printStackTrace();
                    }
                    dialog.dismiss();
                    if(upload_to_server) {
                        HashMap<String, String> data = new HashMap<>();
                        String blub;

                        String encoded_json = Base64.encodeToString(jsonObject.toString().getBytes(), Base64.NO_WRAP);
                        String user_id = getCurrentUserId();
                        if(user_id != null) {
                            if (privileged == Type.CATOWNER) {
                                blub = "catowner:" + type + ":" + user_id + ":" + encoded_json;
                            } else {
                                blub = "personel:" + type + ":" + user_id + ":" + encoded_json;
                            }
                            blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
                            data.put("misc_action", "update_user_info");
                            data.put("blub", blub);
                            try {
                                addDatapacketToNetworkThread(new DataPacket(data, Type.UPLOAD_MISC));
                                signalQueueToSend();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        else{
                            new BottomDialog(HomePage.this).popup("Databasfel (Inget ID)");
                        }
                    }

                }
            });

            dialog.setContentView(relativeLayout);
            dialog.show();
        }

    }


    public void updateUserNumber(String table, String user_email, String number) throws JSONException {
        String blub;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("cell", number);
        String encoded_json = Base64.encodeToString(jsonObject.toString().getBytes(), Base64.NO_WRAP);
        blub = table+":" + "cell" + ":" + user_email + ":" + encoded_json;
        /*
        if (privileged == Type.CATOWNER) {
            blub = "catowner:" + "address" + ":" + user_email + ":" + encoded_json;
        } else {
            blub = "personel:" + "address" + ":" + user_email + ":" + encoded_json;
        }
        */
        blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("misc_action", "update_user_info");
        data.put("blub", blub);
        prepareAndSendData(data, Type.UPLOAD_MISC);
        if(table.equals("catowner")){
            homePageHandler.getCatownerManager().updateNumber(number);
        }else{
            homePageHandler.getPersonelManager().updateNumber(number);
        }
    }

    public void updateUserEmail(String table, String user_email, String new_email) throws JSONException {
        String blub;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", new_email);
        String encoded_json = Base64.encodeToString(jsonObject.toString().getBytes(), Base64.NO_WRAP);
        blub = table+":" + "email" + ":" + user_email + ":" + encoded_json;
        /*
        if (privileged == Type.CATOWNER) {
            blub = "catowner:" + "address" + ":" + user_email + ":" + encoded_json;
        } else {
            blub = "personel:" + "address" + ":" + user_email + ":" + encoded_json;
        }
        */
        blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("misc_action", "update_user_info");
        data.put("blub", blub);
        prepareAndSendData(data, Type.UPLOAD_MISC);
        if(table.equals("catowner")){
            homePageHandler.getCatownerManager().updateEmail(new_email);
        }else{
            homePageHandler.getPersonelManager().updateEmail(new_email);
        }
    }

    public void updateUserAddress(String table, String user_email, String street, String zipcode, String city) throws JSONException {
        String blub;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("address", street);
        jsonObject.put("zipcode", zipcode);
        jsonObject.put("city", city);
        String encoded_json = Base64.encodeToString(jsonObject.toString().getBytes(), Base64.NO_WRAP);
        blub = table+":" + "address" + ":" + user_email + ":" + encoded_json;
        /*
        if (privileged == Type.CATOWNER) {
            blub = "catowner:" + "address" + ":" + user_email + ":" + encoded_json;
        } else {
            blub = "personel:" + "address" + ":" + user_email + ":" + encoded_json;
        }
        */
        blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("misc_action", "update_user_info");
        data.put("blub", blub);
        prepareAndSendData(data, Type.UPLOAD_MISC);
        if(table.equals("catowner")){
            homePageHandler.getCatownerManager().updateAddressInfo(street, zipcode, city);
        }else{
            homePageHandler.getPersonelManager().updateAddressInfo(street, zipcode, city);
        }
    }

    public void updateUserInfo(String type, String user_email, String encoded_json){
        String blub;
        if (privileged == Type.CATOWNER) {
            blub = "catowner:" + type + ":" + user_email + ":" + encoded_json;
        } else {
            blub = "personel:" + type + ":" + user_email + ":" + encoded_json;
        }
        blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("misc_action", "update_user_info");
        data.put("blub", blub);
        prepareAndSendData(data, Type.UPLOAD_MISC);
    }

    public void userInfoUpdateSuccess(String encoded){
        String type = new String(Base64.decode(encoded.getBytes(), Base64.NO_WRAP));
        type = type.split(":")[1];
        switch (type){
            case "name":
                new BottomDialog(this).popup("Namn uppdaterat");
                break;
            case "email":
                new BottomDialog(this).popup("Epostadress uppdaterad");
                break;
            case "address":
                new BottomDialog(this).popup("Adress uppdaterad");
                break;
            case "password":
                new BottomDialog(this).popup("Lösenord uppdaterat");
                break;
            case "cell":
                new BottomDialog(this).popup("Mobilnummer uppdaterat");
                break;
            default:break;
        }
    }

    public void handleAboutUs(View view){

        String openHourText = ((TextView)(settingsPage.findViewById(R.id.center_open_hours_text))).getText().toString();
        String[] openHourTextSplit = openHourText.split("\n");
        String monday = openHourTextSplit[0].split(" ")[1];
        String tuesday = openHourTextSplit[1].split(" ")[1];
        String wednesday = openHourTextSplit[2].split(" ")[1];
        String thursday = openHourTextSplit[3].split(" ")[1];
        String friday = openHourTextSplit[4].split(" ")[1];
        String saturday = openHourTextSplit[5].split(" ")[1];
        String sunday = openHourTextSplit[6].split(" ")[1];

        try{
            JSONObject jsonObject = new JSONObject();
            if(monday.equals("Stängt")){
                jsonObject.put("monday_open", "10:00");
                jsonObject.put("monday_close", "18:00");
                jsonObject.put("monday_is_closed", "true");
            }
            else{
                jsonObject.put("monday_open", monday.split("-")[0]);
                jsonObject.put("monday_close", monday.split("-")[1]);
                jsonObject.put("monday_is_closed", "false");
            }
            if(tuesday.equals("Stängt")){
                jsonObject.put("tuesday_open", "10:00");
                jsonObject.put("tuesday_close", "18:00");
                jsonObject.put("tuesday_is_closed", "true");
            }
            else{
                jsonObject.put("tuesday_open", tuesday.split("-")[0]);
                jsonObject.put("tuesday_close", tuesday.split("-")[1]);
                jsonObject.put("tuesday_is_closed", "false");
            }
            if(wednesday.equals("Stängt")){
                jsonObject.put("wednesday_open", "10:00");
                jsonObject.put("wednesday_close", "18:00");
                jsonObject.put("wednesday_is_closed", "true");
            }
            else{
                jsonObject.put("wednesday_open", wednesday.split("-")[0]);
                jsonObject.put("wednesday_close", wednesday.split("-")[1]);
                jsonObject.put("wednesday_is_closed", "false");
            }
            if(thursday.equals("Stängt")){
                jsonObject.put("thursday_open", "10:00");
                jsonObject.put("thursday_close", "18:00");
                jsonObject.put("thursday_is_closed", "true");
            }
            else{
                jsonObject.put("thursday_open", thursday.split("-")[0]);
                jsonObject.put("thursday_close", thursday.split("-")[1]);
                jsonObject.put("thursday_is_closed", "false");
            }
            if(friday.equals("Stängt")){
                jsonObject.put("friday_open", "10:00");
                jsonObject.put("friday_close", "16:00");
                jsonObject.put("friday_is_closed", "true");
            }
            else{
                jsonObject.put("friday_open", friday.split("-")[0]);
                jsonObject.put("friday_close", friday.split("-")[1]);
                jsonObject.put("friday_is_closed", "false");
            }
            if(saturday.equals("Stängt")){
                jsonObject.put("saturday_open", "10:00");
                jsonObject.put("saturday_close", "18:00");
                jsonObject.put("saturday_is_closed", "true");
            }
            else{
                jsonObject.put("saturday_open", saturday.split("-")[0]);
                jsonObject.put("saturday_close", saturday.split("-")[1]);
                jsonObject.put("saturday_is_closed", "false");
            }
            if(sunday.equals("Stängt")){
                jsonObject.put("sunday_open", "10:00");
                jsonObject.put("sunday_close", "18:00");
                jsonObject.put("sunday_is_closed", "true");
            }
            else{
                jsonObject.put("sunday_open", sunday.split("-")[0]);
                jsonObject.put("sunday_close", sunday.split("-")[1]);
                jsonObject.put("sunday_is_closed", "false");
            }
            jsonObject.put("divergent", (settingsPage.findViewById(R.id.alertMessage)).getVisibility() == View.VISIBLE ? true : false);
            jsonObject.put("comment", ((TextView)(settingsPage.findViewById(R.id.alertMessageText))).getText().toString());
            /*
            jsonObject.put("monday_open", openHourTextSplit[0].split(" ")[1].split("-")[0]);
            jsonObject.put("monday_close", openHourTextSplit[0].split(" ")[1].split("-")[1]);
            jsonObject.put("monday_is_closed", openHourTextSplit[0].split(" ")[1].split("-")[0]);
            jsonObject.put("tuesday_open", selectedTime_openTuesday);
            jsonObject.put("tuesday_close", selectedTime_closeTuesday);
            jsonObject.put("tuesday_is_closed", tuesdayClosed);
            jsonObject.put("wednesday_open", selectedTime_openWednesday);
            jsonObject.put("wednesday_close", selectedTime_closeWednesday);
            jsonObject.put("wednesday_is_closed", wednesdayClosed);
            jsonObject.put("thursday_open", selectedTime_openThursday);
            jsonObject.put("thursday_close", selectedTime_closeThursday);
            jsonObject.put("thursday_is_closed", thursdayClosed);
            jsonObject.put("friday_open", selectedTime_openFriday);
            jsonObject.put("friday_close", selectedTime_closeFriday);
            jsonObject.put("friday_is_closed", fridayClosed);
            jsonObject.put("saturday_open", selectedTime_openSaturday);
            jsonObject.put("saturday_close", selectedTime_closeSaturday);
            jsonObject.put("saturday_is_closed", saturdayClosed);
            jsonObject.put("sunday_open", selectedTime_openSunday);
            jsonObject.put("sunday_close", selectedTime_closeSunday);
            jsonObject.put("sunday_is_closed", sundayClosed);
               */
            AboutUsSettings aboutUsSettings = new AboutUsSettings(this);
            aboutUsSettings.showDialog(jsonObject);
        } catch (JSONException jsone){
            jsone.printStackTrace();
        }


        /*
        final Dialog setOpeningHoursDialog = new Dialog(this);
        setOpeningHoursDialog.setTitle("Öppettider");
        final RelativeLayout openHoursContent = (RelativeLayout) getLayoutInflater().inflate(R.layout.set_aboutus, null);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.monday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.monday_end_hour), 8);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.tuesday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.tuesday_end_hour), 8);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.wednesday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.wednesday_end_hour), 8);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.thursday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.thursday_end_hour), 8);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.friday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.friday_end_hour), 6);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.saturday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.saturday_end_hour), 1);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.sunday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.sunday_end_hour), 1);
        CheckBox abnormalOpenTimes = (CheckBox) openHoursContent.findViewById(R.id.enableAbnormalTimes);
        abnormalOpenTimes.setChecked(footerBar.findViewById(R.id.alert_pic_image).getVisibility() == View.VISIBLE);
        abnormalOpenTimes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                footerBar.findViewById(R.id.alert_pic_image).setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
            }
        });

        Button saveOpenHours = (Button) openHoursContent.findViewById(R.id.saveOpenHours);
        saveOpenHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedTime_openMonday = (String)((Spinner)(openHoursContent.findViewById(R.id.monday_start_hour))).getSelectedItem();
                String selectedTime_closeMonday = (String)((Spinner)(openHoursContent.findViewById(R.id.monday_end_hour))).getSelectedItem();
                String selectedTime_openTuesday = (String)((Spinner)(openHoursContent.findViewById(R.id.tuesday_start_hour))).getSelectedItem();
                String selectedTime_closeTuesday = (String)((Spinner)(openHoursContent.findViewById(R.id.tuesday_end_hour))).getSelectedItem();
                String selectedTime_openWednesday = (String)((Spinner)(openHoursContent.findViewById(R.id.wednesday_start_hour))).getSelectedItem();
                String selectedTime_closeWednesday = (String)((Spinner)(openHoursContent.findViewById(R.id.wednesday_end_hour))).getSelectedItem();
                String selectedTime_openThursday = (String)((Spinner)(openHoursContent.findViewById(R.id.thursday_start_hour))).getSelectedItem();
                String selectedTime_closeThursday = (String)((Spinner)(openHoursContent.findViewById(R.id.thursday_end_hour))).getSelectedItem();
                String selectedTime_openFriday = (String)((Spinner)(openHoursContent.findViewById(R.id.friday_start_hour))).getSelectedItem();
                String selectedTime_closeFriday = (String)((Spinner)(openHoursContent.findViewById(R.id.friday_end_hour))).getSelectedItem();
                String selectedTime_openSaturday = (String)((Spinner)(openHoursContent.findViewById(R.id.saturday_start_hour))).getSelectedItem();
                String selectedTime_closeSaturday = (String)((Spinner)(openHoursContent.findViewById(R.id.saturday_end_hour))).getSelectedItem();
                String selectedTime_openSunday = (String)((Spinner)(openHoursContent.findViewById(R.id.sunday_start_hour))).getSelectedItem();
                String selectedTime_closeSunday = (String)((Spinner)(openHoursContent.findViewById(R.id.sunday_end_hour))).getSelectedItem();
                setOpeningHoursDialog.dismiss();
            }
        });

        setOpeningHoursDialog.setContentView(openHoursContent);
        setOpeningHoursDialog.show();
        */
    }

    private String open_hours_text = "";

    public void getHours(String json){
        //Log.i("HOMEPAGE", "JSON= "+json);

        try {
            //updateOpenHours(new JSONObject(json));
            String[] rows = json.split("\n");
            for(String row: rows){
                JSONObject jsonObject = new JSONObject(row);
                updateOpenHours(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateOpenHours(JSONObject jsonObject) throws JSONException{
        if(jsonObject.has("monday_open")){
            /*
            String mon_text = jsonObject.getString("monday_open")+"-"+jsonObject.getString("monday_close");
            mon_text = (jsonObject.getString("monday_is_closed").equals("true")) ? "Stängt" : mon_text;
            open_hours_text += "Måndag "+mon_text+"\n";
            */
            Log.i("HOMEPAGE", "UPDATE FROM CHANGED");
            String mon_text = jsonObject.getString("monday_open")+"-"+jsonObject.getString("monday_close");
            String tue_text = jsonObject.getString("tuesday_open")+"-"+jsonObject.getString("tuesday_close");
            String wed_text = jsonObject.getString("wednesday_open")+"-"+jsonObject.getString("wednesday_close");
            String thu_text = jsonObject.getString("thursday_open")+"-"+jsonObject.getString("thursday_close");
            String fri_text = jsonObject.getString("friday_open")+"-"+jsonObject.getString("friday_close");
            String sat_text = jsonObject.getString("saturday_open")+"-"+jsonObject.getString("saturday_close");
            String sun_text = jsonObject.getString("sunday_open")+"-"+jsonObject.getString("sunday_close");
            mon_text = (jsonObject.getBoolean("monday_is_closed")) ? "Stängt" : mon_text;
            tue_text = (jsonObject.getBoolean("tuesday_is_closed")) ? "Stängt" : tue_text;
            wed_text = (jsonObject.getBoolean("wednesday_is_closed")) ? "Stängt" : wed_text;
            thu_text = (jsonObject.getBoolean("thursday_is_closed")) ? "Stängt" : thu_text;
            fri_text = (jsonObject.getBoolean("friday_is_closed")) ? "Stängt" : fri_text;
            sat_text = (jsonObject.getBoolean("saturday_is_closed")) ? "Stängt" : sat_text;
            sun_text = (jsonObject.getBoolean("sunday_is_closed")) ? "Stängt" : sun_text;
            open_hours_text = "Måndag "+mon_text+"\nTisdag "+tue_text+"\nOnsdag "+wed_text+"\nTorsdag "+thu_text+"\nFredag "+fri_text+"\nLördag "+sat_text+"\nSöndag "+sun_text;
            ((TextView)(settingsPage.findViewById(R.id.center_open_hours_text))).setText(open_hours_text);
            String isDivergent = jsonObject.getString("divergent_hours");
            if(isDivergent.equals("true")){
                String comment = jsonObject.getString("comment");
                ((TextView)settingsPage.findViewById(R.id.alertMessageText)).setText(comment);
                settingsPage.findViewById(R.id.alertMessage).setVisibility(View.VISIBLE);
                //footerLayout.findViewById(R.id.alert_pic_image).setVisibility(View.VISIBLE);
            }
            else{
                settingsPage.findViewById(R.id.alertMessage).setVisibility(View.GONE);
                //footerLayout.findViewById(R.id.alert_pic_image).setVisibility(View.INVISIBLE);
            }
            open_hours_text = "";
        }
        else{
            Log.i("HOMEPAGE", "GET HOURS: "+jsonObject.toString());
            String day = jsonObject.getString("day");
            String dayText = null;
            switch (day){
                case "monday":
                    dayText = "Måndag";
                    break;
                case "tuesday":
                    dayText = "Tisdag";
                    break;
                case "wednesday":
                    dayText = "Onsdag";
                    break;
                case "thursday":
                    dayText = "Torsdag";
                    break;
                case "friday":
                    dayText = "Fredag";
                    break;
                case "saturday":
                    dayText = "Lördag";
                    break;
                case "sunday":
                    dayText = "Söndag";
                    break;
                case "general":
                    break;
                default:break;
            }
            if(dayText == null){
                ((TextView)(settingsPage.findViewById(R.id.center_open_hours_text))).setText(open_hours_text);
                String isDivergent = jsonObject.getString("divergent");
                if(isDivergent.equals("true")){
                    String comment = jsonObject.getString("info");
                    ((TextView)settingsPage.findViewById(R.id.alertMessageText)).setText(comment);
                    settingsPage.findViewById(R.id.alertMessage).setVisibility(View.VISIBLE);
                    //footerLayout.findViewById(R.id.alert_pic_image).setVisibility(View.VISIBLE);
                }
                else{
                    settingsPage.findViewById(R.id.alertMessage).setVisibility(View.GONE);
                    //footerLayout.findViewById(R.id.alert_pic_image).setVisibility(View.INVISIBLE);
                }
            }
            else{
                String time_text = jsonObject.getString("open_time")+"-"+jsonObject.getString("close_time");
                time_text = (jsonObject.getString("closed").equals("1")) ? "Stängt" : time_text;
                open_hours_text += dayText+": "+time_text+"\n";
            }


            /*
            if(jsonObject.has("monday")){
                String mon_text = jsonObject.getString("open_time")+"-"+jsonObject.getString("close_time");
                mon_text = (jsonObject.getString("closed").equals("true")) ? "Stängt" : mon_text;
                open_hours_text += "Måndag "+mon_text+"\n";
            }
            if(jsonObject.has("tuesday")){
                String tue_text = jsonObject.getString("open_time")+"-"+jsonObject.getString("close_time");
                tue_text = (jsonObject.getString("closed").equals("true")) ? "Stängt" : tue_text;
                open_hours_text += "Tisdag "+tue_text+"\n";
            }
            if(jsonObject.has("wednesday")){
                String wed_text = jsonObject.getString("open_time")+"-"+jsonObject.getString("close_time");
                wed_text = (jsonObject.getString("closed").equals("true")) ? "Stängt" : wed_text;
                open_hours_text += "Onsdag "+wed_text+"\n";
            }
            if(jsonObject.has("thursday")){
                String thu_text = jsonObject.getString("open_time")+"-"+jsonObject.getString("close_time");
                thu_text = (jsonObject.getString("closed").equals("true")) ? "Stängt" : thu_text;
                open_hours_text += "Torsdag "+thu_text+"\n";
            }
            if(jsonObject.has("friday")){
                String fri_text = jsonObject.getString("open_time")+"-"+jsonObject.getString("close_time");
                fri_text = (jsonObject.getString("closed").equals("true")) ? "Stängt" : fri_text;
                open_hours_text += "Fredag "+fri_text+"\n";
            }
            if(jsonObject.has("saturday")){
                String sat_text = jsonObject.getString("open_time")+"-"+jsonObject.getString("close_time");
                sat_text = (jsonObject.getString("closed").equals("true")) ? "Stängt" : sat_text;
                open_hours_text += "Lördag "+sat_text+"\n";
            }
            if(jsonObject.has("sunday")){
                String sun_text = jsonObject.getString("open_time")+"-"+jsonObject.getString("close_time");
                sun_text = (jsonObject.getString("closed").equals("true")) ? "Stängt" : sun_text;
                open_hours_text += "Söndag "+sun_text+"\n";
            }
            if(jsonObject.has("general")){
                String isDivergent = jsonObject.getString("divergent_hours");
                if(isDivergent.equals("true")){
                    String comment = jsonObject.getString("comment");
                    ((TextView)aboutUsPage.findViewById(R.id.alertMessageText)).setText(comment);
                    aboutUsPage.findViewById(R.id.alertMessage).setVisibility(View.VISIBLE);
                    footerLayout.findViewById(R.id.alert_pic_image).setVisibility(View.VISIBLE);
                }
                else{
                    aboutUsPage.findViewById(R.id.alertMessage).setVisibility(View.GONE);
                    footerLayout.findViewById(R.id.alert_pic_image).setVisibility(View.INVISIBLE);
                }
            }
            */
        }
        /*
        if(jsonObject.has("tuesday_open")){
            String tue_text = jsonObject.getString("tuesday_open")+"-"+jsonObject.getString("tuesday_close");
            tue_text = (jsonObject.getString("tuesday_is_closed").equals("true")) ? "Stängt" : tue_text;
            open_hours_text += "Tisdag "+tue_text+"\n";
        }
        if(jsonObject.has("wednesday_open")){
            String wed_text = jsonObject.getString("wednesday_open")+"-"+jsonObject.getString("wednesday_close");
            wed_text = (jsonObject.getString("wednesday_is_closed").equals("true")) ? "Stängt" : wed_text;
            open_hours_text += "Onsdag "+wed_text+"\n";
        }
        if(jsonObject.has("thursday_open")){
            String thu_text = jsonObject.getString("thursday_open")+"-"+jsonObject.getString("thursday_close");
            thu_text = (jsonObject.getString("thursday_is_closed").equals("true")) ? "Stängt" : thu_text;
            open_hours_text += "Torsdag "+thu_text+"\n";
        }
        if(jsonObject.has("friday_open")){
            String fri_text = jsonObject.getString("friday_open")+"-"+jsonObject.getString("friday_close");
            fri_text = (jsonObject.getString("friday_is_closed").equals("true")) ? "Stängt" : fri_text;
            open_hours_text += "Fredag "+fri_text+"\n";
        }
        if(jsonObject.has("saturday_open")){
            String sat_text = jsonObject.getString("saturday_open")+"-"+jsonObject.getString("saturday_close");
            sat_text = (jsonObject.getString("saturday_is_closed").equals("true")) ? "Stängt" : sat_text;
            open_hours_text += "Lördag "+sat_text+"\n";
        }
        if(jsonObject.has("sunday_open")){
            String sun_text = jsonObject.getString("sunday_open")+"-"+jsonObject.getString("sunday_close");
            sun_text = (jsonObject.getString("sunday_is_closed").equals("true")) ? "Stängt" : sun_text;
            open_hours_text += "Söndag "+sun_text+"\n";
        }
        if(jsonObject.has("divergent_hours")){
            String isDivergent = jsonObject.getString("divergent_hours");
            if(isDivergent.equals("true")){
                String comment = jsonObject.getString("comment");
                ((TextView)aboutUsPage.findViewById(R.id.alertMessageText)).setText(comment);
                aboutUsPage.findViewById(R.id.alertMessage).setVisibility(View.VISIBLE);
                footerLayout.findViewById(R.id.alert_pic_image).setVisibility(View.VISIBLE);
            }
            else{
                aboutUsPage.findViewById(R.id.alertMessage).setVisibility(View.GONE);
                footerLayout.findViewById(R.id.alert_pic_image).setVisibility(View.INVISIBLE);
            }
        }
        */
        /*
        String mon_text = jsonObject.getString("monday_open")+"-"+jsonObject.getString("monday_close");
        String tue_text = jsonObject.getString("tuesday_open")+"-"+jsonObject.getString("tuesday_close");
        String wed_text = jsonObject.getString("wednesday_open")+"-"+jsonObject.getString("wednesday_close");
        String thu_text = jsonObject.getString("thursday_open")+"-"+jsonObject.getString("thursday_close");
        String fri_text = jsonObject.getString("friday_open")+"-"+jsonObject.getString("friday_close");
        String sat_text = jsonObject.getString("saturday_open")+"-"+jsonObject.getString("saturday_close");
        String sun_text = jsonObject.getString("sunday_open")+"-"+jsonObject.getString("sunday_close");
        mon_text = (jsonObject.getString("monday_is_closed").equals("true")) ? "Stängt" : mon_text;
        tue_text = (jsonObject.getString("tuesday_is_closed").equals("true")) ? "Stängt" : tue_text;
        wed_text = (jsonObject.getString("wednesday_is_closed").equals("true")) ? "Stängt" : wed_text;
        thu_text = (jsonObject.getString("thursday_is_closed").equals("true")) ? "Stängt" : thu_text;
        fri_text = (jsonObject.getString("friday_is_closed").equals("true")) ? "Stängt" : fri_text;
        sat_text = (jsonObject.getString("saturday_is_closed").equals("true")) ? "Stängt" : sat_text;
        sun_text = (jsonObject.getString("sunday_is_closed").equals("true")) ? "Stängt" : sun_text;
        open_hours_text = "Måndag "+mon_text+"\nTisdag "+tue_text+"\nOnsdag "+wed_text+"\nTorsdag "+thu_text+"\nFredag "+fri_text+"\nLördag "+sat_text+"\nSöndag "+sun_text;
        ((TextView)(aboutUsPage.findViewById(R.id.center_open_hours_text))).setText(open_hours_text);
        String isDivergent = jsonObject.getString("divergent_hours");
        if(isDivergent.equals("true")){
            String comment = jsonObject.getString("comment");
            ((TextView)aboutUsPage.findViewById(R.id.alertMessageText)).setText(comment);
            aboutUsPage.findViewById(R.id.alertMessage).setVisibility(View.VISIBLE);
            footerLayout.findViewById(R.id.alert_pic_image).setVisibility(View.VISIBLE);
        }
        else{
            aboutUsPage.findViewById(R.id.alertMessage).setVisibility(View.GONE);
            footerLayout.findViewById(R.id.alert_pic_image).setVisibility(View.INVISIBLE);
        }
        */
    }

    public void takePhotoFromCamera(){
        try {

            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            camera_uri = getContentResolver()
                    .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            photoFromCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //String imageFileName = "JPEG_" + timeStamp + "_";
        String imageFileName = "taken_image";
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = fileManager.getImageDir();

        //File image = File.createTempFile(
        //        imageFileName,  /* prefix */
        //        ".jpg",         /* suffix */
        //        storageDir      /* directory */
        //);

        Log.i("HOMEPAGE", "IMAGE PATH: "+fileManager.getImageDir().getAbsolutePath());
        File image = new File(fileManager.getImageDir(), "taken_image.jpg");


        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void photoFromCamera(){
        Intent cameraPicker = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(cameraPicker.resolveActivity(getPackageManager()) != null){
            //File camera_photo_file = new File(fileManager.getImageDir(), "taken_image.jpg");
            File camera_photo_file = null;
            try{
                camera_photo_file = createImageFile();
            } catch (IOException ioe){
                ioe.printStackTrace();
            }
            if(camera_photo_file != null){
                Log.i("HOMEPAGE", "IMAGE URL: "+camera_photo_file.getAbsolutePath());
                camera_uri = FileProvider.getUriForFile(this,
                        "kattcenter.norrtljekattcenter.provider",
                        camera_photo_file);
                cameraPicker.putExtra(MediaStore.EXTRA_OUTPUT, camera_uri);
                startActivityForResult(cameraPicker, CAMERA_INTENT);
            }
            /*
            camera_uri = Uri.fromFile(camera_photo_file);
            Log.i(INTENT_TAG, "Camera URI: "+camera_uri);
            cameraPicker.putExtra(MediaStore.EXTRA_OUTPUT, camera_uri);
            startActivityForResult(cameraPicker, CAMERA_INTENT);
            */
        }

    }



    public void setCurrentSampleFilename(String currentSampleFilename){
        this.currentSampleFileName = currentSampleFilename;
    }

    public String getCurrentSampleFilename(){
        return currentSampleFileName;
    }

    public void photoFromGallery(){
        Intent galleryPicker = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryPicker, GALLERY_INTENT);
    }


    private void clearButton(View parent){
        final LinearLayout linearLayout = (LinearLayout)parent;
        final TextView clear = (TextView) linearLayout.getChildAt(1);
        clear.setVisibility(View.VISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear.setVisibility(View.GONE);
                ((TextView)linearLayout.getChildAt(0)).setText("");
            }
        });

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        if(requestCode == SCAN_CODE_REQUEST){
            Log.i(INTENT_TAG, "Result code: "+resultCode);
            if(resultCode == RESULT_OK){
                String contents = intent.getStringExtra("SCAN_RESULT");
                //String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Log.i(INTENT_TAG, "Content: "+contents);
                //((TextView)catInfoBasic.findViewById(R.id.scannedChipCode)).setText(contents);
                ((TextView)catInfoBasic.findViewById(R.id.catbasic_chip_code_text)).setText(contents);
            }
            else if(resultCode == RESULT_CANCELED){
                Log.i(INTENT_TAG, "Result canceled");

            }
        }
        else if(requestCode == PICK_FILE || requestCode == PICK_VETDOC || requestCode == PICK_REGSKKDOC){
            if(resultCode == RESULT_OK){
                Uri fileUri = intent.getData();
                System.out.println("URI: "+fileUri.toString()+" PATH: "+fileUri.getPath());
                Cursor cursor = getContentResolver().query(fileUri, null, null, null, null, null);
                try{
                    if(cursor != null && cursor.moveToFirst()){
                        String name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        System.out.println("NAME: "+name);
                        setCurrentSampleFilename(name);
                        if(requestCode == PICK_FILE){
                            ((TextView)catInfoVet.findViewById(R.id.catvet_bloodsample_result_text)).setText(name);
                            clearButton(catInfoVet.findViewById(R.id.catvet_bloodsample_result_text_holder));
                            //boolean saved = fileManager.saveSampleToLocalStorage(this, fileUri, name, "sample.pdf", true);
                            Object[] result = fileManager.saveDocToFolder(this, fileUri, name, "sample.pdf", Type.SAMPLE_FILE, true);
                            boolean saved = (boolean)result[0];
                            Log.i("HOMEPAGE", "FILE " + fileUri+" SAVED TO APP STORAGE: " + saved);
                        }
                        else if(requestCode == PICK_VETDOC){
                            //((TextView)catInfoVet.findViewById(R.id.catvet_bloodsample_result_text)).setText(name);
                            //clearButton(catInfoVet.findViewById(R.id.catvet_bloodsample_result_text_holder));

                            Object[] result = fileManager.saveDocToFolder(this, fileUri, name, "vetdoc", Type.VETDOC_FILE, false);
                            boolean saved = (boolean)result[0];
                            String tag = new String((int)result[1]+"");
                            vetRegInfo.addDocumentToList(new String[]{name, tag});
                            Log.i("HOMEPAGE", "FILE " + fileUri+" SAVED TO APP STORAGE: " + saved);
                        }
                        else if(requestCode == PICK_REGSKKDOC){


                            Object[] result = fileManager.saveDocToFolder(this, fileUri, name, "skkdoc", Type.SKKDOC_FILE, false);
                            boolean saved = (boolean)result[0];
                            String tag = new String((int)result[1]+"");
                            regSKKInfo.addDocumentToList(new String[]{name, tag});
                            Log.i("HOMEPAGE", "FILE " + fileUri+" SAVED TO APP STORAGE: " + saved);
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    cursor.close();
                }


            }
        }
        else if(requestCode == CREATE_FILE){
            if(resultCode == RESULT_OK){
                System.out.println("CREATED FILE: "+intent.getData().toString());
            }
        }
        else if(requestCode == CAMERA_INTENT){
            if(resultCode == RESULT_OK){
                try {
                    Bitmap image = MediaStore.Images.Media.getBitmap(getContentResolver(), camera_uri);
                    /**
                     * IMAGE PROCESSING IS WEIRD HERE
                     *
                     * IMPORTANT!!!! Image NOT ALWAYS processed in landscape mode
                     *
                     * Portrait mode on some devices:
                     * - Image height = image width and vice versa
                     */
                    int image_height = image.getWidth();
                    int image_width = image.getHeight();
                    int a;
                    if(image_height < image_width){
                        int temp = image_height;
                        image_height = image_width;
                        image_width = temp;
                        a = (image_height - image_width)/2;
                        image = Bitmap.createBitmap(image, 0, a, image_width, (image_height-(2*a)));
                    }
                    else{
                        a = (image_height - image_width)/2;
                        image = Bitmap.createBitmap(image, a, 0, (image_height-(2*a)), image_width);
                        Matrix matrix = new Matrix();
                        matrix.postRotate(90);
                        image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
                    }
                    System.out.println("HEIGHT: "+image_height+"  WIDTH: "+image_width+"   A: "+a);


                    //image_height = image.getWidth();
                    //image_width = image.getHeight();
                    //System.out.println("HEIGHT: "+image_height+"  WIDTH: "+image_width);

                    infoSelectedCat.setImageBitmap(image);
                    infoSelectedCat.setTag("TAKEN_PHOTO");
                    //saveCatInfoButton.setEnabled(true);
                    //(findViewById(R.id.cat_photo_status_text)).setVisibility(View.VISIBLE);


                    boolean saved = fileManager.saveImageToLocalStorage("taken_image.jpg", image, true);

                    Log.i("HOMEPAGE", "TAKEN PHOTO SAVED STATUS: "+saved);
                    setChangedCatPhoto(true);
                    //changedCatPhoto = true;


                    //url_data.put("FILEURI", camera_uri.toString());
                } catch (Exception e){
                    Toast.makeText(this, "Kunde inte spara bilden", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }
        else if(requestCode == GALLERY_INTENT && resultCode == RESULT_OK){
            try {
                Uri selectedPhoto = intent.getData();
                System.out.println("URI: "+selectedPhoto.getPath());
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedPhoto, filePath, null, null, null);
                System.out.println("CURSOR: "+cursor.toString()+"\nCOUNT: "+cursor.getColumnCount());
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePath[0]);
                System.out.println("INDEX: "+columnIndex+" VALUE: "+cursor.getString(columnIndex));

                Bitmap bitmap = BitmapFactory.decodeFile(cursor.getString(columnIndex));
                //bitmap = Bitmap.createScaledBitmap(bitmap, 200, 200, false);
                int image_height = bitmap.getWidth();
                int image_width = bitmap.getHeight();
                if(image_height < image_width){
                    int a = (image_width - image_height)/2;
                    System.out.println("WIDTH: "+image_width+" HEIGHT: "+image_height+" a: "+a);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, image_height, image_width-a);
                }else{
                    int a = (image_height - image_width)/2;
                    System.out.println("WIDTH: "+image_width+" HEIGHT: "+image_height+" a: "+a);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, image_height-a, image_width);
                }

                infoSelectedCat.setTag("SELECTED_PHOTO");
                infoSelectedCat.setImageBitmap(bitmap);
                boolean saved = fileManager.saveImageToLocalStorage("taken_image.jpg", bitmap, true);
                Log.i("HOMEPAGE", "SELECTED PHOTO SAVED STATUS: "+saved);
                setChangedCatPhoto(true);
            } catch (Exception e){
                System.out.println("Kunde inte spara bilden");
                Toast.makeText(this, "Kunde inte spara bilden", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }




    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------

    private void addNewHeaderLayout(boolean catowner, boolean searchField, String searchHint,
                                    boolean newForm, boolean userInfo, boolean previewPage, String headerTitle, boolean settingsPage){
        System.out.println(catowner+" "+searchField+" "+searchHint+" "+newForm+" "+userInfo+" "+previewPage+" "+headerTitle+" "+settingsPage);
        if(catowner){
            if(newForm)
                headerLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_header_basic_catform, null);
            else if(previewPage || settingsPage) {
                headerLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_catowner_cat_preview_header, null);
                if(previewPage)
                    ((TextView)headerLayout.findViewById(R.id.header_title)).setText(headerTitle);
                else
                    ((TextView)headerLayout.findViewById(R.id.header_title)).setText("Inställningar");
            }
            else
                headerLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_catowner_homepage_header, null);
        }
        else {
            if (newForm) {
                headerLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_header_basic_catform, null);
                ((TextView) headerLayout.findViewById(R.id.header_title)).setText(headerTitle);
            } else if (previewPage) {
                headerLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_header_preview_page, null);
                ((TextView) headerLayout.findViewById(R.id.header_title)).setText(headerTitle);
            } else if (userInfo) {
                headerLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_header_user_profile, null);
                ((TextView)headerLayout.findViewById(R.id.header_title)).setText(headerTitle);
            } else {
                headerLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.new_header, null);
                //((TextView) headerLayout.findViewById(R.id.header_title)).setText(currentHeaderTitle);
                //((TextView)headerLayout.findViewById(R.id.header_title)).setText(headerTitle);
                //((TextView)headerLayout.findViewById(R.id.add_object_text)).setText(headerTitle);
                if (searchField) {
                    ((EditText) headerLayout.findViewById(R.id.searchField)).setHint(searchHint);
                    ((EditText) headerLayout.findViewById(R.id.searchField)).addTextChangedListener(new MySearchEngine());
                }


                if (settingsPage) {
                    headerLayout.findViewById(R.id.settings_page).setVisibility(View.GONE);
                    headerLayout.findViewById(R.id.add_object).setVisibility(View.GONE);
                } else {
                    headerLayout.findViewById(R.id.settings_page).setVisibility(View.VISIBLE);
                    headerLayout.findViewById(R.id.add_object).setVisibility(View.VISIBLE);
                }

                headerLayout.findViewById(R.id.search_view).setVisibility(searchField ? View.VISIBLE : View.GONE);

            }
        }

        updateHeader();
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.new_header));
       // headerToolbar.setBackground(getResources().getDrawable(R.drawable.background_gradient_2, null));
    }




    public class MySearchEngine implements TextWatcher{

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * are about to be replaced by new text with length <code>after</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param count
         * @param after
         */
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * have just replaced old text that had length <code>before</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param before
         * @param count
         */
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Log.i("HOMEPAGE", "Searching for ["+ currentSearchType +"]: "+s.toString());
            searchEngine.query(s);
        }

        /**
         * This method is called to notify you that, somewhere within
         * <code>s</code>, the text has been changed.
         * It is legitimate to make further changes to <code>s</code> from
         * this callback, but be careful not to get yourself into an infinite
         * loop, because any changes you make will cause this method to be
         * called again recursively.
         * (You are not told where the change took place because other
         * afterTextChanged() methods may already have made other changes
         * and invalidated the offsets.  But if you need to know here,
         * you can use {@link Spannable#setSpan} in {@link #onTextChanged}
         * to mark your place and then look up from here where the span
         * ended up.
         *
         * @param s
         */
        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private void updateHeader(){
        headerToolbar = (Toolbar) findViewById(R.id.toolBar);
        headerToolbar.setTitle("");
        headerToolbar.removeAllViews();
        headerToolbar.addView(headerLayout);
        setSupportActionBar(headerToolbar);
    }


    public void setUploaded_catform_docs_failed(boolean uploaded_catform_docs_failed){
        this.uploaded_catform_docs_failed = uploaded_catform_docs_failed;
    }

    public boolean getUploaded_catform_docs_failed(){
        return uploaded_catform_docs_failed;
    }

    public synchronized int incrementUploaded_catform_docs(){
        uploaded_catform_docs++;
        return uploaded_catform_docs;
    }

    public synchronized void resetUploaded_catform_docs(){
        uploaded_catform_docs = 0;
    }

    public void addReminder(String reminder){
        //reminderHandler.addReminder(reminder);
        new ReminderHandlerAsync(this, smsSender, reminder).execute();
    }

    public void incrementReminderStatusBar(){
        notificationHandler.progressUpdate(1);
    }

    public void reminderCompleted(){
        notificationHandler.completed();
    }

    public void startDownloadDocuments(String encoded_file_name, String downloadText, boolean mult_files){

        multiFileDownload = mult_files;
        /**
         * REC FORMAT:
         * B64(B64(B64(fileName):B64(TIMESTAMP):B64(CATID)).pdf)
         */
        Log.i("HOMEPAGE", encoded_file_name+" "+downloadText+" "+mult_files);
        String dec = new String(Base64.decode(encoded_file_name.getBytes(), Base64.NO_WRAP));
        if(mult_files){
            String[] encodedFiles = dec.split(":");
            for(String encoded_file: encodedFiles){
                Log.i("HOMEPAGE", "ENCODED FILE: "+encoded_file);
                dec = new String(Base64.decode(encoded_file.getBytes(), Base64.NO_WRAP));
                Log.i("HOMEPAGE", "DEC: "+dec);
                if (dec.split("\\.").length == 2) {
                    String name_part = dec.split("\\.")[0];
                    Log.i("HOMEPAGE", "NAMEPART: "+name_part);
                    String dec_name_part = new String(Base64.decode(name_part.getBytes(), Base64.NO_WRAP));
                    String[] parts = dec_name_part.split(":");
                    String decoded_name = new String(Base64.decode(parts[0].getBytes(), Base64.NO_WRAP));
                    //setCurrentSampleFilename(new String(Base64.decode(parts[0].getBytes(), Base64.NO_WRAP)));
                    HashMap<String, String> data = new HashMap<>();
                    data.put("file_name", dec);
                    data.put("file_name_decoded", decoded_name);
                    prepareData(data, Type.DOWNLOAD_FILE);
                    //prepareAndSendData(data, Type.DOWNLOAD_FILE);
                    //notificationHandler.createDownloadNotification(getCurrentSampleFilename(), downloadText);
                }
            }
            signalQueueToSend();
            notificationHandler.createMultiDownloadNotification(encodedFiles.length);
        }
        else {
            if (dec.split("\\.").length == 2) {
                String name_part = dec.split("\\.")[0];
                String dec_name_part = new String(Base64.decode(name_part.getBytes(), Base64.NO_WRAP));
                String[] parts = dec_name_part.split(":");
                String decoded_name = new String(Base64.decode(parts[0].getBytes(), Base64.NO_WRAP));
                //setCurrentSampleFilename(new String(Base64.decode(parts[0].getBytes(), Base64.NO_WRAP)));
                HashMap<String, String> data = new HashMap<>();
                data.put("file_name", dec);
                data.put("file_name_decoded", decoded_name);
                prepareAndSendData(data, Type.DOWNLOAD_FILE);
                notificationHandler.createDownloadNotification(getCurrentSampleFilename(), downloadText);
            }
        }

    }

    public void handleDownloadedFile(String decoded_file_name){
        if(multiFileDownload){
            if(notificationHandler.updateMultiDownloadProgress()){
                setMultiFileDownloadComplete();
                multiFileDownload = false;
            }
            else{
                signalQueueToSend();
            }
        }
        else{
            setFileDownloadComplete(decoded_file_name);
        }
    }

    public void setFileDownloadComplete(String file_name){
        //Log.i("HOMEPAGE", "FILE DOWNLOADED");
        //notificationHandler.downloadComplete(getCurrentSampleFilename());
        setCurrentSampleFilename(file_name);
        notificationHandler.downloadComplete(file_name);
    }

    public void setMultiFileDownloadComplete(){
        notificationHandler.multiDownloadComplete();
    }


    public PendingIntent openDownloadFolderIntent(){
        Intent openFolderIntent;
        openFolderIntent = new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS);
        openFolderIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return PendingIntent.getActivity(this, 0, openFolderIntent, 0);
    }

    public PendingIntent openPDFIntent(){
        String filePath = fileManager.getDownloadFolder()+"/"+getCurrentSampleFilename();
        System.out.println("FILE PATH: "+filePath);
        //String filePath = APP_DOWNLOAD_DIR + filename;
        File file = new File(filePath);
        //Log.d(AppConfig.APP_TAG, "File to download = " + String.valueOf(file));
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String ext=file.getName().substring(file.getName().indexOf(".")+1);
        String type = mime.getMimeTypeFromExtension(ext);
        Intent openFile;
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            openFile = new Intent(Intent.ACTION_VIEW, Uri.fromFile(file));
            openFile.setDataAndType(Uri.fromFile(file), type);
            openFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        else {
            Log.i("HOMEPAGE", "APP ID: "+BuildConfig.APPLICATION_ID);
            Uri fileUri = FileProvider.getUriForFile(HomePage.this, BuildConfig.APPLICATION_ID+".provider", file);
            //openFile = new Intent(Intent.ACTION_VIEW, Uri.fromFile(file));  //FIXA
            openFile = new Intent(Intent.ACTION_VIEW, fileUri);
            //openFile.setDataAndType(Uri.fromFile(file), type);
            openFile.setDataAndType(fileUri, type);
            openFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            openFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        PendingIntent p = PendingIntent.getActivity(this, 0, openFile, 0);
        return p;
    }


    public void setCurrentSearchType(String currentSearchType){
        this.currentSearchType = currentSearchType;
    }

    public String getCurrentSearchType(){
        return currentSearchType;
    }

    public String getCurrentHeaderSecondaryTitle(){
        return currentHeaderSecondaryTitle;
    }

    public HomePageHandler getHomePageHandler(){
        return homePageHandler;
    }

    public HashMap<String, LinearLayout> getUploadingForm(){
        return uploadingForm;
    }


    public void uploadComplete(){
        if(privileged == Type.CATOWNER){
            homePageHandler.getListHandler().clearList();
            homePageHandler.loadCatownerHomepage(getUserEmail());
            homePageHandler.closeSaveformDialog();

        }
        else if(homePageHandler.isSaveformShowing()){
            String formTable = titlesToDbtables.get(selectedForm);
            if(selectedForm.equals(catformtitles[catformtitles.length-1])) {
                //homePageHandler.closeSaveformDialog();
                System.out.println("UPLOADED DB TABLE: "+selectedForm);
                goToCatowner();

            }
            else {

                System.out.println("UPLOADED DB TABLE: " + formTable);
                //String formTable = ;     /* ALWAYS INITIAL FORM TO LOAD WHEN CLICKED ON CAT IN LIST */
                if (formTable != null && !formTable.isEmpty() && !formTable.equals("null")) {
                    if (formTable.equals("catform_vetdoc")) {
                        setLoadCatVetItems(true);
                        new LoadPreviewHandler(this).clearPreview();
                        loadCatVetForms();
                        controller.startDownloadForms();
                        homePageHandler.showStandardDialog("Hämtar formulär...");
                    } else {
                        String blub = formTable + ":" + currentCatId;
                        Log.i("HOMEPAGE", "CURRENT BLUB: " + blub);
                        String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
                        HashMap<String, String> data = new HashMap<>();
                        if (formTable.equals("catform_connection")) {
                            data.put("form_action", "GET_FAMILY_CONNECTIONS");
                        } else {
                            data.put("form_action", "DOWNLOAD");
                        }

                        data.put("blub", encoded_blub);
                        prepareAndSendData(data, Type.DOWNLOAD_FORM);
                        ((AutoCompleteTextView) findViewById(R.id.catbasic_catname_autocomplete_text)).setAdapter((AutoCompleteAdapter) null);
                    }

                } else if ((formTable == null || formTable.equals("null")) && currentCatId != null) {

                    setLoadPreviousCatownerList(true);
                    HashMap<String, String> data = new HashMap<>();
                    data.put("blub", Base64.encodeToString("ALL_CATS".getBytes(), Base64.NO_WRAP));
                    prepareAndSendData(data, Type.DOWNLOAD_LIST);
                }
            }


            //setView(IndexHolder.FORM_PREVIEW_PAGE);
           // Log.i("HOMEPAGE", "UPLOAD COMPLETE");
            homePageHandler.closeSaveformDialog();
            if(privileged != Type.CATOWNER)
                currentViewedCatname = autoCompleteTextView.getText().toString();
            new BottomDialog(this).popup(addNewCat ? "Katten "+currentViewedCatname+" lades till" : "Uppdaterade information för katten "+currentViewedCatname);
            //if(addNewCat) addNewCat = false;    //TRYING TO RESET VARIABLE HERE (POSSIBLE THE BEST PLACE)
        }
    }


    public String getCurrentViewedCatname(){
        return currentViewedCatname;
    }

    public void setCurrentViewedCatname(String currentViewedCatname){
        this.currentViewedCatname = currentViewedCatname;
    }

    public HashMap<String, String> getTitlesToDbtables(){
        return titlesToDbtables;
    }

    public HashMap<String, String> getDbTablesToTitles() { return dbTablesToTitles; }

    public String[] getCatformtitles(){
        return catformtitles;
    }


    public void flipView2(View view){
        int toIndex = IndexHolder.HOMEPAGE_INDEX;
        String title = "";
        switch (view.getId()){
            case R.id.backToHome:

                if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.OWNER_INFO_PAGE, privileged)){
                    homePageHandler.getListHandler().viewList(Type.CATOWNER);
                    title = "NY KATTÄGARE";
                    System.out.println("PRE ADDING: "+currentHeaderTitle);
                    toIndex = IndexHolder.OWNER_LIST_PAGE;
                }
                else if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.PERSONAL_INFO_PAGE, privileged)){
                    homePageHandler.getListHandler().viewList(Type.PERSONAL);
                    title = "NY PERSONAL";
                    toIndex = IndexHolder.PERSONAL_LIST_PAGE;
                }
                else if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.OWNER_CATS_INFO_PAGE, privileged)){
                    toIndex = IndexHolder.OWNER_LIST_PAGE;
                }
                else if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged)){
                    Log.i("HOMEPAGE", "ADDNEWCAT: "+addNewCat);
                    if(homePageHandler.getListHandler().getCatListImageAdapter().getTotalCats() == 0){
                        getHomePageHandler().getListHandler().viewList(Type.ALL_CATS);
                        return;
                    }
                    if(privileged == Type.CATOWNER){
                        title = "Mina katter";
                        toIndex = 0;
                    }
                    else{
                        if(addNewCat){

                        }
                        else{
                            title = catListTitle;
                            toIndex = IndexHolder.authenticateIndex(IndexHolder.CATLIST_PAGE, privileged);
                        }
                    }


                }
                else if(currentFlippedViewIndex >= IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged)){
                    toIndex = IndexHolder.FORM_PREVIEW_PAGE;
                }
                else if(currentFlippedViewIndex < IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged)){
                    title = getHeaderTitle();
                }

                break;
            case R.id.backToPreview:
                if(!addNewCat)
                    toIndex = IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged);

                title = currentHeaderTitle;

                break;
            default:break;
        }
        setView(toIndex, title);
    }

    public void flipView(View view){
        String title = currentHeaderTitle, secondTitle = currentHeaderSecondaryTitle;
        int toIndex = IndexHolder.authenticateIndex(IndexHolder.HOMEPAGE_INDEX, privileged), colorPrimary = R.color.newColorPrimary, colorPrimaryDark = R.color.newColorPrimary;
        switch (view.getId()){
            case R.id.homepage_button:
                title = getHeaderTitle();
                break;
            case R.id.backToHome:
                if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.OWNER_INFO_PAGE, privileged)){
                    homePageHandler.getListHandler().viewList(Type.CATOWNER);
                    title = getString(R.string.user);
                    toIndex = IndexHolder.OWNER_LIST_PAGE;
                }
                else if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.OWNER_CATS_INFO_PAGE, privileged)){
                    toIndex = IndexHolder.OWNER_LIST_PAGE;
                }
                else if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged)){
                    Log.i("HOMEPAGE", "ADDNEWCAT: "+addNewCat);
                    if(homePageHandler.getListHandler().getCatListImageAdapter().getTotalCats() == 0){
                        getHomePageHandler().getListHandler().viewList(Type.ALL_CATS);
                        return;
                    }
                    if(privileged == Type.CATOWNER){
                        title = "Mina katter";
                        toIndex = 0;
                    }
                    else{
                        if(addNewCat){

                        }
                        else{
                            title = "Sök...";
                            toIndex = IndexHolder.authenticateIndex(IndexHolder.CATLIST_PAGE, privileged);
                        }
                    }


                }
                else if(currentFlippedViewIndex >= IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged)){
                    toIndex = IndexHolder.FORM_PREVIEW_PAGE;
                }
                else if(currentFlippedViewIndex < IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged)){
                    title = getHeaderTitle();
                }
                break;
            case R.id.backToHomeFromPersonal:
                toIndex = IndexHolder.authenticateIndex(IndexHolder.PERSONAL_LIST_PAGE, privileged);
                break;
            case R.id.backToPreview:
                if(!addNewCat)
                    toIndex = IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged);
                break;
            /*
            case R.id.catcenter_information:
                toIndex = IndexHolder.authenticateIndex(IndexHolder.INFOPAGE_INDEX, privileged);
                title = "Information";
                colorPrimary = R.color.newColorPrimary;
                colorPrimaryDark = R.color.newColorPrimary;
                break;
                */
            case R.id.settings_button:          // SETTINGS ON BOTH
                toIndex = IndexHolder.authenticateIndex(IndexHolder.SETTINGSPAGE_INDEX, privileged);
                title = "Inställningar";
                break;
            default:break;
        }
        Log.i("HOMEPAGE", "Toindex: "+toIndex);
        if((privileged == Type.PERSONAL || privileged == Type.ADMIN) && viewFlipper.getDisplayedChild() >= IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged)) {
            if(toIndex == IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged)){
                Log.i("HOMEPAGE", "To PREVIEW..");
            }
            leaveUnsavedFormsToPage(toIndex, title, secondTitle);
        }
        else
            if(toIndex == IndexHolder.OWNER_LIST_PAGE) {
                System.out.println("HÄÄÄÄR");
                //newSetView(toIndex, "test", "test2");
            }
            else
                setView(toIndex, title);
                //setView(toIndex, title, secondTitle, colorPrimary, colorPrimaryDark);

    }




    public void setView(int viewIndex, Type type){
        HashMap<Type, String> hashMap = new HashMap<>();
        hashMap.put(Type.ALL_CATS, "Alla katter");
        hashMap.put(Type.CATS_HOME, "Katter på katthemmet");
        hashMap.put(Type.CATS_CENTER, "Katter i Jourhem");
        hashMap.put(Type.TNR, "TNR");
        hashMap.put(Type.ADOPTED, "Adopterade");
        hashMap.put(Type.DECEASED, "Avlidna");
        hashMap.put(Type.RETURNED_TO_OWNER, "Åter ägaren");
        hashMap.put(Type.MISSING, "Saknade");
        hashMap.put(Type.WAITING_LIST, "Väntelista");
        setView(viewIndex, hashMap.get(type));
    }

    public void setView(int viewIndex, String title){
        viewIndex = IndexHolder.authenticateIndex(viewIndex, privileged);
        System.out.println("VIEWINDEX: "+viewIndex);
        viewFlipper.setDisplayedChild(viewIndex);
        currentFlippedViewIndex = viewIndex;
        currentHeaderTitle = title;
        boolean searchField = false;
        //boolean appMenu = true;
        //boolean formMenu = false;
        boolean newForm = false;
        boolean userInfo = false;
        boolean previewPage = false;
        boolean settingsPage = false;
        String headerTitle = title;
        String searchHint = "Sök";
        boolean catowner = privileged == Type.CATOWNER ? true : false;
        if(viewIndex == IndexHolder.CATLIST_PAGE)
            catListTitle = title;
        if(viewIndex == IndexHolder.OWNER_LIST_PAGE){
            searchField = true;
            searchHint = "Sök Kattägare";
        }
        else if(viewIndex == IndexHolder.PERSONAL_LIST_PAGE){
            searchField = true;
            searchHint = "Sök Personal";
        }
        else if(viewIndex == IndexHolder.CATLIST_PAGE){
            searchField = true;
            searchHint = "Sök Katt";
        }
        if(viewIndex == IndexHolder.OWNER_LIST_PAGE || viewIndex == IndexHolder.PERSONAL_LIST_PAGE || viewIndex == IndexHolder.CATLIST_PAGE)
            searchField = true;
        if(viewIndex == IndexHolder.FORM_PREVIEW_PAGE || (viewIndex == IndexHolder.CATOWNER_CAT_PREVIEW_PAGE && privileged == Type.CATOWNER)) {
            previewPage = true;
        }
        if(viewIndex >= IndexHolder.FIRST_FORMPAGE_INDEX || (catowner && viewIndex == IndexHolder.PRIVATE_CAT_INFO_PAGE)){
            newForm = true;
        }
        if(viewIndex == IndexHolder.OWNER_INFO_PAGE || viewIndex == IndexHolder.PERSONAL_INFO_PAGE){
            userInfo = true;
            headerTitle = (viewIndex == IndexHolder.OWNER_INFO_PAGE) ? "KATTÄGARE" : "PERSONAL";
        }
        if(viewIndex == IndexHolder.SETTINGSPAGE_INDEX){
            settingsPage = true;
        }
        System.out.println("ADDING HEADER: "+headerTitle);
        addNewHeaderLayout(catowner, searchField, searchHint, newForm, userInfo, previewPage, headerTitle, settingsPage);
        //addNewHeaderLayout(searchField, appMenu, formMenu, newForm, catownerInfo);
        if(viewIndex == IndexHolder.HOMEPAGE_INDEX && privileged != Type.CATOWNER){
            homePageHandler.refreshPersonalHomepage();
        }
    }

    public void setView(int viewIndex, String title, String secondaryTitle, int colorPrimary, int colorPrimaryDark) {
        System.out.println("SETVIEW");

    }





    private void clearForms(){
        HashMap<String, Bundle> clearBundle = controller.clearAllForms();
        new RestoreFormInstanceHandler(this, clearBundle);
        /*
        for(int i = 0; i < dbTables.length; i++){
            controller.setLockForm(dbTables[i], true);
        }
        */
    }

    public RelativeLayout getCatInfoBasic(){
        return (privileged == Type.CATOWNER) ? privateCatInfoView : catInfoBasic;
    }

    public RelativeLayout getCatInfoRegSKK(){
        return catInfoRegSKK;
    }

    public RelativeLayout getCatInfoConnection(){
        return catInfoConnection;
    }


    public void editForm(JSONObject jsonObject){
        showForm(currentPreviewedForm, currentHeaderSecondaryTitle);
        System.out.println("JSON-----");
        System.out.println(jsonObject);
    }

    public void removeTimeLineSuccess(){
        selectForm(currentPreviewedForm);
    }

    public void removeTimeLineEntry(final String reg_date){
        final Dialog dialog = new Dialog(this);
        LinearLayout content = (LinearLayout) getLayoutInflater().inflate(R.layout.new_delete_content_dialog, null);
        dialog.setContentView(content);
        dialog.show();

        content.findViewById(R.id.delete_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    String table = dbTables[currentPreviewedForm];
                    HashMap<String, String> data = new HashMap<>();
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("table", table);
                    jsonObject.put("reg_date", reg_date);
                    jsonObject.put("catid", currentCatId);
                    data.put("misc_action", "delete_timeline_entry");
                    data.put("blub", Base64.encodeToString(jsonObject.toString().getBytes(), Base64.NO_WRAP));
                    prepareAndSendData(data, Type.UPLOAD_MISC);
                    dialog.dismiss();
                }catch (JSONException jsone){
                    jsone.printStackTrace();
                }

            }
        });
        content.findViewById(R.id.abort_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    public void manageForm(View view){
        switch (view.getId()){
            case R.id.saveCatForm:
                checkSave();
                break;
            case R.id.new_form:
                if(currentPreviewedForm != IndexHolder.GENERAL_FORM_INDEX && currentPreviewedForm != IndexHolder.VET_DOC_FORM_INDEX)
                    clearForms();
                showForm(currentPreviewedForm, currentHeaderSecondaryTitle);
                break;

            case R.id.catform_share_function:
                homePageHandler.showShareOptionsDialog();
                break;

            case R.id.catowner_form:
                goToCatowner();
                break;
            default:break;
        }

    }


    public void setGenerateID(boolean generateID){
        this.generateID = generateID;
    }

    public void checkDownloadedNamesAndID(final String json){
        if(generateID){
            //homePageHandler.closeSaveformDialog();
            controller.setGeneratedCatID(json);
            /*
            ArrayList<String> arrayList = new ArrayList<>();
            ArrayList<Integer> catid_integers = new ArrayList<>();
            try{
                String[] jsonRows = json.split("\n");
                if(jsonRows.length == 0){
                    confirmSave();
                }
                else {
                    boolean catid_exists = false;
                    for (String jsonRow : jsonRows) {
                        System.out.println("JSONROW: " + jsonRow);
                        JSONObject jsonObject = new JSONObject(jsonRow);
                        String rec_catid = jsonObject.getString("catcenter_id");
                        if(getCurrentCatId().equals(rec_catid)){
                            catid_exists = true;
                        }
                        String[] catid_split = rec_catid.split("-");
                        String year = catid_split[0];
                        String order_number = catid_split[1];
                        if(year.equals(currentCatId.split("-")[0])) {
                            if (order_number.charAt(0) == '0') {
                                if (order_number.charAt(1) == '0') {
                                    catid_integers.add(Integer.parseInt(order_number.substring(2, 3)));
                                } else {
                                    catid_integers.add(Integer.parseInt(order_number.substring(1, 3)));
                                }
                            } else {
                                catid_integers.add(Integer.parseInt(order_number));
                            }
                            arrayList.add(rec_catid);
                        }
                        //arrayList.add(new String[]{, jsonObject.getString("name")});
                    }
                    if(catid_exists == false){
                        confirmSave();
                    }
                    else{
                        Collections.sort(catid_integers);
                        String nextAvailableCatId = null;
                        int currentCatIdNumber;
                        String currentCatIdString = currentCatId.split("-")[1];
                        String currentCatYear = currentCatId.split("-")[0];
                        if (currentCatIdString.charAt(0) == '0') {
                            if (currentCatIdString.charAt(1) == '0') {
                                currentCatIdNumber = Integer.parseInt(currentCatIdString.substring(2, 3));
                            } else {
                                currentCatIdNumber = Integer.parseInt(currentCatIdString.substring(1, 3));
                            }
                        } else {
                            currentCatIdNumber = Integer.parseInt(currentCatIdString);
                        }
                        boolean checkNext = false;
                        int k = 1;
                        int catintsize = catid_integers.size();
                        for(int i = 0; i < catintsize; i++){
                            if(checkNext){
                                if(catid_integers.get(i) != currentCatIdNumber+k){
                                    if(catid_integers.get(i) < 10)
                                        nextAvailableCatId = currentCatYear+"-00"+(currentCatIdNumber+k);
                                    else if(catid_integers.get(i) < 100)
                                        nextAvailableCatId = currentCatYear+"-0"+(currentCatIdNumber+k);
                                    else
                                        nextAvailableCatId = currentCatYear+"-"+(currentCatIdNumber+k);
                                    break;
                                }
                                k++;
                            }
                            else{
                                if(catid_integers.get(i) == currentCatIdNumber){
                                    checkNext = true;
                                }
                            }
                            Log.i("HOMEPAGE","   EXISTING CATID: "+catid_integers.get(i));
                        }
                        if(checkNext && nextAvailableCatId == null){
                            if(catid_integers.get(catintsize-1) < 10)
                                nextAvailableCatId = currentCatYear+"-00"+(currentCatIdNumber+k);
                            else if(catid_integers.get(catintsize-1) < 100)
                                nextAvailableCatId = currentCatYear+"-0"+(currentCatIdNumber+k);
                            else
                                nextAvailableCatId = currentCatYear+"-"+(currentCatIdNumber+k);
                        }
                        Log.i("HOMEPAGE","   NEXT AVAILABLE: "+nextAvailableCatId);

                        if(nextAvailableCatId != null) {
                            Log.i("HOMEPAGE", "REASSIGNED CATID TO: " + nextAvailableCatId);
                            ((TextView) catInfoBasic.findViewById(R.id.catbasic_cathome_id_text)).setText(nextAvailableCatId);
                            final Dialog catidChangedDialog = new Dialog(this);
                            catidChangedDialog.setTitle("Nytt ID: " + nextAvailableCatId);
                            LinearLayout oneButtonLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.one_button_dialog, null);
                            Button oneButton = (Button) oneButtonLayout.findViewById(R.id.option1);
                            oneButton.setText("OK");
                            oneButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    catidChangedDialog.dismiss();
                                    confirmSave();
                                }
                            });
                            catidChangedDialog.setCanceledOnTouchOutside(false);
                            catidChangedDialog.setContentView(oneButtonLayout);
                            catidChangedDialog.show();
                        }
                        else{
                            confirmSave();
                        }
                    }

                }
            } catch (JSONException e){
                e.printStackTrace();
            }
            */
        }
        else{
            addCatInfoToPicker(json);
        }
    }

    private void checkSave(){
        //homePageHandler.showSaveformDialog();
        if(addNewCat) {


            currentCatId = ((TextView) catInfoBasic.findViewById(R.id.catbasic_cathome_id_text)).getText().toString();
            if (currentCatId == null || currentCatId.equals("") || currentCatId.replaceAll(" ", "").equals("")) {
                homePageHandler.showMessageDialog("Fel", getString(R.string.errorSave));
                return;
            } else {
                selectedForm = catformtitles[0];
                confirmSave();
            }


        }
        else if(privileged == Type.CATOWNER){
            resetUploadedBasicInfo();
            selectedForm = catformtitles[0];
            confirmSave();
        }
        else{
            confirmSave();
        }
    }

    public boolean isNewPrivateCat(){
        return newPrivateCat;
    }

    public void setNewPrivateCat(boolean newPrivateCat){
        this.newPrivateCat = newPrivateCat;
    }

    private void confirmSave(){
        //addNewCat = false;
        //currentViewedCatname = autoCompleteTextView.getText().toString();

        homePageHandler.showSaveformDialog();
        System.out.println("SAVING FORM: "+selectedForm);
        controller.uploadFormToServer(selectedForm, catformtitles);
        controller.startUpload();
        signalQueueToSend();
        setView(IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged), currentViewedCatname);
        //setView(IndexHolder.authenticateIndex(IndexHolder.FORM_PREVIEW_PAGE, privileged), currentViewedCatname, currentHeaderSecondaryTitle, R.color.newColorPrimary, R.color.newColorPrimary);
    }

    private void goToCatowner(){
        setLoadPreviousCatownerList(true);
        currentPreviewedForm = dbTables.length;
        selectedForm = catformtitles[catformtitles.length-1];
        currentHeaderTitle = selectedForm;
        setView(IndexHolder.FORM_PREVIEW_PAGE, selectedForm);

        //setView(IndexHolder.FORM_PREVIEW_PAGE, currentViewedCatname, "Kattägare", R.color.newColorPrimary, R.color.newColorPrimary);
        new LoadPreviewHandler(this).clearPreview();
        HashMap<String, String> data = new HashMap<>();
        String blub = "CAT_INFO:"+getCurrentCatId();
        data.put("blub", Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP));
        prepareAndSendData(data, Type.DOWNLOAD_LIST);

        //new BottomDialog(this).popup("Funktion under bearbetning");
    }


    public void bitmapCompressed(Bitmap bitmap){

    }

    private void catownerPrivateCatViewSettings(){
        privateCatInfoView.findViewById(R.id.arrived_date_holder).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catid_holder).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.moved_holder).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_neuter_date).setVisibility(View.VISIBLE);
        privateCatInfoView.findViewById(R.id.tatoo_holder).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.chip_holder).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_birthday_date).setVisibility(View.VISIBLE);
        privateCatInfoView.findViewById(R.id.catbasic_select_sex_type).setVisibility(View.VISIBLE);
        privateCatInfoView.findViewById(R.id.catbasic_select_race).setVisibility(View.VISIBLE);
        privateCatInfoView.findViewById(R.id.catbasic_select_color).setVisibility(View.VISIBLE);
        privateCatInfoView.findViewById(R.id.catbasic_select_hair_type).setVisibility(View.VISIBLE);
        ((ImageView)privateCatInfoView.findViewById(R.id.info_cat_photo)).setImageResource(android.R.color.transparent);
        ((TextView)privateCatInfoView.findViewById(R.id.catbasic_neuter_date_text)).setText("");
        ((TextView)privateCatInfoView.findViewById(R.id.catbasic_birthday_date_text)).setText("");
        ((TextView)privateCatInfoView.findViewById(R.id.catbasic_birthday_guess)).setText("");
        ((TextView)privateCatInfoView.findViewById(R.id.catbasic_sex_type_text)).setText("");
        ((TextView)privateCatInfoView.findViewById(R.id.catbasic_race_text)).setText("");
        ((TextView)privateCatInfoView.findViewById(R.id.catbasic_color_text)).setText("");
        ((TextView)privateCatInfoView.findViewById(R.id.catbasic_hair_type)).setText("");
    }
    private void catownerCatViewSettings(){
        privateCatInfoView.findViewById(R.id.catbasic_select_arrived_date).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_catid).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_moved_date).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_neuter_date).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_tatoo_date_and_text).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_chip_date).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_birthday_date).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_sex_type).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_race).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_color).setVisibility(View.GONE);
        privateCatInfoView.findViewById(R.id.catbasic_select_hair_type).setVisibility(View.GONE);
    }






    public void addObject(View view){
        boolean isCatOwner = false;
        switch (privileged){
            case CATOWNER:
                isCatOwner = true;
                currentViewedCatname = "Ny egen katt";
                currentHeaderSecondaryTitle = "Allmän Info";
                newPrivateCat = true;

                catownerPrivateCatViewSettings();
                setView(IndexHolder.authenticateIndex(IndexHolder.PRIVATE_CAT_INFO_PAGE, privileged), currentViewedCatname);
                //setView(IndexHolder.authenticateIndex(IndexHolder.PRIVATE_CAT_INFO_PAGE, privileged), currentViewedCatname, currentHeaderSecondaryTitle, R.color.newColorPrimary, R.color.newColorPrimary);
                break;
            default:break;
        }
        if(isCatOwner) return;

        if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.HOMEPAGE_INDEX, privileged) || currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.CATLIST_PAGE, privileged)){
            clearForms();
            addNewCat = true;
            currentViewedCatname = "Ny katt";
            currentViewedCatImageFileName = null;
            setView(IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged), currentViewedCatname);
            //setView(IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged), currentViewedCatname, catformtitles[0], R.color.newColorPrimary, R.color.newColorPrimary);
        }
        else if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.OWNER_LIST_PAGE, privileged)){

            homePageHandler.getCatownerManager().newCatownerForm();
            /*
            addNewCatowner = true;
            catOwnerInfo.findViewById(R.id.confirmSaveField).setVisibility(View.VISIBLE);
            homePageHandler.getCatownerManager().clearCatownerInfo();
            //getChosenCatFromListAdapter().setCatInfoList(new ArrayList<String[]>());
            //getChosenCatFromListAdapter().notifyDataSetChanged();
            //getConfirmChosenCatFromListAdapter().setCatInfoList(new ArrayList<String[]>());
            //getConfirmChosenCatFromListAdapter().notifyDataSetChanged();
            setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_INFO_PAGE, privileged), "Ny kattägare");
            */
            //setView(IndexHolder.authenticateIndex(IndexHolder.OWNER_INFO_PAGE, privileged), "Ny kattägare", null, R.color.newColorPrimary, R.color.newColorPrimary);
        }
        else if(currentFlippedViewIndex == IndexHolder.authenticateIndex(IndexHolder.PERSONAL_LIST_PAGE, privileged)){
            addNewPersonal = true;
            //homePageHandler.getPersonelManager().clearPersonelInfo();

            homePageHandler.getPersonelManager().newPersonelForm();
            //setView(IndexHolder.authenticateIndex(IndexHolder.PERSONAL_INFO_PAGE, privileged), "Ny personal");
            //setView(IndexHolder.authenticateIndex(IndexHolder.PERSONAL_INFO_PAGE, privileged), "Ny personal", null, R.color.newColorPrimary, R.color.newColorPrimary);
        }
        /*
        switch (currentFlippedViewIndex){
            case IndexHolder.HOMEPAGE_INDEX:
                clearForms();
                addNewCat = true;
                currentViewedCatname = "Ny katt";
                currentViewedCatImageFileName = null;
                setView(IndexHolder.authenticateIndex(IndexHolder.FIRST_FORMPAGE_INDEX, privileged), currentViewedCatname, catformtitles[0], R.color.newColorPrimary, R.color.newColorPrimary);
                break;
            case IndexHolder.OWNER_LIST_PAGE:
                addNewCatowner = true;
                homePageHandler.getCatownerManager().clearCatownerInfo();

                //catOwnerInfo.findViewById(R.id.catowner_info_buttons).setVisibility(View.GONE);
                //catOwnerInfo.findViewById(R.id.catowners_cats).setVisibility(View.VISIBLE);
                getChosenCatFromListAdapter().setCatInfoList(new ArrayList<String[]>());
                getChosenCatFromListAdapter().notifyDataSetChanged();
                setView(IndexHolder.OWNER_INFO_PAGE, "Ny kattägare", null, R.color.newColorPrimary, R.color.newColorPrimary);
                break;
            case IndexHolder.PERSONAL_LIST_PAGE:
                addNewPersonal = true;
                homePageHandler.getPersonelManager().clearPersonelInfo();
                setView(IndexHolder.PERSONAL_INFO_PAGE, "Ny personal", null, R.color.newColorPrimary, R.color.newColorPrimary);
                break;
            default:break;
        }
        */

    }

    public boolean getNewCat(){
        return addNewCat;
    }

    public void setNewCat(boolean new_cat){
        addNewCat = new_cat;
    }

    public void isNewCat(boolean new_cat){
        addNewCat = new_cat;
    }

    public boolean getNewCatOwner(){
        return addNewCatowner;
    }

    public void setAddNewPersonal(boolean addNewPersonal){
        this.addNewPersonal = addNewPersonal;
    }



    private void setDeviceOrientation(int checkid){
        switch (checkid){
            case R.id.portraitmode:
                DEVICE_ORIENTATION = 0;
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            case R.id.landscapemode:
                DEVICE_ORIENTATION = 1;
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                System.out.println("LANDSCAPE MODE");
                break;
            case R.id.dynamicmode:
                DEVICE_ORIENTATION = 2;
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                break;
            default:break;
        }
    }


    public void changeDeviceOrientation(View view){
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Ändra enhetens läge");
        RelativeLayout optionLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.screenorientationoptions, null);
        RadioGroup radioGroup = (RadioGroup) optionLayout.findViewById(R.id.orientationGroup);
        switch (DEVICE_ORIENTATION){
            case 0:
                radioGroup.check(R.id.portraitmode);
                break;
            case 1:
                radioGroup.check(R.id.landscapemode);
                break;
            case 2:
                radioGroup.check(R.id.dynamicmode);
                break;
            default:break;
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                System.out.println("ID: "+checkedId);
                setDeviceOrientation(checkedId);
                dialog.dismiss();
            }
        });
        dialog.setContentView(optionLayout);
        dialog.show();
    }
    public void logout(View view){
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Vill du logga ut?");
        LinearLayout contentLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.new_confirm_exit_dialog, null);
        contentLayout.findViewById(R.id.abort_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        contentLayout.findViewById(R.id.logout_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Email", null);
                editor.putString("CalendarEmail", null);
                editor.putString("Password", null);
                editor.putString("Privilige", null);
                editor.commit();
                Intent loginPage = new Intent(HomePage.this, LoginPage.class);
                loginPage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginPage);
            }
        });
        /*
        LinearLayout contentLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.general_confirm_dialog, null);
        Button abortButton = (Button) contentLayout.findViewById(R.id.abort);
        abortButton.setText("Avbryt");
        Button confirmButton = (Button) contentLayout.findViewById(R.id.confirm);
        confirmButton.setText("Logga ut");
        abortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Email", null);
                editor.putString("CalendarEmail", null);
                editor.putString("Password", null);
                editor.putString("Privilige", null);
                editor.commit();
                Intent loginPage = new Intent(HomePage.this, LoginPage.class);
                loginPage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginPage);
            }
        });
        */
        dialog.setContentView(contentLayout);
        dialog.show();

    }

}
