package kattcenter.norrtljekattcenter;

/**
 * Created by Joakim on 2017-06-27.
 */

public class PriviligeHolder {

    public static Type type;

    public static void setType(Type type){
        PriviligeHolder.type = type;
    }

    public static Type getType(){
        return PriviligeHolder.type;
    }
}
