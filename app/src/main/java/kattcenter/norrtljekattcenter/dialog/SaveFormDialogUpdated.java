package kattcenter.norrtljekattcenter.dialog;

import android.app.Dialog;
import android.content.Context;
import android.widget.RelativeLayout;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Joakim on 2017-07-03.
 */

public class SaveFormDialogUpdated {

    private Context context;
    private Dialog dialog;

    public SaveFormDialogUpdated(Context context){
        this.context = context;
        dialog = new Dialog(this.context);
        dialog.setTitle("Sparar");
        RelativeLayout progressBarContent = (RelativeLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.savestatusdialogcontent, null);
        dialog.setContentView(progressBarContent);
    }

    public boolean isShowing(){
        return dialog != null && dialog.isShowing();
    }

    public void showDialog(){
        dialog.show();
    }

    public void closeDialog(){
        if(isShowing())
            dialog.dismiss();
    }
}
