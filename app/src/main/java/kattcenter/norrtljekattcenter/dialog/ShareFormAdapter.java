package kattcenter.norrtljekattcenter.dialog;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.SaveformHandler;
import kattcenter.norrtljekattcenter.catlist.ChosenCatFromListAdapter;

/**
 * Created by Joakim on 2017-07-07.
 */

public class ShareFormAdapter extends BaseAdapter{

    private Context context;
    private SaveformHandler saveformHandler;
    private HashMap<String, Boolean> checkedLockedForms = new HashMap<>();
    private HashMap<String, Boolean> checkedMailForms = new HashMap<>();
    private HashMap<String, Boolean> shareForms = new HashMap<>();
    private HashMap<String, View> rows = new HashMap<>();
    private HashMap<String, Boolean> lockedForms = null;
    private ArrayList<String> forms;
    private String[] dbTables;
    private View parentView;
    private boolean allSelected = false;
    private int totalChecked = 0;

    public ShareFormAdapter(Context context, View parentView, ArrayList<String> forms, SaveformHandler saveformHandler){
        this.context = context;
        this.parentView = parentView;
        this.forms = forms;
        this.saveformHandler = saveformHandler;
        for(String s: forms){
            shareForms.put(s, false);
        }
    }

    /*
    public ShareFormAdapter(Context context, View parentView, ArrayList<String> forms, String[] dbTables, SaveformHandler saveformHandler){
        this.context = context;
        this.parentView = parentView;
        this.forms = forms;
        this.saveformHandler = saveformHandler;
        this.dbTables = dbTables;
        lockedForms = new HashMap<>();
        for(String s: forms){
            checkedLockedForms.put(s, false);
            checkedMailForms.put(s, false);
        }
        for(String s: dbTables){
            lockedForms.put(s, false);
        }

        this.saveformHandler.setCheckedForms(checkedLockedForms);
    }
    */

    public View getRowByFormTitle(String title){
        return rows.get(title);
    }

    public void setLockedForms(HashMap<String, Boolean> lockedForms){
        this.lockedForms = lockedForms;
    }

    public void setAllCheckBoxesSelected(boolean selected){
        allSelected = selected;
    }

    public HashMap<String, Boolean> getShareForms(){
        return shareForms;
    }

    @Override
    public int getCount() {
        return forms.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent){
        final String form = forms.get(position);
        if(convertView == null){
            convertView = ((HomePage) context).getLayoutInflater().inflate(R.layout.share_form_dialog_row, null);
        }
        //String dbTable = dbTables[position];

        /*
        CheckBox lock_checkBox = (CheckBox) convertView.findViewById(R.id.form_locker_checkbox);
        if(!((HomePage)context).isFormEnabled(dbTable)){
            lock_checkBox.setEnabled(false);
        }
        */



        ((TextView)convertView.findViewById(R.id.share_row_title)).setText(form);

        CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.form_checkbox);
        checkBox.setChecked(allSelected);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) totalChecked++;
                else{
                    if(totalChecked > 0) totalChecked--;
                }
                shareForms.put(form, isChecked);
                checkTotalCheckedForms();
            }
        });


        if(checkBox.isChecked()) totalChecked++;
        else{
            if(totalChecked > 0) totalChecked--;
        }
        shareForms.put(form, checkBox.isChecked());
        checkTotalCheckedForms();

        //final LinearLayout row = (LinearLayout) convertView.findViewById(R.id.rowLinearLayout);
        //final TextView lockStatus = (TextView) convertView.findViewById(R.id.share_row_title);

        /*
        lock_checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkedLockedForms.put(form, isChecked);
                int amount_checked = 0;

                Set<String> keys = checkedLockedForms.keySet();
                HashMap<String, String> titlesToDBTables = ((HomePage)context).getTitlesToDbtables();
                for(String s: keys){
                    if(checkedLockedForms.get(s)){
                        amount_checked++;
                    }
                }
                if(amount_checked > 0){
                    (parentView.findViewById(R.id.lockForms)).setBackgroundColor(Color.parseColor("#555555"));
                    (parentView.findViewById(R.id.lockForms)).setEnabled(true);
                    //(parentView.findViewById(R.id.mailForms)).setBackgroundColor(Color.parseColor("#555555"));
                    //(parentView.findViewById(R.id.mailForms)).setEnabled(true);
                }
                else{
                    (parentView.findViewById(R.id.lockForms)).setBackgroundColor(Color.parseColor("#AAAAAA"));
                    (parentView.findViewById(R.id.lockForms)).setEnabled(false);
                    //(parentView.findViewById(R.id.mailForms)).setBackgroundColor(Color.parseColor("#AAAAAA"));
                    //(parentView.findViewById(R.id.mailForms)).setEnabled(false);
                }
                rows.put(form, row);
                saveformHandler.setCheckedForms(checkedLockedForms);
            }
        });
        */



        return convertView;
    }

    private void checkTotalCheckedForms(){
        if(totalChecked > 0){
            (parentView.findViewById(R.id.shareSelectForms)).setBackgroundColor(Color.parseColor("#555555"));
            (parentView.findViewById(R.id.shareSelectForms)).setEnabled(true);
        }
        else{
            (parentView.findViewById(R.id.shareSelectForms)).setBackgroundColor(Color.parseColor("#AAAAAA"));
            (parentView.findViewById(R.id.shareSelectForms)).setEnabled(false);
        }
    }
}
