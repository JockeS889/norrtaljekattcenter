package kattcenter.norrtljekattcenter.dialog;

import android.app.Dialog;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Joakim on 2017-07-04.
 */

public class MessageDialog {

    private Context context;
    private Dialog messageDialog = null;
    public MessageDialog(Context context){
        this.context = context;
    }

    public void showDialog(String title, String message){
        messageDialog = new Dialog(context);
        messageDialog.setTitle(title);
        TextView textView = (TextView)((HomePage)context).getLayoutInflater().inflate(R.layout.my_simple_textview, null);
        textView.setText(message);
        messageDialog.setContentView(textView);
        messageDialog.show();
    }

    public void closeDialog(){
        if(messageDialog != null && messageDialog.isShowing()){
            messageDialog.dismiss();
        }
    }
}
