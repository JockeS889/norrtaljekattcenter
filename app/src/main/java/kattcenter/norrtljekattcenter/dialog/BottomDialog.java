package kattcenter.norrtljekattcenter.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-23.
 */

public class BottomDialog {

    private Context context;
    public BottomDialog(Context context){
        this.context = context;
    }

    public void popup(String message){
        final Dialog bottomDialog = new Dialog(context, R.style.MaterialDialogSheet);
        RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_1, null);
        relativeLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
        TextView textView = (TextView) relativeLayout.findViewById(R.id.listEntry);
        textView.setText(message);
        textView.setGravity(Gravity.CENTER);
        bottomDialog.setContentView(relativeLayout);
        //bottomDialog.setCancelable(true);
        bottomDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
        bottomDialog.show();


        final Handler dialogHandler = new Handler();
        final Runnable dialogRunnable = new Runnable() {
            @Override
            public void run() {
                if(bottomDialog.isShowing()){
                    bottomDialog.dismiss();
                }
            }
        };

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialogHandler.removeCallbacks(dialogRunnable);
            }
        });

        dialogHandler.postDelayed(dialogRunnable, 4000);
    }
}
