package kattcenter.norrtljekattcenter.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.SaveformAdapter;
import kattcenter.norrtljekattcenter.SaveformHandler;

/**
 * Created by Joakim on 2017-06-26.
 */

public class SaveFormDialog {
    /*

    private Context context;
    private HomePage homePage;
    private Dialog saveFormsDialog;
    private SaveformAdapter saveformAdapter;
    private HashMap<String, String> titlesToDbtables;
    private String[] catformtitles;
    private String[] catformDbTables;

    public SaveFormDialog(Context context){
        this.context = context;
        homePage = (HomePage) this.context;
        saveFormsDialog = new Dialog(context);
        titlesToDbtables = homePage.getTitlesToDbtables();
        catformtitles = homePage.getCatformtitles();
        catformDbTables = homePage.getDbTables();
    }

    public void show(){
        String catID = ((TextView)homePage.getCatInfoBasic().findViewById(R.id.catbasic_cathome_id_text)).getText().toString();
        if(catID == null || catID.equals("") || catID.replaceAll(" ", "").equals("")){
            saveFormsDialog.setTitle("Fel");
            RelativeLayout relativeLayout = (RelativeLayout) homePage.getLayoutInflater().inflate(R.layout.list_entry_textview_1, null);
            ((TextView)relativeLayout.findViewById(R.id.listEntry)).setText(context.getString(R.string.errorSave));
            saveFormsDialog.setContentView(relativeLayout);
            saveFormsDialog.show();
            return;
        }


        RelativeLayout saveLockDialog = (RelativeLayout) homePage.getLayoutInflater().inflate(R.layout.save_lock_dialog, null);

        saveFormsDialog.setTitle("Hantera");
        ListView listView = (ListView) saveLockDialog.findViewById(R.id.formList);
        ArrayList<String> catForms = new ArrayList<>();

        for(String s: catformtitles) {
            //if(homePage.isFormEnabled(titlesToDbtables.get(s)))
            if(!s.equals("Historik"))
                catForms.add(s);
        }
        final SaveformHandler saveformHandler = new SaveformHandler(context);
        //final ArrayAdapter<String> formListAdapter = new SaveformAdapter(this, catForms, saveformHandler);
        saveformAdapter = new SaveformAdapter(context, saveLockDialog, catForms, catformDbTables, saveformHandler);
        //saveformHandler.setCheckedForms();
        listView.setAdapter(saveformAdapter);
        final Button done = (Button) saveLockDialog.findViewById(R.id.abortSave);
        final Button lockForms = (Button) saveLockDialog.findViewById(R.id.lockForms);
        final Button mailForms = (Button) saveLockDialog.findViewById(R.id.mailForms);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFormsDialog.dismiss();
            }
        });
        lockForms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lockForms.setBackgroundColor(Color.parseColor("#AAAAAA"));
                lockForms.setEnabled(false);
                mailForms.setBackgroundColor(Color.parseColor("#AAAAAA"));
                mailForms.setEnabled(false);
                HashMap<String, Boolean> checkedForms = saveformHandler.getCheckedForms();
                Set<String> keys = checkedForms.keySet();
                int formIndex = 0;
                for(String s: keys){
                    if(checkedForms.get(s)){
                        LinearLayout dialog_row = (LinearLayout) saveformAdapter.getRowByFormTitle(s);
                        (dialog_row.findViewById(R.id.form_locker_progressbar)).setVisibility(View.VISIBLE);
                        homePage.getUploadingForm().put(titlesToDbtables.get(s), dialog_row);
                    }
                }
                homePage.getController().clearUploadQueue();
                for(String s: keys){
                    if(checkedForms.get(s)){
                        //FORM IS SELECTED TO LOCK (SAVE/UPLOAD TO DATABASE)
                        System.out.println("UPLOADING FORM: "+s);
                        homePage.getController().uploadFormToServer(s, catformtitles);
                    }
                    formIndex++;
                }
                homePage.getController().startUpload();
                homePage.signalQueueToSend();
                done.setBackgroundColor(Color.parseColor("#AAAAAA"));
                done.setEnabled(false);

            }
        });

        mailForms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Boolean> checkedMailForms = saveformHandler.getCheckedMailForms();
                saveformHandler.mail(checkedMailForms);
                saveFormsDialog.dismiss();
            }
        });

        if(saveFormsDialog != null && !saveFormsDialog.isShowing()) {
            saveFormsDialog.setContentView(saveLockDialog);
            saveFormsDialog.show();
        }
    }

    public void close(){
        saveFormsDialog.dismiss();
    }

    public Dialog getSaveFormsDialog(){
        return saveFormsDialog;
    }

    public boolean isDialogShowing(){
        return saveFormsDialog != null && saveFormsDialog.isShowing();
    }
    */
}
