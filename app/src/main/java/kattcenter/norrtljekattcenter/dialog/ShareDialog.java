package kattcenter.norrtljekattcenter.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.SaveformHandler;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DataPacket;

/**
 * Created by Joakim on 2017-07-07.
 */

public class ShareDialog {

    private Context context;
    private String selectedOption = null;
    private String selectedForms = null;
    private Uri fileUri = null;
    private ShareFormAdapter shareFormAdapter;
    private Dialog loadingDialog = null;
    private String shareOption = null;
    private SaveformHandler saveFormHandler = null;
    private HashMap<String, Boolean> helpFormMap = new HashMap<>();
    private boolean currentFormSelected = false;

    public ShareDialog(Context context){
        this.context = context;
    }

    public void showShareOptionDialog(String option){
        /*
        selectedOption = null;
        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Dela");
        LinearLayout linearLayout = (LinearLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.three_button_dialog, null);
        Button mailPDF = (Button) linearLayout.findViewById(R.id.option1);
        mailPDF.setText("Maila PDF");
        mailPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedOption = "MAIL";
                dialog.dismiss();
            }
        });
        Button createPDF = (Button) linearLayout.findViewById(R.id.option2);
        createPDF.setText("Skapa endast PDF");
        createPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedOption = "PDF";
                dialog.dismiss();
            }
        });
        Button abort = (Button) linearLayout.findViewById(R.id.option3);
        abort.setText("Avbryt");
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                showSelectedShareOptionCatformSelectionDialog(selectedOption);
                //Log.i("SHAREDIALOG", "Selected Option: "+selectedOption);
            }
        });
        dialog.setContentView(linearLayout);
        dialog.show();
        */
        showSelectedShareOptionCatformSelectionDialog(option);
    }

    private void showSelectedShareOptionCatformSelectionDialog(final String shareOption){
        if(shareOption == null) return;
        selectedForms = null;
        final Dialog dialog = new Dialog(context);
        /*
        switch (shareOption){
            case "PDF":
                dialog.setTitle("Skapa PDF");
                break;
            case "MAIL":
                dialog.setTitle("Maila Formulär");
                break;
            default:break;
        }
        */
        LinearLayout linearLayout = (LinearLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.new_dialog_share_option_selected, null);
        linearLayout.findViewById(R.id.all_form_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selectedForms = "ALL";
                selectFormForSharedOptionDialog(shareOption);
            }
        });
        linearLayout.findViewById(R.id.current_form_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selectedForms = "CURRENT";
                selectFormForSharedOptionDialog(shareOption);
            }
        });
        linearLayout.findViewById(R.id.share_main_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((HomePage)context).getHomePageHandler().showShareOptionsDialog();
            }
        });
        linearLayout.findViewById(R.id.close_share_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        //LinearLayout linearLayout = (LinearLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.three_button_dialog, null);
        //linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60));
        /*
        Button opt1 = (Button) linearLayout.findViewById(R.id.option1);
        opt1.setText("Aktuellt formulär");
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //selectedOption = "MAIL";
                selectedForms = "CURRENT";
                dialog.dismiss();
            }
        });
        Button opt2 = (Button) linearLayout.findViewById(R.id.option2);
        opt2.setText("Välj bland alla formulär");
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //selectedOption = "PDF";
                selectedForms = "ALL";
                dialog.dismiss();
            }
        });
        Button abort = (Button) linearLayout.findViewById(R.id.option3);
        abort.setText("Avbryt");
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                selectFormForSharedOptionDialog(shareOption);
            }
        });
        */
        dialog.setContentView(linearLayout);
        dialog.show();
    }

    private void selectFormForSharedOptionDialog(final String shareOption){
        if(selectedForms == null) return;
        fileUri = null;
        saveFormHandler = new SaveformHandler(context);
        ArrayList<String> forms = new ArrayList<>();
        String[] formTitles = ((HomePage)context).getCatformtitles();
        for(String s: formTitles){
            System.out.println("SHARE FORM: "+s);
            forms.add(s);
        }
        if(selectedForms.equals("CURRENT")){
            ShareDialog.this.shareOption = shareOption;
            String secTitle = ((HomePage)context).getCurrentHeaderSecondaryTitle();

            for(String s: forms){
                helpFormMap.put(s, s.equals(secTitle));
                Log.i("SHAREDIALOG", "FORM: "+s+" SECTTITLE: "+secTitle);
            }
            currentFormSelected = true;
            //Log.i("SHAREDIALOG", "CURRENT: "+secTitle);

            ((HomePage)context).setLoadPreview(false);
            ((HomePage)context).setLoadPdfGenerator(true);
            ((HomePage) context).getHomePageHandler().showStandardDialog("Förbereder mail....");

            HashMap<String, String> dbTables = ((HomePage)context).getTitlesToDbtables();
            String formName = dbTables.get(secTitle);
            if(formName.equals("catform_vetdoc")){
                String[] catvetdocTables = ((HomePage)context).getCatvetdoctables();
                for(String docTable: catvetdocTables){
                    ((HomePage)context).prepareDataToQueue(docTable);
                }
            }
            else{
                ((HomePage)context).prepareDataToQueue(formName);

            }
            ((HomePage)context).getController().startDownloadForms();




            return;
        }

        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Välj formulär");
        RelativeLayout content = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.share_selectedform_dialog, null);
        ListView listView = (ListView) content.findViewById(R.id.shareFormList);

        shareFormAdapter = new ShareFormAdapter(context, content, forms, saveFormHandler);
        listView.setAdapter(shareFormAdapter);
        final CheckBox chooseAll = (CheckBox) content.findViewById(R.id.chooseAll);
        chooseAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                shareFormAdapter.setAllCheckBoxesSelected(isChecked);
                shareFormAdapter.notifyDataSetChanged();
                if(isChecked){
                    chooseAll.setText("Välj inga");
                }
                else{
                    chooseAll.setText("Välj alla");
                }
            }
        });
        Button confirm = (Button) content.findViewById(R.id.shareSelectForms);
        confirm.setText((shareOption.equals("PDF") ? "Skapa PDF" : "Maila Formulär"));
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showLoadingDialog();
                ShareDialog.this.shareOption = shareOption;
                ((HomePage)context).setLoadPreview(false);
                ((HomePage)context).setLoadPdfGenerator(true);
                HashMap<String, String> dbTables = ((HomePage)context).getTitlesToDbtables();
                HashMap<String, Boolean> selectedShareForms = shareFormAdapter.getShareForms();
                Set<String> keys = selectedShareForms.keySet();
                for(String form: keys){
                    if(selectedShareForms.get(form)){
                        if(form.equals("Kattägare")){
                            Log.i("SHAREDIALOG", "Förbereder formulär: "+form);
                            HashMap<String, String> data = new HashMap<String, String>();
                            String catid = ((HomePage)context).getCurrentCatId();
                            String blub = Base64.encodeToString(("CAT_INFO:"+catid).getBytes(), Base64.NO_WRAP);
                            data.put("blub", blub);
                            ((HomePage)context).prepareData(data, Type.DOWNLOAD_LIST);
                        }
                        else{
                            Log.i("SHAREDIALOG", "Förbereder formulär: "+form +" (DB: "+dbTables.get(form)+")");
                            if(dbTables.get(form).equals("catform_vetdoc")){
                                String[] catvetdocTables = ((HomePage)context).getCatvetdoctables();
                                for(String docTable: catvetdocTables){
                                    ((HomePage)context).prepareDataToQueue(docTable);
                                }
                            }
                            else
                                ((HomePage)context).prepareDataToQueue(dbTables.get(form));
                        }

                    }
                }
                ((HomePage)context).getController().startDownloadForms();
                //preparePDF(shareOption);
                /*
                switch (shareOption){
                    case "PDF":
                        dialog.dismiss();
                        fileUri = saveformHandler.pdf(shareFormAdapter.getShareForms());
                        break;
                    case "MAIL":
                        dialog.dismiss();
                        saveformHandler.mail(shareFormAdapter.getShareForms());
                        break;
                    default:break;
                }
                */
            }
        });
        (content.findViewById(R.id.abortShare)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedForms = null;
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                switch (shareOption){
                    case "PDF":
                        if(selectedForms != null || currentFormSelected)
                            confirmPDF();
                        break;
                    default:break;
                }
            }
        });
        dialog.setContentView(content);
        dialog.show();
    }

    public void showLoadingDialog(){
        loadingDialog = new Dialog(context);
        RelativeLayout progressBar = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.loader_dialog, null);
        loadingDialog.setTitle("Laddar ner formulär...");
        loadingDialog.setContentView(progressBar);
        loadingDialog.show();
    }

    public void closeLoadingDialog(){
        if(loadingDialog != null && loadingDialog.isShowing()){
            loadingDialog.dismiss();
            preparePDF();
        }
        else if(currentFormSelected){
            preparePDF();
            if(shareOption.equals("PDF"))
                confirmPDF();
        }

    }

    public void initPDFDocumentGenerator(){
        saveFormHandler.initPDFDocumentGenerator();
    }

    public void addContentToPDF(String form_table, String content){
        saveFormHandler.addContentToPdfGenerator(form_table, content);
    }

    private void preparePDF(){
        switch (shareOption){
            case "PDF":
                if(shareFormAdapter == null)
                    fileUri = saveFormHandler.pdf(helpFormMap);
                else
                    fileUri = saveFormHandler.pdf(shareFormAdapter.getShareForms());
                break;
            case "MAIL":
                /*
                if(shareFormAdapter == null)
                    saveFormHandler.mail(helpFormMap);
                else
                    saveFormHandler.mail(shareFormAdapter.getShareForms());
                //saveFormHandler.mail(shareFormAdapter.getShareForms());
                */
                saveFormHandler.mail2();

                break;
            default:break;
        }
    }

    private void confirmPDF(){
        String message;
        if(fileUri == null) message = "PDF kunde inte skapas";
        else message = "PDF skapad: "+fileUri.getPath();
        new BottomDialog(context).popup(message);
    }
}
