package kattcenter.norrtljekattcenter.dialog;

import android.app.Dialog;
import android.content.Context;
import android.widget.RelativeLayout;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Joakim on 2017-06-25.
 */

public class StandardDialog {

    private Context context;
    private Dialog loadingDialog = null;
    public StandardDialog(Context context){
        this.context = context;
    }

    public void showLoadingDialog(String title){
        loadingDialog = new Dialog(context);
        loadingDialog.setTitle(title);
        RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.loader_dialog, null);
        loadingDialog.setContentView(relativeLayout);
        loadingDialog.show();
    }

    public void closeLoadingDialog(){
        if(loadingDialog != null && loadingDialog.isShowing()){
            loadingDialog.dismiss();
        }
    }
}
