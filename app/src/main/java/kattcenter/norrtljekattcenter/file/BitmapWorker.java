package kattcenter.norrtljekattcenter.file;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;

import kattcenter.norrtljekattcenter.HomePage;

/**
 * Created by Jocke on 2017-06-18.
 */

public class BitmapWorker extends AsyncTask<Void, Void, Bitmap> {

    private Context context;
    public BitmapWorker(Context context){
        this.context = context;
    }
    @Override
    protected Bitmap doInBackground(Void... params) {
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap){
        ((HomePage)context).bitmapCompressed(bitmap);
    }


    private void compressBitmap(){
        /*
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        basicInfoBundle.putByteArray("CATPHOTO", byteArrayOutputStream.toByteArray());
        */
    }
}
