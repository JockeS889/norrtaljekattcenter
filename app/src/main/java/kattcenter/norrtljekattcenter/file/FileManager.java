package kattcenter.norrtljekattcenter.file;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import kattcenter.norrtljekattcenter.Type;

/**
 * Created by Jocke on 2017-06-17.
 */

public class FileManager {

    private Type privilege;
    public FileManager(Type privilege){
        this.privilege = privilege;
    }

    public void createDir(){
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        if(!rootDir.exists()){
            try {
                rootDir.mkdir();
            } catch (SecurityException security){
                security.printStackTrace();
                Log.i("FILEMANAGER", "Security issue");
            }
        }

        Log.i("FILEMANAGER", "ROOTDIR: "+ rootDir+" EXISTS: "+rootDir.exists());
        if(!rootDir.exists()){
            try {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Log.i("FILEMANAGER", "Current OS VERSION = NOUGAT");
                    rootDir.createNewFile();
                    Log.i("FILEMANAGER", "ROOTDIR: "+ rootDir+" EXISTS: "+rootDir.exists());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            case CATOWNER: privDir = new File(rootDir, "Catowner"); break;
            default: break;
        }
        if(!privDir.exists()){
            privDir.mkdir();
        }
        Log.i("FILEMANAGER", "PRIVDIR: "+ privDir+" EXISTS: "+privDir.exists());
        File imageDir = new File(privDir, "Images");
        if(!imageDir.exists()){
            imageDir.mkdir();
        }
        File metadataDir = new File(privDir, "Metadata");
        if(!metadataDir.exists()){
            metadataDir.mkdir();
        }
        if(privilege == Type.PERSONAL || privilege == Type.ADMIN){
            File samplesDir = new File(privDir, "Samples");
            if(!samplesDir.exists()){
                samplesDir.mkdir();
            }
            File vetdocDir = new File(privDir, "VetDocs");
            if(!vetdocDir.exists()){
                vetdocDir.mkdir();
            }
            else{
                Log.i("FILEMANAGER", "------------------------------------------- EMPTYING VET DOC FOLDER ----------------------");
                emptyDirContent(getVetDocDir());
                emptyDirContent(getSkkDocDir());
            }
            File skkDocDir = new File(privDir, "SKKDocs");
            if(!skkDocDir.exists()){
                skkDocDir.mkdir();
            }
        }
    }

    public File getImageDir(){
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            case CATOWNER: privDir = new File(rootDir, "Catowner"); break;
            default: break;
        }
        File imageDir = new File(privDir, "Images");
        if(imageDir.exists()) return imageDir;
        else return null;
    }

    public File getSamplesDir(){
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            default: break;
        }
        if(privDir == null) return null;
        File samplesDir = new File(privDir, "Samples");
        if(samplesDir.exists()) return samplesDir;
        else return null;
    }

    public File getVetDocDir(){
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            default: break;
        }
        if(privDir == null) return null;
        File vetDocDir = new File(privDir, "VetDocs");
        if(vetDocDir.exists()) return vetDocDir;
        else return null;
    }
    public File getSkkDocDir(){
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            default: break;
        }
        if(privDir == null) return null;
        File skkDocDir = new File(privDir, "SKKDocs");
        if(skkDocDir.exists()) return skkDocDir;
        else return null;
    }

    public boolean emptyDirContent(File dir){
        /*
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            default: break;
        }
        if(privDir == null) return false;
        File vetDocDir = new File(privDir, "VetDocs");
        */

        if(dir.exists()){
            String[] contentFiles = dir.list();
            if(contentFiles != null && contentFiles.length > 0) {
                for (String contentFile : contentFiles) {
                    new File(dir, contentFile).delete();
                }
            }
        }
        else return false;
        if(dir.list() == null || dir.list().length == 0) return true;
        else return false;
    }

    public boolean deleteFromDocDir(File dir, int tag){
        //File dir = getVetDocDir();
        if(dir != null){
            String[] contentFiles = dir.list();
            if(contentFiles != null && contentFiles.length > 0) {
                for (String fileName : contentFiles) {
                    int fileTag = Integer.parseInt(fileName.split("--")[1]);
                    if (fileTag == tag) {
                        new File(dir, fileName).delete();
                        return true;
                    }
                }
            }
            return false;
        }
        else{
            return false;
        }
    }

    public boolean saveSampleToLocalStorage(Context context, Uri from_file_uri, String from_filename, String to_filename, boolean overwrite) throws IOException{
        System.out.println("SAVING "+from_filename+" TO LOCAL STORAGE");
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        Log.i("FILEMANAGER", "ROOTDIR: "+rootDir);
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            case CATOWNER: privDir = new File(rootDir, "Catowner"); break;
            default: break;
        }
        Log.i("FILEMANAGER", "PRIVDIR: "+privDir+" EXISTS: "+privDir.exists());
        if(!privDir.exists()) return false;
        File sampleDir = new File(privDir, "Samples");
        Log.i("FILEMANAGER", "SAMPLEDIR: "+sampleDir);
        if(!sampleDir.exists()) return false;
        File file = new File(sampleDir, to_filename);
        Log.i("FILEMANAGER", "FILE: "+file);
        if(file.exists() && !overwrite) return false;
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        /*
        String uri_path = from_file_uri.getPath();
        String new_uri_path = getRealPathFromURI(context, from_file_uri);
        Log.i("FILEMANAGER", "OLD PATH: "+uri_path+"    NEW PATH: "+new_uri_path);
        uri_path = uri_path.substring(0, uri_path.lastIndexOf('/')+1) + from_filename;
        File sample = new File(uri_path);
        FileInputStream fileInputStream = new FileInputStream(sample);
        */
        InputStream fileInputStream = context.getContentResolver().openInputStream(from_file_uri);
        byte[] buf = new byte[1024];
        int len;
        while ((len = fileInputStream.read(buf)) > 0) {
            fileOutputStream.write(buf, 0, len);
        }
        //bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        fileInputStream.close();
        return true;
    }

    public Object[] saveDocToFolder(Context context, Uri from_file_uri, String from_filename, String to_filename, Type fileType, boolean overwrite) throws IOException{
        System.out.println("SAVING "+from_filename+" TO LOCAL STORAGE");
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        Log.i("FILEMANAGER", "ROOTDIR: "+rootDir);
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            case CATOWNER: privDir = new File(rootDir, "Catowner"); break;
            default: break;
        }
        Log.i("FILEMANAGER", "PRIVDIR: "+privDir+" EXISTS: "+privDir.exists());
        if(!privDir.exists()) return new Object[]{false, 0};
        File file = null;
        File fileParentDir = null;
        int fileIncrementer = 0;
        switch (fileType){
            case SAMPLE_FILE:
                fileParentDir = new File(privDir, "Samples");
                Log.i("FILEMANAGER", "SAMPLEDIR: "+fileParentDir);
                if(!fileParentDir.exists()) return new Object[]{false, fileIncrementer};
                file = new File(fileParentDir, to_filename);
                break;
            case VETDOC_FILE:
                fileIncrementer = getVetDocDir().list().length + 1;

                //String[] to_filename_parts = to_filename.split(".");
                Log.i("FILEMANAGER", "FILENAME: "+to_filename+" INCREMENTER: "+fileIncrementer);
                to_filename = to_filename + "--"+fileIncrementer + "--.pdf";
                fileParentDir = new File(privDir, "VetDocs");
                Log.i("FILEMANAGER", "VETDOC: "+fileParentDir);
                if(!fileParentDir.exists()) return new Object[]{false, fileIncrementer};
                file = new File(fileParentDir, to_filename);
                break;
            case SKKDOC_FILE:
                fileIncrementer = getSkkDocDir().list().length + 1;
                to_filename = to_filename + "--"+fileIncrementer + "--.pdf";
                fileParentDir = new File(privDir, "SKKDocs");
                Log.i("FILEMANAGER", "SSKDOCS: "+fileParentDir);
                if(!fileParentDir.exists()) return new Object[]{false, fileIncrementer};
                file = new File(fileParentDir, to_filename);
                break;
            default:return new Object[]{false, fileIncrementer};
        }
        Log.i("FILEMANAGER", "FILE: "+file);
        if(file.exists() && !overwrite) return new Object[]{false, fileIncrementer};
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        /*
        String uri_path = from_file_uri.getPath();
        String new_uri_path = getRealPathFromURI(context, from_file_uri);
        Log.i("FILEMANAGER", "OLD PATH: "+uri_path+"    NEW PATH: "+new_uri_path);
        uri_path = uri_path.substring(0, uri_path.lastIndexOf('/')+1) + from_filename;
        File sample = new File(uri_path);
        FileInputStream fileInputStream = new FileInputStream(sample);
        */
        InputStream fileInputStream = context.getContentResolver().openInputStream(from_file_uri);
        byte[] buf = new byte[1024];
        int len;
        while ((len = fileInputStream.read(buf)) > 0) {
            fileOutputStream.write(buf, 0, len);
        }
        //bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        fileInputStream.close();
        return new Object[]{true, fileIncrementer};
    }

    public boolean saveFileToDownloadFolder(){
        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if(downloadDir.exists()){

        }
        return false;
    }

    public File getDownloadFolder(){
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    }



    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] file_proj = { MediaStore.Files.FileColumns.DATA };
            String[] proj = { MediaStore.Images.Media.DATA };
            String[] gen_proj = null;

            String selectionMimeType = MediaStore.Files.FileColumns.MIME_TYPE + "=?";
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension("pdf");
            String[] selectionArgsPdf = new String[]{ mimeType };
            String sortOrder = null;
            cursor= context.getContentResolver().query(contentUri, gen_proj, selectionMimeType, selectionArgsPdf, sortOrder);
            cursor.moveToFirst();
            //Log.i("FILEMANAGER", cursor.toString());
            /*
            for(int i = 0; i < cursor.getColumnCount(); i++){
                Log.i("FILEMANAGER", "CURSOR: "+cursor.getString(i));

            }
            try{
                Log.i("FILEMANAGER", "INDEX: "+cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DISPLAY_NAME));
                Log.i("FILEMANAGER", "INDEX: "+cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA));
                Log.i("FILEMANAGER", "INDEX: "+cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MEDIA_TYPE));
            } catch (Exception e){
                e.printStackTrace();
            }
            */
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DISPLAY_NAME);

            //cursor = context.getContentResolver().query(contentUri, file_proj, null, null, null);
            //int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            //cursor.moveToFirst();
            return cursor.getString(column_index);
            //return null;

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }



    public File getSampleFile(){
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            case CATOWNER: privDir = new File(rootDir, "Catowner"); break;
            default: break;
        }
        File sampleDir = new File(privDir, "Samples");
        if(sampleDir.exists()){
            File file = new File(sampleDir, "sample.pdf");
            return file;
        }
        else return null;
    }

    public File[] getDocuments(Type docType){
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            case CATOWNER: privDir = new File(rootDir, "Catowner"); break;
            default: break;
        }
        File docDir = null;
        switch (docType){
            case VETDOC_FILE:
                docDir = new File(privDir, "VetDocs");
                break;
            case SKKDOC_FILE:
                docDir = new File(privDir, "SKKDocs");
                break;
            default:break;
        }
        if(docDir == null) return null;
        else{
            return docDir.listFiles();
        }
    }

    public String getSampleFileName(){
        return null;
    }

    public File getLatestTakenPicture(){
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            case CATOWNER: privDir = new File(rootDir, "Catowner"); break;
            default: break;
        }
        File imageDir = new File(privDir, "Images");
        if(imageDir.exists()){
            File file = new File(imageDir, "taken_image.jpg");
            return file;
        }
        else return null;
    }

    public File getCurrentViewedCatPicture(){
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            case CATOWNER: privDir = new File(rootDir, "Catowner"); break;
            default: break;
        }
        File imageDir = new File(privDir, "Images");
        if(imageDir.exists()){
            File file = new File(imageDir, "current_cat.jpg");
            return file;
        }
        else return null;
    }

    public boolean saveImageToLocalStorage(String filename, Bitmap bitmap, boolean overwrite) throws IOException {
        System.out.println("SAVING "+filename+" TO LOCAL STORAGE");
        File rootDir = new File(Environment.getExternalStorageDirectory(), "NKC");
        Log.i("FILEMANAGER", "ROOTDIR: "+rootDir);
        File privDir = null;
        switch (privilege){
            case ADMIN:
            case PERSONAL:
                privDir = new File(rootDir, "Personal"); break;
            case CATOWNER: privDir = new File(rootDir, "Catowner"); break;
            default: break;
        }
        Log.i("FILEMANAGER", "PRIVDIR: "+privDir+" EXISTS: "+privDir.exists());
        if(!privDir.exists()) return false;
        File imageDir = new File(privDir, "Images");
        Log.i("FILEMANAGER", "IMAGEDIR: "+imageDir);
        if(!imageDir.exists()) return false;
        File file = new File(imageDir, filename);
        Log.i("FILEMANAGER", "FILE: "+file);
        if(file.exists() && !overwrite) return false;
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        return true;
    }
}
