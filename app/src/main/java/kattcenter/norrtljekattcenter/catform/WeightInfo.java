package kattcenter.norrtljekattcenter.catform;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-03.
 */

public class WeightInfo implements DatePickerDialog.OnDateSetListener{

    private Context context;
    private int year, month, day;
    private Calendar c = Calendar.getInstance();
    private View clickedView;
    private Dialog dialog;

    public WeightInfo(Context context, View clickedView){
        this.context = context;
        this.clickedView = clickedView;
        dialog = new Dialog(context);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void clearButton(){
        final LinearLayout linearLayout = (LinearLayout)clickedView.getParent();
        final TextView clear = (TextView) linearLayout.getChildAt(1);
        clear.setVisibility(View.VISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear.setVisibility(View.GONE);
                ((TextView)linearLayout.getChildAt(0)).setText("");
            }
        });

    }

    public void setDate(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
    }

    public void setWeight(){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        RelativeLayout options = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.input_number_dialog, null);
        Button opt1 = (Button) options.findViewById(R.id.abort);
        Button opt2 = (Button) options.findViewById(R.id.confirm);
        final EditText input_field = (EditText) options.findViewById(R.id.phoneNumber); //REUSE PHONE INPUT FIELD LAYOUT
        input_field.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
        opt1.setText("AVBRYT");
        opt2.setText("LÄGG TILL");
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((TextView)((HomePage)context).findViewById(R.id.catweight_weight_text)).setText(input_field.getText().toString());
                clearButton();

            }
        });
        dialog.setContentView(options);
        dialog.show();
    }

    public void setComment(){
        final InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        dialog.setTitle("Lägg till kommentar");
        final RelativeLayout comment_form = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.comment_form, null);
        final EditText editText = (EditText) comment_form.findViewById(R.id.commentField);
        editText.setText(((TextView)clickedView).getText().toString());
        (comment_form.findViewById(R.id.addCommentButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(text != null && !text.equals("")) {
                    ((TextView) clickedView.findViewById(R.id.catweight_comment_text)).setText(text);
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.INVISIBLE);
                    inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    dialog.dismiss();
                    clearButton();
                }
                else{
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.VISIBLE);
                }
            }
        });

        (comment_form.findViewById(R.id.abortButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                dialog.dismiss();
                inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            }
        });

        dialog.setContentView(comment_form);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        switch (clickedView.getId()){
            case R.id.catweight_start_date_text:
                ((TextView)clickedView.findViewById(R.id.catweight_start_date_text)).setText(year+"-"+monthText+"-"+dayText);
                break;
            case R.id.catweight_check_weight_date_text:
                ((TextView)clickedView.findViewById(R.id.catweight_check_weight_date_text)).setText(year+"-"+monthText+"-"+dayText);
                break;
            default:break;
        }
        clearButton();
    }
}
