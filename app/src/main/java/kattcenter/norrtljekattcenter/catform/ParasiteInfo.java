package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DownloadProduct;
import kattcenter.norrtljekattcenter.network.UploadProduct;

/**
 * Created by Joakim on 2017-07-29.
 */

public class ParasiteInfo implements DatePickerDialog.OnDateSetListener{

    private Context context;
    private View clickedView;
    private int year, month, day;
    private Calendar calendar = Calendar.getInstance();
    private ArrayList<String> parasiteTypes, medicalTypes, treatmentTypes, doseTypes;
    private ListView listView;
    private MyListAdapter myListAdapter;
    private Dialog dialog;

    public ParasiteInfo(Context context, View clickedView){
        this.context = context;
        this.clickedView = clickedView;
        dialog = new Dialog(context);
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private void clearButton(){
        final LinearLayout linearLayout = (LinearLayout)clickedView.getParent();
        final TextView clear = (TextView) linearLayout.getChildAt(1);
        clear.setVisibility(View.VISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear.setVisibility(View.GONE);
                ((TextView)linearLayout.getChildAt(0)).setText("");
            }
        });

    }

    /*
    public void updateParasiteTypes(ArrayList<String> parasiteTypes){
        myListAdapter.notifyDataSetInvalidated();
        myListAdapter = new MyListAdapter(context, parasiteTypes);
        listView.setAdapter(myListAdapter);
    }
    */

    public void updateTypes(ArrayList<String> types, String type_list_name){
        myListAdapter.notifyDataSetInvalidated();
        myListAdapter = new MyListAdapter(context, types, type_list_name);
        listView.setAdapter(myListAdapter);
    }

    public void setParasiteType(ArrayList<String> parasiteTypes){
        this.parasiteTypes= parasiteTypes;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        myListAdapter = new MyListAdapter(context, this.parasiteTypes, "parasite_types");
        listView.setAdapter(myListAdapter);
        Button addNewParasiteType = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newParasiteType = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewParasiteType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String parasiteType = newParasiteType.getText().toString();
                if(parasiteType != null && !parasiteType.equals("")){
                    ((TextView)clickedView.findViewById(R.id.catparasite_type_text)).setText(parasiteType);
                    clearButton();
                    try {
                        new UploadProduct(context, "parasite_types", Type.UPLOAD.toString(), parasiteType).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj typ av parasit");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "parasite_info", "parasite_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setMedicalType(ArrayList<String> medicalTypes){
        this.medicalTypes = medicalTypes;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        myListAdapter = new MyListAdapter(context, medicalTypes, "medicin_types");
        listView.setAdapter(myListAdapter);
        Button addNewMedicalType = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newMedicalType = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewMedicalType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String medicalType = newMedicalType.getText().toString();
                if(medicalType != null && !medicalType.equals("")){
                    //((TextView)clickedView.findViewById(R.id.catparasite_med_type_text)).setText(medicalType);
                    addTypeToList(medicalType);

                    try {
                        new UploadProduct(context, "medicin_types", Type.UPLOAD.toString(), medicalType).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj Läkemedel");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "parasite_info", "medicin_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setAdministrationType(ArrayList<String> treatmentTypes){
        this.treatmentTypes = treatmentTypes;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        myListAdapter = new MyListAdapter(context, treatmentTypes, "treatment_types");
        listView.setAdapter(myListAdapter);
        Button addNewTreatment = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newTreatment = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewTreatment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String treatment = newTreatment.getText().toString();
                if(treatment != null && !treatment.equals("")){
                    ((TextView)clickedView.findViewById(R.id.catparasite_administration_text)).setText(treatment);
                    clearButton();
                    try {
                        new UploadProduct(context, "treatment_types", Type.UPLOAD.toString(), treatment).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj behandling");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "parasite_info", "treatment_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /*
    public void setDoseType(ArrayList<String> doseTypes){
        this.doseTypes = doseTypes;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        myListAdapter = new MyListAdapter(context, doseTypes);
        listView.setAdapter(myListAdapter);
        Button addNewDose = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newDose = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewDose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dose = newDose.getText().toString();
                if(dose != null && !dose.equals("")){
                    ((TextView)clickedView.findViewById(R.id.catparasite_dose_text)).setText(dose);
                    clearButton();
                    try {
                        new UploadProduct(context, "dose_types", Type.UPLOAD.toString(), dose).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj dos");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "parasite_info", "dose_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    */


    public void setComment(){
        final InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        dialog.setTitle("Lägg till kommentar");
        final RelativeLayout comment_form = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.comment_form, null);
        final EditText editText = (EditText) comment_form.findViewById(R.id.commentField);
        (comment_form.findViewById(R.id.addCommentButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(text != null && !text.equals("")) {
                    ((TextView) clickedView.findViewById(R.id.catparasite_comment_text)).setText(text);
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.INVISIBLE);
                    inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    dialog.dismiss();
                    clearButton();
                }
                else{
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.VISIBLE);
                }
            }
        });

        (comment_form.findViewById(R.id.abortButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                dialog.dismiss();
            }
        });

        dialog.setContentView(comment_form);
        dialog.show();
    }


    public void setDate(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        switch (clickedView.getId()){
            case R.id.catparasite_admininstration_date_text:
                ((TextView)clickedView.findViewById(R.id.catparasite_admininstration_date_text)).setText(year+"-"+monthText+"-"+dayText);
                break;
            case R.id.catparasite_repeat_date_text:
                ((TextView)clickedView.findViewById(R.id.catparasite_repeat_date_text)).setText(year+"-"+monthText+"-"+dayText);
                break;
            default:break;
        }
        clearButton();
    }

    private void addTypeToList(String type){
        final LinearLayout list = (LinearLayout)((LinearLayout)clickedView.getParent()).findViewById(R.id.catparasite_med_type_list);
        final RelativeLayout entry = (RelativeLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.removable_list_entry, null);
        ((TextView)entry.findViewById(R.id.first_object_text)).setText(type);
        entry.findViewById(R.id.second_object_text).setVisibility(View.GONE);
        (entry.findViewById(R.id.remove_object)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.removeView(entry);
            }
        });
        list.addView(entry);
    }

    private class MyListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> list;
        private String type_list_name;

        public MyListAdapter(@NonNull Context context, ArrayList<String> list, String type_list_name) {
            super(context, 0, list);
            this.list = list;
            this.type_list_name = type_list_name;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            final String type = list.get(position);
            if(view == null){
                view = ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_2, null);
            }
            TextView entryName = (TextView) view.findViewById(R.id.listEntry);
            entryName.setText(type);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (clickedView.getId()){
                        case R.id.catparasite_type_text:
                            ((TextView)clickedView.findViewById(R.id.catparasite_type_text)).setText(type);
                            clearButton();
                            break;
                        case R.id.catparasite_med_add_type:
                            addTypeToList(type);
                            //((TextView)clickedView.findViewById(R.id.catparasite_med_type_text)).setText(type);
                            break;

                        case R.id.catparasite_administration_text:
                            ((TextView)clickedView.findViewById(R.id.catparasite_administration_text)).setText(type);
                            clearButton();
                            break;

                        case R.id.catparasite_dose_text:
                            ((TextView)clickedView.findViewById(R.id.catparasite_dose_text)).setText(type);
                            clearButton();
                        default:break;
                    }

                    dialog.dismiss();
                }
            });
            view.setOnLongClickListener(new MyLongItemPressListener(context, list, type, type_list_name, "parasite_info"));
            return view;
        }
    }
}
