package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.Calendar;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-05.
 */

public class StatusInfo implements DatePickerDialog.OnDateSetListener{

    private Context context;
    private View clickedView;
    private ViewGroup parent;
    private Calendar calendar;
    private int year, month, day;

    public StatusInfo(Context context, View clickedView){
        this.context = context;
        this.clickedView = clickedView;
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private void setDate(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if(clickedView instanceof CheckBox){
                    ((CheckBox)clickedView).setChecked(false);
                }
                else if(clickedView instanceof RadioButton){
                    ((RadioButton)clickedView).setChecked(false);
                }
            }
        });
    }

    public void handleRadioButton(ViewGroup parent){
        this.parent = parent;
        String string = ((RadioButton)clickedView).getText().toString();
        setDate();
        /*
        if (string.split(" ").length == 2) {
            //((RadioButton) clickedView).setText(string.split(" ")[0]);
            if(string.split(" ")[0].equals("Icke"))
            setDate();
        } else {
            setDate();
        }
        */

    }

    public void handleCheckbox(){
        switch (clickedView.getId()){
            case R.id.catstatus_external_cathome:
                CheckBox checkBox = (CheckBox) clickedView;
                (((HomePage)context).findViewById(R.id.catstatus_external_cathome_text)).setVisibility((checkBox.isChecked()) ? View.VISIBLE : View.GONE);
                break;
            default:
                CheckBox checkBox1 = (CheckBox) clickedView;
                String checkBoxText = checkBox1.getText().toString();
                if(checkBoxText.split(" ").length == 2 && !checkBoxText.split(" ")[0].equals("Åter")){
                    checkBox1.setText(checkBoxText.split(" ")[0]);
                    checkBox1.setChecked(false);
                }
                else if(checkBoxText.split(" ").length == 3 && checkBoxText.split(" ")[0].equals("Åter")){
                    checkBox1.setText(checkBoxText.split(" ")[0]+" "+checkBoxText.split(" ")[1]);
                    checkBox1.setChecked(false);
                }
                else{
                    setDate();
                }
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        System.out.println(year + " "+month+" "+dayOfMonth);
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        String dateText = "("+year+"-"+monthText+"-"+dayText+")";
        if(clickedView instanceof CheckBox){
            String prevText = ((CheckBox)clickedView).getText().toString();
            ((CheckBox)clickedView).setText(prevText+" "+dateText);
        }
        else if(clickedView instanceof  RadioButton){
            RadioButton otherButton1, otherButton2;
            String string1, string2;
            switch (clickedView.getId()){
                case R.id.catstatus_not_bought:
                    otherButton1 = (RadioButton)parent.getChildAt(1);
                    otherButton2 = (RadioButton)parent.getChildAt(2);
                    string1 = otherButton1.getText().toString();
                    if(string1.split(" ").length == 2)
                        otherButton1.setText(string1.split(" ")[0]);
                    string2 = otherButton2.getText().toString();
                    if(string2.split(" ").length == 2)
                        otherButton2.setText(string2.split(" ")[0]);
                    break;
                case R.id.catstatus_new_arrived:
                    otherButton1 = (RadioButton)parent.getChildAt(0);
                    otherButton2 = (RadioButton)parent.getChildAt(2);
                    string1 = otherButton1.getText().toString();
                    if(string1.split(" ").length == 3)
                        otherButton1.setText(string1.split(" ")[0] +" "+string1.split(" ")[1]);
                    string2 = otherButton2.getText().toString();
                    if(string2.split(" ").length == 2)
                        otherButton2.setText(string2.split(" ")[0]);
                    break;
                case R.id.catstatus_adoptable:
                    otherButton1 = (RadioButton)parent.getChildAt(0);
                    otherButton2 = (RadioButton)parent.getChildAt(1);
                    string1 = otherButton1.getText().toString();
                    if(string1.split(" ").length == 3)
                        otherButton1.setText(string1.split(" ")[0] +" "+string1.split(" ")[1]);
                    string2 = otherButton2.getText().toString();
                    if(string2.split(" ").length == 2)
                        otherButton2.setText(string2.split(" ")[0]);
                    break;
                default:break;
            }
            String prevText = ((RadioButton)clickedView).getText().toString();
            String newText;
            if(prevText.split(" ").length == 3)
                newText = prevText.split(" ")[0]+" "+prevText.split(" ")[1]+" "+dateText;
            else
                newText = prevText+" "+dateText;
            ((RadioButton)clickedView).setText(newText);
        }

    }
}
