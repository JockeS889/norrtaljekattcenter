package kattcenter.norrtljekattcenter.catform;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Jocke on 2017-06-11.
 */

public class BundleResourceMapping {

    public HashMap<String, String> getFormTableTranslated(String form_table){
        HashMap<String, String> table = null;
        switch (form_table){
            case "catform_basic":
                table = reverseTable(getBasicTable());
                break;
            case "catform_temperament":
                table = reverseTable(getTemperTable());
                break;
            case "catform_medical":
                table = reverseTable(getMedicalTable());
                break;
            case "catform_vaccination":
                table = reverseTable(getVaccinTable());
                break;
            case "catform_parasites":
                table = reverseTable(getParasitesTable());
                break;
            /*
            case "catform_deworming":
                table = reverseTable(getDewormingTable());
                break;
            case "catform_verm":
                table = reverseTable(getVerminTable());
                break;
                */
            case "catform_claws":
                table = reverseTable(getClawTable());
                break;
            case "catform_weight":
                table = reverseTable(getWeightTable());
                break;
            case "catform_regskk":
                table = reverseTable(getRegSKKTable());
                break;
            case "catform_vet":
                table = reverseTable(getVetTable());
                break;
            case "catform_insurance":
                table = reverseTable(getInsuranceTable());
                break;
            case "catform_background":
                table = reverseTable(getBackgroundTable());
                break;
            case "catform_status":
                table = reverseTable(getStatusTable());
                break;
            default:break;
        }
        return table;
    }

    HashMap<String, String> reverseTable(HashMap<String, String> table){
        HashMap<String, String> reversedTable = new HashMap<>();
        Set<String> keys = table.keySet();
        for(String key: keys){
            String value = table.get(key);
            reversedTable.put(value, key);
        }
        return reversedTable;
    }

    HashMap<String, String> getBasicTable(){
        HashMap<String, String> basicformMap = new HashMap<>();
        basicformMap.put("CATPHOTO", "photo_src");
        basicformMap.put("CATNAME", "name");
        basicformMap.put("CATHOME_NAME", "catcenter_name");
        basicformMap.put("CATHOME_ID", "catcenter_id");
        basicformMap.put("BIRTHDAY", "birthdate");
        basicformMap.put("BIRTHDAY_GUESS", "birthdate_guess");
        basicformMap.put("SEX", "sex");
        basicformMap.put("RACE", "race");
        basicformMap.put("COLOR", "color");
        basicformMap.put("HAIR", "furrtype");
        basicformMap.put("ARRIVED_DATE", "income_date");
        //basicformMap.put("PRESENT_DATE", "income_date");
        //basicformMap.put("BOX", "box");
        basicformMap.put("MOVED", "moved_date");
        //basicformMap.put("CONNECTION", "connection");
        //basicformMap.put("TEMPER_TAME", "temper_tame");
        //basicformMap.put("TEMPER_SHY", "temper_shy");
        //basicformMap.put("NEUTER", "neuter");
        basicformMap.put("NEUTER_DATE", "neuter_date");
        //basicformMap.put("TATOO", "tatoo");
        basicformMap.put("TATOO_STRING", "tatoo_string");
        basicformMap.put("TATOO_DATE", "tatoo_date");
        //basicformMap.put("CHIP", "chip");
        basicformMap.put("CHIP_CODE", "chip_code");
        basicformMap.put("CHIP_DATE", "chip_date");
        return basicformMap;
    }

    /*
    HashMap<String, String> getTableReversed(HashMap<String, String> table){
        return reverseTable(table);
    }
    */


    HashMap<String, String> getTemperTable(){
        HashMap<String, String> temperFormMap = new HashMap<>();
        temperFormMap.put("GENERAL", "general");
        temperFormMap.put("TYPE", "type");
        temperFormMap.put("ADAPTABLE_SKILL", "adaptable_skill");
        temperFormMap.put("USED_TO_CHILDREN", "used_to_children");
        temperFormMap.put("USED_TO_DOGS", "used_to_dogs");
        temperFormMap.put("CAR_SICK", "car_sick");
        temperFormMap.put("TALKS", "talks");
        temperFormMap.put("COMMENT", "comment");
        return temperFormMap;
    }

    HashMap<String, String> getMedicalTable(){
        HashMap<String, String> medicalFormMap = new HashMap<>();
        medicalFormMap.put("START_DATE", "start_date");
        medicalFormMap.put("END_DATE", "end_date");
        medicalFormMap.put("MEDICATION", "medicine");
        medicalFormMap.put("DOSE", "dose");
        medicalFormMap.put("TREATMENT_TYPE", "treatment_type");
        medicalFormMap.put("COMMENT", "comment");
        medicalFormMap.put("REPEAT_DATE", "repeat_date");
        return medicalFormMap;
    }

    HashMap<String, String> getVaccinTable(){
        HashMap<String, String> vaccinFormMap = new HashMap<>();
        vaccinFormMap.put("START_DATE", "start_date");
        vaccinFormMap.put("VACCIN_TYPE", "vaccin_type");
        vaccinFormMap.put("TIME", "vaccin_time");
        vaccinFormMap.put("COMMENT", "comment");
        vaccinFormMap.put("REPEAT_DATE", "repeat_date");
        return vaccinFormMap;
    }

    HashMap<String, String> getParasitesTable(){
        HashMap<String, String> parasitesFormMap = new HashMap<>();
        parasitesFormMap.put("ADMINISTRATION_DATE", "administration_date");
        parasitesFormMap.put("PARASITE_TYPE", "parasite_type");
        parasitesFormMap.put("MEDICAL", "medical");
        parasitesFormMap.put("DOSE", "dose");
        parasitesFormMap.put("ADMINISTRATION_TYPE", "treatment_type");
        parasitesFormMap.put("COMMENT", "comment");
        parasitesFormMap.put("REPEAT_DATE", "repeat_date");
        return parasitesFormMap;
    }

    /*
    HashMap<String, String> getDewormingTable(){
        HashMap<String, String> dewormingFormMap = new HashMap<>();
        dewormingFormMap.put("START_DATE", "start_date");
        dewormingFormMap.put("DEWORMING_TYPE", "deworm_type");
        dewormingFormMap.put("DOSE", "dose");
        dewormingFormMap.put("COMMENT", "comment");
        dewormingFormMap.put("REPEAT_DATE", "repeat_date");
        return dewormingFormMap;
    }

    HashMap<String, String> getVerminTable(){
        HashMap<String, String> verminFormMap = new HashMap<>();
        verminFormMap.put("START_DATE", "start_date");
        verminFormMap.put("VERMIN_TYPE", "vermin_type");
        verminFormMap.put("TREATMENT_TYPE", "treatment_type");
        verminFormMap.put("COMMENT", "comment");
        verminFormMap.put("REPEAT_DATE", "repeat_date");
        return verminFormMap;
    }
    */

    HashMap<String, String> getClawTable(){
        HashMap<String, String> clawFormTable = new HashMap<>();
        clawFormTable.put("START_DATE", "start_date");
        clawFormTable.put("COMMENT", "comment");
        clawFormTable.put("REPEAT_DATE", "repeat_date");
        return clawFormTable;
    }

    HashMap<String, String> getWeightTable(){
        HashMap<String, String> weightFormTable = new HashMap<>();
        weightFormTable.put("START_DATE", "start_date");
        weightFormTable.put("WEIGHT", "weight");
        weightFormTable.put("COMMENT", "comment");
        weightFormTable.put("CHECK_WEIGHT", "repeat_date");
        return weightFormTable;
    }

    public HashMap<String, String> getCatVetItemTable(){
        HashMap<String, String> catvetdocTable = new HashMap<>();
        catvetdocTable.put("catform_vetdoc_generalstate", "Allmäntillstånd");
        catvetdocTable.put("catform_vetdoc_lynne", "Lynne");
        catvetdocTable.put("catform_vetdoc_skinpaws", "Hud, hårrem, tassar");
        catvetdocTable.put("catform_vetdoc_lymf", "Palpabla lymfknutar");
        catvetdocTable.put("catform_vetdoc_eyes", "Ögon");
        catvetdocTable.put("catform_vetdoc_ears", "Öron");
        catvetdocTable.put("catform_vetdoc_mouth", "Munhåla, tänder, svalg");
        catvetdocTable.put("catform_vetdoc_abdomen", "Bukorgan, buk");
        catvetdocTable.put("catform_vetdoc_circulationorgan", "Ciculationsorgan");
        catvetdocTable.put("catform_vetdoc_respirationorgan", "Respirationsorgan");
        catvetdocTable.put("catform_vetdoc_outergenitalias", "Yttre genitalorgan");
        catvetdocTable.put("catform_vetdoc_locomotiveorgan", "Rörelseorgan");
        catvetdocTable.put("without_remark", "Utan anmärkningar");
        catvetdocTable.put("general_without_remark", "A.T. u.a.");
        catvetdocTable.put("generally_low", "A.T. Nedsatt");
        catvetdocTable.put("note", "Kommentar");
        catvetdocTable.put("documents", "Dokument");
        return catvetdocTable;
    }

    HashMap<String, String> getRegSKKTable(){
        HashMap<String, String> regSKKFormMap = new HashMap<>();
        //regSKKFormMap.put("REG_SKK", "reg_skk");
        regSKKFormMap.put("REG_SKK_DATE", "reg_skk_date");
        //regSKKFormMap.put("REG_SVE", "reg_svekatt");
        regSKKFormMap.put("REG_SVE_DATE", "reg_svekatt_date");
        regSKKFormMap.put("DOCUMENTS", "documents");
        return regSKKFormMap;
    }


    HashMap<String, String> getVetTable(){
        HashMap<String, String> vetFormMap = new HashMap<>();
        vetFormMap.put("NO_KNOWN", "no_healthproblem");
        vetFormMap.put("NO_KNOWN_DATE", "no_healthproblem_date");
        vetFormMap.put("SAMPLE_DATE", "bloodsample_date");
        vetFormMap.put("SAMPLE_RESULT", "bloodsample_result");
        vetFormMap.put("KNOWN_EVENTS", "known_events");
        vetFormMap.put("COMMENT", "comment");
        return vetFormMap;
    }

    HashMap<String, String> getInsuranceTable(){
        HashMap<String, String> insuranceFormMap = new HashMap<>();
        insuranceFormMap.put("TAKEN_DATE", "taken_date");
        insuranceFormMap.put("OLD_COMPANY", "old_company");
        insuranceFormMap.put("OLD_NUMBER", "old_number");
        insuranceFormMap.put("SIGNED_DATE", "signed_date");
        insuranceFormMap.put("NEW_COMPANY", "new_company");
        insuranceFormMap.put("NEW_NUMBER", "new_number");
        insuranceFormMap.put("TRANSFER_DATE", "transfer_date");
        return insuranceFormMap;
    }

    HashMap<String, String> getBackgroundTable(){
        HashMap<String, String> backgroundFormMap = new HashMap<>();
        backgroundFormMap.put("BORN", "born");
        //backgroundFormMap.put("BORN_OUTSIDE_DATE", "born_date");
        backgroundFormMap.put("HOMELESS", "homeless");
        //backgroundFormMap.put("HOMELESS_DATE", "homeless_date");
        backgroundFormMap.put("DUMPED", "dumped");
        //backgroundFormMap.put("DUMPED_DATE", "dumped");
        backgroundFormMap.put("LOST", "lost");
        //backgroundFormMap.put("LOST_DATE", "lost_date");
        backgroundFormMap.put("LEFT_BEHIND", "left_behind");
        //backgroundFormMap.put("LEFT_BEHIND_DATE", "left_behind_date");
        backgroundFormMap.put("ESTATE", "estate");

        backgroundFormMap.put("REPLACED", "replaced");
        //backgroundFormMap.put("RELOCATE_DATE", "replaced_date");
        backgroundFormMap.put("VIA_VET", "via_vet");
        //backgroundFormMap.put("VIA_VET_DATE", "via_vet_date");
        backgroundFormMap.put("COMMENT", "comment");
        return backgroundFormMap;
    }

    /*
    HashMap<String, String> getPrevOwnerTable(){
        return null;
    }
*/
    HashMap<String, String> getStatusTable(){
        HashMap<String, String> statusFormMap = new HashMap<>();
        statusFormMap.put("CATHOME", "cathome");
        statusFormMap.put("NOT_BOUGHT", "not_bought");
        statusFormMap.put("NEWLY_ARRIVED", "newly_arrived");
        statusFormMap.put("ADOPTABLE", "adoptable");
        statusFormMap.put("BOOKED", "booked");
        statusFormMap.put("ADOPTED", "adopted");
        statusFormMap.put("CALLCENTER", "callcenter");
        statusFormMap.put("TNR", "tnr");
        statusFormMap.put("MOVED", "moved");
        statusFormMap.put("EXTERNAL_CATHOME", "external_cathome");
        statusFormMap.put("MISSING", "missing");
        statusFormMap.put("RETURNEDOWNER", "returned_owner");
        statusFormMap.put("DECEASED", "deceased");
        return statusFormMap;
    }

    HashMap<String, String> getConnectionColumns(){
        HashMap<String, String> connectionFormMap = new HashMap<>();
        connectionFormMap.put("MOTHER", "mother");
        connectionFormMap.put("FATHER", "father");
        connectionFormMap.put("SIBLINGS", "siblings");
        connectionFormMap.put("CHILDREN", "children");
        return connectionFormMap;
    }




}
