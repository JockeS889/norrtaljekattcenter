package kattcenter.norrtljekattcenter.catform;

import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itextpdf.text.pdf.parser.Line;

import net.hockeyapp.android.metrics.model.Base;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.dialog.BottomDialog;

/**
 * Created by Joakim on 2017-08-03.
 */

public class TimeLine {

    private Context context;

    public TimeLine(Context context){
        this.context = context;
    }


    private class MyScreenSlidePagerAdapter extends PagerAdapter{

        private Context context;
        private ArrayList<View> views;
        public MyScreenSlidePagerAdapter(Context context){
            this.context = context;
        }

        public void setViews(ArrayList<View> views){
            this.views = views;
        }

        public ArrayList<View> getItems(){
            return views;
        }


        /**
         * Return the number of views available.
         */
        @Override
        public int getCount() {
            return views.size();
        }




        /**
         * Determines whether a page View is associated with a specific key object
         * as returned by {@link #instantiateItem(ViewGroup, int)}. This method is
         * required for a PagerAdapter to function properly.
         *
         * @param view   Page View to check for association with <code>object</code>
         * @param object Object to check for association with <code>view</code>
         * @return true if <code>view</code> is associated with the key object <code>object</code>
         */
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            //View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);
            //ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            //imageView.setImageResource(mResources[position]);
            View itemView = views.get(position);
            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }



    public void removeTimeLineEntry(JSONObject jsonObject) throws JSONException{
        String reg_date = jsonObject.getString("reg_date");
        ((HomePage)context).removeTimeLineEntry(reg_date);
    }


    public ArrayList<View> createTimeLineFromPreviousCatowners(final String current_owner, String current_owner_date_time, String previous_owners){
        ArrayList<View> timeLineList = new ArrayList<>();
        HashMap<Long, View> unsortedViews = new HashMap<>();
        ArrayList<Long> dateIntegers = new ArrayList<>();
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        if(current_owner != null && !current_owner.isEmpty()){
            current_owner_date_time = new String(Base64.decode(current_owner_date_time.getBytes(), Base64.NO_WRAP));
            String date_part = current_owner_date_time.split(" ")[0];
            String time_part = current_owner_date_time.split(" ")[1];
            RelativeLayout timeLineEntry = (RelativeLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.new_timeline_entry_owners, null);
            String owner_info = new String(Base64.decode(current_owner.getBytes(), Base64.NO_WRAP));
            String owner_name = owner_info.split(":")[0];
            final String owner_mail = (owner_info.split(":")[1].equals("NULL")) ? "" : owner_info.split(":")[1];
            ((TextView) timeLineEntry.findViewById(R.id.timeline_prev_catowner_name)).setText(owner_name);
            ((TextView) timeLineEntry.findViewById(R.id.timeline_prev_catowner_email)).setText(owner_mail);
            ((TextView) timeLineEntry.findViewById(R.id.timeline_entry_date)).setText(date_part);
            ((TextView) timeLineEntry.findViewById(R.id.timeline_entry_time)).setText(time_part);
            (timeLineEntry.findViewById(R.id.cats_owner_entry)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(owner_mail.equals("")){
                        new BottomDialog(context).popup("Kan ej ladda information om: 'Kattcenter'");
                    }
                    else{
                        ((HomePage)context).getHomePageHandler().getCatownerManager().selectCatOwner(current_owner);
                    }

                }
            });

            try {
                Long date = simpleDateFormat1.parse(current_owner_date_time).getTime();
                System.out.println("DATE TIME LONG: "+date);
                dateIntegers.add(date);
                unsortedViews.put(date, timeLineEntry);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //setCurrentCatownerEntry(timeLineEntry);
            //timeLineList.add(timeLineEntry);
        }
        if(previous_owners != null && !previous_owners.isEmpty()){
            String decoded_data = new String(Base64.decode(previous_owners.getBytes(), Base64.NO_WRAP));
            if(decoded_data != null && !decoded_data.isEmpty()){
                String[] prev_owners = decoded_data.split(":");
                for(String prev_owner_encoded: prev_owners){
                    final String owner_date = new String(Base64.decode(prev_owner_encoded.getBytes(), Base64.NO_WRAP));
                    final String pre_owner_info = owner_date.split(":")[0];
                    String decoded_pre_owner_info = new String(Base64.decode(pre_owner_info.getBytes(), Base64.NO_WRAP));
                    final String owner_name = decoded_pre_owner_info.split(":")[0];
                    final String owner_mail = (decoded_pre_owner_info.split(":")[1].equals("NULL")) ? "" : decoded_pre_owner_info.split(":")[1];
                    final String date = new String(Base64.decode((owner_date.split(":")[1]).getBytes(), Base64.NO_WRAP));
                    String date_part = date.split(" ")[0];
                    String time_part = date.split(" ")[1];
                    RelativeLayout timeLineEntry = (RelativeLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.new_timeline_entry_owners, null);
                    ((TextView) timeLineEntry.findViewById(R.id.timeline_prev_catowner_name)).setText(owner_name);
                    ((TextView) timeLineEntry.findViewById(R.id.timeline_prev_catowner_email)).setText(owner_mail);
                    ((TextView) timeLineEntry.findViewById(R.id.timeline_entry_date)).setText(date_part);
                    ((TextView) timeLineEntry.findViewById(R.id.timeline_entry_time)).setText(time_part);
                    (timeLineEntry.findViewById(R.id.cats_owner_entry)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(owner_mail.equals("")){
                                new BottomDialog(context).popup("Kan ej ladda information om: 'Kattcenter'");
                            }
                            else{
                                ((HomePage)context).getHomePageHandler().getCatownerManager().selectCatOwner(pre_owner_info);
                            }

                        }
                    });
                    try {
                        Long prev_date = simpleDateFormat1.parse(date).getTime();
                        System.out.println("PREV DATE TIME LONG: "+prev_date);
                        dateIntegers.add(prev_date);
                        unsortedViews.put(prev_date, timeLineEntry);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    //timeLineList.add(timeLineEntry);
                }
            }
        }
        Collections.sort(dateIntegers, Collections.<Long>reverseOrder());
        for (int i = 0; i < dateIntegers.size(); i++) {
            Log.i("TIMELINE", "DATE LONG: "+dateIntegers.get(i)+" VIEW: "+unsortedViews.get(dateIntegers.get(i)));
            //if(i == 0) setCurrentCatownerEntry(unsortedViews.get(dateIntegers.get(i)));
            timeLineList.add(unsortedViews.get(dateIntegers.get(i)));
        }

        return timeLineList;
    }

    public void createTimeLineCatVetItems(String json, String form_table, LinearLayout timeLine){
        HashMap<Long, View> unsortedViews = new HashMap<>();
        ArrayList<Long> dateIntegers = new ArrayList<>();
        String decoded_rows = new String(Base64.decode(json.getBytes(), Base64.NO_WRAP));
        String[] json_rows = decoded_rows.split("\n");
        BundleResourceMapping bundleResourceMapping = new BundleResourceMapping();
        HashMap<String, String> catvetTitles = bundleResourceMapping.getCatVetItemTable();
        if(timeLine.getChildCount() > 0){
            for (String row : json_rows) {
                try {
                    /*
                    RelativeLayout entry = (RelativeLayout) timeLine.getChildAt(i);
                    TextView textView = new TextView(context);
                    */
                    JSONObject jsonObject = new JSONObject(row);
                    String reg_date = jsonObject.getString("reg_date");
                    for(int i = 0; i < timeLine.getChildCount(); i++){
                        RelativeLayout entry = (RelativeLayout) timeLine.getChildAt(i);
                        String entry_date = ((TextView)entry.findViewById(R.id.timeline_entry_date)).getText().toString();
                        if(reg_date.equals(entry_date)){
                            //LinearLayout content_list = (LinearLayout)entry.findViewById(R.id.timeline_entry_content);
                            //ViewPager viewPager = (ViewPager) entry.findViewById(R.id.viewPager);
                            CatVetViewPager catVetViewPager = (CatVetViewPager) entry.findViewById(R.id.catVetViewPager);
                            //MyScreenSlidePagerAdapter myScreenSlidePagerAdapter = (MyScreenSlidePagerAdapter) viewPager.getAdapter();
                            MyScreenSlidePagerAdapter myScreenSlidePagerAdapter = (MyScreenSlidePagerAdapter) catVetViewPager.getAdapter();
                            ArrayList<View> views = myScreenSlidePagerAdapter.getItems();
                            TextView title = (TextView) ((HomePage)context).getLayoutInflater().inflate(R.layout.my_simple_textview, null);
                            title.setText(catvetTitles.get(form_table));
                            final LinearLayout content_element = new LinearLayout(context);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            content_element.setOrientation(LinearLayout.VERTICAL);
                            content_element.setLayoutParams(layoutParams);
                            content_element.addView(title);
                            Iterator<String> keys = jsonObject.keys();
                            while(keys.hasNext()){
                                String key = keys.next();
                                String value = jsonObject.getString(key);
                                if(key.equals("reg_date") || key.equals("reg_author") || key.equals("id") ||
                                        key.equals("catcenter_id") || key.equals("date")) continue;
                                key = (catvetTitles.containsKey(key)) ? catvetTitles.get(key) : key;
                                key = key.replaceAll("_", " ");
                                /*
                                if(key.equals("documents")){
                                    content_element.setTag(value);
                                }
                                */
                                if(value.equals("1")) value = "Ja";
                                else if(value.isEmpty()) value = "";

                                //RelativeLayout content_row = (RelativeLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.timeline_content_row, null);
                                LinearLayout content_row = (LinearLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.new_timeline_entry_row, null);
                                ((TextView)content_row.findViewById(R.id.key)).setText(key);
                                ((TextView)content_row.findViewById(R.id.value)).setText(value);
                                content_element.addView(content_row);
                            }
                            views.add(content_element);
                            myScreenSlidePagerAdapter.setViews(views);
                            myScreenSlidePagerAdapter.notifyDataSetChanged();

                            /*
                            if(i == 0){
                                setFirstTimelineEntryCatVet(content_element);
                            }
                            else{
                                setRestTimeLineEntryCatVet(content_element);
                            }
                            */


                            //content_list.addView(content_element);
                        }
                    }
                    /*
                    textView.setText(form_table + " " + jsonObject.getString("reg_date"));
                    textView.setTextColor(Color.parseColor("#000000"));
                    timeLine.addView(textView);
                    */
                } catch (JSONException jsone) {
                    jsone.printStackTrace();
                }
            }
        }
        else {
            int i = 0;
            for (String row : json_rows) {
                final int index = i;
                try {
                    //final RelativeLayout entry = (RelativeLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.timeline_entry, null);
                    final RelativeLayout entry = (RelativeLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.new_timeline_entry, null);
                    JSONObject jsonObject = new JSONObject(row);
                    String reg_date = jsonObject.getString("reg_date");
                    String reg_author_encoded = jsonObject.getString("reg_author");
                    String reg_author_decoded = new String(Base64.decode(reg_author_encoded.getBytes(), Base64.NO_WRAP));
                    String author = reg_author_decoded.split(":")[0];
                    String authorization = "FEL";
                    if (reg_author_decoded.split(":").length == 2)
                        authorization = reg_author_decoded.split(":")[1];
                    ((TextView) entry.findViewById(R.id.timeline_author_name_text)).setText(author);
                    ((TextView) entry.findViewById(R.id.timeline_author_authorization_text)).setText(authorization);
                    ((TextView)entry.findViewById(R.id.timeline_entry_date)).setText(reg_date);

                    //ViewPager viewPager = (ViewPager)entry.findViewById(R.id.viewPager);
                    CatVetViewPager catVetViewPager = (CatVetViewPager) entry.findViewById(R.id.catVetViewPager);
                    MyScreenSlidePagerAdapter myScreenSlidePagerAdapter = new MyScreenSlidePagerAdapter(context);
                    ArrayList<View> views = new ArrayList<>();
                    //viewPager.setAdapter(new MyScreenSlidePagerAdapter());

                    //LinearLayout content_list = (LinearLayout)entry.findViewById(R.id.timeline_entry_content);

                    TextView title = (TextView) ((HomePage)context).getLayoutInflater().inflate(R.layout.my_simple_textview, null);
                    title.setText(catvetTitles.get(form_table));
                    final LinearLayout content_element = new LinearLayout(context);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    content_element.setOrientation(LinearLayout.VERTICAL);
                    content_element.setLayoutParams(layoutParams);
                    content_element.addView(title);
                    Iterator<String> keys = jsonObject.keys();

                    while(keys.hasNext()){
                        boolean doc = false;
                        String key = keys.next();
                        String value = jsonObject.getString(key);
                        if(key.equals("reg_date") || key.equals("reg_author") || key.equals("id") ||
                                key.equals("catcenter_id") || key.equals("date")) continue;
                        key = (catvetTitles.containsKey(key)) ? catvetTitles.get(key) : key;
                        key = key.replaceAll("_", " ");

                        if(key.equals("documents")){
                            key = "Dokument";
                            if(value.isEmpty()){
                                value = "Inga";
                            }
                            else {
                                content_element.setTag(value);
                                String[] documents = (new String(Base64.decode(value.getBytes(), Base64.NO_WRAP))).split(":");
                                String newValue = "";
                                for (String s : documents) {
                                    newValue += decodeDocumentFile(s) + "\n";
                                }
                                value = newValue;
                                doc = true;
                            }
                        }
                        if(value.equals("1")) value = "Ja";
                        else if(value.isEmpty()) value = "";
                        Log.i("TIMELINE", "KEY: "+key+" VALUE: "+value+" NODOC: "+doc);
                        //RelativeLayout content_row = (RelativeLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.timeline_content_row, null);
                        LinearLayout content_row;
                        if(!doc)
                            content_row = (LinearLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.new_timeline_entry_row, null);
                        else {
                            content_row = (LinearLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.new_timeline_entry_row_downloadable_file, null);
                            content_row.findViewById(R.id.download_file_holder).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showDownloadFileDialog((String) content_element.getTag(), "Laddar ner dokument ", true);
                                }
                            });
                        }
                        ((TextView)content_row.findViewById(R.id.key)).setText(key);
                        ((TextView)content_row.findViewById(R.id.value)).setText(value);
                        content_element.addView(content_row);
                    }
                    /*
                    if(!noDoc) {
                        final String file_name = value;
                        content_element.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showDownloadFileDialog((String) content_element.getTag(), "Laddar ner dokument ", true);
                            }
                        });
                    }
                    */
                    //content_list.addView(content_element);
                    views.add(content_element);
                    myScreenSlidePagerAdapter.setViews(views);
                    //viewPager.setAdapter(myScreenSlidePagerAdapter);
                    catVetViewPager.setAdapter(myScreenSlidePagerAdapter);
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date regDate = simpleDateFormat.parse(reg_date);
                    Long dateLong = regDate.getTime();
                    unsortedViews.put(dateLong, entry);
                    dateIntegers.add(dateLong);
                    Log.i("TIMELINE", "DATELONG: "+dateLong);

                } catch (JSONException jsone) {
                    jsone.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                i++;
            }

            Collections.sort(dateIntegers, Collections.<Long>reverseOrder());
            Log.i("TIMELINE", "LIST SIZE: "+dateIntegers.size() +" LIST: "+dateIntegers.toString());
            for(int k = 0; k < dateIntegers.size(); k++){
                final int index = k;

                final RelativeLayout entry = (RelativeLayout)unsortedViews.get(dateIntegers.get(k));
                final TextView expandButton = (TextView) entry.findViewById(R.id.expand_entry_content);
                expandButton.setTag("open");
                (entry.findViewById(R.id.timeline_title_holder)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tag = (String) expandButton.getTag();
                        LinearLayout frame = (LinearLayout) entry.findViewById(R.id.timeline_entry_frame);
                        LinearLayout editFrame = (LinearLayout) entry.findViewById(R.id.handle_timeline_entry);
                        if(tag.equals("closed")){
                            frame.setVisibility(View.VISIBLE);
                            editFrame.setVisibility(View.VISIBLE);
                            expandButton.setTag("open");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                expandButton.setBackground(context.getResources().getDrawable(R.drawable.ic_expand_less_black_24dp, null));
                            } else {
                                expandButton.setBackground(context.getResources().getDrawable(R.drawable.ic_expand_less_black_24dp));
                            }
                            if (index != 0) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    expandButton.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.gray_tint));
                                } else {
                                    ViewCompat.setBackgroundTintList(expandButton, new ColorStateList(new int[][]{new int[0]}, new int[]{0xaaaa00}));
                                }
                            }
                        }
                        else if(tag.equals("open")){
                            frame.setVisibility(View.GONE);
                            editFrame.setVisibility(View.GONE);
                            expandButton.setTag("closed");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                expandButton.setBackground(context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp, null));
                            } else {
                                expandButton.setBackground(context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));
                            }
                            if (index != 0) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    expandButton.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.gray_tint));
                                } else {
                                    ViewCompat.setBackgroundTintList(expandButton, new ColorStateList(new int[][]{new int[0]}, new int[]{0xaaaa00}));
                                }
                            }
                        }
                    }
                });
                //if(k == 0) setFirstTimeLineEntry(entry, true);
                //else setRestTimeLineEntry(entry, true);
                timeLine.addView(entry);

            }


        }
    }

    public ArrayList<View> createTimeLineFromHistory(final String json, String form_table){
        String decoded_rows = new String(Base64.decode(json.getBytes(), Base64.NO_WRAP));
        final String[] json_rows = decoded_rows.split("\n");
        ArrayList<View> timeLineList = new ArrayList<>();
        HashMap<Long, View> unsortedViews = new HashMap<>();
        final HashMap<Long, JSONObject> contentMapping = new HashMap<>();
        ArrayList<Long> dateIntegers = new ArrayList<>();
        FormFieldMapper formFieldMappar = new FormFieldMapper();
        BundleResourceMapping bundleResourceMapping = new BundleResourceMapping();

        HashMap<String, String> tableMapper = bundleResourceMapping.getFormTableTranslated(form_table);
        try {

            for (String row : json_rows) {
                JSONObject jsonObject = new JSONObject(row);
                //final RelativeLayout timeLineEntry = (RelativeLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.timeline_entry, null);
                final RelativeLayout timeLineEntry = (RelativeLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.new_timeline_entry, null);
                String reg_author_encoded = jsonObject.getString("reg_author");
                String reg_author_decoded = new String(Base64.decode(reg_author_encoded.getBytes(), Base64.NO_WRAP));
                String author = reg_author_decoded.split(":")[0];
                String authorization = "FEL";
                if (reg_author_decoded.split(":").length == 2)
                    authorization = reg_author_decoded.split(":")[1];
                ((TextView) timeLineEntry.findViewById(R.id.timeline_author_name_text)).setText(author);
                ((TextView) timeLineEntry.findViewById(R.id.timeline_author_authorization_text)).setText(authorization);

                String reg_date = jsonObject.getString("reg_date");



                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long dateInteger = 0;
                try {
                    Date date = simpleDateFormat.parse(reg_date);
                    dateInteger = date.getTime();
                    dateIntegers.add(dateInteger);
                    contentMapping.put(dateInteger, jsonObject);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                ((TextView) timeLineEntry.findViewById(R.id.timeline_entry_date)).setText(reg_date);
                final LinearLayout timeLineEntryContent = (LinearLayout) timeLineEntry.findViewById(R.id.timeline_entry_content);
                Iterator<String> iterator = jsonObject.keys();
                /*
                if(form_table.equals("catform_vet")){
                    timeLineEntryContent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDownloadFileDialog((String)timeLineEntryContent.getTag(), "Laddar ner provresulat ", false);
                        }
                    });
                }
                */

                boolean noDoc = false;
                while (iterator.hasNext()) {
                    String key = iterator.next();
                    if (key.equals("id") || key.equals("catcenter_id") || key.equals("reg_date") || key.equals("reg_author") || key.equals("table_name")
                            || key.equals("start_date_reminded") || key.equals("repeat_date_reminded"))
                        continue;
                    String value = jsonObject.getString(key);
                    if(key.equals("bloodsample_result")){
                        timeLineEntryContent.setTag(value);
                        value = decodeDocumentFile(value);
                    }
                    if(key.equals("documents")){
                        key = "Dokument";
                        if(value.isEmpty()) {
                            value = null;
                            noDoc = true;
                        }
                        else{
                            timeLineEntryContent.setTag(value);
                            String[] documents = (new String(Base64.decode(value.getBytes(), Base64.NO_WRAP))).split(":");
                            String newValue = "";
                            for (String s : documents) {
                                newValue += decodeDocumentFile(s) + "\n";
                            }
                            value = newValue;
                        }
                    }
                    String translatedKey = (formFieldMappar.containsTranslation(tableMapper.get(key))) ? formFieldMappar.getTranslation(tableMapper.get(key)) : key;
                    translatedKey = (form_table.equals("catform_medical") && key.equals("start_date")) ? "Startdatum" : translatedKey;
                    Log.i("TIMELINE", "KEY: "+key+" TRANSLATED KEY: "+translatedKey+"  VALUE: "+value);
                    if(form_table.equals("catform_background") || form_table.equals("catform_status")){
                        if(value.contains("(") && value.contains(")")){
                            value = value.substring(value.indexOf("(")+1, value.indexOf(")"));
                        }
                        else if(key.equals("comment")){

                        }
                        else{
                            continue;
                            /*
                            if(!key.equals("external_cathome"))
                                value = null;
                             */
                        }
                    }
                    else if(form_table.equals("catform_vet")){

                    }
                    if(value == null || value.isEmpty()) value = "";
                    //RelativeLayout timeLineContentRow = (RelativeLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.timeline_content_row, null);
                    LinearLayout timeLineContentRow;
                    if(key.equals("documents") || key.equals("bloodsample_result")) {
                        timeLineContentRow = (LinearLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.new_timeline_entry_row_downloadable_file, null);
                        if(value.isEmpty()){
                            timeLineContentRow.findViewById(R.id.download_file_holder).setVisibility(View.GONE);
                        }
                        else{
                            timeLineContentRow.findViewById(R.id.download_file_holder).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showDownloadFileDialog((String)timeLineEntryContent.getTag(), "Laddar ner dokument ", false);
                                }
                            });
                        }
                    }else
                        timeLineContentRow = (LinearLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.new_timeline_entry_row, null);
                    ((TextView) timeLineContentRow.findViewById(R.id.key)).setText(translatedKey);
                    ((TextView) timeLineContentRow.findViewById(R.id.value)).setText(value);
                    timeLineEntryContent.addView(timeLineContentRow);
                }
                if(form_table.equals("catform_regskk") && !noDoc){
                    timeLineEntryContent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDownloadFileDialog((String)timeLineEntryContent.getTag(), "Laddar ner dokument ", true);
                        }
                    });
                }
                //timeLineList.add(timeLineEntry);
                unsortedViews.put(dateInteger, timeLineEntry);
            }
            Collections.sort(dateIntegers, Collections.<Long>reverseOrder());
            for (int i = 0; i < dateIntegers.size(); i++) {
                final int index = i;
                final Long l = dateIntegers.get(i);
                final View sortedView = unsortedViews.get(l);

                RelativeLayout expandEntryContentHolder = (RelativeLayout) sortedView.findViewById(R.id.timeline_title_holder);
                final TextView expandEntryContentButton = (TextView) sortedView.findViewById(R.id.expand_entry_content);

                sortedView.findViewById(R.id.handle_timeline_entry).findViewById(R.id.edit_timeline_entry).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((HomePage)context).editForm(contentMapping.get(l));
                    }
                });

                sortedView.findViewById(R.id.handle_timeline_entry).findViewById(R.id.remove_timeline_entry).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            removeTimeLineEntry(contentMapping.get(l));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });


                expandEntryContentHolder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tag = (String) expandEntryContentButton.getTag();
                        Log.i("TIMELINE", "TAG: " + tag);
                        if (tag.equals("closed")) {
                            sortedView.findViewById(R.id.timeline_entry_frame).setVisibility(View.VISIBLE);
                            sortedView.findViewById(R.id.handle_timeline_entry).setVisibility(View.VISIBLE);

                            expandEntryContentButton.setTag("open");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                expandEntryContentButton.setBackground(context.getResources().getDrawable(R.drawable.ic_expand_less_black_24dp, null));
                            } else {
                                expandEntryContentButton.setBackground(context.getResources().getDrawable(R.drawable.ic_expand_less_black_24dp));
                            }
                            if (index != 0) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    expandEntryContentButton.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.gray_tint));
                                } else {
                                    ViewCompat.setBackgroundTintList(expandEntryContentButton, new ColorStateList(new int[][]{new int[0]}, new int[]{0xaaaa00}));
                                }
                            }


                        } else if (tag.equals("open")) {
                            sortedView.findViewById(R.id.timeline_entry_frame).setVisibility(View.GONE);
                            sortedView.findViewById(R.id.handle_timeline_entry).setVisibility(View.GONE);
                            expandEntryContentButton.setTag("closed");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                expandEntryContentButton.setBackground(context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp, null));
                            } else {
                                expandEntryContentButton.setBackground(context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));
                            }
                            if (index != 0) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    expandEntryContentButton.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.gray_tint));
                                } else {
                                    ViewCompat.setBackgroundTintList(expandEntryContentButton, new ColorStateList(new int[][]{new int[0]}, new int[]{0xaaaa00}));
                                }
                            }

                        }
                    }
                });


                //if (i == 0) setFirstTimeLineEntry(sortedView, false);
                //else setRestTimeLineEntry(sortedView, false);
                timeLineList.add(unsortedViews.get(l));

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return timeLineList;
    }


    private String decodeDocumentFile(String encoded){
        /**
         * REC FORMAT:
         * B64(B64(B64(fileName):B64(TIMESTAMP):B64(CATID)).pdf)
         */

        String dec = new String(Base64.decode(encoded.getBytes(), Base64.NO_WRAP));
        //Log.i("TIMELINE", "DECODED: "+dec+" LENGTH: "+dec.split("").length);
        if(dec.split("\\.").length == 2) {
            String name_part = dec.split("\\.")[0];
            String dec_name_part = new String(Base64.decode(name_part.getBytes(), Base64.NO_WRAP));
            String[] parts = dec_name_part.split(":");
            return new String(Base64.decode(parts[0].getBytes(), Base64.NO_WRAP));
        }
        return null;
    }

    private void showDownloadFileDialog(final String file_name_encoded, final String downloadText, final boolean mult_files){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //LinearLayout linearLayout = (LinearLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.general_confirm_dialog, null);
        LinearLayout linearLayout = (LinearLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.new_dialog_fetch_file, null);
        RelativeLayout abort = (RelativeLayout) linearLayout.findViewById(R.id.abort);
        RelativeLayout confirm = (RelativeLayout) linearLayout.findViewById(R.id.confirm);
        ((TextView)linearLayout.findViewById(R.id.text_file_header)).setText("Ladda ner dokument?");
        //abort.setText("Avbryt");
        //confirm.setText("Hämta Dokument");
        //confirm.setTextSize(10);
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((HomePage)context).startDownloadDocuments(file_name_encoded, downloadText, mult_files);
                Log.i("TIMELINE", "START DOWNLOADING: "+file_name_encoded);
            }
        });
        dialog.setContentView(linearLayout);
        dialog.show();
    }

    private void setCurrentCatownerEntry(View view){
        RelativeLayout entry = (RelativeLayout) view;
        ((TextView) entry.findViewById(R.id.timeline_prev_catowner_text)).setTextColor(Color.parseColor("#000000"));
        ((TextView) entry.findViewById(R.id.timeline_entry_date)).setTextColor(Color.parseColor("#000000"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            entry.findViewById(R.id.arrow_point).setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.black_tint));
            entry.findViewById(R.id.arrow_line).setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.black_tint));
        }
        else{
            ViewCompat.setBackgroundTintList(entry.findViewById(R.id.arrow_line), new ColorStateList(new int[][]{new int[0]}, new int[]{0x000000}));
            ViewCompat.setBackgroundTintList(entry.findViewById(R.id.arrow_point), new ColorStateList(new int[][]{new int[0]}, new int[]{0x000000}));
        }
    }

    private void setFirstTimelineEntryCatVet(View view){
        LinearLayout catVetEntryPage = (LinearLayout) view;
        for(int i = 0; i < catVetEntryPage.getChildCount(); i++){
            if(catVetEntryPage.getChildAt(i) instanceof RelativeLayout) {
                RelativeLayout content_row = (RelativeLayout) catVetEntryPage.getChildAt(i);
                ((TextView) content_row.findViewById(R.id.key)).setTextColor(Color.parseColor("#000000"));
                ((TextView) content_row.findViewById(R.id.value)).setTextColor(Color.parseColor("#000000"));
            }
        }
    }
    private void setRestTimeLineEntryCatVet(View view){
        LinearLayout catVetEntryPage = (LinearLayout) view;
        for(int i = 0; i < catVetEntryPage.getChildCount(); i++){
            if(catVetEntryPage.getChildAt(i) instanceof TextView) {
                ((TextView) catVetEntryPage.getChildAt(i)).setTextColor(Color.parseColor("#aaaaaa"));
            }
        }
    }

    private void setFirstTimeLineEntry(View view, boolean catVetItems){
        RelativeLayout entry = (RelativeLayout) view;
        entry.findViewById(R.id.entrySplitter).setVisibility(View.GONE);
        ((TextView)entry.findViewById(R.id.timeline_entry_date)).setTextColor(Color.parseColor("#000000"));
        ((TextView)entry.findViewById(R.id.expand_entry_content)).setTextColor(Color.parseColor("#000000"));
        entry.findViewById(R.id.timeline_author_holder).setBackgroundColor(Color.parseColor("#000000"));
        if(catVetItems){
            CatVetViewPager catVetViewPager = (CatVetViewPager) entry.findViewById(R.id.catVetViewPager);
            MyScreenSlidePagerAdapter myScreenSlidePagerAdapter = (MyScreenSlidePagerAdapter) catVetViewPager.getAdapter();
            ArrayList<View> views = myScreenSlidePagerAdapter.getItems();
            for(View catvetEntryPage: views){
                setFirstTimelineEntryCatVet(catvetEntryPage);
            }
        }
        else {
            LinearLayout content = (LinearLayout) entry.findViewById(R.id.timeline_entry_content);
            int totalChildViews = content.getChildCount();
            for (int i = 0; i < totalChildViews; i++) {
                LinearLayout row = (LinearLayout) content.getChildAt(i);
                ((TextView) row.findViewById(R.id.key)).setTextColor(Color.parseColor("#000000"));
                ((TextView) row.findViewById(R.id.value)).setTextColor(Color.parseColor("#000000"));
            }
        }
    }

    private void setRestTimeLineEntry(View view, boolean catVetItems){
        RelativeLayout entry = (RelativeLayout) view;
        TextView expandEntryContentButton = (TextView) entry.findViewById(R.id.expand_entry_content);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            expandEntryContentButton.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.gray_tint));
        }
        else{
            ViewCompat.setBackgroundTintList(expandEntryContentButton, new ColorStateList(new int[][]{new int[0]}, new int[]{0xaaaa00}));
        }
        if(catVetItems){
            CatVetViewPager catVetViewPager = (CatVetViewPager) entry.findViewById(R.id.catVetViewPager);
            MyScreenSlidePagerAdapter myScreenSlidePagerAdapter = (MyScreenSlidePagerAdapter) catVetViewPager.getAdapter();
            ArrayList<View> views = myScreenSlidePagerAdapter.getItems();
            for(View catvetEntryPage: views){
                setRestTimeLineEntryCatVet(catvetEntryPage);
            }
        }
    }


}
