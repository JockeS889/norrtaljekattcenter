package kattcenter.norrtljekattcenter.catform;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.Spinner;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
/**
 * Created by Jocke on 2017-06-16.
 */

class FormLocker {

    private Context context;
    private HomePage homePage;
    FormLocker(Context context){
        this.context = context;
        homePage = (HomePage) context;
    }

    /*
    void setLockBasicForm(boolean locked){
        homePage.findViewById(R.id.info_cat_photo).setClickable(locked);
        homePage.findViewById(R.id.catbasic_catname_autocomplete_text).setEnabled(locked);
        homePage.findViewById(R.id.catbasic_present_date).setClickable(locked);
        homePage.findViewById(R.id.catbasic_box_text).setEnabled(locked);
        homePage.findViewById(R.id.catbasic_moved_text).setEnabled(locked);
        homePage.findViewById(R.id.catbasic_connection_text).setEnabled(locked);
        homePage.findViewById(R.id.tatooCheckbox).setEnabled(locked);
        homePage.findViewById(R.id.catbasic_tatoo_text).setEnabled(locked);
        homePage.findViewById(R.id.tatooCheckbox).setEnabled(locked);
        //homePage.findViewById(R.id.catbasic_temper_tame).setEnabled(locked);
        //homePage.findViewById(R.id.catbasic_temper_shy).setEnabled(locked);
        homePage.findViewById(R.id.chipCheckbox).setEnabled(locked);
        if(((CheckBox)homePage.findViewById(R.id.chipCheckbox)).isChecked()){
            homePage.findViewById(R.id.scanCode).setEnabled(locked);
        }

        homePage.findViewById(R.id.scannedChipCode).setEnabled(locked);
        homePage.findViewById(R.id.chipCheckbox).setEnabled(locked);
        homePage.findViewById(R.id.neuterCheckbox).setEnabled(locked);
        homePage.findViewById(R.id.neuterCheckbox).setEnabled(locked);
        homePage.findViewById(R.id.catbasic_cathome_name_text).setEnabled(locked);
        homePage.findViewById(R.id.catbasic_cathome_id_text).setEnabled(locked);
        homePage.findViewById(R.id.catbasic_birthday).setClickable(locked);
        homePage.findViewById(R.id.catbasic_birthday_guess).setEnabled(locked);
        homePage.findViewById(R.id.catbasic_sex_type).setEnabled(locked);

        homePage.findViewById(R.id.catbasic_rase_text).setEnabled(locked);
        homePage.findViewById(R.id.catbasic_color_text).setEnabled(locked);
        homePage.findViewById(R.id.catbasic_hair_type).setEnabled(locked);

    }

    void setLockMedicinForm(boolean locked){
        homePage.findViewById(R.id.catmed_start_date).setClickable(locked);
        homePage.findViewById(R.id.catmed_end_date).setClickable(locked);
        homePage.findViewById(R.id.catmed_type).setClickable(locked);
        homePage.findViewById(R.id.catmed_dose_amount).setClickable(locked);
        homePage.findViewById(R.id.catmed_administration).setClickable(locked);
    }

    void setLockVaccinForm(boolean locked){
        homePage.findViewById(R.id.catvaccin_start_date).setClickable(locked);
        homePage.findViewById(R.id.catvaccin_type).setClickable(locked);
        RadioGroup vaccinTime = (RadioGroup)homePage.findViewById(R.id.catvaccin_vaccinTime);
        vaccinTime.getChildAt(0).setEnabled(locked);
        vaccinTime.getChildAt(1).setEnabled(locked);
        homePage.findViewById(R.id.catvaccin_comment).setClickable(locked);
        homePage.findViewById(R.id.catvaccin_repeat_date).setClickable(locked);
    }

    void setLockDewormForm(boolean locked){
        homePage.findViewById(R.id.catdeworming_start_date).setClickable(locked);
        homePage.findViewById(R.id.catdeworming_type).setClickable(locked);
        homePage.findViewById(R.id.catdeworming_dose_text).setEnabled(locked);
        homePage.findViewById(R.id.catdeworming_comment).setClickable(locked);
        homePage.findViewById(R.id.catdeworming_repeat_date).setClickable(locked);
    }

    void setLockVermForm(boolean locked){
        homePage.findViewById(R.id.catvermin_start_date).setClickable(locked);
        homePage.findViewById(R.id.catvermin_type).setClickable(locked);
        homePage.findViewById(R.id.catvermin_treatment).setClickable(locked);
        homePage.findViewById(R.id.catvermin_comment).setClickable(locked);
        homePage.findViewById(R.id.catvermin_repeat_date).setClickable(locked);
    }

    void setLockClawForm(boolean locked){
        System.out.println("SETTING LOCK: "+locked+" FOR CLAWS FORM");
        homePage.findViewById(R.id.catclaw_start_date).setClickable(locked);
        homePage.findViewById(R.id.catclaw_comment).setClickable(locked);
        homePage.findViewById(R.id.catclaw_repeat_date).setClickable(locked);
    }

    void setLockWeightForm(boolean locked){
        homePage.findViewById(R.id.catweight_start_date).setClickable(locked);
        homePage.findViewById(R.id.catweight_weight_text).setEnabled(locked);
        homePage.findViewById(R.id.catweight_comment).setClickable(locked);
        homePage.findViewById(R.id.catweight_check_weight_date).setClickable(locked);
    }

    void setLockVetDocForm(boolean locked){
        homePage.findViewById(R.id.catvet_reg_date).setClickable(locked);
        ((Spinner)homePage.findViewById(R.id.catvet_reg_spinner)).setSelection(0);
    }

    void setLockRegSKKForm(boolean locked){
        RadioGroup regskk = (RadioGroup)homePage.findViewById(R.id.catreg_skk_rg);
        regskk.getChildAt(0).setEnabled(locked);
        regskk.getChildAt(1).setEnabled(locked);
        RadioGroup regsve = (RadioGroup)homePage.findViewById(R.id.catreg_svekatt_rg);
        regsve.getChildAt(0).setEnabled(locked);
        regsve.getChildAt(1).setEnabled(locked);
    }

    void setLockVetForm(boolean locked){
        homePage.findViewById(R.id.catvet_no_known).setEnabled(locked);
        homePage.findViewById(R.id.catvet_bloodsample_date).setEnabled(locked);
        homePage.findViewById(R.id.catvet_bloodsample_result).setEnabled(locked);
        homePage.findViewById(R.id.catvet_known_events).setEnabled(locked);
        homePage.findViewById(R.id.catvet_comment).setEnabled(locked);
    }

    void setLockInsuranceForm(boolean locked){
        homePage.findViewById(R.id.catinsurance_taken_date).setClickable(locked);
        homePage.findViewById(R.id.catinsurance_taken_company).setClickable(locked);
        homePage.findViewById(R.id.catinsurance_old_number).setEnabled(locked);
        homePage.findViewById(R.id.catinsurance_signed_date).setClickable(locked);
        homePage.findViewById(R.id.catinsurance_signed_company).setClickable(locked);
        homePage.findViewById(R.id.catinsurance_new_number).setEnabled(locked);
        homePage.findViewById(R.id.catinsurance_transfered_date).setClickable(locked);
    }

    void setLockBackgroundForm(boolean locked){

        homePage.findViewById(R.id.catbackground_born_outside).setEnabled(locked);
        homePage.findViewById(R.id.catbackground_homeless).setEnabled(locked);
        homePage.findViewById(R.id.catbackground_dumped).setEnabled(locked);
        homePage.findViewById(R.id.catbackground_lost).setEnabled(locked);
        homePage.findViewById(R.id.catbackground_left).setEnabled(locked);
        homePage.findViewById(R.id.catbackground_replacing).setEnabled(locked);
        homePage.findViewById(R.id.catbackground_via_vet).setEnabled(locked);
        homePage.findViewById(R.id.catbackground_comment).setClickable(locked);

    }

    void setLockPrevOwnersForm(boolean locked){

    }

    void setLockStatusForm(boolean locked){
        RadioGroup status = (RadioGroup)homePage.findViewById(R.id.catstatus_status_rg);
        status.getChildAt(0).setEnabled(locked);
        status.getChildAt(1).setEnabled(locked);
        status.getChildAt(2).setEnabled(locked);
        homePage.findViewById(R.id.catstatus_booked).setEnabled(locked);
        homePage.findViewById(R.id.catstatus_adopted).setEnabled(locked);
        homePage.findViewById(R.id.catstatus_callcenter).setEnabled(locked);
        homePage.findViewById(R.id.catstatus_tnr).setEnabled(locked);
        homePage.findViewById(R.id.catstatus_external_cathome).setEnabled(locked);
        homePage.findViewById(R.id.catstatus_external_cathome_text).setClickable(locked);
        homePage.findViewById(R.id.catstatus_missing).setEnabled(locked);
        homePage.findViewById(R.id.catstatus_returned).setEnabled(locked);
        homePage.findViewById(R.id.catstatus_deceased).setEnabled(locked);
    }
    */
}
