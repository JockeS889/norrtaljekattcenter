package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DatabaseConnection;
import kattcenter.norrtljekattcenter.network.DownloadProduct;
import kattcenter.norrtljekattcenter.network.UploadProduct;

/**
 * Created by Jocke on 2017-06-03.
 */

public class VerminInfo implements DatePickerDialog.OnDateSetListener {
    private Context context;
    private Calendar c = Calendar.getInstance();
    private int year, month, day;
    private Dialog dialog;
    private ArrayList<String> verminTypeList, treatmentTypeList;
    private View clickedView;
    private ListView listView;
    private ListAdapter listAdapter;

    public VerminInfo(Context context, View clickedView){
        this.context = context;
        this.clickedView = clickedView;
        dialog = new Dialog(context);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    public void setDate(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
    }

    public void updateTypes(ArrayList<String> types){
        listAdapter.notifyDataSetInvalidated();
        listAdapter = new ListAdapter(context, types);
        listView.setAdapter(listAdapter);
        //listAdapter.notifyDataSetChanged();
    }

    /*
    public void setVerminType(ArrayList<String> verminTypeList){
        this.verminTypeList = verminTypeList;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        listAdapter = new ListAdapter(context, this.verminTypeList);
        listView.setAdapter(listAdapter);
        Button addNewVerminType = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newVerminType = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewVerminType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String verminType = newVerminType.getText().toString();
                if(verminType != null && !verminType.equals("")){
                    ((TextView)clickedView.findViewById(R.id.catvermin_type_text)).setText(verminType);
                    try {
                        new UploadProduct(context, "vermin_types", Type.UPLOAD.toString(), verminType).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj typ av ohyra");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "vermin_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    */

    /*
    public void setTreatmentType(ArrayList<String> treatmentTypeList){
        this.treatmentTypeList = treatmentTypeList;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        listAdapter = new ListAdapter(context, treatmentTypeList);
        listView.setAdapter(listAdapter);
        Button addNewTreatment = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newTreatment = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewTreatment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String treatment = newTreatment.getText().toString();
                if(treatment != null && !treatment.equals("")){
                    ((TextView)clickedView.findViewById(R.id.catvermin_treatment_text)).setText(treatment);
                    try {
                        new UploadProduct(context, "treatment_types", Type.UPLOAD.toString(), treatment).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj behandling");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "treatment_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    */

    public void setComment(){
        final InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        dialog.setTitle("Lägg till kommentar");
        final RelativeLayout comment_form = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.comment_form, null);
        final EditText editText = (EditText) comment_form.findViewById(R.id.commentField);
        (comment_form.findViewById(R.id.addCommentButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(text != null && !text.equals("")) {
                    ((TextView) clickedView.findViewById(R.id.catvermin_comment_text)).setText(text);
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.INVISIBLE);
                    inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    dialog.dismiss();
                }
                else{
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.VISIBLE);
                }
            }
        });

        (comment_form.findViewById(R.id.abortButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                dialog.dismiss();
            }
        });

        dialog.setContentView(comment_form);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        switch (clickedView.getId()){
            case R.id.catvermin_start_date:
                ((TextView)clickedView.findViewById(R.id.catvermin_start_date_text)).setText(year+"-"+monthText+"-"+dayText);
                break;
            case R.id.catvermin_repeat_date:
                ((TextView)clickedView.findViewById(R.id.catvermin_repeat_date_text)).setText(year+"-"+monthText+"-"+dayText);
                break;
            default:break;
        }
    }

    private class ListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> list;
        public ListAdapter(@NonNull Context context, ArrayList<String> list) {
            super(context, 0, list);
            this.list = list;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            final String type = list.get(position);
            if(view == null){
                view = ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_1, null);
            }
            TextView entryName = (TextView) view.findViewById(R.id.listEntry);
            entryName.setText(type);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (clickedView.getId()){
                        case R.id.catvermin_treatment:
                            ((TextView)clickedView.findViewById(R.id.catvermin_treatment_text)).setText(type);
                            break;
                        case R.id.catvermin_type:
                            ((TextView)clickedView.findViewById(R.id.catvermin_type_text)).setText(type);
                            break;
                        default:break;
                    }
                    dialog.dismiss();
                }
            });
            return view;
        }
    }
}
