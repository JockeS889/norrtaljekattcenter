package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DownloadProduct;
import kattcenter.norrtljekattcenter.network.UploadProduct;

/**
 * Created by Jocke on 2017-06-03.
 */

public class MedicalInfo implements DatePickerDialog.OnDateSetListener {

    private Context context;
    private int year, month, day;
    private Calendar c = Calendar.getInstance();
    private ArrayList<String> medicalTypeList, treatmentTypes, doseTypes;
    private Dialog dialog;
    private View clickedView;
    private int doseValue = 0;
    private ListView listView;
    private ListAdapter listAdapter;

    public MedicalInfo(Context context, View clickedView){
        this.context = context;
        this.clickedView = clickedView;


        this.dialog = new Dialog(context);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void clearButton(){
        final LinearLayout linearLayout = (LinearLayout)clickedView.getParent();
        final TextView clear = (TextView) linearLayout.getChildAt(1);
        clear.setVisibility(View.VISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear.setVisibility(View.GONE);
                ((TextView)linearLayout.getChildAt(0)).setText("");
            }
        });

    }

    public void setDate(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
    }

    public void updateTypes(ArrayList<String> types, String type_list_name){
        listAdapter.notifyDataSetInvalidated();
        listAdapter = new ListAdapter(context, types, type_list_name);
        listView.setAdapter(listAdapter);
        //listAdapter.notifyDataSetChanged();
    }

    public void setMedicalType(ArrayList<String> medicalTypeList){
        this.medicalTypeList = medicalTypeList;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        listAdapter = new ListAdapter(context, medicalTypeList, "medicin_types");
        listView.setAdapter(listAdapter);
        Button addNewMedicinType = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newMedicinType = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewMedicinType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String medicinType = newMedicinType.getText().toString();
                if(medicinType != null && !medicinType.equals("")){
                    //((TextView)clickedView.findViewById(R.id.catmed_type_text)).setText(medicinType);
                    addTypeToList(medicinType);
                    try {
                        new UploadProduct(context, "medicin_types", Type.UPLOAD.toString(), medicinType).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj läkemedel");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "medical_info", "medicin_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setDoseType(ArrayList<String> doseTypes){
        this.doseTypes = doseTypes;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        listAdapter = new ListAdapter(context, doseTypes, "dose_types");
        listView.setAdapter(listAdapter);
        Button addNewDose = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newDose = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewDose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dose = newDose.getText().toString();
                if(dose != null && !dose.equals("")){
                    ((TextView)clickedView.findViewById(R.id.catmed_dose_amount_text)).setText(dose);
                    try {
                        new UploadProduct(context, "dose_types", Type.UPLOAD.toString(), dose).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj dos");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "medical_info", "dose_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    public void setTreatmentTypes(ArrayList<String> treatmentTypes){
        this.treatmentTypes = treatmentTypes;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        listAdapter = new ListAdapter(context, treatmentTypes, "treatment_types");
        listView.setAdapter(listAdapter);
        Button addNewTreatmentType = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newTreatmentType = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewTreatmentType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String treatmentType = newTreatmentType.getText().toString();
                if(treatmentType != null && !treatmentType.equals("")){
                    ((TextView)clickedView.findViewById(R.id.catmed_treatment_type_text)).setText(treatmentType);
                    clearButton();
                    try {
                        new UploadProduct(context, "treatment_types", Type.UPLOAD.toString(), treatmentType).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj administrering");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "medical_info", "treatment_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setComment(){
        final InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        final RelativeLayout comment_form = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.comment_form, null);
        final EditText editText = (EditText) comment_form.findViewById(R.id.commentField);
        (comment_form.findViewById(R.id.addCommentButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(text != null && !text.equals("")) {
                    ((TextView) clickedView.findViewById(R.id.catmed_comment_text)).setText(text);
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.INVISIBLE);
                    inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    dialog.dismiss();
                    clearButton();
                }
                else{
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.VISIBLE);
                }
            }
        });

        (comment_form.findViewById(R.id.abortButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                dialog.dismiss();
            }
        });
        dialog.setTitle("Lägg till kommentar");
        dialog.setContentView(comment_form);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        System.out.println(year + " "+month+" "+dayOfMonth);
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        switch (clickedView.getId()){
            case R.id.catmed_start_date_text:
                ((TextView)clickedView.findViewById(R.id.catmed_start_date_text)).setText(year+"-"+monthText+"-"+dayText);

                break;
            case R.id.catmed_end_date_text:
                ((TextView)clickedView.findViewById(R.id.catmed_end_date_text)).setText(year+"-"+monthText+"-"+dayText);
                break;
            case R.id.catmed_repeat_date_text:
                ((TextView)clickedView.findViewById(R.id.catmed_repeat_date_text)).setText(year+"-"+monthText+"-"+dayText);
            default:break;
        }
        clearButton();

    }

    private void addTypeToList(String type){
        final LinearLayout list = (LinearLayout)((LinearLayout)clickedView.getParent()).findViewById(R.id.catmed_type_list);
        final RelativeLayout entry = (RelativeLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.removable_list_entry, null);
        ((TextView)entry.findViewById(R.id.first_object_text)).setText(type);
        entry.findViewById(R.id.second_object_text).setVisibility(View.GONE);
        (entry.findViewById(R.id.remove_object)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.removeView(entry);
            }
        });
        list.addView(entry);
    }

    public class ListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> list;
        private String type_list_name;
        public ListAdapter(@NonNull Context context, ArrayList<String> list, String type_list_name) {
            super(context, 0, list);
            this.list = list;
            this.type_list_name = type_list_name;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            final String type = list.get(position);
            if(view == null){
                view = ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_2, null);
            }
            TextView entryName = (TextView) view.findViewById(R.id.listEntry);
            entryName.setText(type);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (clickedView.getId()){
                        case R.id.catmed_add_type:
                            //REMAKE THIS - MULTIPLE MEDICATIONS SHOULD BE SELECTABLE!!!!!!!!!!

                            addTypeToList(type);
                            //((TextView)clickedView.findViewById(R.id.catmed_type_text)).setText(type);
                            break;
                        case R.id.catmed_treatment_type_text:
                            ((TextView)clickedView.findViewById(R.id.catmed_treatment_type_text)).setText(type);
                            clearButton();
                            break;
                        case R.id.catmed_dose_amount_text:
                            ((TextView)clickedView.findViewById(R.id.catmed_dose_amount_text)).setText(type);
                            clearButton();
                            break;
                        default:break;
                    }
                    dialog.dismiss();
                }
            });
            view.setOnLongClickListener(new MyLongItemPressListener(context, list, type, type_list_name, "medical_info"));
            return view;
        }
    }
}
