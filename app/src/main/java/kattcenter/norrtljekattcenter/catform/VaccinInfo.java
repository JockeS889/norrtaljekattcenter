package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.Category;
import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DatabaseConnection;
import kattcenter.norrtljekattcenter.network.DownloadProduct;
import kattcenter.norrtljekattcenter.network.UploadProduct;

/**
 * Created by Jocke on 2017-06-03.
 */

public class VaccinInfo implements DatePickerDialog.OnDateSetListener {
    private Context context;
    private Calendar c = Calendar.getInstance();
    private int year, month, day;
    private Dialog dialog;
    private ArrayList<String> types;
    private ListAdapter listAdapter;
    private ListView listView;
    private View clickedView;

    public VaccinInfo(Context context, View clickedView){
        this.context = context;
        this.clickedView = clickedView;
        dialog = new Dialog(context);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void clearButton(){
        final LinearLayout linearLayout = (LinearLayout)clickedView.getParent();
        final TextView clear = (TextView) linearLayout.getChildAt(1);
        clear.setVisibility(View.VISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear.setVisibility(View.GONE);
                ((TextView)linearLayout.getChildAt(0)).setText("");
            }
        });

    }

    public void setDate(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
    }

    public void updateTypes(ArrayList<String> types, String type_list_name){
        listAdapter.notifyDataSetInvalidated();
        listAdapter = new ListAdapter(context, types, type_list_name);
        listView.setAdapter(listAdapter);
        //listAdapter.notifyDataSetChanged();
    }

    public void setType(ArrayList<String> types){
        this.types = types;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        listAdapter = new ListAdapter(context, types, "vaccin_types");
        listView.setAdapter(listAdapter);
        Button addNewVaccin = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newVaccin = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewVaccin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String vaccin = newVaccin.getText().toString();
                if(vaccin != null && !vaccin.equals("")){
                    ((TextView)clickedView.findViewById(R.id.catvaccin_type_text)).setText(vaccin);
                    clearButton();
                    try {
                        new UploadProduct(context, "vaccin_types", Type.UPLOAD.toString(), vaccin).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj typ");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "vaccin_info", "vaccin_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setComment(){
        final InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        final RelativeLayout comment_form = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.comment_form, null);
        final EditText editText = (EditText) comment_form.findViewById(R.id.commentField);
        (comment_form.findViewById(R.id.addCommentButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(text != null && !text.equals("")) {
                    ((TextView) clickedView.findViewById(R.id.catvaccin_comment_text)).setText(text);
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.INVISIBLE);
                    inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    dialog.dismiss();
                    clearButton();
                }
                else{
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.VISIBLE);
                }
            }
        });

        (comment_form.findViewById(R.id.abortButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                dialog.dismiss();
            }
        });
        dialog.setTitle("Lägg till kommentar");
        dialog.setContentView(comment_form);
        dialog.show();
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        switch (clickedView.getId()){
            case R.id.catvaccin_start_date_text:
                ((TextView)clickedView.findViewById(R.id.catvaccin_start_date_text)).setText(year+"-"+monthText+"-"+dayText);
                break;
            case R.id.catvaccin_repeat_date_text:
                ((TextView)clickedView.findViewById(R.id.catvaccin_repeat_date_text)).setText(year+"-"+monthText+"-"+dayText);
                break;
            default:break;
        }
        clearButton();
    }

    private class ListAdapter extends ArrayAdapter<String>{

        private ArrayList<String> list;
        private String type_list_name;
        public ListAdapter(@NonNull Context context, ArrayList<String> list, String type_list_name) {
            super(context, 0, list);
            this.list = list;
            this.type_list_name = type_list_name;
        }

        public void setList(ArrayList<String> list){
            this.list = list;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            final String type = list.get(position);
            if(view == null){
                view = ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_2, null);
            }
            TextView entryName = (TextView) view.findViewById(R.id.listEntry);
            entryName.setText(type);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TextView)clickedView.findViewById(R.id.catvaccin_type_text)).setText(type);
                    dialog.dismiss();
                    clearButton();
                }
            });
            view.setOnLongClickListener(new MyLongItemPressListener(context, list, type, type_list_name, "vaccin_info"));
            return view;
        }
    }
}
