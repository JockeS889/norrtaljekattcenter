package kattcenter.norrtljekattcenter.catform;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import net.hockeyapp.android.metrics.model.Base;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.SaveFormInstanceHandler;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.calendar.CalendarManager;
import kattcenter.norrtljekattcenter.file.BitmapWorker;
import kattcenter.norrtljekattcenter.network.DataPacket;


/**
 * Created by Jocke on 2017-06-05.
 */

public class FormController {

    private Context context;
    //private HashMap<String, String> basicformMap = new HashMap<>();
    //private HashMap<String, String> medicalFormMap = new HashMap<>();
    private BasicInfo basicInfo;
    private MedicalInfo medicalInfo;
    private VaccinInfo vaccinInfo;
    private VerminInfo verminInfo;
    private ClawInfo clawInfo;
    private WeightInfo weightInfo;
    private InsuranceInfo insuranceInfo;
    private DewormingInfo dewormingInfo;
    private ParasiteInfo parasiteInfo;

    private ConnectionInfo connectionInfo;
    private CatownerInfo catownerInfo;

    private Temperament temperInfo;

    private LinkedList<HashMap<String, String>> uploadQueue = new LinkedList<>();
    private LinkedList<HashMap<String, String>> downloadQueue = new LinkedList<>();
    private VetInfo vetInfo;


    public FormController(Context context) {
        this.context = context;
        //initFormMaps();
    }



    public void uploadFormToServer(String formObject, String[] list) {
        int formIndex = 0;
        for (String s : list) {
            System.out.println(formObject+" "+s);
            if (s.equals(formObject)) break;
            formIndex++;
        }
        Log.i("CONTROLLER", "FORM NR: "+formIndex);
        SaveFormInstanceHandler saveFormInstanceHandler = new SaveFormInstanceHandler((HomePage) context);
        Bundle data = null;
        String catId = null;
        if(((HomePage)context).getPrivileged() != Type.CATOWNER)
            catId = saveFormInstanceHandler.getCatID();
        //Log.i("FORMCONTROLLER", "SAVING FORM WITH CATID: "+catId);
        String tableName = null;
        HashMap<String, String> databaseMapping = null;



        switch (formIndex) {
            case FormIndexHolder.GENERAL_FORM_INDEX:
                //System.out.println("before retrieving basic form");
                data = saveFormInstanceHandler.saveBasicFormInstance();
                if(((HomePage)context).getPrivileged() != Type.CATOWNER)
                    ((HomePage)context).setCurrentCatId(data.getString("CATHOME_ID"));
                tableName = "catform_basic";
                databaseMapping = new BundleResourceMapping().getBasicTable();
                //System.out.println("after retrieving basic form2");
                break;
            case FormIndexHolder.TEMPERAMENT_FORM_INDEX:
                //TEMPER
                data = saveFormInstanceHandler.saveTemperFormInstance();
                tableName = "catform_temperament";
                databaseMapping = new BundleResourceMapping().getTemperTable();
                break;
            case FormIndexHolder.MEDICAL_FORM_INDEX:
                data = saveFormInstanceHandler.saveMedicationFormInstance();
                tableName = "catform_medical";
                databaseMapping = new BundleResourceMapping().getMedicalTable();
                break;
            case FormIndexHolder.VACCIN_FORM_INDEX:
                data = saveFormInstanceHandler.saveVaccinFormInstance();
                tableName = "catform_vaccination";
                databaseMapping = new BundleResourceMapping().getVaccinTable();
                break;
            case FormIndexHolder.PARASITE_FORM_INDEX:
                data = saveFormInstanceHandler.saveParasitesFormInstance();
                tableName = "catform_parasites";
                databaseMapping = new BundleResourceMapping().getParasitesTable();
                break;
            /*
            case 3:
                data = saveFormInstanceHandler.saveDewormingFormInstance();
                tableName = "catform_deworming";
                databaseMapping = new BundleResourceMapping().getDewormingTable();
                break;
            case 4:
                data = saveFormInstanceHandler.saveVerminFormInstance();
                tableName = "catform_verm";
                databaseMapping = new BundleResourceMapping().getVerminTable();
                break;
                */
            case FormIndexHolder.CLAW_FORM_INDEX:
                data = saveFormInstanceHandler.saveClawFormInstance();
                tableName = "catform_claws";
                databaseMapping = new BundleResourceMapping().getClawTable();
                break;
            case FormIndexHolder.WEIGHT_FORM_INDEX:
                data = saveFormInstanceHandler.saveWeightFormInstance();
                tableName = "catform_weight";
                databaseMapping = new BundleResourceMapping().getWeightTable();
                break;
            case FormIndexHolder.VET_DOC_FORM_INDEX:
                //data = saveFormInstanceHandler.saveVetDocFormInstance();
                try {
                    data = saveFormInstanceHandler.saveVetDocFormInstance();
                    uploadCatVetForms(data, catId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case FormIndexHolder.REG_SKK_FORM_INDEX:
                data = saveFormInstanceHandler.saveRegSKKFormInstance();
                tableName = "catform_regskk";
                databaseMapping = new BundleResourceMapping().getRegSKKTable();
                break;
            case FormIndexHolder.VET_FORM_INFO:
                data = saveFormInstanceHandler.saveVetFormInstance();
                tableName = "catform_vet";
                databaseMapping = new BundleResourceMapping().getVetTable();
                break;
            case FormIndexHolder.INSURANCE_FORM_INDEX:
                data = saveFormInstanceHandler.saveInsuranceFormInstance();
                tableName = "catform_insurance";
                databaseMapping = new BundleResourceMapping().getInsuranceTable();
                break;
            case FormIndexHolder.BACKGROUND_FORM_INDEX:
                //BAKGRUND
                data = saveFormInstanceHandler.saveBackgroundFormInstance();
                tableName = "catform_background";
                databaseMapping = new BundleResourceMapping().getBackgroundTable();
                break;
            case FormIndexHolder.STATUS_FORM_INDEX:
                data = saveFormInstanceHandler.saveStatusFormInstance();
                tableName = "catform_status";
                databaseMapping = new BundleResourceMapping().getStatusTable();
                //uploadHistory(catId, data);
                break;
            case FormIndexHolder.CONNECTION_FORM_INDEX:
                //KOPPLING
                data = saveFormInstanceHandler.saveConnectionFormInstance();
                tableName = "cats";
                databaseMapping = new BundleResourceMapping().getConnectionColumns();
                break;
            case IndexHolder.CATOWNER_FORM_INDEX:
                //Log.i("CONTROLLER", "RIGHT PLACE");
                data = saveFormInstanceHandler.saveCatsNewOwnerFormInstance();
                //catId = ((HomePage)context).getCurrentCatId();
                break;
            default:
                break;
        }
        if(((HomePage)context).getPrivileged() == Type.CATOWNER){
            try {
                uploadPrivateCat(data, databaseMapping);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            catId = ((HomePage)context).getCurrentCatId();
            if(formIndex == IndexHolder.CATOWNER_FORM_INDEX){
                uploadCatowners(data, catId);
            }
            else if(formIndex != IndexHolder.VET_DOC_FORM_INDEX){
                try {
                    uploadForm(data, tableName, catId, databaseMapping);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void addToUploadQueue(HashMap<String, String> data) {

        System.out.println("ADDING TO UPLOAD QUEUE: "+data);
        uploadQueue.add(data);
    }

    public void startUpload() {
        if (uploadQueue.isEmpty()) {
            System.out.println("TOM UPPLADDNINGSKÖ");
            ((HomePage) context).uploadComplete();
        } else {

            try {
                ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(uploadQueue.pollFirst(), Type.UPLOAD));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //new DatabaseConnection(context, uploadQueue.pollFirst(), Type.UPLOAD).execute();
        }
    }


    public void clearUploadQueue() {
        uploadQueue.clear();
    }


    public HashMap<String, Bundle> clearAllForms() {

        HashMap<String, Bundle> clearBundle = new HashMap<>();
        ClearFormHandler clearFormHandler = new ClearFormHandler(context);

        clearBundle.put("BASIC_FORM", clearFormHandler.clearBasicForm());
        clearBundle.put("MEDICAL_FORM", clearFormHandler.clearMedicationForm());
        clearBundle.put("VACCIN_FORM", clearFormHandler.clearVaccinForm());
        clearBundle.put("DEWORMING_FORM", clearFormHandler.clearDewormingForm());
        clearBundle.put("VERMIN_FORM", clearFormHandler.clearVerminForm());
        clearBundle.put("CLAW_FORM", clearFormHandler.clearClawForm());
        clearBundle.put("WEIGHT_FORM", clearFormHandler.clearWeightForm());
        clearBundle.put("VETDOC_FORM", clearFormHandler.clearVetDocForm());
        clearBundle.put("REGSKK_FORM", clearFormHandler.clearRegSKKForm());
        clearBundle.put("VET_FORM", clearFormHandler.clearVetForm());
        clearBundle.put("INSURANCE_FORM", clearFormHandler.clearInsuranceForm());
        clearBundle.put("BACKGROUND_FORM", clearFormHandler.clearBackgroundForm());
        clearBundle.put("PREVIOUS_OWNERS_FORM", clearFormHandler.clearPrevOwnersForm());
        clearBundle.put("STATUS_FORM", clearFormHandler.clearStatusForm());
        clearBundle.put("CATOWNER_FORM", clearFormHandler.clearCatownerForm());


        return clearBundle;
    }


    private void uploadCatowners(Bundle formData, String catid){
        String date = formData.getString("DATE");
        String time = formData.getString("TIME");
        if(time.isEmpty()) time = "00:00";
        ArrayList<CharSequence> arrayList = formData.getCharSequenceArrayList("SELECTED_OWNERS");
        HashMap<String, String> data = new HashMap<>();
        String firstEntry = (String) arrayList.get(0);
        firstEntry = new String(Base64.decode(firstEntry.getBytes(), Base64.NO_WRAP));
        //FORMAT= mail:catid:date
        String[] nameAndMailOwners = firstEntry.split(":");
        String blub = null;
        if(nameAndMailOwners.length == 1){
            //CATCENTER IS OWNER = NO MAIL
            blub = nameAndMailOwners[0] +":NULL:"+catid+":"+Base64.encodeToString((date+" "+time).getBytes(), Base64.NO_WRAP);
        }
        else{
            //STANDARD CATOWNER = MAIL
            blub = nameAndMailOwners[0]+":"+nameAndMailOwners[1] +":"+catid+":"+Base64.encodeToString((date+" "+time).getBytes(), Base64.NO_WRAP);
        }
        data.put("misc_action", "set_catowners");
        data.put("blub", Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP));
        ((HomePage)context).prepareAndSendData(data, Type.UPLOAD_MISC);
    }

    private void uploadPrivateCat(Bundle formData, HashMap<String, String> databaseMapping) throws JSONException{
        Set<String> keys = formData.keySet();
        JSONObject jsonObject = new JSONObject();
        String owner_email = ((HomePage)context).getUserEmail();
        String owner_name = ((TextView)((HomePage)context).getSettingsPage().findViewById(R.id.user_name_text)).getText().toString();
        String owner = Base64.encodeToString((owner_name+":"+owner_email).getBytes(), Base64.NO_WRAP);
        for(String key: keys){
            if(key.equals("CATPHOTO") && formData.getBoolean("CATPHOTO")){
                jsonObject.put(databaseMapping.get(key), uploadPrivateImage(owner));
            }else {
                jsonObject.put(databaseMapping.get(key), formData.get(key));
            }
        }
        jsonObject.put("owner", owner);
        String blub = Base64.encodeToString(jsonObject.toString().getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("form_action", "UPLOAD_PRIVATE_CAT");
        data.put("blub", blub);
        addToUploadQueue(data);
    }

    private void uploadForm(Bundle formData, String tableName, String catId, HashMap<String, String> databaseMapping) throws JSONException{
        HashMap<String, String> data = new HashMap<>();
        Set<String> keys = formData.keySet();
        JSONObject jsonObject = new JSONObject();
        String catName = ((HomePage)context).getCurrentViewedCatname();
        for(String key: keys){
            Log.i("CONTROLLER", "KEY: "+key);
            Log.i("CONTROLLER", "DATA: "+formData.get(key));
            Log.i("CONTROLLER", "MAPPING: "+ databaseMapping.get(key));

            //System.out.println("KEY: "+key+" VALUE: "+formData.get(key)+" DBM: "+databaseMapping.get(key));
            if(tableName.equals("catform_medical")){
                if(key.equals("START_DATE") || key.equals("REPEAT_DATE") || key.equals("END_DATE")) {
                    String info =
                            "Kattid: "+catId+"\n"+
                            "Läkemedel: "+formData.get("MEDICATION")+"\n"+
                            "Dosering: "+formData.get("DOSE")+"\n"+
                            "Behandling: "+formData.get("TREATMENT_TYPE");
                    String desc = info+"\nKommentar: " + formData.get("COMMENT");

                    new CalendarManager(context).addEvent("Medicinering\nKatt: "+catName, desc, formData.getString(key), formData.getString(key));
                }
            }else if(tableName.equals("catform_vaccination")){
                if(key.equals("START_DATE") || key.equals("REPEAT_DATE")) {
                    String info =
                            "Kattid: "+catId+"\n"+
                                    "Vaccintyp: "+formData.get("VACCIN_TYPE");
                    String desc = info + "\nKommentar: " + formData.get("COMMENT");
                    new CalendarManager(context).addEvent("Vaccination\nKatt: "+catName, desc, formData.getString(key), formData.getString(key));
                }
            }else if(tableName.equals("catform_parasites")){
                if(key.equals("ADMINISTRATION_DATE") || key.equals("REPEAT_DATE")) {
                    String info =
                            "Kattid: "+catId+"\n"+
                            "Parasit-typ: "+formData.get("PARASITE_TYPE")+"\n"+
                            "Läkemedel: "+formData.get("MEDICAL")+"\n"+
                            "Dosering: "+formData.get("DOSE")+"\n"+
                            "Administration: "+formData.get("ADMINISTRATION_TYPE");
                    String desc = info + "\nKommentar: " + formData.get("COMMENT");
                    new CalendarManager(context).addEvent("Parasitbehandling\nKatt: "+catName, desc, formData.getString(key), formData.getString(key));
                }
            }


            if(key.equals("CATPHOTO") && formData.getBoolean("CATPHOTO")){
                jsonObject.put(databaseMapping.get(key), uploadImage(catId));
            }
            else if(key.equals("SAMPLE_RESULT")){
                jsonObject.put(databaseMapping.get(key), uploadFile(catId, null, 0, true));
            }
            else if(databaseMapping.containsKey(key)){
                if(tableName.equals("catform_regskk")){
                    String value;
                    if(key.equals("DOCUMENTS")){
                        String[] docNames = formData.getString(key).split(":");
                        //String[] docNames = bundle.getString("DOCUMENTS").split(":");
                        String encodedString ="";
                        ArrayList<String> unEncodedList = new ArrayList<>();
                        for(int i = 0; i < docNames.length; i++){
                            if(docNames[i] != null && !docNames[i].isEmpty()) {
                                String unEncodedfileName = uploadFile(catId, docNames[i], i, false);
                                unEncodedList.add(unEncodedfileName);
                                encodedString += Base64.encodeToString(unEncodedfileName.getBytes(), Base64.NO_WRAP) + ":";
                            }
                        }
                        HashMap<String, String> fileData = new HashMap<>();
                        fileData.put("form_action", Type.SKKDOC_FILE.toString());
                        fileData.put("file_names", encodedString);
                        addToUploadQueue(fileData);
                        jsonObject.put("documents", Base64.encodeToString(encodedString.getBytes(), Base64.NO_WRAP));
                    }
                    else {
                        if (formData.getString(key).equals("Ja")) {
                            value = "";
                        } else {
                            value = formData.getString(key).split("  ")[1];
                        }
                        jsonObject.put(databaseMapping.get(key), value);
                    }
                }
                else if(tableName.equals("catform_vet")){
                    if(!key.equals("NO_KNOWN")){
                        String value = formData.getString(key);
                        String newKey = key;
                        if(key.equals("NO_KNOWN_DATE")) {
                            newKey = "NO_KNOWN";
                            if (value.contains("(") && value.contains(")")) {
                                value = value.substring(value.indexOf("(")+1, value.indexOf(")"));
                            }
                            else{
                                value = "";
                            }
                        }
                        System.out.println("NEWKEY: "+newKey+" VALUE: "+value);
                        jsonObject.put(databaseMapping.get(newKey), value);
                    }
                }
                else {
                    jsonObject.put(databaseMapping.get(key), formData.get(key));
                }
            }
            //jsonObject.put(databaseMapping.get(key), formData.get(key));
        }
        jsonObject.put("catcenter_id", catId);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDate = simpleDateFormat.format(Calendar.getInstance().getTime());
        //Log.i("CONTROLLER", "CURRENT DATE: "+currentDate);
        String currentUserName = ((HomePage)context).getCurrentUserName();
        String priv = ((HomePage)context).getPrivileged().toString();
        String currentAuthor = Base64.encodeToString((currentUserName+":"+priv).getBytes(), Base64.NO_WRAP);

        if(!tableName.equals("catform_basic") && !tableName.equals("cats")) {
            jsonObject.put("reg_date", currentDate);
            jsonObject.put("reg_author", currentAuthor);
        }

        System.out.println("JSON: "+jsonObject.toString());
        String blub = Base64.encodeToString(jsonObject.toString().getBytes(), Base64.NO_WRAP);

        if(tableName.equals("cats")){
            blub = Base64.encodeToString((tableName+":"+blub+":"+catId+":"+((HomePage)context).getCurrentViewedCatname()+":json").getBytes(), Base64.NO_WRAP);
            data.put("form_action", "UPDATE_FAMILY");
        }
        else {
            String isPrivateCat = "";
            String owner = "";
            if(tableName.equals("catform_basic")){
                if(((HomePage)context).isNewPrivateCat()){
                    isPrivateCat = "yes";
                    catId = "none";
                    owner = ((HomePage)context).getUserEmail();
                    ((HomePage)context).setNewPrivateCat(false);
                }
                else{
                    isPrivateCat = "no";
                }
            }
            blub = Base64.encodeToString((tableName+":"+blub+":"+catId+":json:"+owner+":"+isPrivateCat).getBytes(), Base64.NO_WRAP);
            data.put("form_action", "UPLOAD");
        }
        data.put("blub", blub);
        addToUploadQueue(data);
    }

    private String generateFileName(){
        String fileName = Base64.encodeToString((new String((new Random()).nextInt(15000) + "")).getBytes(), Base64.NO_WRAP);
        Long ts = System.currentTimeMillis() / 1000;
        String timestamp = ts.toString();
        fileName += ":" + Base64.encodeToString(timestamp.getBytes(), Base64.NO_WRAP);
        fileName = Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
        fileName += ".jpg";
        return fileName;
    }

    private String generateSecureFileName(){
        SecureRandom secureRandom = new SecureRandom();
        byte[] bytes = new byte[32];
        secureRandom.nextBytes(bytes);
        byte[] firstEncoding = Base64.encode(bytes, Base64.NO_WRAP);
        return Base64.encodeToString(firstEncoding, Base64.NO_WRAP) + ".jpg";
    }

    public String uploadPrivateImage(String owner){
        String fileName = generateSecureFileName();
        System.out.println("PRIVATE CAT FILENAME: "+fileName);
        HashMap<String, String> data = new HashMap<>();
        data.put("form_action", "UPLOAD_IMAGE");
        data.put("file_name", fileName);

        /*
        HashMap<String, String> data2 = new HashMap<>();
        data2.put("form_action", "UPLOAD_PRIVATE_IMAGE_SRC");
        data2.put("file_name", fileName);
        data2.put("owner", owner);
        */
        addToUploadQueue(data);
        //addToUploadQueue(data2);
        return fileName;
    }
    public String uploadImage(String catId){
        //new BitmapWorker(context).execute();

        String fileName;
        String currentFileName = ((HomePage)context).getCurrentViewedCatImageFileName();
        if(currentFileName == null || currentFileName.equals(""))
            currentFileName = "NONE";
        System.out.println("CURRENT FILE NAME: "+currentFileName);
        System.out.println("PHOTO CHANGED: "+((HomePage)context).isChangedCatPhoto());
        Log.i("CONTROLLER", "TAKEN PHOTO: "+((HomePage)context).isChangedCatPhoto());
        if(((HomePage)context).isChangedCatPhoto()){
            /*
            fileName = Base64.encodeToString((new String((new Random()).nextInt(15000) + "")).getBytes(), Base64.NO_WRAP);
            Long ts = System.currentTimeMillis() / 1000;
            String timestamp = ts.toString();
            fileName += ":" + Base64.encodeToString(timestamp.getBytes(), Base64.NO_WRAP);
            fileName = Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
            fileName += ".jpg";
            */
            fileName = generateSecureFileName();

            HashMap<String, String> data = new HashMap<>();
            data.put("form_action", "UPLOAD_IMAGE");
            data.put("file_name", fileName);

            HashMap<String, String> data2 = new HashMap<>();
            data2.put("form_action", "UPLOAD_IMAGE_SRC");
            data2.put("file_name", fileName);
            data2.put("previous_file_name", currentFileName);
            data2.put("catid", catId);

            addToUploadQueue(data);
            addToUploadQueue(data2);
        }
        else{
            fileName = currentFileName;
        }
        return fileName;
    }

    private String uploadFile(String catid, String fileName, int index, boolean upload){
        //String fileName;
        //fileName = Base64.encodeToString((new String((new Random()).nextInt(15000) + "")).getBytes(), Base64.NO_WRAP);
        if(fileName == null){
            fileName = ((HomePage)context).getCurrentSampleFilename();
            if(fileName == null){
                return null;
            }
        }

        Long ts = System.currentTimeMillis() / 1000;
        String timestamp = ts.toString();

        /**
         * FORMAT:
         * B64(B64(fileName):B64(TIMESTAMP):B64(CATID)).pdf
         */

        //fileName = ((HomePage)context).getCurrentSampleFilename();
        fileName = Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
        fileName += ":" + Base64.encodeToString(timestamp.getBytes(), Base64.NO_WRAP);
        //fileName = Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
        fileName += ":" + Base64.encodeToString(catid.getBytes(), Base64.NO_WRAP);
        fileName = Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
        fileName += ".pdf";
        Log.i("FORMCONTROLLER", "ADDING FILE TO UPLOAD QUEUE: "+fileName);
        if(upload) {
            HashMap<String, String> data = new HashMap<>();
            data.put("form_action", "UPLOAD_SAMPLE_FILE");
            data.put("file_name", fileName);
            data.put("file_index", new String(index+""));
            addToUploadQueue(data);
            return Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
        }
        else{
            return fileName;
        }
    }



    public String uploadSample(String catid){
        String fileName;
        //String currentFileName = ((HomePage)context).getCurrentViewedCatImageFileName();
        //if(currentFileName == null || currentFileName.equals(""))
        //    currentFileName = "NONE";
        //System.out.println("CURRENT FILE NAME: "+currentFileName);
        //System.out.println("PHOTO CHANGED: "+((HomePage)context).isChangedCatPhoto());
        //Log.i("CONTROLLER", "TAKEN PHOTO: "+((HomePage)context).isChangedCatPhoto());
        //if(((HomePage)context).isChangedCatPhoto()){
        fileName = Base64.encodeToString((new String((new Random()).nextInt(15000) + "")).getBytes(), Base64.NO_WRAP);
        Long ts = System.currentTimeMillis() / 1000;
        String timestamp = ts.toString();

        /**
         * FORMAT:
         * B64(B64(fileName):B64(TIMESTAMP):B64(CATID)).pdf
         */

        fileName = ((HomePage)context).getCurrentSampleFilename();
        fileName = Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
        fileName += ":" + Base64.encodeToString(timestamp.getBytes(), Base64.NO_WRAP);
        //fileName = Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
        fileName += ":" + Base64.encodeToString(catid.getBytes(), Base64.NO_WRAP);
        fileName = Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
        fileName += ".pdf";
        Log.i("FORMCONTROLLER", "ADDING FILE TO UPLOAD QUEUE: "+fileName);
        HashMap<String, String> data = new HashMap<>();
        data.put("form_action", "UPLOAD_FILE");
        data.put("file_name", fileName);

        /*
        HashMap<String, String> data2 = new HashMap<>();
        data2.put("form_action", "UPLOAD_FILE_SRC");
        data2.put("file_name", fileName);
        //data2.put("previous_file_name", currentFileName);
        data2.put("catid", catid);
         */
        addToUploadQueue(data);
        //addToUploadQueue(data2);
        //}
        //else{
            //fileName = currentFileName;
        //}
        return Base64.encodeToString(fileName.getBytes(), Base64.NO_WRAP);
    }


    private void uploadCatVetForms(Bundle bundle, String catID) throws JSONException{
        String[] vetForms = context.getResources().getStringArray(R.array.catvet_items);
        HashMap<String, String> dbMapping = new HashMap<>();
        dbMapping.put(vetForms[0], "catform_vetdoc_generalstate");
        dbMapping.put(vetForms[1], "catform_vetdoc_lynne");
        dbMapping.put(vetForms[2], "catform_vetdoc_skinpaws");
        dbMapping.put(vetForms[3], "catform_vetdoc_lymf");
        dbMapping.put(vetForms[4], "catform_vetdoc_eyes");
        dbMapping.put(vetForms[5], "catform_vetdoc_ears");
        dbMapping.put(vetForms[6], "catform_vetdoc_mouth");
        dbMapping.put(vetForms[7], "catform_vetdoc_abdomen");
        dbMapping.put(vetForms[8], "catform_vetdoc_circulationorgan");
        dbMapping.put(vetForms[9], "catform_vetdoc_respirationorgan");
        dbMapping.put(vetForms[10], "catform_vetdoc_outergenitalias");
        dbMapping.put(vetForms[11], "catform_vetdoc_locomotiveorgan");


        HashMap<String, HashMap<String, Boolean>> record = ((HomePage)context).getRecord();
        HashMap<String, String> notes = ((HomePage)context).getCatvetreg_comments();
        Set<String> keys = record.keySet();
        for(String key: keys){
            //System.out.println("KEY: "+key+"  MAP: "+record.get(key).toString());
            HashMap<String, String> data = new HashMap<>();
            HashMap<String, Boolean> recordData = record.get(key);
            String table = dbMapping.get(key);
            Set<String> dataKeys = recordData.keySet();
            JSONObject jsonObject = new JSONObject();
            for(String dataKey: dataKeys){
                String dbCol = "";
                if(dataKey.equals("U.a."))
                    dbCol = "without_remark";
                else if(dataKey.equals("A.T. u.a."))
                    dbCol = "general_without_remark";
                else if(dataKey.equals("AT. Nedsatt"))
                    dbCol = "generally_low";
                else {
                    dbCol = dataKey;
                    dbCol = dbCol.replaceAll(" ", "_");
                    dbCol = dbCol.replaceAll("/", "_");
                }
                jsonObject.put(dbCol, recordData.get(dataKey));
                System.out.println("COLUMNNAME: "+dbCol+"  VALUE: "+recordData.get(dataKey));
            }
            jsonObject.put("catcenter_id", catID);
            jsonObject.put("note", notes.get(key));
            jsonObject.put("date", bundle.get("DATE"));
            if(table.equals("catform_vetdoc_generalstate")){
                String[] docNames = bundle.getString("DOCUMENTS").split(":");
                String encodedString ="";
                ArrayList<String> unEncodedList = new ArrayList<>();
                for(int i = 0; i < docNames.length; i++){
                    Log.i("FORMCONTROLLER", "UPLOADING FILE: "+docNames[i]);
                    if(docNames[i] != null && !docNames[i].isEmpty()) {
                        String unEncodedfileName = uploadFile(catID, docNames[i], i, false);
                        unEncodedList.add(unEncodedfileName);
                        encodedString += Base64.encodeToString(unEncodedfileName.getBytes(), Base64.NO_WRAP) + ":";
                    }
                }
                HashMap<String, String> fileData = new HashMap<>();
                fileData.put("form_action", Type.VETDOC_FILE.toString());
                fileData.put("file_names", encodedString);
                addToUploadQueue(fileData);
                jsonObject.put("documents", Base64.encodeToString(encodedString.getBytes(), Base64.NO_WRAP));
            }


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDate = simpleDateFormat.format(Calendar.getInstance().getTime());
            //Log.i("CONTROLLER", "CURRENT DATE: "+currentDate);
            String currentUserName = ((HomePage)context).getCurrentUserName();
            String priv = ((HomePage)context).getPrivileged().toString();
            String currentAuthor = Base64.encodeToString((currentUserName+":"+priv).getBytes(), Base64.NO_WRAP);
            jsonObject.put("reg_date", currentDate);
            jsonObject.put("reg_author", currentAuthor);

            System.out.println("UPLOADING TO TABLE: "+table);
            String blub = Base64.encodeToString(jsonObject.toString().getBytes(), Base64.NO_WRAP);
            blub = Base64.encodeToString((table+":"+blub+":"+catID+":json").getBytes(), Base64.NO_WRAP);
            data.put("form_action", "UPLOAD_VETDOC");
            data.put("blub", blub);
            addToUploadQueue(data);
        }
    }


    public void addToDownloadQueue(HashMap<String, String> data){
        downloadQueue.add(data);
    }

    public void startDownloadForms(){
        if(downloadQueue.isEmpty()){
            System.out.println("DOWNLOAD QUEUE IS EMPTY");
            ((HomePage)context).formDownloadComplete();
        }
        else{
            try {
                ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(downloadQueue.pollFirst(), Type.DOWNLOAD_FORM));
                ((HomePage)context).signalQueueToSend();
                System.out.println("Added Packet to Thread");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //new DatabaseConnection(context, downloadQueue.pollFirst(), Type.DOWNLOAD_FORM).execute();
        }
    }


    public void clearDownloadQueue(){
        downloadQueue.clear();
    }

    public void handleAction(View view){
        switch (view.getId()){
            /*
            case R.id.info_cat_photo:
            case R.id.catbasic_present_date:
            case R.id.catbasic_birthday:
            case R.id.neuterCheckbox:
            case R.id.tatooCheckbox:
            case R.id.catbasic_tatoo:
            case R.id.chipCheckbox:
            case R.id.scanCode:
            */
            case R.id.info_cat_photo:
            case R.id.catbasic_select_catid:
            case R.id.catbasic_select_arrived_date:
            case R.id.catbasic_select_moved_date:
            case R.id.catbasic_select_neuter_date:
            case R.id.catbasic_select_tatoo_date_and_text:
            case R.id.catbasic_select_chip_date:
            case R.id.catbasic_select_birthday_date:
            case R.id.catbasic_select_sex_type:
            case R.id.catbasic_select_race:
            case R.id.catbasic_select_color:
            case R.id.catbasic_select_hair_type:
            case R.id.selected_date_catowner_cat:
            case R.id.reminder_date_catowner_cat:
            case R.id.saveCatownerCatActionReminder:
            case R.id.send_reminder_holder:
                System.out.println("HEEL");
                setBasicInfo(view);
                break;
            case R.id.catmed_start_date_text:
            case R.id.catmed_end_date_text:
            case R.id.catmed_add_type:
            case R.id.catmed_dose_amount_text:
            case R.id.catmed_treatment_type_text:
            case R.id.catmed_comment_text:
            case R.id.catmed_repeat_date_text:
                setMedicalInfo(view);
                break;
            case R.id.catvaccin_start_date_text:
            case R.id.catvaccin_type_text:
            case R.id.catvaccin_vaccinTime:
            case R.id.catvaccin_comment_text:
            case R.id.catvaccin_repeat_date_text:
                setVaccinInfo(view);
                break;
            case R.id.catparasite_admininstration_date_text:
            case R.id.catparasite_type_text:
            case R.id.catparasite_med_add_type:
            case R.id.catparasite_administration_text:
            case R.id.catparasite_comment_text:
            case R.id.catparasite_repeat_date_text:
                setParasiteInfo(view);
                break;
            case R.id.catclaw_start_date_text:
            case R.id.catclaw_comment_text:
            case R.id.catclaw_repeat_date_text:
                setClawInfo(view);
                break;
            case R.id.catweight_start_date_text:
            case R.id.catweight_weight_text:
            case R.id.catweight_comment_text:
            case R.id.catweight_check_weight_date_text:
                setWeightInfo(view);
                break;
            case R.id.catvet_reg_date_text:
            case R.id.catvetreg_note_text:
            case R.id.catvetreg_addDocumentToList:
                setVetDocInfo(view);
                break;
            case R.id.catreg_skk_yes_rb:
            case R.id.catreg_skk_no_rb:
            case R.id.catreg_svekatt_yes_rb:
            case R.id.catreg_svekatt_no_rb:
            case R.id.catskkreg_addDocumentToList:
                setRegSKKInfo(view);
                break;
            case R.id.catvet_no_known:
            case R.id.catvet_bloodsample_date_text:
            case R.id.catvet_bloodsample_result_text:
            case R.id.catvet_known_events_text:
            case R.id.catvet_comment_text:
                setVetInfo(view);
                break;
            case R.id.catinsurance_taken_date:
            case R.id.catinsurance_taken_company:
            case R.id.catinsurance_signed_date:
            case R.id.catinsurance_signed_company:
            case R.id.catinsurance_transfered_date:
                setInsuranceInfo(view);
                break;
            case R.id.catbackground_comment:
            case R.id.catbackground_born_outside:
            case R.id.catbackground_homeless:
            case R.id.catbackground_dumped:
            case R.id.catbackground_lost:
            case R.id.catbackground_left:
            case R.id.catbackground_estate:
            case R.id.catbackground_replacing:
            case R.id.catbackground_via_vet:
                setBackgroundInfo(view);
                break;
            case R.id.catstatus_cathome:
            case R.id.catstatus_not_bought:
            case R.id.catstatus_new_arrived:
            case R.id.catstatus_adoptable:
            case R.id.catstatus_booked:
            case R.id.catstatus_adopted:
            case R.id.catstatus_callcenter:
            case R.id.catstatus_tnr:
            case R.id.catstatus_external_cathome:
            case R.id.catstatus_missing:
            case R.id.catstatus_returned:
            case R.id.catstatus_deceased:
                setStatusInfo(view);
                break;
            case R.id.catconnection_mother:
            case R.id.catconnection_father:
            case R.id.catconnection_add_sibling:
            case R.id.catconnection_add_child:
                setConnectionInfo(view);
                break;
            case R.id.catform_catowner_date:
            case R.id.catform_catowner_time:
            case R.id.cat_add_catowner:
                setCatownerInfo(view);
                break;
            case R.id.catform_temperament_tame:
            case R.id.catform_temperament_shy:
            case R.id.catform_temperament_temper_text:
            case R.id.catform_temperament_adaptable_skills_text:
            case R.id.catform_temperament_used_to_children_text:
            case R.id.catform_temperament_used_to_dogs_text:
            case R.id.catform_temperament_carsick_text:
            case R.id.catform_temperament_talking_text:
            case R.id.catform_temperament_comment_text:
                setTemperInfo(view);
            default:break;
        }
    }

    public void updateProductList(String table_name, String current_form, ArrayList<String> types){
        switch (table_name){
            case "medicin_types":
            case "administration_types":
                switch (current_form){
                    case "parasite_info":
                        parasiteInfo.updateTypes(types, table_name);
                        break;
                    case "medical_info":
                        medicalInfo.updateTypes(types, table_name);
                        break;
                    default:break;
                }

                break;
            case "vaccin_types":
                vaccinInfo.updateTypes(types, table_name);
                break;
            case "parasite_types":
                parasiteInfo.updateTypes(types, table_name);
                break;
            case "treatment_types":
                switch (current_form){
                    case "parasite_info":
                        parasiteInfo.updateTypes(types, table_name);
                        break;
                    case "medical_info":
                        medicalInfo.updateTypes(types, table_name);
                        break;
                    default:break;
                }
                break;
            case "company_types":
                insuranceInfo.updateCompanies(types, table_name);
                break;
            case "dose_types":
                switch (current_form){
                    case "parasite_info":
                        parasiteInfo.updateTypes(types, table_name);
                        break;
                    case "medical_info":
                        medicalInfo.updateTypes(types, table_name);
                        break;
                    default:break;
                }
                break;
            case "race_types":
            case "color_types":
            case "hair_types":
                basicInfo.updateList(types, table_name);
                break;
            default:break;
        }
    }

    public void updateProductList(String table_name, String encoded_list){
        ArrayList<String> types = new ArrayList<>();
        String decoded_list = new String(Base64.decode(encoded_list.getBytes(), Base64.NO_WRAP));
        String[] list = decoded_list.split(":");
        String current_form = list[0];
        if(list[1].equals("EMPTY_LIST")){
            Toast.makeText(context, "Listan är tom", Toast.LENGTH_LONG).show();
        }
        else {

            for (int i = 1; i < list.length; i++) {
                String s = list[i];
                if (s != null && !s.equals("")) {
                    types.add(s);
                }
            }
        }
        updateProductList(table_name, current_form, types);

    }


    private void setBasicInfo(View view){
        basicInfo = new BasicInfo(context, view);
        switch (view.getId()){
            case R.id.info_cat_photo:
                basicInfo.takePhoto();
                break;
            case R.id.catbasic_select_catid:
                basicInfo.showIdDialog();
                break;
            case R.id.catbasic_select_birthday_date:
                basicInfo.birthdayOptions();
                break;
            case R.id.catbasic_select_arrived_date:
            case R.id.catbasic_select_moved_date:
            case R.id.catbasic_select_neuter_date:
                basicInfo.dateOptions();
                break;
            case R.id.catbasic_select_tatoo_date_and_text:
                basicInfo.tatooOptions();
                break;
            case R.id.catbasic_select_chip_date:
                basicInfo.chipOptions();
                break;
            case R.id.catbasic_select_race:
                basicInfo.loadListDialog("race_table", "race_types");
                break;
            case R.id.catbasic_select_color:
                basicInfo.loadListDialog("color_table", "color_types");
                break;
            case R.id.catbasic_select_hair_type:
                basicInfo.loadListDialog("hair_table", "hair_types");
                break;
            case R.id.catbasic_select_sex_type:
                basicInfo.setSexType();
                break;
            case R.id.send_reminder_holder:
                basicInfo.showCatownerCatActionWindow();
                break;
            /*
            case R.id.catbasic_present_date:
            case R.id.catbasic_birthday:
                basicInfo.setDate();
                break;
            case R.id.neuterCheckbox:
            case R.id.tatooCheckbox:
            case R.id.chipCheckbox:
                basicInfo.handleCheckBox();
                break;
            case R.id.catbasic_tatoo:
                basicInfo.handleTatooInput();
                break;
            case R.id.scanCode:
                basicInfo.scanCode();
                break;
                */
            default:break;
        }


    }

    public void setGeneratedCatID(String json){
        basicInfo.setGeneratedCatID(json);
    }

    public void unavailableCatID(){
        basicInfo.unavailableCatID();
    }

    public void availableCatID(){
        basicInfo.availableCatID();
    }

    private void setMedicalInfo(View view){
        medicalInfo = new MedicalInfo(context, view);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Laddar lista...");
        switch (view.getId()){
            case R.id.catmed_start_date_text:
            case R.id.catmed_end_date_text:
            case R.id.catmed_repeat_date_text:
                medicalInfo.setDate();
                break;
            case R.id.catmed_add_type:
                medicalInfo.setMedicalType(arrayList);
                break;
            case R.id.catmed_dose_amount_text:
                medicalInfo.setDoseType(arrayList);
                break;
            case R.id.catmed_treatment_type_text:
                medicalInfo.setTreatmentTypes(arrayList);
                break;
            case R.id.catmed_comment_text:
                medicalInfo.setComment();
                break;
            default:break;
        }
    }

    private void setVaccinInfo(View view){

        vaccinInfo = new VaccinInfo(context, view);
        switch (view.getId()){
            case R.id.catvaccin_start_date_text:
            case R.id.catvaccin_repeat_date_text:
                vaccinInfo.setDate();
                break;
            case R.id.catvaccin_comment_text:
                vaccinInfo.setComment();
                break;
            case R.id.catvaccin_type_text:
                ArrayList<String> arrayList = new ArrayList<>();
                arrayList.add("Laddar lista...");
                vaccinInfo.setType(arrayList);
                break;
            default:break;
        }
    }

    public void updateVaccinList(String encoded_list){
        ArrayList<String> types = new ArrayList<>();
        String decoded_list = new String(Base64.decode(encoded_list.getBytes(), Base64.NO_WRAP));
        String[] list = decoded_list.split(":");
        for(String s: list){
            System.out.println("LISTELEMENT: "+s);
            if(s != null && !s.equals("")){
                types.add(s);
            }
        }
        System.out.println("NEW TYPES: "+types.size());
        vaccinInfo.updateTypes(types, "vaccin_types");
    }

    public void setParasiteInfo(View view){
        parasiteInfo = new ParasiteInfo(context, view);
        ArrayList<String> arrayList = new ArrayList<>();
        //arrayList.add("Laddar lista...");
        switch (view.getId()){
            case R.id.catparasite_admininstration_date_text:
            case R.id.catparasite_repeat_date_text:
                parasiteInfo.setDate();
                break;
            case R.id.catparasite_type_text:
                parasiteInfo.setParasiteType(arrayList);
                break;
            case R.id.catparasite_med_add_type:
                parasiteInfo.setMedicalType(arrayList);
                break;
            case R.id.catparasite_administration_text:
                parasiteInfo.setAdministrationType(arrayList);
                break;
            case R.id.catparasite_comment_text:
                parasiteInfo.setComment();
                break;
            /*
            case R.id.catparasite_dose_text:
                parasiteInfo.setDoseType(arrayList);
                */
            default:break;
        }
    }

    private void setClawInfo(View view){
        clawInfo = new ClawInfo(context, view);
        switch (view.getId()){
            case R.id.catclaw_start_date_text:
                clawInfo.setDate();
                break;
            case R.id.catclaw_comment_text:
                clawInfo.setComment();
                break;
            case R.id.catclaw_repeat_date_text:
                clawInfo.setDate();
                break;
            default:break;
        }
    }

    private void setWeightInfo(View view){
        weightInfo = new WeightInfo(context, view);
        switch (view.getId()){
            case R.id.catweight_start_date_text:
            case R.id.catweight_check_weight_date_text:
                weightInfo.setDate();
                break;
            case R.id.catweight_weight_text:
                weightInfo.setWeight();
                break;
            case R.id.catweight_comment_text:
                weightInfo.setComment();
                break;
            default:break;
        }
    }


    private void setVetDocInfo(View view){
        //VetRegInfo vetRegInfo = new VetRegInfo(context);
        VetRegInfo vetRegInfo = ((HomePage)context).getVetRegInfo();
        switch (view.getId()){
            case R.id.catvet_reg_date_text:
                vetRegInfo.setDate(view);
                break;
            case R.id.catvetreg_note_text:
                vetRegInfo.setComment(view);
                break;
            case R.id.catvetreg_addDocumentToList:
                vetRegInfo.documentChooser();
                break;
            default:break;
        }
    }

    private void setRegSKKInfo(View view){
        //RegSKKInfo regSKKInfo = new RegSKKInfo(context);
        RegSKKInfo regSKKInfo = ((HomePage)context).getRegSKKInfo();
        switch (view.getId()){
            case R.id.catreg_skk_yes_rb:
            case R.id.catreg_skk_no_rb:
            case R.id.catreg_svekatt_yes_rb:
            case R.id.catreg_svekatt_no_rb:
                RadioButton radioButton = (RadioButton) view;
                regSKKInfo.setDate(radioButton);
                break;
            case R.id.catskkreg_addDocumentToList:
                regSKKInfo.documentChooser();
                break;
            default:break;
        }
    }


    private void setVetInfo(View view){
        vetInfo = new VetInfo(context, view);
        switch (view.getId()){
            case R.id.catvet_no_known:
                vetInfo.handleCheckBox();
                break;
            case R.id.catvet_bloodsample_date_text:
                vetInfo.setDate();
                break;
            case R.id.catvet_bloodsample_result_text:
                vetInfo.fileChooser();
                break;
            case R.id.catvet_known_events_text:
                vetInfo.setComment();
                break;
            case R.id.catvet_comment_text:
                vetInfo.setComment();
                break;
            default:break;
        }
    }

    private void setInsuranceInfo(View view){
        insuranceInfo = new InsuranceInfo(context, view);
        switch (view.getId()){
            case R.id.catinsurance_taken_date:
            case R.id.catinsurance_signed_date:
            case R.id.catinsurance_transfered_date:
                insuranceInfo.setDate();
                break;
            case R.id.catinsurance_taken_company:
            case R.id.catinsurance_signed_company:
                ArrayList<String> arrayList = new ArrayList<>();
                arrayList.add("Laddar lista...");
                insuranceInfo.setCompany(arrayList);
                break;
            default:break;
        }
    }

    private void setBackgroundInfo(View view){
        BackgroundInfo backgroundInfo = new BackgroundInfo(context, view);
        switch (view.getId()){
            case R.id.catbackground_comment:
                backgroundInfo.setComment(view);
                break;
            case R.id.catbackground_born_outside:
            case R.id.catbackground_homeless:
            case R.id.catbackground_dumped:
            case R.id.catbackground_lost:
            case R.id.catbackground_left:
            case R.id.catbackground_estate:
            case R.id.catbackground_replacing:
            case R.id.catbackground_via_vet:
                backgroundInfo.handleCheckbox();
                break;
            default:break;
        }

    }

    private void setStatusInfo(View view){
        StatusInfo statusInfo = new StatusInfo(context, view);
        switch (view.getId()){
            case R.id.catstatus_not_bought:
            case R.id.catstatus_new_arrived:
            case R.id.catstatus_adoptable:
                statusInfo.handleRadioButton((ViewGroup)view.getParent());
                break;
            case R.id.catstatus_cathome:
            case R.id.catstatus_booked:
            case R.id.catstatus_adopted:
            case R.id.catstatus_callcenter:
            case R.id.catstatus_tnr:
            case R.id.catstatus_missing:
            case R.id.catstatus_returned:
            case R.id.catstatus_deceased:
            case R.id.catstatus_external_cathome:
                statusInfo.handleCheckbox();
                break;
            default:break;
        }

    }

    private void setConnectionInfo(){
        connectionInfo = new ConnectionInfo(context);
        //connectionInfo.showCatListDialog();
    }


    private void setConnectionInfo(View view){
        connectionInfo = new ConnectionInfo(context);
        switch (view.getId()){
            case R.id.catconnection_mother:
            case R.id.catconnection_father:
            case R.id.catconnection_add_sibling:
            case R.id.catconnection_add_child:
                connectionInfo.showCatListDialog(view);
                break;
            default:break;
        }
    }

    private void setCatownerInfo(View view){
        catownerInfo = new CatownerInfo(context, view);
        switch (view.getId()){
            case R.id.catform_catowner_date:
                catownerInfo.setDate();
                break;
            case R.id.catform_catowner_time:
                catownerInfo.setTime();
                break;
            case R.id.cat_add_catowner:
                catownerInfo.showCatownerDialog();
                break;
            default:break;
        }
    }

    public void updateCatowners(ArrayList<String[]> catowners){
        catownerInfo.updateCatowners(catowners);
    }

    public void setTemperInfo(View view){
        temperInfo = new Temperament(context);
        switch (view.getId()){
            case R.id.catform_temperament_tame:
            case R.id.catform_temperament_shy:
                temperInfo.setTameShyOption(view);
                break;
            case R.id.catform_temperament_temper_text:
            case R.id.catform_temperament_adaptable_skills_text:
            case R.id.catform_temperament_used_to_children_text:
            case R.id.catform_temperament_used_to_dogs_text:
            case R.id.catform_temperament_carsick_text:
            case R.id.catform_temperament_talking_text:
                temperInfo.selectItem(view);
                break;
            case R.id.catform_temperament_comment_text:
                temperInfo.addComment(view);
                break;
            default:break;
        }
    }

}
