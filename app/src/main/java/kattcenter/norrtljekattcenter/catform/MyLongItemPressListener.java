package kattcenter.norrtljekattcenter.catform;

import android.app.Dialog;
import android.content.Context;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DataPacket;

/**
 * Created by Joakim on 2017-11-09.
 */

public class MyLongItemPressListener implements View.OnLongClickListener{

    private Context context;
    private ArrayList<String> list;
    private String name;
    private String table_name;
    private String form_name;

    public MyLongItemPressListener(Context context, ArrayList<String> list, String name, String table_name, String form_name){
        this.context = context;
        this.list = list;
        this.name = name;
        this.table_name = table_name;
        this.form_name = form_name;
    }


    private void showConfirmDialog(){
        final Dialog dialog = new Dialog(context);
        LinearLayout content = (LinearLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.general_info_confirm_dialog, null);
        ((TextView)content.findViewById(R.id.info_text)).setText("Ta bort "+name+" från listan?");
        Button abort = (Button)content.findViewById(R.id.abort);
        final Button confirm = (Button)content.findViewById(R.id.confirm);
        abort.setText("Avbryt");
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirm.setText("OK");
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(name);
                ((HomePage)context).getController().updateProductList(table_name, form_name, list);
                String blub = table_name+":DELETE:"+Base64.encodeToString(name.getBytes(), Base64.NO_WRAP);
                String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> data = new HashMap<>();
                data.put("blub", encoded_blub);
                try {
                    ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.DELETE_PRODUCT));
                    ((HomePage)context).signalQueueToSend();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });
        dialog.setContentView(content);
        dialog.show();
    }

    /**
     * Called when a view has been clicked and held.
     *
     * @param v The view that was clicked and held.
     * @return true if the callback consumed the long click, false otherwise.
     */
    @Override
    public boolean onLongClick(View v) {
        showConfirmDialog();
        return true;
    }
}
