package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ResourceCursorTreeAdapter;
import android.widget.TextView;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Jocke on 2017-06-05.
 */

public class RegSKKInfo implements DatePickerDialog.OnDateSetListener{

    private Context context;
    private Calendar c = Calendar.getInstance();
    private int year, month, day;
    private View clickedView;
    private ListView listView;
    private MyDocListAdapter myDocListAdapter;

    public RegSKKInfo(Context context){
        this.context = context;
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        initDocList();
    }

    private void initDocList(){
        listView = (ListView)((HomePage)context).findViewById(R.id.catskkreg_documentList);
        myDocListAdapter = new MyDocListAdapter(context);
        myDocListAdapter.setList(new ArrayList<String[]>());
        listView.setAdapter(myDocListAdapter);
    }

    public void setDate(final RadioButton clickedView){
        this.clickedView = clickedView;
        switch (clickedView.getId()){
            case R.id.catreg_skk_yes_rb:
            case R.id.catreg_svekatt_yes_rb:
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
                datePickerDialog.show();
                /*
                datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Avbryt", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == DialogInterface.BUTTON_NEGATIVE){
                            System.out.println(clickedView+"  NEGATIV");
                            clickedView.setChecked(false);
                            clickedView.setText("Ja");
                        }
                    }
                });
                */
                break;
            case R.id.catreg_skk_no_rb:
                ((RadioButton)((RadioGroup)clickedView.getParent()).findViewById(R.id.catreg_skk_yes_rb)).setText("Ja");
                break;
            case R.id.catreg_svekatt_no_rb:
                ((RadioButton)((RadioGroup)clickedView.getParent()).findViewById(R.id.catreg_svekatt_yes_rb)).setText("Ja");
            default:break;
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        switch (clickedView.getId()){
            case R.id.catreg_skk_yes_rb:
                ((RadioButton)clickedView).setText("Ja  "+year+"-"+monthText+"-"+dayText);
                break;
            case R.id.catreg_svekatt_yes_rb:
                ((RadioButton)clickedView).setText("Ja  "+year+"-"+monthText+"-"+dayText);
                break;
            default:break;
        }

    }

    public void addDocumentToList(String[] documentInfo){
        ArrayList<String[]> list = myDocListAdapter.getList();
        list.add(documentInfo);
        Log.i("REGSKKINFO", "LIST SIZE: "+list.size());
        Log.i("REGSKKINFO", "ADDED: "+documentInfo[0]);
        myDocListAdapter.setList(list);
        myDocListAdapter.notifyDataSetChanged();
    }

    public void removeDocumentFromList(int position){
        ArrayList<String[]> list = myDocListAdapter.getList();
        //for(int i = 0; i < list.size(); i++){
        //
        //}
        list.remove(position);
        myDocListAdapter.setList(list);
        myDocListAdapter.notifyDataSetChanged();
    }

    public void documentChooser(){
        Intent filePicker = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        filePicker.setType("*/*");
        String[] mimeTypes = {"text/*", "application/pdf"};
        //String[] mimeTypes = {"file/*"};
        filePicker.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        filePicker.addCategory(Intent.CATEGORY_OPENABLE);
        ((HomePage)context).startActivityForResult(filePicker, 24);
    }

    public void confirmRemoveDocFromList(final int position, final String[] document){
        final Dialog dialog = new Dialog(context);
        LinearLayout linearLayout = (LinearLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.text_button_dialog, null);
        ((TextView)linearLayout.findViewById(R.id.general_dialog_text)).setText("Ta bort dokumentet:\n"+document[0]+"\n\nfrån listan?");
        Button abort = (Button)linearLayout.findViewById(R.id.abort);
        Button confirm = (Button)linearLayout.findViewById(R.id.confirm);
        abort.setText("Avbryt");
        confirm.setText("Ok");
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeDocumentFromList(position);
                boolean removed = ((HomePage)context).getFileManager().deleteFromDocDir(((HomePage)context).getFileManager().getSkkDocDir(), Integer.parseInt(document[1]));
                dialog.dismiss();
                Dialog removedDialog = new Dialog(context);
                TextView textView = new TextView(context);
                textView.setText((removed ? "Lyckad" : "Misslyckad") + " borttagning");
                textView.setTextColor(Color.parseColor("#ffffff"));
                removedDialog.setContentView(textView);
                removedDialog.show();
            }
        });
        dialog.setContentView(linearLayout);
        dialog.show();
    }

    public class MyDocListAdapter extends BaseAdapter {

        private ArrayList<String[]> list;

        public MyDocListAdapter(Context context){

        }

        public ArrayList<String[]> getList(){
            return list;
        }

        public void setList(ArrayList<String[]> list){
            this.list = list;
        }

        /**
         * How many items are in the data set represented by this Adapter.
         *
         * @return Count of items.
         */
        @Override
        public int getCount() {
            return list.size();
        }

        /**
         * Get the data item associated with the specified position in the data set.
         *
         * @param position Position of the item whose data we want within the adapter's
         *                 data set.
         * @return The data at the specified position.
         */
        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        /**
         * Get the row id associated with the specified position in the list.
         *
         * @param position The position of the item within the adapter's data set whose row id we want.
         * @return The id of the item at the specified position.
         */
        @Override
        public long getItemId(int position) {
            return position;
        }

        /**
         * Get a View that displays the data at the specified position in the data set. You can either
         * create a View manually or inflate it from an XML layout file. When the View is inflated, the
         * parent View (GridView, ListView...) will apply default layout parameters unless you use
         * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
         * to specify a root view and to prevent attachment to the root.
         *
         * @param position    The position of the item within the adapter's data set of the item whose view
         *                    we want.
         * @param convertView The old view to reuse, if possible. Note: You should check that this view
         *                    is non-null and of an appropriate type before using. If it is not possible to convert
         *                    this view to display the correct data, this method can create a new view.
         *                    Heterogeneous lists can specify their number of view types, so that this View is
         *                    always of the right type (see {@link #getViewTypeCount()} and
         *                    {@link #getItemViewType(int)}).
         * @param parent      The parent that this view will eventually be attached to
         * @return A View corresponding to the data at the specified position.
         */
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = ((HomePage)context).getLayoutInflater().inflate(R.layout.my_blue_textview, null);
            }
            ((TextView)convertView).setText(list.get(position)[0]);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmRemoveDocFromList(position, list.get(position));
                }
            });
            return convertView;
        }
    }
}
