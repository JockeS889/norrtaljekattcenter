package kattcenter.norrtljekattcenter.catform;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-21.
 */

public class AutoCompleteAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<String> usedNames;

    public AutoCompleteAdapter(Context context, ArrayList<String> usedNames){
        super(context, 0, usedNames);
        this.context = context;
        this.usedNames = usedNames;
    }


    @Override
    public int getCount() {
        return usedNames.size();
    }

    @Override
    public Object getItem(int position) {
        return usedNames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_1, null);
        }
        ((TextView)convertView.findViewById(R.id.listEntry)).setText(usedNames.get(position));
        return convertView;
    }
}
