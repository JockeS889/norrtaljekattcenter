package kattcenter.norrtljekattcenter.catform;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-16.
 */

class ClearFormHandler {

    private Context context;
    ClearFormHandler(Context context){
        this.context = context;
    }

    Bundle clearBasicForm(){
        Bundle basicInfoBundle = new Bundle();
        //Drawable drawable = ContextCompat.getDrawable(context, R.drawable.gray_dash_round_border);
        ((ImageView)((HomePage)context).findViewById(R.id.info_cat_photo)).setImageDrawable(null);
        ((HomePage)context).findViewById(R.id.info_cat_photo).setTag("DEFAULT_PHOTO");
        basicInfoBundle.putBoolean("CATPHOTO", false);
        basicInfoBundle.putString("CATNAME", "");
        //basicInfoBundle.putString("PRESENT_DATE", "");
        basicInfoBundle.putString("ARRIVED_DATE", "");
        //basicInfoBundle.putString("BOX", "");
        basicInfoBundle.putString("MOVED", "");
        //basicInfoBundle.putString("CONNECTION", "");
        //basicInfoBundle.putBoolean("TATOO", false);
        basicInfoBundle.putString("TATOO_STRING", "");
        basicInfoBundle.putString("TATOO_DATE", "");
        //basicInfoBundle.putBoolean("TEMPER_TAME", false);
        //basicInfoBundle.putBoolean("TEMPER_SHY", false);
        //basicInfoBundle.putBoolean("CHIP", false);
        basicInfoBundle.putString("CHIP_CODE", "");
        basicInfoBundle.putString("CHIP_DATE", "");
        //basicInfoBundle.putBoolean("NEUTER", false);
        basicInfoBundle.putString("NEUTER_DATE", "");
        basicInfoBundle.putString("CATHOME_NAME", "");
        basicInfoBundle.putString("CATHOME_ID", "");
        basicInfoBundle.putString("BIRTHDAY", "");
        basicInfoBundle.putString("BIRTHDAY_GUESS", "");
        basicInfoBundle.putString("SEX", "");
        basicInfoBundle.putString("RACE", "");
        basicInfoBundle.putString("COLOR", "");
        basicInfoBundle.putString("HAIR", "");
        return basicInfoBundle;
    }

    Bundle clearMedicationForm(){
        Bundle medicationInfoBundle = new Bundle();
        medicationInfoBundle.putString("START_DATE", "");
        medicationInfoBundle.putString("END_DATE", "");
        medicationInfoBundle.putString("MEDICATION", "");
        medicationInfoBundle.putString("DOSE", "");
        medicationInfoBundle.putString("ADMINISTRATION", "");
        return medicationInfoBundle;
    }

    Bundle clearVaccinForm(){
        Bundle vaccinInfoBundle = new Bundle();
        vaccinInfoBundle.putString("START_DATE", "");
        vaccinInfoBundle.putString("VACCIN_TYPE", "");
        vaccinInfoBundle.putInt("TIME", -1);
        vaccinInfoBundle.putString("COMMENT", "");
        vaccinInfoBundle.putString("REPEAT_DATE", "");
        return vaccinInfoBundle;
    }

    Bundle clearDewormingForm(){
        Bundle dewormingBundle = new Bundle();
        dewormingBundle.putString("START_DATE", "");
        dewormingBundle.putString("DEWORMING_TYPE", "");
        dewormingBundle.putString("DOSE", "");
        dewormingBundle.putString("COMMENT", "");
        dewormingBundle.putString("REPEAT_DATE", "");
        return dewormingBundle;
    }

    Bundle clearVerminForm(){
        Bundle verminInfoBundle = new Bundle();
        verminInfoBundle.putString("START_DATE", "");
        verminInfoBundle.putString("VERMIN_TYPE", "");
        verminInfoBundle.putString("TREATMENT_TYPE", "");
        verminInfoBundle.putString("COMMENT", "");
        verminInfoBundle.putString("REPEAT_DATE", "");
        return verminInfoBundle;
    }

    Bundle clearClawForm(){
        Bundle clawInfoBundle = new Bundle();
        clawInfoBundle.putString("START_DATE", "");
        clawInfoBundle.putString("COMMENT", "");
        clawInfoBundle.putString("REPEAT_DATE", "");
        return clawInfoBundle;
    }

    Bundle clearWeightForm(){
        Bundle weightInfoBundle = new Bundle();
        weightInfoBundle.putString("START_DATE", "");
        weightInfoBundle.putString("WEIGHT", "");
        weightInfoBundle.putString("COMMENT", "");
        weightInfoBundle.putString("CHECK_WEIGHT", "");
        return weightInfoBundle;
    }

    Bundle clearVetDocForm(){
        Bundle vetDocBundle = new Bundle();
        vetDocBundle.putString("DATE", "");
        ((HomePage)context).initSpinnerRecord();
        vetDocBundle.putSerializable("VETDOCCHECKBOXES", ((HomePage)context).getRecord());
        vetDocBundle.putSerializable("VETDOCNOTES", ((HomePage)context).getCatvetreg_comments());
        return vetDocBundle;
    }

    Bundle clearRegSKKForm(){
        Bundle regSKKBundle = new Bundle();
        regSKKBundle.putInt("REG_SKK", -1);
        regSKKBundle.putString("REG_SKK_DATE", "Ja");
        regSKKBundle.putInt("REG_SVE", -1);
        regSKKBundle.putString("REG_SVE_DATE", "Ja");
        return regSKKBundle;
    }

    Bundle clearVetForm(){
        Bundle vetBundle = new Bundle();
        vetBundle.putBoolean("NO_KNOWN", false);
        vetBundle.putString("NO_KNOWN_DATE", "Inga kända");
        vetBundle.putString("SAMPLE_DATE", "");
        vetBundle.putString("SAMPLE_URI", "");
        vetBundle.putString("KNOWN_EVENTS", "");
        vetBundle.putString("COMMENT", "");
        return vetBundle;
    }

    Bundle clearInsuranceForm(){
        Bundle insuranceBundle = new Bundle();
        insuranceBundle.putString("TAKEN_DATE", "");
        insuranceBundle.putString("OLD_COMPANY", "");
        insuranceBundle.putString("OLD_NUMBER", "");
        insuranceBundle.putString("SIGNED_DATE", "");
        insuranceBundle.putString("NEW_COMPANY", "");
        insuranceBundle.putString("NEW_NUMBER", "");
        insuranceBundle.putString("TRANSFER_DATE", "");
        return insuranceBundle;
    }

    Bundle clearBackgroundForm(){
        Bundle backgroundBundle = new Bundle();
        backgroundBundle.putString("BORN_OUTSIDE", "Född ute");
        backgroundBundle.putString("HOMELESS", "Hemlös");
        backgroundBundle.putString("DUMPED", "Dumpad");
        backgroundBundle.putString("LOST", "Vilse");
        backgroundBundle.putString("LEFT_BEHIND", "Kvarlämnad");
        backgroundBundle.putString("ESTATE", "Dödsbo");
        backgroundBundle.putString("RELOCATE", "Omplacering");
        backgroundBundle.putString("VIA_VET", "Via veterinär");
        backgroundBundle.putString("COMMENT", "");
        return backgroundBundle;
    }

    Bundle clearPrevOwnersForm(){
        return null;
    }

    Bundle clearStatusForm(){
        Bundle statusBundle = new Bundle();
        statusBundle.putString("NOT_BOUGHT", "Icke köpt");
        statusBundle.putString("NEWLY_ARRIVED", "Nyinkommen");
        statusBundle.putString("ADOPTABLE", "Adoptionsbar");
        statusBundle.putString("CATHOME", "Katthem");
        statusBundle.putString("BOOKED", "Bokad");
        statusBundle.putString("ADOPTED", "Adopterad");
        statusBundle.putString("CALLCENTER", "Jourhem");
        statusBundle.putString("TNR", "TNR");
        statusBundle.putString("MOVED", "Flyttad till annat katthem");
        statusBundle.putString("EXTERNAL_CATHOME", "");
        statusBundle.putString("MISSING", "Saknad");
        statusBundle.putString("RETURNEDOWNER", "Åter ägaren");
        statusBundle.putString("DECEASED", "Avliden");

        return statusBundle;
    }

    Bundle clearCatownerForm(){
        Bundle catownerBundle = new Bundle();
        catownerBundle.putString("DATE", "");
        catownerBundle.putString("TIME", "");
        catownerBundle.putCharSequenceArrayList("SELECTED_OWNERS", null);
        return catownerBundle;
    }
}
