package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DownloadProduct;
import kattcenter.norrtljekattcenter.network.UploadProduct;

/**
 * Created by Jocke on 2017-06-05.
 */

public class InsuranceInfo implements DatePickerDialog.OnDateSetListener{

    private Context context;
    private View clickedView;
    private Calendar c = Calendar.getInstance();
    private int year, month, day;
    private ListView listView;
    private Dialog dialog;
    private ArrayList<String> companies;
    private ListAdapter listAdapter;

    public InsuranceInfo(Context context, View clickedView){
        this.context = context;
        this.clickedView = clickedView;
        dialog = new Dialog(context);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void clearButton(){
        //final LinearLayout linearLayout = (LinearLayout)clickedView.getParent();
        final LinearLayout linearLayout = (LinearLayout)((LinearLayout)clickedView).getChildAt(1);
        final TextView clear = (TextView) linearLayout.getChildAt(1);
        clear.setVisibility(View.VISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear.setVisibility(View.GONE);
                ((TextView)linearLayout.getChildAt(0)).setText("");
            }
        });

    }

    public void updateCompanies(ArrayList<String> companies, String type_list_name){
        listAdapter.notifyDataSetInvalidated();
        listAdapter = new ListAdapter(context, companies, type_list_name);
        listView.setAdapter(listAdapter);
    }

    public void setCompany(final ArrayList<String> companies){
        this.companies = companies;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        listAdapter = new ListAdapter(context, companies, "insurance_info");
        listView.setAdapter(listAdapter);
        final TextView companyTextView = (clickedView.getId() == R.id.catinsurance_signed_company) ? (TextView)clickedView.findViewById(R.id.catinsurance_signed_company_text) : (TextView) clickedView.findViewById(R.id.catinsurance_taken_company_text);
        Button addNewCompany = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newCompany = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String company = newCompany.getText().toString();
                if(company != null && !company.equals("")){
                    companyTextView.setText(company);
                    clearButton();
                    try {
                        new UploadProduct(context, "company_types", Type.UPLOAD.toString(), company).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj typ");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "insurance_info", "company_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private class ListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> list;
        private String type_list_name;

        public ListAdapter(@NonNull Context context, ArrayList<String> list, String type_list_name) {
            super(context, 0, list);
            this.list = list;
            this.type_list_name = type_list_name;
        }

        public void setList(ArrayList<String> list){
            this.list = list;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            final String type = list.get(position);
            if(view == null){
                view = ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_2, null);
            }
            TextView entryName = (TextView) view.findViewById(R.id.listEntry);
            entryName.setText(type);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView companyTextView = (clickedView.getId() == R.id.catinsurance_signed_company) ? (TextView)clickedView.findViewById(R.id.catinsurance_signed_company_text) : (TextView) clickedView.findViewById(R.id.catinsurance_taken_company_text);
                    companyTextView.setText(type);
                    dialog.dismiss();
                    clearButton();
                }
            });
            view.setOnLongClickListener(new MyLongItemPressListener(context, list, type, type_list_name, "insurance_info"));
            return view;
        }
    }



    public void setDate(){
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(((HomePage)context).getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        String dateString = year+"-"+monthText+"-"+dayText;
        switch (clickedView.getId()){
            case R.id.catinsurance_taken_date:
                ((TextView)clickedView.findViewById(R.id.catinsurance_taken_date_text)).setText(dateString);
                break;
            case R.id.catinsurance_signed_date:
                ((TextView)clickedView.findViewById(R.id.catinsurance_signed_date_text)).setText(dateString);
                break;
            case R.id.catinsurance_transfered_date:
                ((TextView)clickedView.findViewById(R.id.catinsurance_transfered_date_text)).setText(dateString);
                break;
            default:break;
        }
        clearButton();
    }
}
