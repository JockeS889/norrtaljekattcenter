package kattcenter.norrtljekattcenter.catform;

import java.util.HashMap;

/**
 * Created by Joakim on 2017-07-02.
 */

public class FormFieldMapper {
    
    private HashMap<String, String> translationTable = new HashMap<>();

    public FormFieldMapper(){
        initTranslationTable();
    }

    private void initTranslationTable(){
        translationTable.put("CATNAME", "Kattnamn");
        translationTable.put("PRESENT_DATE", "Inkom datum");
        translationTable.put("INCOME_DATE", "Inkom datum");
        translationTable.put("BOX", "Box");
        translationTable.put("MOVED", "Flyttad");
        translationTable.put("CONNECTION", "Koppling");
        translationTable.put("TATOO", "Tatuerad");
        translationTable.put("TATOO_STRING", "Tatuering");
        translationTable.put("TATOO_DATE", "Tatueringsdatum");
        translationTable.put("TEMPER_TAME", "Tam");
        translationTable.put("TEMPER_SHY", "Skygg");
        translationTable.put("CHIP", "Chip");
        translationTable.put("CHIP_DATE", "Datum chip");
        translationTable.put("CHIP_CODE", "Chip kod");
        //translationTable.put("CHIP_STRING", "Kod");
        translationTable.put("NEUTER", "Kastrerad");
        translationTable.put("NEUTER_DATE", "Kastreringsdatum");
        translationTable.put("CATHOME_NAME", "Katthems namn");
        translationTable.put("CATHOME_ID", "Katthems id");
        translationTable.put("BIRTHDAY", "Födelsedag");
        translationTable.put("BIRTHDAY_GUESS", "Födelsedatums-typ");
        translationTable.put("SEX", "Kön");
        translationTable.put("RACE", "Ras");
        translationTable.put("COLOR", "Färg");
        translationTable.put("HAIR", "Päls");
        //translationTable.put("START_DATE", "Startdatum");
        translationTable.put("START_DATE", "Utfört senast");
        translationTable.put("END_DATE", "Slutdatum");
        translationTable.put("MEDICATION", "Läkemedel");
        translationTable.put("MEDICAL", "Läkemedel");
        translationTable.put("DOSE", "Dosering");
        translationTable.put("ADMINISTRATION", "Administrering");
        translationTable.put("ADMINISTRATION_TYPE", "Administration");
        translationTable.put("ADMINISTRATION_DATE", "Datum administrering");
        translationTable.put("PARASITE_TYPE", "Typ av parasit");
        translationTable.put("VACCIN_TYPE", "Vaccintyp");
        translationTable.put("TIME", "Företeelse");
        translationTable.put("COMMENT", "Kommentar");
        translationTable.put("REPEAT_DATE", "Upprepas");
        translationTable.put("VERMIN_TYPE", "Typ av ohyra");
        translationTable.put("TREATMENT_TYPE", "Typ av behandling");
        translationTable.put("WEIGHT", "Vikt");
        translationTable.put("CHECK_WEIGHT", "Kolla vikt igen");
        translationTable.put("AMOUNT", "Antal");
        translationTable.put("DEWORMING_TYPE", "Typ av avmaskning");
        translationTable.put("REG_SKK", "Registrerad i SKK");
        translationTable.put("REG_SVE", "Registrerad i Svekatt");
        translationTable.put("REG_SKK_DATE", "SKK registerad");
        translationTable.put("REG_SVE_DATE", "Svekatt registrerad");
        translationTable.put("TAKEN_DATE", "Datum tagit över försäkring");
        translationTable.put("OLD_COMPANY", "Bolag");
        translationTable.put("OLD_NUMBER", "Försäkringsnummer");
        translationTable.put("SIGNED_DATE", "Datum nytecknad");
        translationTable.put("NEW_COMPANY", "Bolag");
        translationTable.put("NEW_NUMBER", "Försäkringsnummer");
        translationTable.put("TRANSFER_DATE", "Fört över till nya ägaren");
        translationTable.put("SAMPLE_DATE", "Datum provresultat");
        translationTable.put("SAMPLE_RESULT", "Provresultat");
        translationTable.put("NO_KNOWN", "Inga kända");
        translationTable.put("NO_KNOWN_DATE", "Datum");
        translationTable.put("KNOWN_EVENTS", "Kända event");
        translationTable.put("GENERAL", "Allmänt");
        translationTable.put("TYPE", "Typ");
        translationTable.put("ADAPTABLE_SKILL", "Anpassningsförmåga");
        translationTable.put("USED_TO_CHILDREN", "Van vid barn");
        translationTable.put("USED_TO_DOGS", "Van vid hund");
        translationTable.put("CAR_SICK", "Åksjuk");
        translationTable.put("TALKS", "Pratar");
        translationTable.put("BORN", "Född");
        translationTable.put("HOMELESS", "Hemlös");
        translationTable.put("DUMPED", "Dumpad");
        translationTable.put("LOST", "Vilse");
        translationTable.put("LEFT_BEHIND", "Kvarlämnad");
        translationTable.put("REPLACED", "Omplacering");
        translationTable.put("VIA_VET", "Via veterinär");
        translationTable.put("TNR", "TNR");
        translationTable.put("NOT_BOUGHT", "Icke köpt");
        translationTable.put("NEWLY_ARRIVED", "Nyinkommen");
        translationTable.put("ADOPTABLE", "Adoptionsbar");
        translationTable.put("BOOKED", "Bokad");
        translationTable.put("ADOPTED", "Adopterad");
        translationTable.put("CALLCENTER", "Jourhem");
        translationTable.put("EXTERNAL_CATHOME", "- Annat katthem");
        translationTable.put("MISSING", "Bortsprungen/saknad");
        translationTable.put("RETURNEDOWNER", "Åter ägaren");
        translationTable.put("DECEASED", "Avliden");
        translationTable.put("DOCUMENTS", "Dokument");
        translationTable.put("CATHOME", "Katthem");
        translationTable.put("ESTATE", "Dödsbo");
    }

    public boolean containsTranslation(String key){
        return translationTable.containsKey(key);
    }

    public String getTranslation(String key){
        return translationTable.get(key);
    }
}
