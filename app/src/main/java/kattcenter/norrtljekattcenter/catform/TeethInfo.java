package kattcenter.norrtljekattcenter.catform;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-05.
 */

public class TeethInfo {

    private Context context;
    private View clickedView;
    public TeethInfo(Context context){
        this.context = context;
    }

    public void pickTooth(View clickedView){
        this.clickedView = clickedView;
        final Dialog dialog = new Dialog(context);
        final int tagNr = Integer.parseInt((String)clickedView.getTag());
        dialog.setTitle("Tand nr: "+tagNr);
        /*
        int div = tagNr/100;
        int rest = tagNr%100;
        switch (rest){
            case 1:
            case 2:
            case 3:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view.setBackground(context.getResources().getDrawable(R.drawable.fronttooth_selected_icon, null));
                } else {
                    view.setBackground(context.getResources().getDrawable(R.drawable.fronttooth_selected_icon));
                }
                break;
            case 4:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view.setBackground(context.getResources().getDrawable(R.drawable.corner_tooth_selected_icon, null));
                } else {
                    view.setBackground(context.getResources().getDrawable(R.drawable.corner_tooth_selected_icon));
                }
                break;
            case 6:
            case 7:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view.setBackground(context.getResources().getDrawable(R.drawable.inner_tooth_1_selected_icon, null));
                } else {
                    view.setBackground(context.getResources().getDrawable(R.drawable.inner_tooth_1_selected_icon));
                }
                break;
            case 8:
                switch (div){
                    case 1:
                    case 2:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            view.setBackground(context.getResources().getDrawable(R.drawable.inner_tooth_2_selected_icon, null));
                        } else {
                            view.setBackground(context.getResources().getDrawable(R.drawable.inner_tooth_2_selected_icon));
                        }
                        break;
                    case 3:
                    case 4:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            view.setBackground(context.getResources().getDrawable(R.drawable.inner_tooth_1_selected_icon, null));
                        } else {
                            view.setBackground(context.getResources().getDrawable(R.drawable.inner_tooth_1_selected_icon));
                        }
                        break;
                    default:break;
                }
                break;
            case 9:
                switch (div){
                    case 1:
                    case 2:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            view.setBackground(context.getResources().getDrawable(R.drawable.inner_tooth_3_selected_icon, null));
                        } else {
                            view.setBackground(context.getResources().getDrawable(R.drawable.inner_tooth_3_selected_icon));
                        }
                        break;
                    case 3:
                    case 4:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            view.setBackground(context.getResources().getDrawable(R.drawable.inner_tooth_1_selected_icon, null));
                        } else {
                            view.setBackground(context.getResources().getDrawable(R.drawable.inner_tooth_1_selected_icon));
                        }
                        break;
                    default:break;
                }
                break;
            default:break;
        }
        */
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                System.out.println("CANCEL");
                setTooth(tagNr, 0);
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                System.out.println("DISMISS");
            }
        });
        setTooth(tagNr, 1);
        TextView textView = new TextView(context), textView1 = new TextView(context), textView2 = new TextView(context), textView3 = new TextView(context);
        textView.setText("- Saknas");
        textView1.setText("- Tagits bort");
        textView2.setText("- Fraktur");
        textView3.setText("- Tandsten");
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setTextSize(20);
        textView1.setTextSize(20);
        textView2.setTextSize(20);
        textView3.setTextSize(20);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("SAKNAS");
                setTooth(tagNr, 2);
                dialog.dismiss();
            }
        });
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("TAGITS BORT");
                setTooth(tagNr, 3);
                dialog.dismiss();
            }
        });
        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("FRAKTUR");
                setTooth(tagNr, 4);
                dialog.dismiss();
            }
        });
        textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("TANDSTEN");
                setTooth(tagNr, 5);
                dialog.dismiss();
            }
        });
        linearLayout.addView(textView);
        linearLayout.addView(textView1);
        linearLayout.addView(textView2);
        linearLayout.addView(textView3);
        dialog.setContentView(linearLayout);
        dialog.show();
    }

    private void setTooth(int tagNr, int state){
        int drawableID = -1;
        int div = tagNr/100;
        int rest = tagNr%100;
        switch (rest){
            case 1:
            case 2:
            case 3:
                switch (state){
                    case 0: drawableID = R.drawable.fronttooth_icon; break;
                    case 1: drawableID = R.drawable.fronttooth_selected_icon; break;
                    case 2: drawableID = R.drawable.fronttooth_missing_icon; break;
                    case 3: drawableID = R.drawable.fronttooth_removed_icon; break;
                    case 4: drawableID = R.drawable.fronttooth_fracture_icon; break;
                    case 5: drawableID = R.drawable.fronttooth_tartar_icon; break;
                    default: break;
                }
                break;
            case 4:
                switch (state){
                    case 0: drawableID = R.drawable.corner_tooth_icon; break;
                    case 1: drawableID = R.drawable.corner_tooth_selected_icon; break;
                    case 2: drawableID = R.drawable.corner_tooth_missing_icon; break;
                    case 3: drawableID = R.drawable.corner_tooth_removed_icon; break;
                    case 4: drawableID = R.drawable.corner_tooth_fracture_icon; break;
                    case 5: drawableID = R.drawable.corner_tooth_tartar_icon; break;
                    default: break;
                }
                break;
            case 6:
            case 7:
                switch (state){
                    case 0: drawableID = R.drawable.inner_tooth_1_icon; break;
                    case 1: drawableID = R.drawable.inner_tooth_1_selected_icon; break;
                    case 2: drawableID = R.drawable.inner_tooth_1_missing_icon; break;
                    case 3: drawableID = R.drawable.inner_tooth_1_removed_icon; break;
                    case 4: drawableID = R.drawable.inner_tooth_1_fracture_icon; break;
                    case 5: drawableID = R.drawable.inner_tooth_1_tartar_icon; break;
                    default: break;
                }
                break;
            case 8:
                switch (div){
                    case 1:
                    case 2:
                        switch (state){
                            case 0: drawableID = R.drawable.inner_tooth_2_icon; break;
                            case 1: drawableID = R.drawable.inner_tooth_2_selected_icon; break;
                            case 2: drawableID = R.drawable.inner_tooth_2_missing_icon; break;
                            case 3: drawableID = R.drawable.inner_tooth_2_removed_icon; break;
                            case 4: drawableID = R.drawable.inner_tooth_2_fracture_icon; break;
                            case 5: drawableID = R.drawable.inner_tooth_2_tartar_icon; break;
                            default: break;
                        }
                        break;
                    case 3:
                    case 4:
                        switch (state){
                            case 0: drawableID = R.drawable.inner_tooth_1_icon; break;
                            case 1: drawableID = R.drawable.inner_tooth_1_selected_icon; break;
                            case 2: drawableID = R.drawable.inner_tooth_1_missing_icon; break;
                            case 3: drawableID = R.drawable.inner_tooth_1_removed_icon; break;
                            case 4: drawableID = R.drawable.inner_tooth_1_fracture_icon; break;
                            case 5: drawableID = R.drawable.inner_tooth_1_tartar_icon; break;
                            default: break;
                        }
                        break;
                    default:break;
                }
                break;
            case 9:
                switch (div){
                    case 1:
                    case 2:
                        switch (state){
                            case 0: drawableID = R.drawable.inner_tooth_3_icon; break;
                            case 1: drawableID = R.drawable.inner_tooth_3_selected_icon; break;
                            case 2: drawableID = R.drawable.inner_tooth_3_missing_icon; break;
                            case 3: drawableID = R.drawable.inner_tooth_3_removed_icon; break;
                            case 4: drawableID = R.drawable.inner_tooth_3_fracture_icon; break;
                            case 5: drawableID = R.drawable.inner_tooth_3_tartar_icon; break;
                            default: break;
                        }
                        break;
                    case 3:
                    case 4:
                        switch (state){
                            case 0: drawableID = R.drawable.inner_tooth_1_icon; break;
                            case 1: drawableID = R.drawable.inner_tooth_1_selected_icon; break;
                            case 2: drawableID = R.drawable.inner_tooth_1_missing_icon; break;
                            case 3: drawableID = R.drawable.inner_tooth_1_removed_icon; break;
                            case 4: drawableID = R.drawable.inner_tooth_1_fracture_icon; break;
                            case 5: drawableID = R.drawable.inner_tooth_1_tartar_icon; break;
                            default: break;
                        }
                        break;
                    default:break;
                }
                break;
            default:break;
        }
        if(drawableID != -1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                clickedView.setBackground(context.getResources().getDrawable(drawableID, null));
            } else {
                clickedView.setBackground(context.getResources().getDrawable(drawableID));
            }
        }
    }
}
