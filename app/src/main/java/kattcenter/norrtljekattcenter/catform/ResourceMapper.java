package kattcenter.norrtljekattcenter.catform;

import java.util.HashMap;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-10.
 */

public class ResourceMapper {

    private HashMap<String, Integer> stringToResource = new HashMap<>();


    public void initPreview(){
        stringToResource.put("content_holder", R.id.contentHolder);
    }

    public void initBasicPreview(){
        stringToResource.put("photo_src", R.id.catpreview_cat_image);
        stringToResource.put("name", R.id.catpreview_name_text);
        stringToResource.put("catcenter_name", R.id.catpreview_cathome_name_text);
        stringToResource.put("catcenter_id", R.id.catpreview_catid_text);
        stringToResource.put("birthdate", R.id.catpreview_cat_birthdate_text);
        stringToResource.put("birthdate_guess", R.id.catpreview_cat_birthdate_state_text);
        stringToResource.put("sex", R.id.catpreview_cat_sex_text);
        stringToResource.put("race", R.id.catpreview_cat_race_text);
        stringToResource.put("color", R.id.catpreview_cat_color_text);
        stringToResource.put("furrtype", R.id.catpreview_cat_hair_text);
        stringToResource.put("income_date", R.id.catpreview_catarrived_date_text);
        //stringToResource.put("box", R.id.catbasic_box_text);
        stringToResource.put("moved_date", R.id.catpreview_catmoved_date_text);
        //stringToResource.put("connection", R.id.catbasic_connection_text);
        //stringToResource.put("temper_shy", R.id.catbasic_temper_shy);
        //stringToResource.put("temper_tame", R.id.catbasic_temper_tame);
        //stringToResource.put("neuter", R.id.neuterCheckbox);
        stringToResource.put("neuter_date", R.id.catpreview_catneutered_date_text);
        //stringToResource.put("tatoo", R.id.tatooCheckbox);
        stringToResource.put("tatoo_date", R.id.catpreview_cat_tatoodate_text);
        stringToResource.put("tatoo_string", R.id.catpreview_cat_tatoo_text);
        //stringToResource.put("chip", R.id.chipCheckbox);
        stringToResource.put("chip_date", R.id.catpreview_cat_chipdate_text);
        stringToResource.put("chip_code", R.id.catpreview_cat_chip_text);
    }

    public void initBasic(){
        stringToResource.put("photo_src", R.id.info_cat_photo);
        stringToResource.put("name", R.id.catbasic_cathome_name_text);
        stringToResource.put("catcenter_name", R.id.catbasic_catname_autocomplete_text);
        stringToResource.put("catcenter_id", R.id.catbasic_cathome_id_text);
        stringToResource.put("birthdate", R.id.catbasic_birthday_date_text);
        stringToResource.put("birthdate_guess", R.id.catbasic_birthday_guess);
        stringToResource.put("sex", R.id.catbasic_sex_type_text);
        stringToResource.put("race", R.id.catbasic_race_text);
        stringToResource.put("color", R.id.catbasic_color_text);
        stringToResource.put("furrtype", R.id.catbasic_hair_type);
        stringToResource.put("income_date", R.id.catbasic_arrived_date_text);
        //stringToResource.put("box", R.id.catbasic_box_text);
        stringToResource.put("moved_date", R.id.catbasic_moved_date_text);
        //stringToResource.put("connection", R.id.catbasic_connection_text);
        //stringToResource.put("temper_shy", R.id.catbasic_temper_shy);
        //stringToResource.put("temper_tame", R.id.catbasic_temper_tame);
        //stringToResource.put("neuter", R.id.neuterCheckbox);
        stringToResource.put("neuter_date", R.id.catbasic_neuter_date_text);
        //stringToResource.put("tatoo", R.id.tatooCheckbox);
        stringToResource.put("tatoo_date", R.id.catbasic_tatoo_date_text);
        stringToResource.put("tatoo_string", R.id.catbasic_tatoo_text);
        //stringToResource.put("chip", R.id.chipCheckbox);
        stringToResource.put("chip_date", R.id.catbasic_chip_date_text);
        stringToResource.put("chip_code", R.id.catbasic_chip_code_text);
    }

    /*
    public void initMedical(){
        stringToResource.put("start_date", R.id.catmed_start_date_text);
        stringToResource.put("end_date", R.id.catmed_end_date_text);
        stringToResource.put("medicine", R.id.catmed_type_text);
        //stringToResource.put("amount", R.id.catmed_dose_amount_text);
        stringToResource.put("dose", R.id.catmed_dose_amount_text);
        //stringToResource.put("administration", R.id.catmed_administration_text);
        stringToResource.put("treatment_type", R.id.catmed_treatment_type_text);
        stringToResource.put("comment", R.id.catmed_comment_text);
        stringToResource.put("repeat_date", R.id.catmed_repeat_date_text);
    }


    public void initVaccination(){
        stringToResource.put("start_date", R.id.catvaccin_start_date_text);
        stringToResource.put("vaccin_type", R.id.catvaccin_type_text);
        stringToResource.put("vaccin_time", R.id.catvaccin_vaccinTime);
        stringToResource.put("comment", R.id.catvaccin_comment_text);
        stringToResource.put("repeat_date", R.id.catvaccin_repeat_date_text);
    }


    public void initParasites(){
        stringToResource.put("administration_date", R.id.catparasite_admininstration_date_text);
        stringToResource.put("parasite_type", R.id.catparasite_type_text);
        stringToResource.put("medical", R.id.catparasite_med_type_text);
        stringToResource.put("dose", R.id.catparasite_dose_text);
        stringToResource.put("treatment_type", R.id.catparasite_treatment_type_text);
        stringToResource.put("comment", R.id.catparasite_comment_text);
        stringToResource.put("repeat_date", R.id.catparasite_repeat_date_text);
    }


    public void initClaw(){
        stringToResource.put("start_date", R.id.catclaw_start_date_text);
        stringToResource.put("comment", R.id.catclaw_comment_text);
        stringToResource.put("repeat_date", R.id.catclaw_repeat_date_text);
    }

    public void initWeight(){
        stringToResource.put("start_date", R.id.catweight_start_date_text);
        stringToResource.put("weight", R.id.catweight_weight_text);
        stringToResource.put("comment", R.id.catweight_comment_text);
        stringToResource.put("repeat_date", R.id.catweight_check_weight_date_text);
    }

    public void initVetRegDoc(){

    }

    */

    public void initRegSKK(){
        stringToResource.put("reg_skk", R.id.catreg_skk_rg);
        stringToResource.put("reg_skk_date", R.id.catreg_skk_rg);
        stringToResource.put("reg_svekatt", R.id.catreg_svekatt_rg);
        stringToResource.put("reg_svekatt_date", R.id.catreg_svekatt_rg);
    }

    /*

    public void initVet(){
        stringToResource.put("no_healthproblem", R.id.catvet_no_known);
        stringToResource.put("no_healthproblem_date", R.id.catvet_no_known);
        stringToResource.put("bloodsample_date", R.id.catvet_bloodsample_date_text);
        stringToResource.put("bloodsample_result", R.id.catvet_bloodsample_result_text);
        stringToResource.put("known_events", R.id.catvet_known_events_text);
        stringToResource.put("comment", R.id.catvet_comment_text);
    }

    public void initInsurance(){
        stringToResource.put("taken_date", R.id.catinsurance_taken_date_text);
        stringToResource.put("old_company", R.id.catinsurance_taken_company_text);
        stringToResource.put("old_number", R.id.catinsurance_old_number);
        stringToResource.put("signed_date", R.id.catinsurance_signed_date_text);
        stringToResource.put("new_company", R.id.catinsurance_signed_company_text);
        stringToResource.put("new_number", R.id.catinsurance_new_number);
        stringToResource.put("transfer_date", R.id.catinsurance_transfered_date_text);
    }

    public void initBackground(){
        stringToResource.put("born", R.id.catbackground_born_outside);
        //stringToResource.put("born_date", R.id.catbackground_born_outside);
        stringToResource.put("homeless", R.id.catbackground_homeless);
        //stringToResource.put("homeless_date", R.id.catbackground_homeless);
        stringToResource.put("dumped", R.id.catbackground_dumped);
        //stringToResource.put("dumped_date", R.id.catbackground_dumped);
        stringToResource.put("lost", R.id.catbackground_lost);
        //stringToResource.put("lost_date", R.id.catbackground_lost);
        stringToResource.put("left_behind", R.id.catbackground_left);
        //stringToResource.put("left_behind_date", R.id.catbackground_left);
        stringToResource.put("replaced", R.id.catbackground_replacing);
        //stringToResource.put("replaced_date", R.id.catbackground_replacing);
        stringToResource.put("via_vet", R.id.catbackground_via_vet);
        //stringToResource.put("via_vet_date", R.id.catbackground_via_vet);
        stringToResource.put("comment", R.id.catbackground_comment_text);
    }



    public void initStatus(){
        stringToResource.put("not_bought", R.id.catstatus_not_bought);
        stringToResource.put("newly_arrived", R.id.catstatus_new_arrived);
        stringToResource.put("adoptable", R.id.catstatus_adoptable);
        stringToResource.put("booked", R.id.catstatus_booked);
        stringToResource.put("adopted", R.id.catstatus_adopted);
        stringToResource.put("callcenter", R.id.catstatus_callcenter);
        stringToResource.put("tnr", R.id.catstatus_tnr);
        stringToResource.put("moved", R.id.catstatus_external_cathome);
        stringToResource.put("external_cathome", R.id.catstatus_external_cathome_text);
        stringToResource.put("missing", R.id.catstatus_missing);
        stringToResource.put("returned_owner", R.id.catstatus_returned);
        stringToResource.put("deceased", R.id.catstatus_deceased);
    }

    */
    public void initConnection(){
        stringToResource.put("mother", R.id.catconnection_mother_text);
        stringToResource.put("father", R.id.catconnection_father_text);
        stringToResource.put("siblings", R.id.catconnection_siblings_list);
        stringToResource.put("children", R.id.catconnection_children_list);
    }


    public boolean hasResource(String key){
        return stringToResource.containsKey(key);
    }

    public int getResource(String string){
        return stringToResource.get(string);
    }
}
