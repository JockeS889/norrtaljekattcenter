package kattcenter.norrtljekattcenter.catform;

import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Joakim on 2017-08-11.
 */

public class Temperament {

    private Context context;
    private View clickedView;
    private ArrayList<String> selectedItems = new ArrayList<>();
    private String selectedButtonText = null;

    private static final int TYPE_LIST = 1;
    private static final int ADAPTABLE_LIST = 2;
    private static final int USED_TO_LIST = 3;
    private static final int CARSICK_LIST = 4;
    private static final int GENERAL_LIST = 5;
    private static final int CHECKBOX_TYPE = 6;
    private static final int RADIOBUTTON_TYPE = 7;

    public Temperament(Context context){
        this.context = context;
    }


    private void clearButton(){
        final LinearLayout linearLayout = (LinearLayout)clickedView.getParent();
        final TextView clear = (TextView) linearLayout.getChildAt(1);
        clear.setVisibility(View.VISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear.setVisibility(View.GONE);
                ((TextView)linearLayout.getChildAt(0)).setText("");
            }
        });

    }


    public void setTameShyOption(View clickedView){
        LinearLayout parent = (LinearLayout)clickedView.getParent().getParent();
        Log.i("TEMPERAMENT", "PARENT: "+parent);
        Log.i("TEMPERAMENT", "CLICKED: "+((RadioButton)parent.findViewById(R.id.catform_temperament_shy)).isChecked());
        switch (clickedView.getId()){
            case R.id.catform_temperament_tame:
                ((RadioButton)parent.findViewById(R.id.catform_temperament_shy)).setChecked(false);
                break;
            case R.id.catform_temperament_shy:
                ((RadioButton)parent.findViewById(R.id.catform_temperament_tame)).setChecked(false);
                break;
            default:break;
        }
    }

    public void selectItem(View clickedView){
        this.clickedView = clickedView;
        switch (clickedView.getId()){
            case R.id.catform_temperament_temper_text:
                showDialog(TYPE_LIST, CHECKBOX_TYPE);
                break;
            case R.id.catform_temperament_adaptable_skills_text:
                showDialog(ADAPTABLE_LIST, RADIOBUTTON_TYPE);
                break;
            case R.id.catform_temperament_used_to_children_text:
                showDialog(USED_TO_LIST, RADIOBUTTON_TYPE);
                break;
            case R.id.catform_temperament_used_to_dogs_text:
                showDialog(USED_TO_LIST, RADIOBUTTON_TYPE);
                break;
            case R.id.catform_temperament_carsick_text:
                showDialog(CARSICK_LIST, CHECKBOX_TYPE);
                break;
            case R.id.catform_temperament_talking_text:
                showDialog(GENERAL_LIST, RADIOBUTTON_TYPE);
                break;
        }
    }

    private void showDialog(final int type, int button_type){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        RelativeLayout content = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.catform_temperament_itemlist, null);
        initSelectableList((LinearLayout)content.findViewById(R.id.temper_item_list), type, button_type);
        (content.findViewById(R.id.choose_selected_temper)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItemToForm(type);
                dialog.dismiss();
            }
        });
        dialog.setContentView(content);
        dialog.show();
    }

    private void addItemToForm(int type){
        switch (type){
            case TYPE_LIST:
                String selectedTypes = "";
                for(String string: selectedItems){
                    selectedTypes += string +"\n";
                }
                ((TextView)clickedView.findViewById(R.id.catform_temperament_temper_text)).setText(selectedTypes);
                break;
            case CARSICK_LIST:
                String selectedSickBehaviour = "";
                for(String string: selectedItems){
                    selectedSickBehaviour += string +"\n";
                }
                ((TextView)clickedView.findViewById(R.id.catform_temperament_carsick_text)).setText(selectedSickBehaviour);
                break;
            case ADAPTABLE_LIST:
                ((TextView)clickedView.findViewById(R.id.catform_temperament_adaptable_skills_text)).setText(selectedButtonText);
                break;
            case USED_TO_LIST:
                switch (clickedView.getId()){
                    case R.id.catform_temperament_used_to_children_text:
                        ((TextView)clickedView.findViewById(R.id.catform_temperament_used_to_children_text)).setText(selectedButtonText);
                        break;
                    case R.id.catform_temperament_used_to_dogs_text:
                        ((TextView)clickedView.findViewById(R.id.catform_temperament_used_to_dogs_text)).setText(selectedButtonText);
                        break;
                    default:break;
                }

                break;
            case GENERAL_LIST:
                ((TextView)clickedView.findViewById(R.id.catform_temperament_talking_text)).setText(selectedButtonText);
                break;
            default:break;
        }
        clearButton();
    }

    private void initSelectableList(LinearLayout itemHolder, int type, int button_type){
        String[] list_items = null;
        switch (type){
            case TYPE_LIST:
                list_items = context.getResources().getStringArray(R.array.temper_type_list);   //CHECKBOXES
                break;
            case ADAPTABLE_LIST:
                list_items = context.getResources().getStringArray(R.array.temper_adaptable_list);  //RADIOBUTTONS
                break;
            case USED_TO_LIST:
                list_items = context.getResources().getStringArray(R.array.temper_used_to_list);  //RADIOBUTTONS
                break;
            case CARSICK_LIST:
                list_items = context.getResources().getStringArray(R.array.temper_carsick_list);   //CHECKBOXES
                break;
            case GENERAL_LIST:
                list_items = context.getResources().getStringArray(R.array.general_confirm_list);  //RADIOBUTTONS
                break;
            default:break;
        }
        if(list_items != null) {
            itemHolder.removeAllViews();
            switch (button_type){
                case CHECKBOX_TYPE:
                    for (String item : list_items) {
                        addItemToList(itemHolder, item);
                    }
                    break;
                case RADIOBUTTON_TYPE:
                    addItemToList(itemHolder, list_items);
                    break;
                default:break;
            }

        }
    }

    private void addItemToList(LinearLayout itemHolder, String[] strings){
        RelativeLayout radioButtonHolder = (RelativeLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.radiogroup_layout, null);
        RadioGroup radioGroup = (RadioGroup)radioButtonHolder.findViewById(R.id.radiogroup_list);
        for(String string: strings){
            final String current_text = string;
            //RadioButton radioButton = (RadioButton)((HomePage)context).getLayoutInflater().inflate(R.layout.radiobutton_layout, null);
            AppCompatRadioButton radioButton = new AppCompatRadioButton(context);
            radioButton.setTextColor(Color.parseColor("#000000"));
            radioButton.setHighlightColor(Color.parseColor("#0c83bd"));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                radioButton.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.black_tint)));
            }
            else{
                ViewCompat.setBackgroundTintList(radioButton, new ColorStateList(new int[][]{new int[0]}, new int[]{0x000000}));
            }
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        selectedButtonText = current_text;
                    }
                }
            });
            radioButton.setText(string);
            radioGroup.addView(radioButton);
        }
        itemHolder.addView(radioButtonHolder);
    }

    private void addItemToList(LinearLayout itemHolder, final String text){
        RelativeLayout checkBoxHolder = (RelativeLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.checkbox_item, null);
        CheckBox checkBox = (CheckBox)checkBoxHolder.findViewById(R.id.checkbox_list_item);
        checkBox.setText(text);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    Log.i("TEMPERAMENT", "SELECTED TEMPER ITEM: " + text);
                    selectedItems.add(text);
                }
                else {
                    Log.i("TEMPERAMENT", "UNSELECTED TEMPER ITEM: " + text);
                    selectedItems.remove(text);
                }
            }
        });
        itemHolder.addView(checkBoxHolder);

    }

    public void addComment(final View clickedView){
        final InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        this.clickedView = clickedView;
        final Dialog dialog = new Dialog(context);
        final RelativeLayout comment_form = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.comment_form, null);
        final EditText editText = (EditText) comment_form.findViewById(R.id.commentField);
        (comment_form.findViewById(R.id.addCommentButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(text != null && !text.equals("")) {
                    ((TextView) clickedView.findViewById(R.id.catform_temperament_comment_text)).setText(text);
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.INVISIBLE);
                    inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    dialog.dismiss();
                    clearButton();
                }
                else{
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.VISIBLE);
                }
            }
        });

        (comment_form.findViewById(R.id.abortButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                dialog.dismiss();
            }
        });
        dialog.setTitle("Lägg till kommentar");
        dialog.setContentView(comment_form);
        dialog.show();
    }
}
