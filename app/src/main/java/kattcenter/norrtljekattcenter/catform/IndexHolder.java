package kattcenter.norrtljekattcenter.catform;

import kattcenter.norrtljekattcenter.Type;

/**
 * Created by Jocke on 2017-06-22.
 */

public class IndexHolder {
    /*
    public static final int HOMEPAGE_INDEX = 0;
    public static final int INFOPAGE_INDEX = 1;
    public static final int SETTINGSPAGE_INDEX = 2;
    public static final int CATLIST_PAGE = 3;
    public static final int OWNER_LIST_PAGE = 4;
    public static final int OWNER_INFO_PAGE = 5;
    public static final int OWNER_CATS_INFO_PAGE = 6;
    public static final int OWNER_SAVE_PAGE = 7;
    public static final int PERSONAL_LIST_PAGE = 8;
    public static final int PERSONAL_INFO_PAGE = 9;
    public static final int PERSONAL_HISTORY_PAGE = 10;
    public static final int PERSONAL_SAVE_PAGE = 11;
    public static final int FORM_PREVIEW_PAGE = 12;
    public static final int FIRST_FORMPAGE_INDEX = 13;
    public static final int PRIVATE_CAT_INFO_PAGE = 3;
    */
    public static final int HOMEPAGE_INDEX = 0;
    public static final int SETTINGSPAGE_INDEX = 1;

    public static final int CATLIST_PAGE = 2;
    public static final int OWNER_LIST_PAGE = 3;
    public static final int OWNER_INFO_PAGE = 4;
    public static final int OWNER_CATS_INFO_PAGE = 5;
    public static final int OWNER_SAVE_PAGE = 6;
    public static final int PERSONAL_LIST_PAGE = 7;
    public static final int PERSONAL_INFO_PAGE = 8;
    public static final int PERSONAL_HISTORY_PAGE = 9;
    public static final int PERSONAL_SAVE_PAGE = 10;
    public static final int FORM_PREVIEW_PAGE = 11;
    public static final int FIRST_FORMPAGE_INDEX = 12;

    public static final int PRIVATE_CAT_INFO_PAGE = 2;
    public static final int CATOWNER_CAT_PREVIEW_PAGE = 3;

    //FORMS
    public static final int GENERAL_FORM_INDEX = 0;
    public static final int TEMPERAMENT_FORM_INDEX = 1;
    public static final int MEDICAL_FORM_INDEX = 2;
    public static final int VACCIN_FORM_INDEX = 3;
    public static final int PARASITE_FORM_INDEX = 4;
    public static final int CLAW_FORM_INDEX = 5;
    public static final int WEIGHT_FORM_INDEX = 6;
    public static final int VET_DOC_FORM_INDEX = 7;
    public static final int REG_SKK_FORM_INDEX = 8;
    public static final int VET_FORM_INFO = 9;
    public static final int INSURANCE_FORM_INDEX = 10;
    public static final int BACKGROUND_FORM_INDEX = 11;
    public static final int HISTORY_FORM_INDEX = 12;
    public static final int STATUS_FORM_INDEX = 13;
    public static final int CONNECTION_FORM_INDEX = 14;

    public static final int CATOWNER_FORM_INDEX = 14;



    public static int authenticateIndex(final int index, Type authentication){
        int authenticatedIndex;
        switch (authentication){
            case CATOWNER:
                System.out.println("CATOWNER TRYING INDEX: "+index);
                if(index <= 3)
                    authenticatedIndex = index;
                else if(index == 12)
                    authenticatedIndex = 4;
                else if(index == 13)
                    authenticatedIndex = 3;
                else
                    authenticatedIndex = 0;
                break;
            case PERSONAL:
                if(index >= 12){
                    authenticatedIndex = index - 5;
                }
                else{
                    authenticatedIndex = index;
                }
                break;
            default:
                authenticatedIndex = index;
                break;
        }
        return authenticatedIndex;
    }

}
