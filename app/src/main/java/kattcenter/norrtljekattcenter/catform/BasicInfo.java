package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itextpdf.text.pdf.parser.Line;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeoutException;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DataPacket;
import kattcenter.norrtljekattcenter.network.DownloadProduct;
import kattcenter.norrtljekattcenter.network.UploadProduct;

/**
 * Created by Jocke on 2017-06-03.
 */

public class BasicInfo implements DatePickerDialog.OnDateSetListener{
    static final int SCAN_CODE_REQUEST = 1;  // The request code

    private Context context;
    private Calendar c = Calendar.getInstance();
    private int year, month, day;
    private View clickedView;
    Button confirmTatoo = null;
    Button confirmChip = null;
    private EditText[] inputChars = null;
    private InputMethodManager inputMethodManager = null;
    //private TextView catIdText;
    private Button confirmCatId;
    private String arrivedYear = null, setCatID = null;
    private HomePage homePage;
    private ListView typeList;
    private MyListAdapter myListAdapter;
    private Dialog listDialog;
    RelativeLayout content = null;
    private Dialog catownerCatActionDialog = null;
    private int tatooInt = 0, chipInt = 0, birthdayInt = 0;
    private View catownerSelectedDateView;

    private EditText[] editTexts;
    private TextView idOccupied;
    private TextWatcher[] listeners;


    public BasicInfo(Context context, View clickedView){
        this.context = context;
        homePage = (HomePage)context;
        this.clickedView = clickedView;
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(homePage.getPrivileged() == Type.CATOWNER){
            catownerCatActionDialog = new Dialog(context);
        }
    }

    public void showCatownerCatActionWindow(){
        final RelativeLayout content = (RelativeLayout) homePage.getLayoutInflater().inflate(R.layout.catowner_reminder_activities_dialog, null);
        content.findViewById(R.id.saveCatownerCatActionReminder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catownerCatActionDialog.dismiss();
            }
        });
        content.findViewById(R.id.reminder_date_catowner_cat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catownerSelectedDateView = content.findViewById(R.id.reminder_date_catowner_cat);
                setDate();
            }
        });
        content.findViewById(R.id.selected_date_catowner_cat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catownerSelectedDateView = content.findViewById(R.id.selected_date_catowner_cat);
                setDate();
            }
        });
        catownerCatActionDialog.setContentView(content);
        catownerCatActionDialog.show();
    }

    public void takePhoto(){
        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Välj bild från");
        LinearLayout linearLayout = (LinearLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.general_confirm_dialog, null);
        Button pickCamera = (Button) linearLayout.findViewById(R.id.abort);
        Button pickGallery = (Button) linearLayout.findViewById(R.id.confirm);
        pickCamera.setText("Kamera");
        pickGallery.setText("Galleri");
        pickCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((HomePage)context).photoFromCamera();
            }
        });
        pickGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((HomePage)context).photoFromGallery();
            }
        });
        dialog.setContentView(linearLayout);
        dialog.show();

    }

    public void setGeneratedCatID(String json){
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<Integer> catid_integers = new ArrayList<>();
        try{
            if(json == null){
                String nextAvailableCatId = arrivedYear.substring(2, 4)+"-001";
                Log.i("HOMEPAGE", "REASSIGNED CATID TO: " + nextAvailableCatId);

                setCatIdText(nextAvailableCatId);
                //catIdText.setText(nextAvailableCatId);
                confirmCatId.setEnabled(true);
                confirmCatId.setBackgroundColor(Color.parseColor("#00aa00"));
            }
            else {
                String[] jsonRows = json.split("\n");
                if (jsonRows.length == 0) {
                } else {
                    for (String jsonRow : jsonRows) {
                        System.out.println("JSONROW: " + jsonRow);
                        JSONObject jsonObject = new JSONObject(jsonRow);
                        String rec_catid = jsonObject.getString("catcenter_id");
                        if(rec_catid.isEmpty()) continue;
                        String[] catid_split = rec_catid.split("-");
                        String year = catid_split[0];
                        String order_number = catid_split[1];
                        if (year.equals(arrivedYear.substring(2, 4))) {
                            if (order_number.charAt(0) == '0') {
                                if (order_number.charAt(1) == '0') {
                                    catid_integers.add(Integer.parseInt(order_number.substring(2, 3)));
                                } else {
                                    catid_integers.add(Integer.parseInt(order_number.substring(1, 3)));
                                }
                            } else {
                                catid_integers.add(Integer.parseInt(order_number));
                            }
                            arrayList.add(rec_catid);
                        }
                    }
                    Collections.sort(catid_integers);
                    String nextAvailableCatId = null;
                    int catintsize = catid_integers.size();
                    if (catintsize > 0) {
                        int nextAvailableCatIdInt = catid_integers.get(catintsize - 1) + 1;
                        if (nextAvailableCatIdInt < 10)
                            nextAvailableCatId = arrivedYear.substring(2, 4) + "-00" + nextAvailableCatIdInt;
                        else if (nextAvailableCatIdInt < 100)
                            nextAvailableCatId = arrivedYear.substring(2, 4) + "-0" + nextAvailableCatIdInt;
                        else
                            nextAvailableCatId = arrivedYear.substring(2, 4) + "-" + nextAvailableCatIdInt;
                    } else {
                        nextAvailableCatId = arrivedYear.substring(2, 4) + "-001";
                    }
                    Log.i("HOMEPAGE", "   NEXT AVAILABLE: " + nextAvailableCatId);

                    if (nextAvailableCatId != null) {
                        Log.i("HOMEPAGE", "REASSIGNED CATID TO: " + nextAvailableCatId);
                        setCatIdText(nextAvailableCatId);
                        //catIdText.setText(nextAvailableCatId);
                        confirmCatId.setEnabled(true);
                        confirmCatId.setBackgroundColor(Color.parseColor("#00aa00"));
                    }
                }
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void activateManualIDField(){
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        for(int i = 0; i < editTexts.length; i++){
            EditText editText = editTexts[i];
            if(i == 0) editText.setEnabled(true);
            //editText.setEnabled(true);
            editText.addTextChangedListener(new MyIDTextListener(i));
        }
    }

    private class MyIDTextListener implements TextWatcher{

        private int nr;
        public MyIDTextListener(int nr){
            this.nr = nr;
        }
        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * are about to be replaced by new text with length <code>after</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param count
         * @param after
         */
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * have just replaced old text that had length <code>before</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param before
         * @param count
         */
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(before == 1){
                if(nr > 0){
                    editTexts[nr].clearFocus();
                    editTexts[nr-1].requestFocus();
                    editTexts[nr-1].setCursorVisible(true);
                }
            }
            else {
                if (nr < 4) {
                    editTexts[nr].clearFocus();
                    editTexts[nr + 1].setEnabled(true);
                    editTexts[nr + 1].requestFocus();
                    editTexts[nr + 1].setCursorVisible(true);
                } else {
                    String id = editTexts[0].getText().toString() + editTexts[1].getText().toString() + "-" +
                            editTexts[2].getText().toString() + editTexts[3].getText().toString() + editTexts[4].getText().toString();
                    HashMap<String, String> data = new HashMap<>();
                    data.put("misc_action", "check_id");
                    data.put("blub", Base64.encodeToString(id.getBytes(), Base64.NO_WRAP));
                    ((HomePage) context).prepareAndSendData(data, Type.DOWNLOAD_MISC);
                }
            }
        }

        /**
         * This method is called to notify you that, somewhere within
         * <code>s</code>, the text has been changed.
         * It is legitimate to make further changes to <code>s</code> from
         * this callback, but be careful not to get yourself into an infinite
         * loop, because any changes you make will cause this method to be
         * called again recursively.
         * (You are not told where the change took place because other
         * afterTextChanged() methods may already have made other changes
         * and invalidated the offsets.  But if you need to know here,
         * you can use {@link Spannable#setSpan} in {@link #onTextChanged}
         * to mark your place and then look up from here where the span
         * ended up.
         *
         * @param s
         */
        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    public void setCatIdText(String id){
        setCatID = id;
        String year = id.split("-")[0];
        id = id.split("-")[1];
        System.out.println("YEAR: "+year+" CHAR AT 0: "+year.charAt(0));
        editTexts[0].setText(new String(year.charAt(0)+""));
        editTexts[1].setText(new String(year.charAt(1)+""));
        editTexts[2].setText(new String(id.charAt(0)+""));
        editTexts[3].setText(new String(id.charAt(1)+""));
        editTexts[4].setText(new String(id.charAt(2)+""));
    }

    public void unavailableCatID(){
        idOccupied.setVisibility(View.VISIBLE);
        confirmCatId.setEnabled(false);
        confirmCatId.setBackgroundColor(Color.parseColor("#aaaaaa"));
    }
    public void availableCatID(){
        idOccupied.setVisibility(View.INVISIBLE);
        if(!editTexts[4].getText().toString().isEmpty()) {
            setCatID = editTexts[0].getText().toString() + editTexts[1].getText().toString() + "-" +
                    editTexts[2].getText().toString() + editTexts[3].getText().toString() + editTexts[4].getText().toString();

            confirmCatId.setEnabled(true);
            confirmCatId.setBackgroundColor(Color.parseColor("#00aa00"));
        }
    }

    public void showIdDialog(){
        String existingID = ((TextView)homePage.findViewById(R.id.catbasic_cathome_id_text)).getText().toString();
        homePage.setGenerateID(true);
        final Dialog idDialog = new Dialog(context);
        idDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        String arrived_date = ((TextView)((HomePage)context).findViewById(R.id.catbasic_arrived_date_text)).getText().toString();
        if(arrived_date == null || arrived_date.equals("")){
            content = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_1, null);
            ((TextView)content.findViewById(R.id.listEntry)).setText("Ange inkommet datum först");
        }
        else{
            content = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.catid_generator_form, null);
            //catIdText = ((TextView)content.findViewById(R.id.generated_catid_text));
            editTexts = new EditText[5];
            editTexts[0] = (EditText)content.findViewById(R.id.catid_nr1);
            editTexts[1] = (EditText)content.findViewById(R.id.catid_nr2);
            editTexts[2] = (EditText)content.findViewById(R.id.catid_nr3);
            editTexts[3] = (EditText)content.findViewById(R.id.catid_nr4);
            editTexts[4] = (EditText)content.findViewById(R.id.catid_nr5);
            idOccupied = (TextView)content.findViewById(R.id.occupiedID);
            System.out.println("EDITTEXT: "+editTexts[0]);
            arrivedYear = arrived_date.split("-")[0];
            ((TextView)content.findViewById(R.id.arrived_date_year_text)).setText(arrivedYear);
            confirmCatId = (Button) content.findViewById(R.id.confirmGeneratedCatid);
            if(existingID.isEmpty()) {
                (content.findViewById(R.id.generate_catid)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HashMap<String, String> data = new HashMap<>();
                        data.put("misc_action", "names_and_id");
                        ((HomePage) context).prepareAndSendData(data, Type.DOWNLOAD_MISC);
                        (content.findViewById(R.id.generate_catid)).setEnabled(false);
                    /*
                    networkThread.addDataToQueue(new DataPacket(data, Type.DOWNLOAD_MISC));
                    networkThread.nextInQueue();
                    */
                    }
                });
                (content.findViewById(R.id.manual_catid)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activateManualIDField();
                    }
                });

                confirmCatId.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((TextView) homePage.findViewById(R.id.catbasic_cathome_id_text)).setText(setCatID);
                        homePage.setGenerateID(false);
                        idDialog.dismiss();
                    }
                });
            }
            else{
                (content.findViewById(R.id.generate_catid)).setEnabled(false);
                setCatIdText(existingID);
                //catIdText.setText(existingID);
                confirmCatId.setEnabled(true);
                confirmCatId.setBackgroundColor(Color.parseColor("#00aa00"));
                confirmCatId.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //((TextView) homePage.findViewById(R.id.catbasic_cathome_id_text)).setText(catIdText.getText().toString());
                        homePage.setGenerateID(false);
                        idDialog.dismiss();
                    }
                });
            }

        }
        idDialog.setContentView(content);
        idDialog.show();
    }

    public void birthdayOptions(){
        final Dialog birthdayOptionsDialog = new Dialog(context);
        birthdayOptionsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout options = (LinearLayout)homePage.getLayoutInflater().inflate(R.layout.three_button_dialog, null);
        Button opt1 = (Button) options.findViewById(R.id.option1);
        Button opt2 = (Button) options.findViewById(R.id.option2);
        Button opt3 = (Button) options.findViewById(R.id.option3);
        opt1.setText("DATUM");
        opt2.setText("DATUMTYP");
        opt3.setText("AVBRYT");
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                birthdayOptionsDialog.dismiss();
                setDate();
                //((TextView)homePage.findViewById(R.id.catbasic_sex_type_text)).setText("HANE");
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                birthdayOptionsDialog.dismiss();
                setBirthdayType();
                //((TextView)homePage.findViewById(R.id.catbasic_sex_type_text)).setText("HONA");
            }
        });
        opt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                birthdayOptionsDialog.dismiss();
                //((TextView)homePage.findViewById(R.id.catbasic_sex_type_text)).setText("VET EJ");
            }
        });
        birthdayOptionsDialog.setContentView(options);
        birthdayOptionsDialog.show();
    }

    public void setBirthdayType(){
        final Dialog birthdayTypeDialog = new Dialog(context);
        birthdayTypeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout options = (LinearLayout)homePage.getLayoutInflater().inflate(R.layout.general_confirm_dialog, null);
        Button opt1 = (Button) options.findViewById(R.id.confirm);
        Button opt2 = (Button) options.findViewById(R.id.abort);
        opt1.setText("EXAKT");
        opt2.setText("UPPSKATTAT");
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                birthdayTypeDialog.dismiss();
                /*
                birthdayInt++;
                int drawableId = -1;
                if(birthdayInt == 1) drawableId = R.drawable.button_style_nr_4;
                else if(birthdayInt == 2) drawableId = R.drawable.button_style_nr_2;
                if(drawableId != -1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        clickedView.setBackground(homePage.getResources().getDrawable(drawableId, null));
                    } else {
                        clickedView.setBackground(homePage.getResources().getDrawable(drawableId));
                    }
                }
                */
                ((TextView)homePage.findViewById(R.id.catbasic_birthday_guess)).setText("Exakt");
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                birthdayTypeDialog.dismiss();
                /*
                birthdayInt++;
                int drawableId = -1;
                if(birthdayInt == 1) drawableId = R.drawable.button_style_nr_4;
                else if(birthdayInt == 2) drawableId = R.drawable.button_style_nr_2;
                if(drawableId != -1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        clickedView.setBackground(homePage.getResources().getDrawable(drawableId, null));
                    } else {
                        clickedView.setBackground(homePage.getResources().getDrawable(drawableId));
                    }
                }
                */
                ((TextView)homePage.findViewById(R.id.catbasic_birthday_guess)).setText("Uppskattat");
            }
        });
        birthdayTypeDialog.setContentView(options);
        birthdayTypeDialog.show();
    }

    public void dateOptions(){
        final Dialog neuterOptionDialog = new Dialog(context);
        neuterOptionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout options = (LinearLayout)homePage.getLayoutInflater().inflate(R.layout.three_button_dialog, null);
        Button opt1 = (Button) options.findViewById(R.id.option1);
        Button opt2 = (Button) options.findViewById(R.id.option2);
        Button opt3 = (Button) options.findViewById(R.id.option3);
        opt1.setText("DATUM");
        opt2.setText("SEDAN TIDIGARE");
        opt3.setText("AVBRYT");
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                neuterOptionDialog.dismiss();
                setDate();
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                neuterOptionDialog.dismiss();
                setDateText("Sedan tidigare");
                /*
                switch (clickedView.getId()){
                    case R.id.catbasic_select_arrived_date:
                        ((TextView)((HomePage)context).findViewById(R.id.catbasic_arrived_date_text)).setText("Sedan tidigare");
                        break;
                    case R.id.catbasic_select_moved_date:
                        ((TextView)((HomePage)context).findViewById(R.id.catbasic_moved_date_text)).setText("Sedan tidigare");
                        break;
                    case R.id.catbasic_select_neuter_date:
                        ((TextView)homePage.findViewById(R.id.catbasic_neuter_date_text)).setText("Sedan tidigare");
                        break;
                    default:break;
                }
                */

            }
        });
        neuterOptionDialog.setContentView(options);
        opt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                neuterOptionDialog.dismiss();
            }
        });
        neuterOptionDialog.show();
    }

    public void tatooOptions(){
        final Dialog tatooOptionDialog = new Dialog(context);
        tatooOptionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout options = (LinearLayout)homePage.getLayoutInflater().inflate(R.layout.four_button_dialog, null);
        Button opt1 = (Button) options.findViewById(R.id.option1);
        Button opt2 = (Button) options.findViewById(R.id.option2);
        Button opt3 = (Button) options.findViewById(R.id.option3);
        Button opt4 = (Button) options.findViewById(R.id.option4);
        opt1.setText("DATUM");
        opt2.setText("TATUERING");
        opt3.setText("EJ TATUERAD");
        opt4.setText("AVBRYT");
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tatooOptionDialog.dismiss();
                //setDate();
                dateOptions();
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tatooOptionDialog.dismiss();
                handleTatooInput();
            }
        });
        opt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView)homePage.findViewById(R.id.catbasic_tatoo_text)).setText("Ej tatuerad");
                tatooOptionDialog.dismiss();
            }
        });
        opt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tatooOptionDialog.dismiss();
            }
        });
        tatooOptionDialog.setContentView(options);
        tatooOptionDialog.show();
    }

    public void chipOptions(){
        final Dialog chipOptionDialog = new Dialog(context);
        chipOptionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout options = (LinearLayout)homePage.getLayoutInflater().inflate(R.layout.three_button_dialog, null);
        Button opt1 = (Button) options.findViewById(R.id.option1);
        Button opt2 = (Button) options.findViewById(R.id.option2);
        Button opt3 = (Button) options.findViewById(R.id.option3);
        opt1.setText("DATUM");
        opt2.setText("KOD");
        //opt2.setText("SKANNA");
        opt3.setText("AVBRYT");
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chipOptionDialog.dismiss();
                //setDate();
                dateOptions();
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chipOptionDialog.dismiss();
                //scanCode();
                codeOption();
            }
        });
        opt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chipOptionDialog.dismiss();
            }
        });
        chipOptionDialog.setContentView(options);
        chipOptionDialog.show();
    }

    public void codeOption(){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout options = (LinearLayout) homePage.getLayoutInflater().inflate(R.layout.three_button_dialog, null);
        Button opt1 = (Button) options.findViewById(R.id.option1);
        Button opt2 = (Button) options.findViewById(R.id.option2);
        Button opt3 = (Button) options.findViewById(R.id.option3);
        opt1.setText("SKANNA");
        opt2.setText("MANUELLT");
        //opt2.setText("SKANNA");
        opt3.setText("AVBRYT");
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                scanCode();
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //manualInputCode();
                handleChipInput();
            }
        });
        opt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(options);
        dialog.show();
    }

    public void manualInputCode(){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        RelativeLayout options = (RelativeLayout) homePage.getLayoutInflater().inflate(R.layout.input_number_dialog, null);
        Button opt1 = (Button) options.findViewById(R.id.abort);
        Button opt2 = (Button) options.findViewById(R.id.confirm);
        final EditText input_field = (EditText) options.findViewById(R.id.phoneNumber); //REUSE PHONE INPUT FIELD LAYOUT
        opt1.setText("AVBRYT");
        opt2.setText("LÄGG TILL");
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((TextView)homePage.findViewById(R.id.catbasic_chip_code_text)).setText(input_field.getText().toString());
            }
        });
        dialog.setContentView(options);
        dialog.show();
    }

    public void scanCode(){
        //TextView textView = (TextView) view;
        //LinearLayout linearLayout = (LinearLayout) view;
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.setPackage("com.google.zxing.client.android");
        intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
        ((HomePage) context).startActivityForResult(intent, SCAN_CODE_REQUEST);
    }

    public void setSexType(){
        final Dialog sexTypeDialog = new Dialog(context);
        sexTypeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout options = (LinearLayout)homePage.getLayoutInflater().inflate(R.layout.three_button_dialog, null);
        Button opt1 = (Button) options.findViewById(R.id.option1);
        Button opt2 = (Button) options.findViewById(R.id.option2);
        Button opt3 = (Button) options.findViewById(R.id.option3);
        opt1.setText("HANE");
        opt2.setText("HONA");
        opt3.setText("VET EJ");
        opt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sexTypeDialog.dismiss();
                ((TextView)homePage.findViewById(R.id.catbasic_sex_type_text)).setText("HANE");
            }
        });
        opt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sexTypeDialog.dismiss();
                ((TextView)homePage.findViewById(R.id.catbasic_sex_type_text)).setText("HONA");
            }
        });
        opt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sexTypeDialog.dismiss();
                ((TextView)homePage.findViewById(R.id.catbasic_sex_type_text)).setText("VET EJ");
            }
        });
        sexTypeDialog.setContentView(options);
        sexTypeDialog.show();
    }


    public void updateList(ArrayList<String> types, String type_list_name){
        myListAdapter = new MyListAdapter(context, types, type_list_name);
        typeList.setAdapter(myListAdapter);
        listDialog.setTitle("");
    }

    public void loadListDialog(String table_type, final String list_type){
        listDialog = new Dialog(context);
        listDialog.setTitle("Laddar lista...");
        final RelativeLayout listContent = (RelativeLayout)homePage.getLayoutInflater().inflate(R.layout.type_listview_add, null);
        typeList = (ListView) listContent.findViewById(R.id.typeList);
        //ArrayList<String> types = new ArrayList<>();
        //types.add("Laddar lista...");
        //myListAdapter = new MyListAdapter(context, types);
        typeList.setAdapter(myListAdapter);
        Button addNewType = (Button) listContent.findViewById(R.id.addListEntry);
        final EditText newType = (EditText) listContent.findViewById(R.id.newListEntry);
        addNewType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = newType.getText().toString();
                if(type != null && !type.equals("")){

                    switch (clickedView.getId()){
                        case R.id.catbasic_select_race:
                            ((TextView)homePage.findViewById(R.id.catbasic_race_text)).setText(type);
                            break;
                        case R.id.catbasic_select_hair_type:
                            ((TextView)homePage.findViewById(R.id.catbasic_hair_type)).setText(type);
                            break;
                        case R.id.catbasic_select_color:
                            ((TextView)homePage.findViewById(R.id.catbasic_color_text)).setText(type);
                            break;
                        default:break;
                    }


                    try {
                        new UploadProduct(context, list_type, Type.UPLOAD.toString(), type).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    listDialog.dismiss();
                }
                else{
                    (listContent.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        listDialog.setContentView(listContent);
        listDialog.show();
        try {
            new DownloadProduct(context, table_type, list_type, Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public class MyListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> list;
        private String type_list_name;

        public MyListAdapter(@NonNull Context context, ArrayList<String> list, String type_list_name) {
            super(context, 0, list);
            this.list = list;
            this.type_list_name = type_list_name;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            final String type = list.get(position);
            if(view == null){
                view = ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_2, null);
            }
            TextView entryName = (TextView) view.findViewById(R.id.listEntry);
            entryName.setText(type);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    switch (clickedView.getId()){
                        case R.id.catbasic_select_race:
                            ((TextView)homePage.findViewById(R.id.catbasic_race_text)).setText(type);
                            break;
                        case R.id.catbasic_select_color:
                            ((TextView)homePage.findViewById(R.id.catbasic_color_text)).setText(type);
                            break;
                        case R.id.catbasic_select_hair_type:
                            ((TextView)homePage.findViewById(R.id.catbasic_hair_type)).setText(type);
                            break;
                        default:break;
                    }

                    listDialog.dismiss();
                }
            });
            view.setOnLongClickListener(new MyLongItemPressListener(context, list, type, type_list_name, ""));
            return view;
        }
    }


    /*
    public void handleCheckBox(){
        CheckBox checkBox = (CheckBox) clickedView;
        String checkBoxText = checkBox.getText().toString();
        if(checkBoxText.split(" ").length == 2){
            checkBox.setText(checkBoxText.split(" ")[0]);
            checkBox.setChecked(false);
            if(checkBox.getText().toString().equals("Chip")){
                ((HomePage)context).findViewById(R.id.scanCode).setEnabled(false);
                ((TextView)((HomePage)context).findViewById(R.id.scanCode)).setTextColor(Color.parseColor("#888888"));
            }
        }
        else{
            setDate();
        }
    }
    */

    public void handleTatooInput(){

        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Tatuering");
        final RelativeLayout content = (RelativeLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.tatoo_input_dialog, null);
        dialog.setContentView(content);
        Button abortTatoo = (Button) content.findViewById(R.id.abortButton);
        abortTatoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirmTatoo = (Button) content.findViewById(R.id.addTatooStringButton);
        confirmTatoo.setEnabled(false);
        confirmTatoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tatooText = "";
                for(EditText editText: inputChars)
                    tatooText += editText.getText().toString();
                //((TextView)clickedView.findViewById(R.id.catbasic_tatoo_text)).setText(tatooText);
                final String tatooString = tatooText;
                inputMethodManager.hideSoftInputFromWindow(inputChars[4].getWindowToken(), 0);
                /*
                tatooInt++;
                int drawableId = -1;
                if(tatooInt == 1) drawableId = R.drawable.button_style_nr_4;
                else if(tatooInt == 2) drawableId = R.drawable.button_style_nr_2;

                if(drawableId != -1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        clickedView.setBackground(homePage.getResources().getDrawable(drawableId, null));
                    } else {
                        clickedView.setBackground(homePage.getResources().getDrawable(drawableId));
                    }
                }
                */
                if(inputChars[4].getText().toString().isEmpty()){
                    final Dialog dialog1 = new Dialog(context);
                    LinearLayout linearLayout = (LinearLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.text_button_dialog, null);
                    ((TextView)linearLayout.findViewById(R.id.general_dialog_text)).setText("Vill du spara tatuering med 4 tecken?");
                    Button abortSaveFour = (Button) linearLayout.findViewById(R.id.abort);
                    abortSaveFour.setText("Nej");
                    Button saveFour = (Button) linearLayout.findViewById(R.id.confirm);
                    saveFour.setText("Ja");
                    abortSaveFour.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog1.dismiss();
                        }
                    });
                    saveFour.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((TextView)homePage.findViewById(R.id.catbasic_tatoo_text)).setText(tatooString);
                            inputMethodManager.hideSoftInputFromWindow(inputChars[3].getWindowToken(), 0);
                            dialog1.dismiss();
                            dialog.dismiss();
                        }
                    });
                    dialog1.setContentView(linearLayout);
                    dialog1.show();
                }
                else{
                    ((TextView)homePage.findViewById(R.id.catbasic_tatoo_text)).setText(tatooString);
                    dialog.dismiss();
                }
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                inputMethodManager.hideSoftInputFromWindow(clickedView.getWindowToken(), 0);
            }
        });
        String exisitingTatooText = ((TextView)homePage.findViewById(R.id.catbasic_tatoo_text)).getText().toString();
        EditText tatoo_char_1 = (EditText) content.findViewById(R.id.tatoo_char_1);
        EditText tatoo_char_2 = (EditText) content.findViewById(R.id.tatoo_char_2);
        EditText tatoo_char_3 = (EditText) content.findViewById(R.id.tatoo_char_3);
        EditText tatoo_char_4 = (EditText) content.findViewById(R.id.tatoo_char_4);
        EditText tatoo_char_5 = (EditText) content.findViewById(R.id.tatoo_char_5);
        if(!exisitingTatooText.isEmpty()){
            char[] chars = exisitingTatooText.toCharArray();
            for(int i = 0; i < chars.length; i++){
                if(i == 0) tatoo_char_1.setText(String.valueOf(chars[i]));
                if(i == 1) tatoo_char_2.setText(String.valueOf(chars[i]));
                if(i == 2) tatoo_char_3.setText(String.valueOf(chars[i]));
                if(i == 3) tatoo_char_4.setText(String.valueOf(chars[i]));
                if(i == 4) tatoo_char_5.setText(String.valueOf(chars[i]));
            }
        }
        listeners = new TextWatcher[5];
        listeners[0] = new TatooInputHandler(0, false, false, true);
        listeners[1] = new TatooInputHandler(1, false, false, false);
        listeners[2] = new TatooInputHandler(2, false, false, false);
        listeners[3] = new TatooInputHandler(3, true, false, false);
        listeners[4] = new TatooInputHandler(4, true, true, false);
        /*
        tatoo_char_1.addTextChangedListener(new TatooInputHandler(0, false, false, true));
        tatoo_char_2.addTextChangedListener(new TatooInputHandler(1, false, false, false));
        tatoo_char_3.addTextChangedListener(new TatooInputHandler(2, false, false, false));
        tatoo_char_4.addTextChangedListener(new TatooInputHandler(3, true, false, false));
        tatoo_char_5.addTextChangedListener(new TatooInputHandler(4, true, true, false));
        */
        tatoo_char_1.addTextChangedListener(listeners[0]);
        tatoo_char_2.addTextChangedListener(listeners[1]);
        tatoo_char_3.addTextChangedListener(listeners[2]);
        tatoo_char_4.addTextChangedListener(listeners[3]);
        tatoo_char_5.addTextChangedListener(listeners[4]);
        if(exisitingTatooText.isEmpty()){
            tatoo_char_2.setEnabled(false);
            tatoo_char_3.setEnabled(false);
            tatoo_char_4.setEnabled(false);
            tatoo_char_5.setEnabled(false);
        }
        else{
            confirmTatoo.setEnabled(true);
        }

        inputChars = new EditText[5];
        inputChars[0] = tatoo_char_1;
        inputChars[1] = tatoo_char_2;
        inputChars[2] = tatoo_char_3;
        inputChars[3] = tatoo_char_4;
        inputChars[4] = tatoo_char_5;

        int k = 0;
        for(EditText input: inputChars){
            input.setOnKeyListener(new BackSpaceListener(k++, "TATOO"));
        }

        dialog.show();
    }

    public class BackSpaceListener implements View.OnKeyListener{

        private int index;
        private String type;
        public BackSpaceListener(int index, String type){
            this.index = index;
            this.type = type;
        }
        /**
         * Called when a hardware key is dispatched to a view. This allows listeners to
         * get a chance to respond before the target view.
         * <p>Key presses in software keyboards will generally NOT trigger this method,
         * although some may elect to do so in some situations. Do not assume a
         * software input method has to be key-based; even if it is, it may use key presses
         * in a different way than you expect, so there is no way to reliably catch soft
         * input key presses.
         *
         * @param v       The view the key has been dispatched to.
         * @param keyCode The code for the physical key that was pressed
         * @param event   The KeyEvent object containing full information about
         *                the event.
         * @return True if the listener has consumed the event, false otherwise.
         */
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if(keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_UP){
                if(type.equals("TATOO"))
                    ((TatooInputHandler)listeners[index]).backSpace();
                else
                    ((ChipInputHandler)listeners[index]).backSpace();
            }
            return false;
        }
    }

    public class TatooInputHandler implements TextWatcher{

        private boolean lastInput = false;
        private boolean enableButton = false;
        private boolean firstInput = false;
        private int index = 0;
        public TatooInputHandler(int index, boolean enableButton, boolean lastInput, boolean firstInput){
            this.index = index;
            this.enableButton = enableButton;
            this.lastInput = lastInput;
            this.firstInput = firstInput;
        }


        public void backSpace(){
            System.out.println("BACKSPACE ON INDEX: "+index);
            if(index > 0){
                inputChars[index].clearFocus();
                inputChars[index].setEnabled(false);
                inputChars[index-1].setText("");
                inputChars[index-1].setEnabled(true);
                inputChars[index-1].requestFocus();
                inputChars[index-1].setCursorVisible(true);
            }
        }

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * are about to be replaced by new text with length <code>after</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param count
         * @param after
         */
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            System.out.println("-----BEFORE----\n"+s);
        }

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * have just replaced old text that had length <code>before</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param before
         * @param count
         */
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            System.out.println("------ON------\n"+s);
            if(count == 1){
                if(enableButton){
                    confirmTatoo.setEnabled(true);
                }
                if(lastInput) {
                    inputMethodManager.hideSoftInputFromWindow(inputChars[4].getWindowToken(), 0);
                }
                else{
                    inputChars[index].clearFocus();
                    inputChars[index+1].setEnabled(true);
                    inputChars[index+1].requestFocus();
                    inputChars[index+1].setCursorVisible(true);
                }
            }
            /*
            else if(count == 0 && !firstInput && index > 0){
                inputChars[index].clearFocus();
                inputChars[index-1].setEnabled(true);
                inputChars[index-1].requestFocus();
                inputChars[index-1].setCursorVisible(true);
                if(index < 4)
                    confirmTatoo.setEnabled(false);
            }
            */

            else if(count == 0 && !enableButton){
                confirmTatoo.setEnabled(false);
            }

        }

        /**
         * This method is called to notify you that, somewhere within
         * <code>s</code>, the text has been changed.
         * It is legitimate to make further changes to <code>s</code> from
         * this callback, but be careful not to get yourself into an infinite
         * loop, because any changes you make will cause this method to be
         * called again recursively.
         * (You are not told where the change took place because other
         * afterTextChanged() methods may already have made other changes
         * and invalidated the offsets.  But if you need to know here,
         * you can use {@link Spannable#setSpan} in {@link #onTextChanged}
         * to mark your place and then look up from here where the span
         * ended up.
         *
         * @param s
         */
        @Override
        public void afterTextChanged(Editable s) {
            System.out.println("------AFTER----\n"+s);
        }
    }


    public void handleChipInput(){

        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Tatuering");
        final RelativeLayout content = (RelativeLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.chip_input_dialog, null);
        dialog.setContentView(content);
        Button abortTatoo = (Button) content.findViewById(R.id.abortButton);
        abortTatoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirmChip = (Button) content.findViewById(R.id.addChipStringButton);
        confirmChip.setEnabled(false);
        confirmChip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String chipText = "";
                for(EditText editText: inputChars)
                    chipText += editText.getText().toString()+"";
                //((TextView)clickedView.findViewById(R.id.catbasic_tatoo_text)).setText(tatooText);
                final String chipString = chipText;
                /*
                tatooInt++;
                int drawableId = -1;
                if(tatooInt == 1) drawableId = R.drawable.button_style_nr_4;
                else if(tatooInt == 2) drawableId = R.drawable.button_style_nr_2;

                if(drawableId != -1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        clickedView.setBackground(homePage.getResources().getDrawable(drawableId, null));
                    } else {
                        clickedView.setBackground(homePage.getResources().getDrawable(drawableId));
                    }
                }
                */
                inputMethodManager.hideSoftInputFromWindow(inputChars[14].getWindowToken(), 0);
                ((TextView)homePage.findViewById(R.id.catbasic_chip_code_text)).setText(chipString);
                dialog.dismiss();
                /*
                if(inputChars[4].getText().toString().isEmpty()){
                    final Dialog dialog1 = new Dialog(context);
                    LinearLayout linearLayout = (LinearLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.text_button_dialog, null);
                    ((TextView)linearLayout.findViewById(R.id.general_dialog_text)).setText("Vill du spara tatuering med 4 tecken?");
                    Button abortSaveFour = (Button) linearLayout.findViewById(R.id.abort);
                    abortSaveFour.setText("Nej");
                    Button saveFour = (Button) linearLayout.findViewById(R.id.confirm);
                    saveFour.setText("Ja");
                    abortSaveFour.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog1.dismiss();
                        }
                    });
                    saveFour.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((TextView)homePage.findViewById(R.id.catbasic_chip_code_text)).setText(tatooString);
                            dialog1.dismiss();
                            dialog.dismiss();
                        }
                    });
                    dialog1.setContentView(linearLayout);
                    dialog1.show();
                }
                else{

                }
                */
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                inputMethodManager.hideSoftInputFromWindow(clickedView.getWindowToken(), 0);
            }
        });
        String exisitingChipCode = ((TextView)homePage.findViewById(R.id.catbasic_chip_code_text)).getText().toString();
        EditText chip_char_1 = (EditText) content.findViewById(R.id.chip_char_1);
        EditText chip_char_2 = (EditText) content.findViewById(R.id.chip_char_2);
        EditText chip_char_3 = (EditText) content.findViewById(R.id.chip_char_3);
        EditText chip_char_4 = (EditText) content.findViewById(R.id.chip_char_4);
        EditText chip_char_5 = (EditText) content.findViewById(R.id.chip_char_5);
        EditText chip_char_6 = (EditText) content.findViewById(R.id.chip_char_6);
        EditText chip_char_7 = (EditText) content.findViewById(R.id.chip_char_7);
        EditText chip_char_8 = (EditText) content.findViewById(R.id.chip_char_8);
        EditText chip_char_9 = (EditText) content.findViewById(R.id.chip_char_9);
        EditText chip_char_10 = (EditText) content.findViewById(R.id.chip_char_10);
        EditText chip_char_11 = (EditText) content.findViewById(R.id.chip_char_11);
        EditText chip_char_12 = (EditText) content.findViewById(R.id.chip_char_12);
        EditText chip_char_13 = (EditText) content.findViewById(R.id.chip_char_13);
        EditText chip_char_14 = (EditText) content.findViewById(R.id.chip_char_14);
        EditText chip_char_15 = (EditText) content.findViewById(R.id.chip_char_15);
        if(!exisitingChipCode.isEmpty()){
            char[] chars = exisitingChipCode.toCharArray();
            for(int i = 0; i < chars.length; i++){
                Log.i("BASICINFO", "CHAR: "+chars[i]);
                if(i == 0) chip_char_1.setText(String.valueOf(chars[i]));
                if(i == 1) chip_char_2.setText(String.valueOf(chars[i]));
                if(i == 2) chip_char_3.setText(String.valueOf(chars[i]));
                if(i == 3) chip_char_4.setText(String.valueOf(chars[i]));
                if(i == 4) chip_char_5.setText(String.valueOf(chars[i]));
                if(i == 5) chip_char_6.setText(String.valueOf(chars[i]));
                if(i == 6) chip_char_7.setText(String.valueOf(chars[i]));
                if(i == 7) chip_char_8.setText(String.valueOf(chars[i]));
                if(i == 8) chip_char_9.setText(String.valueOf(chars[i]));
                if(i == 9) chip_char_10.setText(String.valueOf(chars[i]));
                if(i == 10) chip_char_11.setText(String.valueOf(chars[i]));
                if(i == 11) chip_char_12.setText(String.valueOf(chars[i]));
                if(i == 12) chip_char_13.setText(String.valueOf(chars[i]));
                if(i == 13) chip_char_14.setText(String.valueOf(chars[i]));
                if(i == 14) chip_char_15.setText(String.valueOf(chars[i]));
            }
        }

        listeners = new TextWatcher[15];
        listeners[0] = new ChipInputHandler(0, false);
        listeners[1] = new ChipInputHandler(1, false);
        listeners[2] = new ChipInputHandler(2, false);
        listeners[3] = new ChipInputHandler(3, false);
        listeners[4] = new ChipInputHandler(4, false);
        listeners[5] = new ChipInputHandler(5, false);
        listeners[6] = new ChipInputHandler(6, false);
        listeners[7] = new ChipInputHandler(7, false);
        listeners[8] = new ChipInputHandler(8, false);
        listeners[9] = new ChipInputHandler(9, false);
        listeners[10] = new ChipInputHandler(10, false);
        listeners[11] = new ChipInputHandler(11, false);
        listeners[12] = new ChipInputHandler(12, false);
        listeners[13] = new ChipInputHandler(13, false);
        listeners[14] = new ChipInputHandler(14, true);



        chip_char_1.addTextChangedListener(listeners[0]);
        chip_char_2.addTextChangedListener(listeners[1]);
        chip_char_3.addTextChangedListener(listeners[2]);
        chip_char_4.addTextChangedListener(listeners[3]);
        chip_char_5.addTextChangedListener(listeners[4]);
        chip_char_6.addTextChangedListener(listeners[5]);
        chip_char_7.addTextChangedListener(listeners[6]);
        chip_char_8.addTextChangedListener(listeners[7]);
        chip_char_9.addTextChangedListener(listeners[8]);
        chip_char_10.addTextChangedListener(listeners[9]);
        chip_char_11.addTextChangedListener(listeners[10]);
        chip_char_12.addTextChangedListener(listeners[11]);
        chip_char_13.addTextChangedListener(listeners[12]);
        chip_char_14.addTextChangedListener(listeners[13]);
        chip_char_15.addTextChangedListener(listeners[14]);
        if(exisitingChipCode.isEmpty()) {
            chip_char_2.setEnabled(false);
            chip_char_3.setEnabled(false);
            chip_char_4.setEnabled(false);
            chip_char_5.setEnabled(false);
            chip_char_6.setEnabled(false);
            chip_char_7.setEnabled(false);
            chip_char_8.setEnabled(false);
            chip_char_9.setEnabled(false);
            chip_char_10.setEnabled(false);
            chip_char_11.setEnabled(false);
            chip_char_12.setEnabled(false);
            chip_char_13.setEnabled(false);
            chip_char_14.setEnabled(false);
            chip_char_15.setEnabled(false);

        }
        else{
            confirmChip.setEnabled(true);
        }
        inputChars = new EditText[15];
        inputChars[0] = chip_char_1;
        inputChars[1] = chip_char_2;
        inputChars[2] = chip_char_3;
        inputChars[3] = chip_char_4;
        inputChars[4] = chip_char_5;
        inputChars[5] = chip_char_6;
        inputChars[6] = chip_char_7;
        inputChars[7] = chip_char_8;
        inputChars[8] = chip_char_9;
        inputChars[9] = chip_char_10;
        inputChars[10] = chip_char_11;
        inputChars[11] = chip_char_12;
        inputChars[12] = chip_char_13;
        inputChars[13] = chip_char_14;
        inputChars[14] = chip_char_15;


        int k = 0;
        for(EditText input: inputChars){
            input.setOnKeyListener(new BackSpaceListener(k++, "CHIP"));
        }

        dialog.show();
    }

    public class ChipInputHandler implements TextWatcher{

        //private boolean lastInput = false;
        private boolean enableButton = false;
        //private boolean firstInput = false;
        private int index = 0;
        public ChipInputHandler(int index, boolean enableButton){
            this.index = index;
            this.enableButton = enableButton;

        }

        public void backSpace(){
            System.out.println("BACKSPACE ON INDEX: "+index);
            if(index > 0){
                inputChars[index].clearFocus();
                inputChars[index].setEnabled(false);
                inputChars[index-1].setText("");
                inputChars[index-1].setEnabled(true);
                inputChars[index-1].requestFocus();
                inputChars[index-1].setCursorVisible(true);
            }
        }

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * are about to be replaced by new text with length <code>after</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param count
         * @param after
         */
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * have just replaced old text that had length <code>before</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param before
         * @param count
         */
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(count == 1){
                if(enableButton){
                    confirmChip.setEnabled(true);
                    inputMethodManager.hideSoftInputFromWindow(inputChars[14].getWindowToken(), 0);
                }
                /*
                if(lastInput) {
                    inputMethodManager.hideSoftInputFromWindow(inputChars[4].getWindowToken(), 0);
                }
                */
                else{
                    inputChars[index].clearFocus();
                    inputChars[index+1].setEnabled(true);
                    inputChars[index+1].requestFocus();
                    inputChars[index+1].setCursorVisible(true);
                }
            }
            /*
            else if(count == 0 && !firstInput && index > 0){
                inputChars[index].clearFocus();
                inputChars[index-1].setEnabled(true);
                inputChars[index-1].requestFocus();
                inputChars[index-1].setCursorVisible(true);
                if(index < 4)
                    confirmTatoo.setEnabled(false);
            }
            */

            else if(count == 0 && enableButton){
                confirmChip.setEnabled(false);
            }

        }

        /**
         * This method is called to notify you that, somewhere within
         * <code>s</code>, the text has been changed.
         * It is legitimate to make further changes to <code>s</code> from
         * this callback, but be careful not to get yourself into an infinite
         * loop, because any changes you make will cause this method to be
         * called again recursively.
         * (You are not told where the change took place because other
         * afterTextChanged() methods may already have made other changes
         * and invalidated the offsets.  But if you need to know here,
         * you can use {@link Spannable#setSpan} in {@link #onTextChanged}
         * to mark your place and then look up from here where the span
         * ended up.
         *
         * @param s
         */
        @Override
        public void afterTextChanged(Editable s) {

        }
    }



    public void setDate(){
        System.out.println("K: "+catownerSelectedDateView);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
    }


    public void setDateText(String text){
        switch (clickedView.getId()){
            case R.id.catbasic_select_arrived_date:
                ((TextView)((HomePage)context).findViewById(R.id.catbasic_arrived_date_text)).setText(text);
                break;
            case R.id.catbasic_select_birthday_date:
                ((TextView)((HomePage)context).findViewById(R.id.catbasic_birthday_date_text)).setText(text);
                break;
            case R.id.catbasic_select_moved_date:
                ((TextView)((HomePage)context).findViewById(R.id.catbasic_moved_date_text)).setText(text);
                break;
            case R.id.catbasic_select_tatoo_date_and_text:
                ((TextView)homePage.findViewById(R.id.catbasic_tatoo_date_text)).setText(text);
                break;
            case R.id.catbasic_select_chip_date:
                ((TextView)homePage.findViewById(R.id.catbasic_chip_date_text)).setText(text);
                break;
            case R.id.catbasic_select_neuter_date:
                ((TextView)homePage.findViewById(R.id.catbasic_neuter_date_text)).setText(text);
                break;
            default:break;
        }
    }

    public void saveCatownerCatAction(){
        System.out.println(catownerCatActionDialog.isShowing());
        catownerCatActionDialog.dismiss();
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        String dateString = year+"-"+monthText+"-"+dayText;
        //System.out.println(clickedView);
        //System.out.println(catownerSelectedDateView);
        if(homePage.getPrivileged() == Type.CATOWNER){
            if(catownerSelectedDateView == null)
                setDateText(dateString);
            else
                ((TextView)catownerSelectedDateView).setText(dateString);
        }else{
            setDateText(dateString);
        }

        /*
        int drawableId = -1;
        switch (clickedView.getId()){
            case R.id.catbasic_select_arrived_date:
                drawableId = R.drawable.button_style_nr_2;
                ((TextView)((HomePage)context).findViewById(R.id.catbasic_arrived_date_text)).setText(dateString);
                break;
            case R.id.catbasic_select_birthday_date:
                birthdayInt++;
                if(birthdayInt == 1) drawableId = R.drawable.button_style_nr_4;
                else if(birthdayInt == 2) drawableId = R.drawable.button_style_nr_2;
                ((TextView)((HomePage)context).findViewById(R.id.catbasic_birthday_date_text)).setText(dateString);
                break;
            case R.id.catbasic_select_moved_date:
                drawableId = R.drawable.button_style_nr_2;
                ((TextView)((HomePage)context).findViewById(R.id.catbasic_moved_date_text)).setText(dateString);
                break;
            case R.id.catbasic_select_tatoo_date_and_text:
                tatooInt++;
                if(tatooInt == 1) drawableId = R.drawable.button_style_nr_4;
                else if(tatooInt == 2) drawableId = R.drawable.button_style_nr_2;
                ((TextView)homePage.findViewById(R.id.catbasic_tatoo_date_text)).setText(dateString);
                break;
            case R.id.catbasic_select_chip_date:
                chipInt++;
                if(chipInt == 1) drawableId = R.drawable.button_style_nr_4;
                else if(chipInt == 2) drawableId = R.drawable.button_style_nr_2;
                ((TextView)homePage.findViewById(R.id.catbasic_chip_date_text)).setText(dateString);
                break;
            case R.id.catbasic_select_neuter_date:
                drawableId = R.drawable.button_style_nr_2;
                ((TextView)homePage.findViewById(R.id.catbasic_neuter_date_text)).setText(dateString);
                break;
            default:break;
        }
        */
        /*
        if(drawableId != -1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                clickedView.setBackground(homePage.getResources().getDrawable(drawableId, null));
            } else {
                clickedView.setBackground(homePage.getResources().getDrawable(drawableId));
            }
        }
        */

        /*
        if(clickedView instanceof LinearLayout){
            switch (clickedView.getId()){

                case R.id.catbasic_present_date:
                    ((TextView)clickedView.findViewById(R.id.catbasic_present_date_text)).setText(dateString);
                    break;
                case R.id.catbasic_birthday:
                    ((TextView)clickedView.findViewById(R.id.catbasic_birthday_text)).setText(dateString);
                    break;

                default:break;
            }

        }
        else if(clickedView instanceof TextView){
            if(((TextView)clickedView).getText().toString().equals("Chip")){
                ((HomePage)context).findViewById(R.id.scanCode).setEnabled(true);
                ((TextView)((HomePage)context).findViewById(R.id.scanCode)).setTextColor(Color.parseColor("#000000"));
            }
            ((TextView)clickedView).setText(((TextView)clickedView).getText().toString()+" "+dateString);

        }
        */

    }


}
