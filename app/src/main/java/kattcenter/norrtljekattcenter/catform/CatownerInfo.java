package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.catowner.PickCatOwnerFromListAdapter;

/**
 * Created by Joakim on 2017-08-05.
 */

public class CatownerInfo implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener{

    private Context context;
    private View clickedView;
    private int year, month, day, hour, minute;
    private Calendar c = Calendar.getInstance();
    private Dialog dialog;
    private ListView listView;
    private PickCatOwnerFromListAdapter pickCatOwnerFromListAdapter;

    public CatownerInfo(Context context, View clickedView){
        this.context = context;
        this.clickedView = clickedView;



        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    public void updateCatowners(ArrayList<String[]> catowners){
        catowners.add(new String[]{"Kattcenter", "", ""});
        pickCatOwnerFromListAdapter.notifyDataSetInvalidated();
        pickCatOwnerFromListAdapter = new PickCatOwnerFromListAdapter(context, catowners);
        listView.setAdapter(pickCatOwnerFromListAdapter);
        pickCatOwnerFromListAdapter.notifyDataSetChanged();
        Log.i("CATOWNERINFO", "LIST UPDATED");
    }

    public void showCatownerDialog(){
        ((HomePage)context).setLoadConnectCatownerList(true);
        dialog = new Dialog(context);
        RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.pick_catowner_list, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        pickCatOwnerFromListAdapter = new PickCatOwnerFromListAdapter(context, new ArrayList<String[]>());
        listView.setAdapter(pickCatOwnerFromListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                addCatownerToListEntry(view);
                clickedView.setEnabled(false);
                dialog.dismiss();
            }
        });
        HashMap<String, String> data = new HashMap<>();
        data.put("blub", Base64.encodeToString("CATOWNER".getBytes(), Base64.NO_WRAP));
        ((HomePage)context).prepareAndSendData(data, Type.DOWNLOAD_LIST);

        dialog.setContentView(relativeLayout);
        dialog.show();
    }

    private void addCatownerToListEntry(View view){
        final LinearLayout chosenCatownerListEntry = (LinearLayout) ((HomePage)context).findViewById(R.id.catform_new_catowner_list);
        RelativeLayout relativeLayout = (RelativeLayout) view;
        final RelativeLayout entry = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.catform_catowner_list_entry, null);
        ((TextView)entry.findViewById(R.id.chosenCatOwnerName)).setText(((TextView)relativeLayout.findViewById(R.id.short_catowner_name_sec)).getText().toString());
        ((TextView)entry.findViewById(R.id.chosenCatownerEmail)).setText(((TextView)relativeLayout.findViewById(R.id.short_catowner_email_sec)).getText().toString());
        entry.findViewById(R.id.catform_catowner_remove_entry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosenCatownerListEntry.removeView(entry);
            }
        });
        chosenCatownerListEntry.addView(entry);
    }

    public void setDate(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        System.out.println(year + " "+month+" "+dayOfMonth);
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        switch (clickedView.getId()){
            case R.id.catform_catowner_date:
                ((TextView)clickedView.findViewById(R.id.catform_catowner_date_text)).setText(year+"-"+monthText+"-"+dayText);
                break;

            default:break;
        }

    }

    public void setTime(){
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, this, hour, minute, true);
        timePickerDialog.show();
    }

    /**
     * Called when the user is done setting a new time and the dialog has
     * closed.
     *
     * @param view      the view associated with this listener
     * @param hourOfDay the hour that was set
     * @param minute    the minute that was set
     */
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        System.out.println("TIME SET: "+hourOfDay+" "+minute);
        String hourText = (hourOfDay < 10) ? "0"+hourOfDay : ""+hourOfDay;
        String minuteText = (minute < 10) ? "0"+minute : ""+minute;
        ((TextView)clickedView.findViewById(R.id.catform_catowner_time_text)).setText(hourText+":"+minuteText);
    }
}
