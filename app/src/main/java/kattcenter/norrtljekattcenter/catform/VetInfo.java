package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-05.
 */

public class VetInfo implements DatePickerDialog.OnDateSetListener{

    private Context context;
    private View clickedView;
    private Calendar c = Calendar.getInstance();
    private int year, month, day;
    private Dialog dialog;

    public VetInfo(Context context, View clickedView){
        this.context = context;
        this.clickedView = clickedView;
        dialog = new Dialog(context);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    public void clearClearButtons(){

    }

    public void clearButton(){
        final LinearLayout linearLayout = (LinearLayout)clickedView.getParent();
        final TextView clear = (TextView) linearLayout.getChildAt(1);
        clear.setVisibility(View.VISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear.setVisibility(View.GONE);
                ((TextView)linearLayout.getChildAt(0)).setText("");
            }
        });

    }


    public void handleCheckBox(){
        CheckBox checkBox = (CheckBox) clickedView;
        String checkBoxText = checkBox.getText().toString();
        if(checkBoxText.split(" ").length == 3){
            checkBox.setText(checkBoxText.split(" ")[0]+" "+checkBoxText.split(" ")[1]);
            checkBox.setChecked(false);
        }
        else{
            setDate();
        }
    }


    public void setDate(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
    }

    public void setComment(){
        final InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        final RelativeLayout comment_form = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.comment_form, null);
        final EditText editText = (EditText) comment_form.findViewById(R.id.commentField);
        (comment_form.findViewById(R.id.addCommentButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(text != null && !text.equals("")) {
                    switch (clickedView.getId()){
                        case R.id.catvet_comment_text:
                            ((TextView) clickedView.findViewById(R.id.catvet_comment_text)).setText(text);
                            break;
                        case R.id.catvet_known_events_text:
                            ((TextView) clickedView.findViewById(R.id.catvet_known_events_text)).setText(text);
                            break;
                        default:break;
                    }
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.INVISIBLE);
                    dialog.dismiss();
                    clearButton();
                }
                else{
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.VISIBLE);
                }
            }
        });

        (comment_form.findViewById(R.id.abortButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                dialog.dismiss();
            }
        });
        dialog.setTitle("Lägg till kommentar");
        dialog.setContentView(comment_form);
        dialog.show();
    }

    public void fileChooser(){
        Intent filePicker = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        filePicker.setType("*/*");
        String[] mimeTypes = {"text/*", "application/pdf"};
        //String[] mimeTypes = {"file/*"};
        filePicker.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        filePicker.addCategory(Intent.CATEGORY_OPENABLE);
        ((HomePage)context).startActivityForResult(filePicker, 22);
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        String dateString = year+"-"+monthText+"-"+dayText;
        switch (clickedView.getId()){
            case R.id.catvet_bloodsample_date_text:
                ((TextView)clickedView.findViewById(R.id.catvet_bloodsample_date_text)).setText(dateString);
                clearButton();
                break;
            case R.id.catvet_no_known:
                ((TextView)clickedView).setText(((TextView)clickedView).getText().toString()+" ("+dateString+")");
                break;
            default:break;
        }
        /*
        if(clickedView instanceof LinearLayout){
            ((TextView)clickedView.findViewById(R.id.catvet_bloodsample_date_text)).setText(dateString);
            clearButton();
        }
        else if(clickedView instanceof TextView){
            ((TextView)clickedView).setText(((TextView)clickedView).getText().toString()+" ("+dateString+")");

        }
        */

    }
}
