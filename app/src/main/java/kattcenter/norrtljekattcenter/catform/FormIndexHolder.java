package kattcenter.norrtljekattcenter.catform;

/**
 * Created by Joakim on 2017-11-09.
 */

public class FormIndexHolder {

    //FORMS
    public static final int GENERAL_FORM_INDEX = 0;
    public static final int TEMPERAMENT_FORM_INDEX = 1;
    public static final int MEDICAL_FORM_INDEX = 2;
    public static final int VACCIN_FORM_INDEX = 3;
    public static final int PARASITE_FORM_INDEX = 4;
    public static final int CLAW_FORM_INDEX = 5;
    public static final int WEIGHT_FORM_INDEX = 6;
    public static final int VET_DOC_FORM_INDEX = 7;
    public static final int REG_SKK_FORM_INDEX = 8;
    public static final int VET_FORM_INFO = 9;
    public static final int INSURANCE_FORM_INDEX = 10;
    public static final int BACKGROUND_FORM_INDEX = 11;
    public static final int STATUS_FORM_INDEX = 12;
    public static final int CONNECTION_FORM_INDEX = 13;

}
