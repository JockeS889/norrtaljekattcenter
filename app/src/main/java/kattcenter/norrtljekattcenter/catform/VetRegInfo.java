package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-05.
 */

public class VetRegInfo implements DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener{

    private Context context;
    private Calendar c = Calendar.getInstance();
    private int year, month, day;
    private View clickedView;
    private RelativeLayout holder;
    private Dialog dialog;
    private ListView listView;
    private MyDocListAdapter myDocListAdapter;


    public VetRegInfo(Context context){
        this.context = context;
        dialog = new Dialog(context);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        initDocList();
    }

    private void initDocList(){
        listView = (ListView)((HomePage)context).findViewById(R.id.catvetreg_documentList);
        myDocListAdapter = new MyDocListAdapter(context);
        myDocListAdapter.setList(new ArrayList<String[]>());
        listView.setAdapter(myDocListAdapter);
    }

    private void clearButton(){
        final LinearLayout linearLayout = (LinearLayout)clickedView.getParent();
        final TextView clear = (TextView) linearLayout.getChildAt(1);
        clear.setVisibility(View.VISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear.setVisibility(View.GONE);
                ((TextView)linearLayout.getChildAt(0)).setText("");
                if(clickedView.getId() == R.id.catvetreg_note_text){
                    String parentOptionText = ((HomePage)context).getCurrentCatvetregParent();
                    ((HomePage) context).updateCatvetregComment(parentOptionText, "");
                }
            }
        });

    }
    private void clearButton(View parent){
        final LinearLayout linearLayout = (LinearLayout)parent;
        final TextView clear = (TextView) linearLayout.getChildAt(1);
        clear.setVisibility(View.VISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear.setVisibility(View.GONE);
                ((TextView)linearLayout.getChildAt(0)).setText("");
                String parentOptionText = ((HomePage)context).getCurrentCatvetregParent();
                ((HomePage) context).updateCatvetregComment(parentOptionText, "");
            }
        });

    }


    public void setDate(View clickedView){
        this.clickedView = clickedView;

        switch (clickedView.getId()){
            case R.id.catvet_reg_date_text:
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
                datePickerDialog.show();
                break;
            default:break;
        }

    }

    public void setParent(View holder){
        this.holder = (RelativeLayout)holder;
        System.out.println("HOLDER: "+holder);
    }

    public void setComment(final View clickedView){
        final InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        this.clickedView = clickedView;
        dialog.setTitle("Kommentar");
        final String parentOptionText = ((HomePage)context).getCurrentCatvetregParent();
        final RelativeLayout comment_form = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.comment_form, null);
        Button abortButton = (Button) comment_form.findViewById(R.id.abortButton);
        Button addCommentButton = (Button) comment_form.findViewById(R.id.addCommentButton);
        final EditText commentField = (EditText) comment_form.findViewById(R.id.commentField);
        commentField.setText(((TextView)clickedView).getText().toString());
        abortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                inputMethodManager.hideSoftInputFromWindow(commentField.getWindowToken(), 0);
            }
        });
        addCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = commentField.getText().toString();
                ((TextView)clickedView.findViewById(R.id.catvetreg_note_text)).setText(comment);
                ((HomePage) context).updateCatvetregComment(parentOptionText, comment);
                inputMethodManager.hideSoftInputFromWindow(commentField.getWindowToken(), 0);
                dialog.dismiss();
                clearButton();
            }
        });
        dialog.setContentView(comment_form);
        dialog.show();
    }

    public void addDocumentToList(String[] documentInfo){
        ArrayList<String[]> list = myDocListAdapter.getList();

        list.add(documentInfo);
        Log.i("VETREGINFO", "LIST SIZE: "+list.size());
        Log.i("VETREGINFO", "ADDED: "+documentInfo[0]);
        myDocListAdapter.setList(list);
        myDocListAdapter.notifyDataSetChanged();
    }

    public void removeDocumentFromList(int position){
        ArrayList<String[]> list = myDocListAdapter.getList();
        list.remove(position);
        myDocListAdapter.setList(list);
        myDocListAdapter.notifyDataSetChanged();
    }

    public void documentChooser(){
        Intent filePicker = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        filePicker.setType("*/*");
        String[] mimeTypes = {"text/*", "application/pdf"};
        //String[] mimeTypes = {"file/*"};
        filePicker.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        filePicker.addCategory(Intent.CATEGORY_OPENABLE);
        ((HomePage)context).startActivityForResult(filePicker, 23);
    }




    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        ((TextView)clickedView.findViewById(R.id.catvet_reg_date_text)).setText(year+"-"+monthText+"-"+dayText);
        clearButton();
        //(clickedView.findViewById(R.id.editCatVetRegDate)).setVisibility(View.GONE);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
        final String parentText = (String)parent.getItemAtPosition(position);
        //RelativeLayout holder = (RelativeLayout) parent.findViewById(R.id.catvetreg_container);
        //System.out.println("TOTAL CHILDS: "+holder.getChildCount());
        RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.vetreg_boxes_and_note, null);
        LinearLayout linearLayout = (LinearLayout) relativeLayout.findViewById(R.id.checkbox_container);
        String[] array = null;

        switch (position){
            case 0:
                array = context.getResources().getStringArray(R.array.generalstate);
                break;
            case 1:
                array = context.getResources().getStringArray(R.array.Lynne);
                break;
            case 2:
                array = context.getResources().getStringArray(R.array.Skin_Paws);
                break;
            case 3:
                array = context.getResources().getStringArray(R.array.Lymf);
                break;
            case 4:
                array = context.getResources().getStringArray(R.array.Eyes);
                break;
            case 5:
                array = context.getResources().getStringArray(R.array.Ears);
                break;
            case 6:
                array = context.getResources().getStringArray(R.array.Mouth_teeth);
                break;
            case 7:
                array = context.getResources().getStringArray(R.array.abdomen);
                break;
            case 8:
                array = context.getResources().getStringArray(R.array.circulation_organ);
                break;
            case 9:
                array = context.getResources().getStringArray(R.array.respiration_organ);
                break;
            case 10:
                array = context.getResources().getStringArray(R.array.Outer_genitalias);
                break;
            case 11:
                array = context.getResources().getStringArray(R.array.locomotive_organs);
                break;
            default:break;

        }

        //boolean formEnabled = ((HomePage) context).isFormEnabled("catform_vetdoc");
        //System.out.println("FORM ENABLED: "+formEnabled);
        LinearLayout commentParent = (LinearLayout) relativeLayout.findViewById(R.id.catvetreg_note_text_holder);
        TextView commentText = (TextView) relativeLayout.findViewById(R.id.catvetreg_note_text);

        HashMap<String, String> comments = ((HomePage) context).getCatvetreg_comments();
        String previousComment = comments.get(parentText);
        String newComment = (previousComment.equals("")) ? "" : previousComment;
        commentText.setText(newComment);
        if(!newComment.isEmpty()) clearButton(commentParent);

        //commentParent.setClickable(formEnabled);

        HashMap<String, HashMap<String, Boolean>> spinnerRecord = ((HomePage) context).getRecord();
        HashMap<String, Boolean> currentOptions = spinnerRecord.get(parentText);
        System.out.println("LENGTH: "+currentOptions.size());

        if(array != null) {
            for (String string : array) {
                RelativeLayout relativeLayout1 = (RelativeLayout) ((HomePage) context).getLayoutInflater().inflate(R.layout.checkbox_item, null);
                CheckBox checkBox = (CheckBox) relativeLayout1.findViewById(R.id.checkbox_list_item);
                checkBox.setText(string);
                System.out.println(string+": "+currentOptions.get(string));
                if(currentOptions.get(string)) checkBox.setChecked(true);
                else checkBox.setChecked(false);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        System.out.println("CHECKED: "+isChecked+"  TEXT: "+buttonView.getText().toString());
                        ((HomePage) context).updateRecord(parentText, buttonView.getText().toString(), isChecked);
                    }
                });
                //checkBox.setEnabled(formEnabled);
                linearLayout.addView(relativeLayout1);
            }
            holder.removeAllViews();
            holder.addView(relativeLayout);
        }

        ((HomePage) context).updateCurrentCatvetregParent(parentText);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void confirmRemoveDocFromList(final int position, final String[] document){
        final Dialog dialog = new Dialog(context);
        LinearLayout linearLayout = (LinearLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.text_button_dialog, null);
        ((TextView)linearLayout.findViewById(R.id.general_dialog_text)).setText("Ta bort dokumentet:\n"+document[0]+"\n\nfrån listan?");
        Button abort = (Button)linearLayout.findViewById(R.id.abort);
        final Button confirm = (Button)linearLayout.findViewById(R.id.confirm);
        abort.setText("Avbryt");
        confirm.setText("Ok");
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeDocumentFromList(position);
                boolean removed = ((HomePage)context).getFileManager().deleteFromDocDir(((HomePage)context).getFileManager().getVetDocDir(), Integer.parseInt(document[1]));
                dialog.dismiss();
                Dialog removedDialog = new Dialog(context);
                TextView textView = new TextView(context);
                textView.setText((removed ? "Lyckad" : "Misslyckad") + " borttagning");
                textView.setTextColor(Color.parseColor("#ffffff"));
                removedDialog.setContentView(textView);
                removedDialog.show();
            }
        });
        dialog.setContentView(linearLayout);
        dialog.show();
    }


    public class MyDocListAdapter extends BaseAdapter{

        /**
         * String: {NAME, TAG}
         */
        private ArrayList<String[]> list;

        public MyDocListAdapter(Context context){

        }

        public ArrayList<String[]> getList(){
            return list;
        }

        public void setList(ArrayList<String[]> list){
            this.list = list;
        }

        /**
         * How many items are in the data set represented by this Adapter.
         *
         * @return Count of items.
         */
        @Override
        public int getCount() {
            return list.size();
        }

        /**
         * Get the data item associated with the specified position in the data set.
         *
         * @param position Position of the item whose data we want within the adapter's
         *                 data set.
         * @return The data at the specified position.
         */
        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        /**
         * Get the row id associated with the specified position in the list.
         *
         * @param position The position of the item within the adapter's data set whose row id we want.
         * @return The id of the item at the specified position.
         */
        @Override
        public long getItemId(int position) {
            return position;
        }

        /**
         * Get a View that displays the data at the specified position in the data set. You can either
         * create a View manually or inflate it from an XML layout file. When the View is inflated, the
         * parent View (GridView, ListView...) will apply default layout parameters unless you use
         * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
         * to specify a root view and to prevent attachment to the root.
         *
         * @param position    The position of the item within the adapter's data set of the item whose view
         *                    we want.
         * @param convertView The old view to reuse, if possible. Note: You should check that this view
         *                    is non-null and of an appropriate type before using. If it is not possible to convert
         *                    this view to display the correct data, this method can create a new view.
         *                    Heterogeneous lists can specify their number of view types, so that this View is
         *                    always of the right type (see {@link #getViewTypeCount()} and
         *                    {@link #getItemViewType(int)}).
         * @param parent      The parent that this view will eventually be attached to
         * @return A View corresponding to the data at the specified position.
         */
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = ((HomePage)context).getLayoutInflater().inflate(R.layout.my_blue_textview, null);
            }
            ((TextView)convertView).setText(list.get(position)[0]);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmRemoveDocFromList(position, list.get(position));
                }
            });
            return convertView;
        }
    }
}
