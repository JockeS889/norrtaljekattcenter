package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DownloadProduct;
import kattcenter.norrtljekattcenter.network.UploadProduct;

/**
 * Created by Jocke on 2017-06-15.
 */

class DewormingInfo implements DatePickerDialog.OnDateSetListener{

    private Context context;
    private View clickedView;
    private ArrayList<String> types;
    private ListView listView;
    private ListAdapter listAdapter;
    private Dialog dialog;
    private Calendar c = Calendar.getInstance();
    private int year, month, day;

    public DewormingInfo(Context context, View clickedView){
        this.context = context;
        this.clickedView = clickedView;
        dialog = new Dialog(context);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    public void setDate(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
    }

    public void updateTypes(ArrayList<String> types){
        listAdapter.notifyDataSetInvalidated();
        listAdapter = new ListAdapter(context, types);
        listView.setAdapter(listAdapter);
    }

    /*
    public void setType(ArrayList<String> types){
        this.types = types;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        listAdapter = new ListAdapter(context, types);
        listView.setAdapter(listAdapter);
        Button addNewCompany = (Button) relativeLayout.findViewById(R.id.addListEntry);
        final EditText newCompany = (EditText) relativeLayout.findViewById(R.id.newListEntry);
        addNewCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String company = newCompany.getText().toString();
                if(company != null && !company.equals("")){
                    ((TextView)clickedView.findViewById(R.id.catdeworming_type_text)).setText(company);
                    try {
                        new UploadProduct(context, "deworming_types", Type.UPLOAD.toString(), company).upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
                else{
                    (relativeLayout.findViewById(R.id.errorAddingEntryText)).setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.setTitle("Välj typ");
        dialog.setContentView(relativeLayout);
        dialog.show();
        try {
            new DownloadProduct(context, "deworming_types", Type.DOWNLOAD.toString()).download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    */

    public void setComment(){
        final InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        final RelativeLayout comment_form = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.comment_form, null);
        final EditText editText = (EditText) comment_form.findViewById(R.id.commentField);
        (comment_form.findViewById(R.id.addCommentButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(text != null && !text.equals("")) {
                    ((TextView) clickedView.findViewById(R.id.catdeworming_comment_text)).setText(text);
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.INVISIBLE);
                    inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    dialog.dismiss();
                }
                else{
                    (comment_form.findViewById(R.id.errorCommentMessage)).setVisibility(View.VISIBLE);
                }
            }
        });

        (comment_form.findViewById(R.id.abortButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                dialog.dismiss();
            }
        });
        dialog.setTitle("Lägg till kommentar");
        dialog.setContentView(comment_form);
        dialog.show();
    }


    private class ListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> list;
        public ListAdapter(@NonNull Context context, ArrayList<String> list) {
            super(context, 0, list);
            this.list = list;
        }

        public void setList(ArrayList<String> list){
            this.list = list;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            final String type = list.get(position);
            if(view == null){
                view = ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_1, null);
            }
            TextView entryName = (TextView) view.findViewById(R.id.listEntry);
            entryName.setText(type);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView dewormingTextView = ((TextView)clickedView.findViewById(R.id.catdeworming_type_text));
                    dewormingTextView.setText(type);
                    dialog.dismiss();
                }
            });
            return view;
        }
    }




    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        String dateString = year+"-"+monthText+"-"+dayText;
        switch (clickedView.getId()){
            case R.id.catdeworming_start_date:
                ((TextView)clickedView.findViewById(R.id.catdeworming_start_date_text)).setText(dateString);
                break;
            case R.id.catdeworming_repeat_date:
                ((TextView)clickedView.findViewById(R.id.catdeworming_repeat_date_text)).setText(dateString);
                break;
            default:break;
        }
    }
}
