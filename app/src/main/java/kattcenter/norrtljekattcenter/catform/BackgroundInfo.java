package kattcenter.norrtljekattcenter.catform;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;

/**
 * Created by Jocke on 2017-06-05.
 */

public class BackgroundInfo implements DatePickerDialog.OnDateSetListener{

    private Context context;
    private Dialog dialog;
    private View clickedView;
    private Calendar calendar;
    private int year, month, day;
    private InputMethodManager inputMethodManager;

    public BackgroundInfo(Context context, View clickedView) {
        this.context = context;
        this.clickedView = clickedView;
        dialog = new Dialog(context);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        inputMethodManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);

    }
    public void setComment(final View clickedView){
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        dialog.setTitle("Kommentar");
        final RelativeLayout comment_form = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.comment_form, null);
        Button abortButton = (Button) comment_form.findViewById(R.id.abortButton);
        Button addCommentButton = (Button) comment_form.findViewById(R.id.addCommentButton);
        final EditText commentField = (EditText) comment_form.findViewById(R.id.commentField);
        abortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        addCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = commentField.getText().toString();
                ((TextView)clickedView.findViewById(R.id.catbackground_comment_text)).setText(comment);
                inputMethodManager.hideSoftInputFromWindow(commentField.getWindowToken(), 0);
                dialog.dismiss();
            }
        });
        dialog.setContentView(comment_form);
        dialog.show();
    }

    private void setDate(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
        datePickerDialog.show();
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if(clickedView instanceof CheckBox){
                    ((CheckBox)clickedView).setChecked(false);
                }
            }
        });
    }

    public void handleCheckbox(){
        switch (clickedView.getId()){
            case R.id.catstatus_external_cathome:
                CheckBox checkBox = (CheckBox) clickedView;
                (((HomePage)context).findViewById(R.id.catstatus_external_cathome_text)).setVisibility((checkBox.isChecked()) ? View.VISIBLE : View.GONE);
                break;
            default:
                CheckBox checkBox1 = (CheckBox) clickedView;
                String checkBoxText = checkBox1.getText().toString();
                String[] texts = checkBoxText.split(" ");
                if(texts.length == 3 && (texts[0].equals("Via") || texts[0].equals("Född"))){
                    checkBox1.setText(texts[0]+" "+texts[1]);
                    checkBox1.setChecked(false);
                }
                else if(texts.length == 2 && !(texts[0].equals("Via") || texts[0].equals("Född"))){
                    checkBox1.setText(texts[0]);
                    checkBox1.setChecked(false);
                }
                else{
                    setDate();
                }
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        System.out.println(year + " "+month+" "+dayOfMonth);
        String monthText = (month < 10) ? "0"+Integer.toString(month) : Integer.toString(month);
        String dayText = (dayOfMonth < 10) ? "0"+Integer.toString(dayOfMonth) : Integer.toString(dayOfMonth);
        String dateText = "("+year+"-"+monthText+"-"+dayText+")";
        String prevText = ((CheckBox)clickedView).getText().toString();
        ((CheckBox)clickedView).setText(prevText+" "+dateText);
    }
}
