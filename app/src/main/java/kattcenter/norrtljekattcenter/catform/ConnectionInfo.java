package kattcenter.norrtljekattcenter.catform;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.catlist.CatListDialog;


/**
 * Created by Joakim on 2017-08-05.
 */

public class ConnectionInfo {

    private Context context;
    private ArrayList<String> cats;
    //private MyListAdapter myListAdapter;
    private ListView listView;
    //private Dialog dialog;
    private CatListDialog catListDialog;

    public ConnectionInfo(Context context){
        this.context = context;
    }

    public void updateCatsList(ArrayList<String> cats){
        this.cats = cats;
        //myListAdapter = new MyListAdapter(context, cats);
        //listView.setAdapter(myListAdapter);
    }

    public void showCatListDialog(View clickedView){
        ((HomePage)context).setManage_catformConnection_catlist(true);
        HashMap<String, String> data = new HashMap<>();
        data.put("misc_action", "names_id_imgsrc_catowner");
        ((HomePage)context).prepareAndSendData(data, Type.DOWNLOAD_MISC);
        catListDialog = new CatListDialog(context, true);
        ((HomePage)context).setCatListDialog(catListDialog);
        System.out.println("CAT LIST DIALOG SHOWING");
        catListDialog.viewPickerDialog(clickedView);
    }

    /*
    public void showCatListDialog(ArrayList<String> cats){
        this.cats = cats;
        final RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.type_listview_add, null);
        listView = (ListView) relativeLayout.findViewById(R.id.typeList);
        myListAdapter = new MyListAdapter(context, cats);
        listView.setAdapter(myListAdapter);
        relativeLayout.findViewById(R.id.addListEntry).setVisibility(View.GONE);
        relativeLayout.findViewById(R.id.newListEntry).setVisibility(View.GONE);
        dialog.setTitle("Välj katt");
        dialog.setContentView(relativeLayout);
        dialog.show();
        HashMap<String, String> data = new HashMap<>();
        data.put("misc_action", "names_id_catowner");
        ((HomePage)context).prepareAndSendData(data, Type.DOWNLOAD_MISC);
    }
    */

    /*
    public class MyListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> list;
        public MyListAdapter(@NonNull Context context, ArrayList<String> list) {
            super(context, 0, list);
            this.list = list;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            final String type = list.get(position);
            if(view == null){
                view = ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_2, null);
            }
            TextView entryName = (TextView) view.findViewById(R.id.listEntry);
            entryName.setText(type);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (clickedView.getId()){
                        case R.id.catmed_type:
                            //REMAKE THIS - MULTIPLE MEDICATIONS SHOULD BE SELECTABLE!!!!!!!!!!
                            ((TextView)clickedView.findViewById(R.id.catmed_type_text)).setText(type);
                            break;
                        case R.id.catmed_treatment_type:
                            ((TextView)clickedView.findViewById(R.id.catmed_treatment_type_text)).setText(type);
                        default:break;
                    }
                    dialog.dismiss();
                }
            });
            return view;
        }
    }
    */
}
