package kattcenter.norrtljekattcenter.catlist;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.catform.BundleResourceMapping;
import kattcenter.norrtljekattcenter.dialog.BottomDialog;
import kattcenter.norrtljekattcenter.network.DataPacket;
import kattcenter.norrtljekattcenter.network.Headers;

/**
 * Created by Joakim on 2017-06-25.
 */

public class CatListDialog {

    private Context context;
    private PickCatFromListAdapter pickCatFromListAdapter;
    private ChosenCatFromListAdapter chosenCatFromListAdapter, confirmChosenCatFromListAdapter;
    private ListView chosenCatList, confirmChosenCatList;
    private ArrayList<String[]> chosenCats;
    private boolean pickParent = false;
    private View clickedView;
    /*
    public CatListDialog(Context context, ListView chosenCatList){
        System.out.println("SHOWING CATLIST DIALOG");
        this.context = context;
        this.chosenCatList = chosenCatList;
        //chosenCatFromListAdapter = new ChosenCatFromListAdapter(context, new ArrayList<String[]>());
        //this.chosenCatFromListAdapter = chosenCatFromListAdapter;
        this.chosenCatList.setAdapter(chosenCatFromListAdapter);
        ArrayList<String[]> testList = new ArrayList<>();
        //testList.add(new String[]{"17-001", "Nils"});
        //testList.add(new String[]{"17-002", "Misse"});
        pickCatFromListAdapter = new PickCatFromListAdapter(context, testList);
        chosenCats = new ArrayList<>();
    }
    */

    public CatListDialog(Context context, boolean pickParent){
        this.context = context;
        this.pickParent = pickParent;
        ArrayList<String[]> testList = new ArrayList<>();
        pickCatFromListAdapter = new PickCatFromListAdapter(context, testList);
    }

    public CatListDialog(Context context){
        this.context = context;
        ArrayList<String[]> testList = new ArrayList<>();
        pickCatFromListAdapter = new PickCatFromListAdapter(context, testList);
    }

    public CatListDialog(Context context, ListView chosenCatList, ListView confirmChosenCatList, ChosenCatFromListAdapter chosenCatFromListAdapter, ChosenCatFromListAdapter confirmChosenCatFromListAdapter){
        //System.out.println("SHOWING CATLIST DIALOG");
        this.context = context;
        this.chosenCatList = chosenCatList;
        this.confirmChosenCatList = confirmChosenCatList;
        //chosenCatFromListAdapter = new ChosenCatFromListAdapter(context, new ArrayList<String[]>());
        this.chosenCatFromListAdapter = chosenCatFromListAdapter;
        this.confirmChosenCatFromListAdapter = confirmChosenCatFromListAdapter;
        this.chosenCatList.setAdapter(chosenCatFromListAdapter);
        this.confirmChosenCatList.setAdapter(confirmChosenCatFromListAdapter);
        ArrayList<String[]> testList = new ArrayList<>();
        //testList.add(new String[]{"17-001", "Nils"});
        //testList.add(new String[]{"17-002", "Misse"});
        pickCatFromListAdapter = new PickCatFromListAdapter(context, testList);
    }

    public void removeChoosenCat(int position){
        if(position >= 0) {
            ArrayList<String[]> arrayList = chosenCatFromListAdapter.getCatInfoList();
            arrayList.remove(position);
            chosenCatFromListAdapter.setCatInfoList(arrayList);
            chosenCatFromListAdapter.notifyDataSetChanged();
            ArrayList<String[]> arrayList1 = confirmChosenCatFromListAdapter.getCatInfoList();
            arrayList1.remove(position);
            confirmChosenCatFromListAdapter.setCatInfoList(arrayList1);
            confirmChosenCatFromListAdapter.notifyDataSetChanged();
        }
        //RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, arrayList.size()*52);
        //layoutParams.addRule(RelativeLayout.BELOW, R.id.catlist_title);
        //chosenCatList.setLayoutParams(layoutParams);
    }

    public void addCatsToPicker(String json){
        ArrayList<String[]> arrayList = new ArrayList<>();
        try{
            String[] jsonRows = json.split("\n");
            if(jsonRows.length == 0) return;
            //ArrayList<String[]> chosenCats = null;
            //if(!pickParent)
            //    chosenCats = chosenCatFromListAdapter.getCatInfoList();
            //ArrayList<String[]> confirmChosenCats = confirmChosenCatFromListAdapter.getCatInfoList();
            String currentCatId = ((HomePage)context).getCurrentCatId();
            for(String jsonRow: jsonRows){
                Log.i("CATLISTDIALOG", "JSONROW: "+jsonRow);
                JSONObject jsonObject = new JSONObject(jsonRow);
                String catid = jsonObject.getString("catcenter_id");
                if(catid.equals(currentCatId)) continue;
                String catname = jsonObject.getString("catcenter_name");
                String owner = jsonObject.getString("owner");
                String imgsrc = jsonObject.getString("photo_src");
                /*
                boolean chooseAble = false;
                if(pickParent){
                    chooseAble = true;
                }
                else{
                    switch (owner){
                        case "kattcenter":
                        case "":
                            chooseAble = true;
                            break;
                        default:break;
                    }
                    boolean alreadyExists = false;
                    for (String[] chosenCat : chosenCats) {
                        //Log.i("CATLISTDIALOG", "EXISTING CATID: "+catid+" LOADING LIST CATID: "+chosenCat[1]);
                        if (catid.equals(chosenCat[1])) {
                            alreadyExists = true;
                            break;
                        }
                    }
                    if (alreadyExists) continue;
                }
                if(!chooseAble) continue;
                */
                arrayList.add(new String[]{catid, catname, imgsrc});
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
        pickCatFromListAdapter.setCatList(arrayList);
        pickCatFromListAdapter.notifyDataSetChanged();
    }

    public ArrayList<String[]> getChosenCats(){
        return chosenCatFromListAdapter.getCatInfoList();
    }

    private void loadImage(final ImageView imageView, String url){
        Glide.with(context)
                .load(Headers.getUrlWithHeaders(context, url))
                .apply(RequestOptions.circleCropTransform())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.error_loading_image));
                        new BottomDialog(context).popup("Ett fel uppstod vid nerladdning av bild");
                        return false;
                    }


                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);

    }

    public void downloadList(){
        ((HomePage)context).setManage_catformConnection_catlist(true);
        HashMap<String, String> data = new HashMap<>();
        data.put("misc_action", "names_id_imgsrc_catowner");
        ((HomePage)context).prepareAndSendData(data, Type.DOWNLOAD_MISC);
        //catListDialog = new CatListDialog(context, true);
       // ((HomePage)context).setCatListDialog(catListDialog);
        System.out.println("CAT LIST DIALOG SHOWING");
        //catListDialog.viewPickerDialog(clickedView);
    }

    public void viewDialog(final ImageView imageView, final RelativeLayout catconnection_holder, final String type){
        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Välj katt");
        RelativeLayout content = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.pick_cat_list, null);
        final String url = context.getString(R.string.serverimagesrc);
        final ListView listView = (ListView) content.findViewById(R.id.typeList);
        listView.setAdapter(pickCatFromListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String[] selectedRow = (String[]) pickCatFromListAdapter.getItem(position);
                String cat_name = selectedRow[1];
                String cat_id = selectedRow[0];
                String cat_url = selectedRow[2];
                System.out.println("SELECTED: "+cat_name+" "+cat_id+" "+cat_url);
                switch (type){
                    case "sibling":
                        LinearLayout sibling_entry = (LinearLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.new_catconnection_sibling_entry, null);
                        loadImage((ImageView)sibling_entry.findViewById(R.id.sibling_cat), url+cat_url);
                        ((LinearLayout)catconnection_holder.findViewById(R.id.sibling_holder)).addView(sibling_entry, 0);
                        break;
                    case "child":
                        LinearLayout child_entry = (LinearLayout)((HomePage)context).getLayoutInflater().inflate(R.layout.new_catconnection_child_entry, null);
                        loadImage((ImageView)child_entry.findViewById(R.id.child_cat), url+cat_url);
                        ((LinearLayout)catconnection_holder.findViewById(R.id.child_holder)).addView(child_entry, 0);
                        break;
                    default:
                        loadImage(imageView, url+cat_url);
                        break;
                }


                try {
                    JSONObject object = new JSONObject();
                    object.put(type, Base64.encodeToString((cat_name+":"+cat_id).getBytes(), Base64.NO_WRAP));
                    String jsonEncoded = Base64.encodeToString(object.toString().getBytes(), Base64.NO_WRAP);
                    HashMap<String, String> data = new HashMap<>();
                    String currentCatID = ((HomePage)context).getCurrentCatId();
                    String currentCatName = ((HomePage)context).getCurrentViewedCatname();
                    String blub = "cats:"+jsonEncoded+":"+currentCatID+":"+currentCatName;
                    System.out.println("PREPARING AND SENDING DATA BLUB: "+blub);
                    data.put("blub", Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP));
                    data.put("form_action", "UPDATE_FAMILY");
                    ((HomePage)context).prepareAndSendData(data, Type.UPLOAD);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*
                System.out.println(selectedRow[0]+" "+selectedRow[1]);
                if(((HomePage)context).getManage_catformConnection_catlist()){
                    switch (clickedView.getId()){
                        case R.id.catconnection_father:
                            addCatToConnectionParent((TextView)clickedView.findViewById(R.id.catconnection_father_text), selectedRow[1], selectedRow[0]);
                            break;
                        case R.id.catconnection_mother:
                            addCatToConnectionParent((TextView)clickedView.findViewById(R.id.catconnection_mother_text), selectedRow[1], selectedRow[0]);
                            break;
                        case R.id.catconnection_add_sibling:
                            addCatToConnectionList((LinearLayout)((View)clickedView.getParent()).findViewById(R.id.catconnection_siblings_list), selectedRow[1], selectedRow[0]);
                            break;
                        case R.id.catconnection_add_child:
                            addCatToConnectionList((LinearLayout)((View)clickedView.getParent()).findViewById(R.id.catconnection_children_list), selectedRow[1], selectedRow[0]);
                            break;
                        default:break;
                    }
                    ((HomePage)context).setManage_catformConnection_catlist(false);
                }
                else{
                    ArrayList<String[]> chosenCats = chosenCatFromListAdapter.getCatInfoList();
                    ArrayList<String[]> confirmChosenCats = confirmChosenCatFromListAdapter.getCatInfoList();
                    //System.out.println(chosenCats.size());
                    chosenCats.add(new String[]{selectedRow[1], selectedRow[0]});
                    confirmChosenCats.add(new String[]{selectedRow[1], selectedRow[0]});
                    chosenCatFromListAdapter.setCatInfoList(chosenCats);
                    chosenCatFromListAdapter.notifyDataSetChanged();
                    chosenCatList.setSelection(chosenCatFromListAdapter.getCount()-1);

                    confirmChosenCatFromListAdapter.setCatInfoList(confirmChosenCats);
                    confirmChosenCatFromListAdapter.notifyDataSetChanged();

                    if(!((HomePage)context).getNewCatOwner()){
                        String email = ((HomePage)context).getHomePageHandler().getCatownerManager().getMail();
                        Log.i("CATLISTDIALOG", "ADDING CAT ["+selectedRow[0]+", "+selectedRow[1]+"] TO OWNER "+email);
                        HashMap<String, String> data = new HashMap<>();
                        data.put("misc_action", "set_cat_owner");
                        String blub = Base64.encodeToString(new String(email+":"+selectedRow[0]).getBytes(), Base64.NO_WRAP);
                        data.put("blub", blub);
                        try {
                            ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.UPLOAD_MISC));
                            ((HomePage)context).signalQueueToSend();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                */


                dialog.dismiss();

            }
        });
        dialog.setContentView(content);
        dialog.show();
        downloadList();
    }

    public void viewPickerDialog(final View clickedView){
        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Välj katt");
        RelativeLayout content = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.pick_cat_list, null);
        final ListView listView = (ListView) content.findViewById(R.id.typeList);
        listView.setAdapter(pickCatFromListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String[] selectedRow = (String[]) pickCatFromListAdapter.getItem(position);
                System.out.println(selectedRow[0]+" "+selectedRow[1]);
                if(((HomePage)context).getManage_catformConnection_catlist()){
                    switch (clickedView.getId()){
                        case R.id.catconnection_father:
                            addCatToConnectionParent((TextView)clickedView.findViewById(R.id.catconnection_father_text), selectedRow[1], selectedRow[0]);
                            break;
                        case R.id.catconnection_mother:
                            addCatToConnectionParent((TextView)clickedView.findViewById(R.id.catconnection_mother_text), selectedRow[1], selectedRow[0]);
                            break;
                        case R.id.catconnection_add_sibling:
                            addCatToConnectionList((LinearLayout)((View)clickedView.getParent()).findViewById(R.id.catconnection_siblings_list), selectedRow[1], selectedRow[0]);
                            break;
                        case R.id.catconnection_add_child:
                            addCatToConnectionList((LinearLayout)((View)clickedView.getParent()).findViewById(R.id.catconnection_children_list), selectedRow[1], selectedRow[0]);
                            break;
                        default:break;
                    }
                    ((HomePage)context).setManage_catformConnection_catlist(false);
                }
                else{
                    ArrayList<String[]> chosenCats = chosenCatFromListAdapter.getCatInfoList();
                    ArrayList<String[]> confirmChosenCats = confirmChosenCatFromListAdapter.getCatInfoList();
                    //System.out.println(chosenCats.size());
                    chosenCats.add(new String[]{selectedRow[1], selectedRow[0]});
                    confirmChosenCats.add(new String[]{selectedRow[1], selectedRow[0]});
                    chosenCatFromListAdapter.setCatInfoList(chosenCats);
                    chosenCatFromListAdapter.notifyDataSetChanged();
                    chosenCatList.setSelection(chosenCatFromListAdapter.getCount()-1);

                    confirmChosenCatFromListAdapter.setCatInfoList(confirmChosenCats);
                    confirmChosenCatFromListAdapter.notifyDataSetChanged();

                    if(!((HomePage)context).getNewCatOwner()){
                        String email = ((HomePage)context).getHomePageHandler().getCatownerManager().getMail();
                        Log.i("CATLISTDIALOG", "ADDING CAT ["+selectedRow[0]+", "+selectedRow[1]+"] TO OWNER "+email);
                        HashMap<String, String> data = new HashMap<>();
                        data.put("misc_action", "set_cat_owner");
                        String blub = Base64.encodeToString(new String(email+":"+selectedRow[0]).getBytes(), Base64.NO_WRAP);
                        data.put("blub", blub);
                        try {
                            ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.UPLOAD_MISC));
                            ((HomePage)context).signalQueueToSend();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }


                dialog.dismiss();

            }
        });
        dialog.setContentView(content);
        dialog.show();
    }

    private void addCatToConnectionParent(TextView textView, String name, String id){
        textView.setText(name+"   "+id);
    }

    private void addCatToConnectionList(final LinearLayout list, String name, String id){
        final RelativeLayout entry = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.chosen_connect_cat_list_entry, null);
        ((TextView)entry.findViewById(R.id.chosenCatName)).setText(name);
        ((TextView)entry.findViewById(R.id.chosenCatID)).setText(id);
        list.addView(entry);
        ((Button)entry.findViewById(R.id.removeCat)).setText("Ta bort");
        (entry.findViewById(R.id.removeCat)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.removeView(entry);
            }
        });
    }

    public void confirmCats(){
        String email = ((HomePage)context).getHomePageHandler().getCatownerManager().getMail();
        ArrayList<String[]> chosenCats = chosenCatFromListAdapter.getCatInfoList();
        for(String[] cat: chosenCats){
            HashMap<String, String> data = new HashMap<>();
            data.put("misc_action", "set_cat_owner");
            String blub = Base64.encodeToString(new String(email+":"+cat[0]).getBytes(), Base64.NO_WRAP);
            data.put("blub", blub);
            try {
                ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.UPLOAD_MISC));

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        ((HomePage)context).signalQueueToSend();

    }
}
