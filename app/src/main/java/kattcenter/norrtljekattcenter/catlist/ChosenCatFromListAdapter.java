package kattcenter.norrtljekattcenter.catlist;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.Headers;

/**
 * Created by Joakim on 2017-06-25.
 */

public class ChosenCatFromListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String[]> catInfoList = null;
    private Dialog dialog;
    private String chosenCatId;
    private int chosenPosition = -1;
    private boolean editableList = true;

    public ChosenCatFromListAdapter(Context context, ArrayList<String[]> catInfoList, boolean editableList){
        this.context = context;
        this.catInfoList = catInfoList;
        this.editableList = editableList;
        Log.i("CHOSENCAT_ADAPTER", "INIT");
        dialog = new Dialog(context);
    }

    public void setCatInfoList(ArrayList<String[]> catInfoList){
        Log.i("CHOSENCAT_ADAPTER", "SET");
        this.catInfoList = catInfoList;
    }

    public ArrayList<String[]> getCatInfoList(){
        return catInfoList;
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return catInfoList.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return catInfoList.get(position);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = ((HomePage)context).getLayoutInflater().inflate(R.layout.catlist_item_third, null);
        }
        ((TextView)convertView.findViewById(R.id.owner_short_catinfo_name)).setText("Namn: "+catInfoList.get(position)[0]);
        ((TextView)convertView.findViewById(R.id.owner_short_catinfo_id)).setText("ID: "+catInfoList.get(position)[1]);
        final ImageView imageView = (ImageView) convertView.findViewById(R.id.owner_short_catinfo_photo);
        imageView.setDrawingCacheEnabled(true);
        Glide.with(context)
                .load(Headers.getUrlWithHeaders(context, catInfoList.get(position)[2]))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.error_loading_image));
                        return false;
                    }


                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);
        /*
        if(convertView == null){
            convertView = ((HomePage)context).getLayoutInflater().inflate(R.layout.chosen_connect_cat_list_entry, null);
        }
        Log.i("CHOSENCAT_ADAPTER", "NEW VIEW");
        final String catid = catInfoList.get(position)[1];
        ((TextView)convertView.findViewById(R.id.chosenCatName)).setText(catInfoList.get(position)[0]);
        ((TextView)convertView.findViewById(R.id.chosenCatID)).setText("["+catid+"]");
        if(editableList) {
            (convertView.findViewById(R.id.removeCat)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmRemoveCat(position, catid);
                    //System.out.println("REMOVING CAT ON POSITION: "+position);
                    //((HomePage)context).removeCatFromChooser(position);
                }
            });
        }
        else{
            (convertView.findViewById(R.id.removeCat)).setVisibility(View.GONE);
        }
        */
        return convertView;
    }

    private void confirmRemoveCat(final int position, final String catid){
        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Ändra");
        LinearLayout content = (LinearLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.three_button_dialog, null);
        Button newOwner = (Button) content.findViewById(R.id.option1);
        newOwner.setText("Byt ägare");
        newOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomePage)context).setLoadNewCatownerList(true);
                viewPickerDialog(catid);
                ((HomePage)context).getHomePageHandler().getListHandler().viewList(Type.CATOWNER);
                chosenPosition = position;
                //((HomePage)context).removeCatFromChooser(position);
                dialog.dismiss();
            }
        });
        Button deceased = (Button) content.findViewById(R.id.option2);
        deceased.setText("Avliden");
        deceased.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomePage)context).removeCatFromChooser(position);
                dialog.dismiss();
            }
        });
        Button abort = (Button) content.findViewById(R.id.option3);
        abort.setText("Avbryt");
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(content);
        dialog.show();
    }

    private void viewPickerDialog(String chosenCatId){
        this.chosenCatId = chosenCatId;
        //final Dialog dialog = new Dialog(context);
        dialog.setTitle("Välj kattägare");
        RelativeLayout content = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.pick_catowner_list, null);
        final ListView listView = (ListView) content.findViewById(R.id.typeList);
        listView.setAdapter(((HomePage)context).getHomePageHandler().getListHandler().getChangeCatownerListAdapter());
        //listView.setAdapter(((HomePage)context).getChosenCatFromListAdapter());
        //listView.setAdapter(pickCatFromListAdapter);
        /*
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("CHOSEN_ADAPTER", "ITEM CLICKED: "+position);

                /*
                String[] selectedRow = (String[]) pickCatFromListAdapter.getItem(position);
                System.out.println(selectedRow[0]+" "+selectedRow[1]);
                ArrayList<String[]> chosenCats = chosenCatFromListAdapter.getCatInfoList();
                System.out.println(chosenCats.size());
                chosenCats.add(new String[]{selectedRow[1], selectedRow[0]});
                chosenCatFromListAdapter.setCatInfoList(chosenCats);
                chosenCatFromListAdapter.notifyDataSetChanged();
                */
                //chosenCatList.setMinimumHeight(chosenCats.size()*25);
                //RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, chosenCats.size()*52);
                //layoutParams.addRule(RelativeLayout.BELOW, R.id.catlist_title);
                //chosenCatList.setLayoutParams(layoutParams);
                /*
                dialog.dismiss();

            }
        });
        */

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Log.i("CHOSEN_ADAPTER", "DIALOG CLOSING");
                ((HomePage)context).setLoadNewCatownerList(false);
            }
        });

        dialog.setContentView(content);
        dialog.show();
    }

    public Dialog getChooseCatownerDialog(){
        return dialog;
    }

    public String getChosenCatId(){
        return chosenCatId;
    }

    public int getChosenPosition(){
        return chosenPosition;
    }
}
