package kattcenter.norrtljekattcenter.catlist;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.CatListImageAdapter;
import kattcenter.norrtljekattcenter.CatownerListAdapter;
import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.LoadPreviewHandler;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.catform.IndexHolder;
import kattcenter.norrtljekattcenter.catowner.ChangeCatownerListAdapter;
import kattcenter.norrtljekattcenter.network.DataPacket;
import kattcenter.norrtljekattcenter.personel.PersonelListAdapter;

/**
 * Created by Joakim on 2017-06-25.
 */

public class ListHandler {

    private Context context;
    private HomePage homePage = null;
    private ArrayList<String> urlImageList;
    private ArrayList<String[]> catsInfo = new ArrayList<>();
    private HashMap<String, String> categorieToDBColumn = new HashMap<>();
    private CatListImageAdapter catListImageAdapter;
    private CatownerListAdapter catownerListAdapter;
    private ChangeCatownerListAdapter changeCatownerListAdapter;
    private PersonelListAdapter personelListAdapter;
    //private CatownerListAdapter changeCatownerListAdapter;

    public ListHandler(Context context){
        this.context = context;
        homePage = (HomePage) context;
        urlImageList = new ArrayList<>();
        homePage.getCatownerListView().setAdapter(catownerListAdapter);
        initAdapters();
        initCategoryMap();
    }

    public ListHandler(Context context, boolean catowner){
        this.context = context;
        homePage = (HomePage) context;
        urlImageList = new ArrayList<>();
        catListImageAdapter = new CatListImageAdapter(context, homePage.getPrivileged());
        catListImageAdapter.setUrlDataList(new ArrayList<String>());
        homePage.getCatListView().setAdapter(catListImageAdapter);
    }

    private void initAdapters(){
        catListImageAdapter = new CatListImageAdapter(context, homePage.getPrivileged());
        catListImageAdapter.setUrlDataList(new ArrayList<String>());
        homePage.getCatListView().setAdapter(catListImageAdapter);
        catownerListAdapter = new CatownerListAdapter(context, homePage.getPrivileged());
        changeCatownerListAdapter = new ChangeCatownerListAdapter(context);
        //changeCatownerListAdapter = new CatownerListAdapter(context, homePage.getPrivileged());
        catownerListAdapter.setCatOwnerNames(new ArrayList<String>());
        catownerListAdapter.setCatOwnerEmails(new ArrayList<String>());
        catownerListAdapter.setCatOwnerStatuses(new ArrayList<String>());

        changeCatownerListAdapter.setCatownerNames(new ArrayList<String>());
        changeCatownerListAdapter.setCatownerEmails(new ArrayList<String>());
        changeCatownerListAdapter.setCatownerStatuses(new ArrayList<String>());

        personelListAdapter = new PersonelListAdapter(context);
        personelListAdapter.setPersonelEmails(new ArrayList<String>());
        personelListAdapter.setPersonelNames(new ArrayList<String>());
        personelListAdapter.setPersonelStatuses(new ArrayList<String>());

        homePage.getCatownerListView().setAdapter(catownerListAdapter);
        //homePage.getChosenCatownerCats().setAdapter(changeCatownerListAdapter);
    }

    private void initCategoryMap() {
        categorieToDBColumn.put(Type.CATS_HOME.toString(), "onCatHome");
        categorieToDBColumn.put(Type.CATS_CENTER.toString(), "onCallCenter");
        categorieToDBColumn.put(Type.TNR.toString(), "onTNR");
        categorieToDBColumn.put(Type.ADOPTED.toString(), "adopted");
        categorieToDBColumn.put(Type.DECEASED.toString(), "deceased");
        categorieToDBColumn.put(Type.RETURNED_TO_OWNER.toString(), "returnedOwner");
        categorieToDBColumn.put(Type.MISSING.toString(), "missing");
        categorieToDBColumn.put(Type.WAITING_LIST.toString(), "waiting");
    }


    public void viewList(Type type){
        homePage.setLoadCatownerCats(false);

        switch (type){
            case ALL_CATS:
                //loadList("Sök katt ...", type);
                homePage.setCurrentSearchType("ALLA KATTER");
                break;
            case CATS_HOME:
                //loadList("Sök i katthemmet ...", type);
                homePage.setCurrentSearchType("KATTHEM-KATTER");
                break;
            case CATS_CENTER:
                //loadList("Sök i jourhemmet ...", type);
                homePage.setCurrentSearchType("JOURHEM-KATTER");
                break;
            case TNR:
                //loadList("Sök TNR", type);
                homePage.setCurrentSearchType("TNR-KATTER");
                break;
            case ADOPTED:
                //loadList("Sök adopterade katter ...", type);
                homePage.setCurrentSearchType("ADOPTERADE KATTER");
                break;
            case DECEASED:
                //loadList("Sök avlivade katter ...", type);
                homePage.setCurrentSearchType("AVLIDNA KATTER");
                break;
            case RETURNED_TO_OWNER:
                //loadList("Sök tillbakalämnande...", type);
                homePage.setCurrentSearchType("KATTER ÅTER TILL ÄGAREN");
                break;
            case MISSING:
                //loadList("Sök saknade katter ...", type);
                homePage.setCurrentSearchType("SAKNADE KATTER");
                break;
            case WAITING_LIST:
                //loadList("Sök i väntelista ...", type);
                homePage.setCurrentSearchType("VÄNTELISTA");
                break;
            case CATOWNER:
                //loadList("Sök kattägare ...", type);
                homePage.setCurrentSearchType("KATTÄGARE");
                break;
            case PERSONAL:
                //loadList("Sök...", type);
                homePage.setCurrentSearchType("PERSONAL");
            default:break;
        }
        loadList(type);
    }

    private void loadList(Type type){
        //System.out.println("LOAD CATOWNER LIST");
        urlImageList.clear();
        catsInfo.clear();
        /*
        if(!homePage.getLoadNewCatownerList()){
            if(type == Type.PERSONAL){
                //homePage.setView(IndexHolder.PERSONAL_LIST_PAGE, "Namn eller Email", null, R.color.newColorPrimary, R.color.newColorPrimary);

            }
            else{
                //String hint = (type == Type.CATOWNER) ? "Namn eller Email" : "ID eller Namn";
                //homePage.setView((type == Type.CATOWNER) ? IndexHolder.OWNER_LIST_PAGE : IndexHolder.CATLIST_PAGE, hint, null, R.color.newColorPrimary, R.color.newColorPrimary);
            }
        }
        */


        HashMap<String, String> data = new HashMap<>();
        //data.put("list_action", "DOWNLOAD_ALL_ID");
        String blub = null;
        if(type.toString().equals(Type.ALL_CATS.toString()) || type.toString().equals(Type.CATOWNER.toString()) || type == Type.PERSONAL){
            blub = Base64.encodeToString(type.toString().getBytes(), Base64.NO_WRAP);
        }
        else {
            //System.out.println("LIST "+type.toString()+", "+ categorieToDBColumn.get(type.toString()));
            blub = Base64.encodeToString(categorieToDBColumn.get(type.toString()).getBytes(), Base64.NO_WRAP);
        }
        data.put("blub", blub);

        try {
            //String loadingText = (type == Type.CATOWNER) ? "Laddar kattägare..." : "Laddar katter...";
            String loadingText;
            switch (type){
                case CATOWNER:
                    loadingText = "Laddar kattägare...";
                    break;
                case PERSONAL:
                    loadingText = "Laddar personal...";
                    break;
                default: loadingText = "Laddar katter...";
                    break;
            }
            homePage.addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_LIST));
            homePage.signalQueueToSend();
            if(!homePage.getLoadNewCatownerList())
                homePage.getHomePageHandler().showStandardDialog(loadingText);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clearList(){
        catsInfo.clear();
        urlImageList.clear();
    }

    public void addCatsToList(String json){

        //catListIdArray.clear();


        try{
            if(!json.equals("")) {
                System.out.println("CATOWNER CATS: "+homePage.getLoadCatownerCats());
                if(homePage.getPrivileged() != Type.CATOWNER) {
                    LinearLayout holder = ((LinearLayout) homePage.getCatOwnerInfo().findViewById(R.id.catowner_catlist_holder));
                    holder.removeAllViews();
                }
                if(homePage.getLoadCatownerCats()){
                    //String decodedJson = new String(Base64.decode(json.getBytes(), Base64.NO_WRAP));
                    System.out.println("JSON: ");
                    System.out.println(json);
                    homePage.getHomePageHandler().getCatownerManager().loadCatOwnerCatsInfo(json);
                    homePage.setLoadCatownerCats(false);
                }
                else {
                    String[] jsonRows = json.split("\n");
                    if (homePage.getLoadPreviousCatownerList()) {
                        new LoadPreviewHandler(context).loadPreview("cats", json);
                        homePage.setLoadPreviousCatownerList(false);
                    } else {
                        //catsInfo.clear();
                        //urlImageList.clear();
                        for (String obj : jsonRows) {
                            if (obj != null && !obj.isEmpty() && !obj.equals("null")) {
                                JSONObject jsonObject = new JSONObject(obj);
                                String cat_id = jsonObject.getString("catcenter_id");
                                String cat_name = "Inget namn";

                                if (homePage.getPrivileged() == Type.CATOWNER) {
                                    if (jsonObject.has("name")) {
                                        cat_name = (jsonObject.getString("name").isEmpty()) ? cat_name : jsonObject.getString("name");
                                    }
                                } else if (jsonObject.has("catcenter_name")) {
                                    cat_name = jsonObject.getString("catcenter_name");
                                }
                                catsInfo.add(new String[]{cat_id, cat_name});
                                urlImageList.add(context.getString(R.string.serverimagesrc) + jsonObject.getString("img_src"));
                                Log.i("LISTHANDLER", cat_name + " " + cat_id + " " + jsonObject.getString("img_src"));
                                Log.i("LISTHANDLER", "LISTSIZE="+catsInfo.size()+"  "+urlImageList.size());
                            }
                        }

                        invalidateAdapter();
                        Log.i("LISTHANDLER", "ADDED CATS TO LIST");
                    }
                }
            }else{
                LinearLayout holder = ((LinearLayout)homePage.getCatOwnerInfo().findViewById(R.id.catowner_catlist_holder));
                holder.removeAllViews();
            }

            homePage.getHomePageHandler().closeStandardDialog();
        } catch (JSONException jexp){
            jexp.printStackTrace();
        }
    }

    public void addPrivateCatsToList(String json){

        //catListIdArray.clear();


        try{
            if(!json.equals("")) {

                String[] jsonRows = json.split("\n");
                for (String obj : jsonRows) {
                    if (obj != null && !obj.isEmpty() && !obj.equals("null")) {
                        JSONObject jsonObject = new JSONObject(obj);
                        String cat_name = "Inget namn";
                        if (jsonObject.has("name")) {
                            cat_name = (jsonObject.getString("name").isEmpty()) ? cat_name : jsonObject.getString("name");
                        }
                        catsInfo.add(new String[]{"Privat katt", cat_name});
                        urlImageList.add(context.getString(R.string.serverimagesrc) + jsonObject.getString("photo_src"));
                        Log.i("LISTHANDLER", cat_name + " " + "privat katt" + " " + jsonObject.getString("photo_src"));
                    }
                }

                invalidateAdapter();
                Log.i("LISTHANDLER", "ADDED CATS TO LIST");

            }

            homePage.getHomePageHandler().closeStandardDialog();
        } catch (JSONException jexp){
            jexp.printStackTrace();
        }
    }

    public void addPreviousCatowners(String json){
        System.out.println("LOAD PREVIOUS CATOWNERS: "+homePage.getLoadPreviousCatownerList());
        if(homePage.getLoadPreviousCatownerList()){
            new LoadPreviewHandler(context).loadPreview("cats", json);
            homePage.setLoadPreviousCatownerList(false);
        }
    }

    public void addCatownersToList(String json){
        String[] jsonRows = json.split("\n");
        ArrayList<String> catownerNames = new ArrayList<>();
        ArrayList<String> catOwnerEmails = new ArrayList<>();
        ArrayList<String> catOwnerStatuses = new ArrayList<>();
        try{
            if(!json.equals("")) {
                if(homePage.getLoadConnectCatownerList()){
                    ArrayList<String[]> catowners = new ArrayList<>();
                    for (String obj : jsonRows) {
                        String[] catowner_info = new String[3];
                        JSONObject jsonObject = new JSONObject(obj);
                        //catOwnerEmails.add(jsonObject.getString("email"));

                        String catowner_surname = jsonObject.getString("surname");
                        String catowner_lastname = jsonObject.getString("lastname");

                        if (catowner_surname != null && !catowner_surname.isEmpty()) {
                            String e_mail = jsonObject.getString("email");
                            String catowner_mobile = jsonObject.getString("cell");
                            catowner_info[0] = catowner_surname+" "+catowner_lastname;
                            catowner_info[1] = e_mail;
                            catowner_info[2] = catowner_mobile;
                            catowners.add(catowner_info);
                        }
                    }
                    homePage.getController().updateCatowners(catowners);
                    homePage.setLoadConnectCatownerList(false);
                }
                else if(homePage.getLoadNewCatownerList()){
                    String currentWatchedCatownerEmail = homePage.getHomePageHandler().getCatownerManager().getMail();
                    for (String obj : jsonRows) {
                        JSONObject jsonObject = new JSONObject(obj);
                        //catOwnerEmails.add(jsonObject.getString("email"));

                        String catowner = jsonObject.getString("surname");
                        String temp_password = jsonObject.getString("temp_password");
                        if (catowner != null && !catowner.isEmpty()) {
                            String e_mail = jsonObject.getString("email");
                            if(e_mail.equals(currentWatchedCatownerEmail)) continue;
                            catownerNames.add(catowner);
                            catOwnerEmails.add(jsonObject.getString("email"));
                            if (temp_password == null || temp_password.isEmpty()) {
                                catOwnerStatuses.add("Senast inloggad " + jsonObject.getString("last_login").split("_")[0]);
                            } else {
                                catOwnerStatuses.add("Inväntar registrering");
                            }


                        }

                    }
                    Log.i("LISTHANDLER", "LOADING CATOWNER LIST FOR CHANGING CATS OWNERS");

                    changeCatownerListAdapter.setCatownerNames(catownerNames);
                    changeCatownerListAdapter.setCatownerEmails(catOwnerEmails);
                    changeCatownerListAdapter.setCatownerStatuses(catOwnerStatuses);
                    changeCatownerListAdapter.notifyDataSetChanged();

                    //homePage.getHomePageHandler().closeStandardDialog();
                }
                else{
                    for (String obj : jsonRows) {
                        JSONObject jsonObject = new JSONObject(obj);
                        String catowner = jsonObject.getString("surname") +" "+ jsonObject.getString("lastname");
                        String temp_password = jsonObject.getString("temp_password");
                        if (catowner != null && !catowner.isEmpty()) {
                            catownerNames.add(catowner);
                            catOwnerEmails.add(jsonObject.getString("email"));
                            if (temp_password == null || temp_password.isEmpty()) {
                                catOwnerStatuses.add("Senast inloggad " + jsonObject.getString("last_login").split("_")[0]);
                            } else {
                                catOwnerStatuses.add("Inväntar registrering");
                            }


                        }
                    }

                    Log.i("LISTHANDLER", "LOADING CATOWNER BASIC LIST");
                    /*
                    catownerListAdapter.setCatOwnerNames(catownerNames);
                    catownerListAdapter.setCatOwnerEmails(catOwnerEmails);
                    catownerListAdapter.setCatOwnerStatuses(catOwnerStatuses);
                    catownerListAdapter.setCatOwnerNamesCopy(catownerNames);
                    catownerListAdapter.setCatOwnerEmailsCopy(catOwnerEmails);
                    catownerListAdapter.setCatOwnerStatusesCopy(catOwnerStatuses);
                    catownerListAdapter.notifyDataSetChanged();
                    */
                    homePage.getHomePageHandler().closeStandardDialog();
                }


            }
            catownerListAdapter.setCatOwnerNames(catownerNames);
            catownerListAdapter.setCatOwnerEmails(catOwnerEmails);
            catownerListAdapter.setCatOwnerStatuses(catOwnerStatuses);
            catownerListAdapter.setCatOwnerNamesCopy(catownerNames);
            catownerListAdapter.setCatOwnerEmailsCopy(catOwnerEmails);
            catownerListAdapter.setCatOwnerStatusesCopy(catOwnerStatuses);
            catownerListAdapter.notifyDataSetChanged();

        } catch (JSONException jexp){
            jexp.printStackTrace();
        }
    }

    public void addPersonelsToList(String json){
        String[] jsonRows = json.split("\n");
        ArrayList<String> personelNames = new ArrayList<>();
        ArrayList<String> personelEmails = new ArrayList<>();
        ArrayList<String> personelStatuses = new ArrayList<>();
        try{
            if(!json.equals("")) {
                //if(homePage.getLoadNewCatownerList()){
                    String currentWatchedCatownerEmail = homePage.getHomePageHandler().getCatownerManager().getMail();
                    for (String obj : jsonRows) {
                        JSONObject jsonObject = new JSONObject(obj);
                        //catOwnerEmails.add(jsonObject.getString("email"));

                        String catowner = jsonObject.getString("surname");
                        String temp_password = jsonObject.getString("temp_password");
                        if (catowner != null && !catowner.isEmpty()) {
                            String e_mail = jsonObject.getString("email");
                            if(e_mail.equals(currentWatchedCatownerEmail)) continue;
                            personelNames.add(catowner);
                            personelEmails.add(jsonObject.getString("email"));
                            personelStatuses.add("Senast inloggad " + jsonObject.getString("last_login").split("_")[0]);
                            /*
                            if (temp_password == null || temp_password.isEmpty()) {
                                personelStatuses.add("Senast inloggad " + jsonObject.getString("last_login").split("_")[0]);
                            } else {
                                personelStatuses.add("Inväntar registrering");
                            }
                            */


                        }

                    }

                    homePage.getHomePageHandler().closeStandardDialog();

            }
            personelListAdapter.setPersonelNames(personelNames);
            personelListAdapter.setPersonelEmails(personelEmails);
            personelListAdapter.setPersonelStatuses(personelStatuses);
            personelListAdapter.setPersonelNamesCopy(personelNames);
            personelListAdapter.setPersonelEmailsCopy(personelEmails);
            personelListAdapter.setPersonelStatusesCopy(personelStatuses);
            personelListAdapter.notifyDataSetChanged();
            System.out.println("PERSONEL ADDED TO LIST");

        } catch (JSONException jexp){
            jexp.printStackTrace();
        }
    }

    public ChangeCatownerListAdapter getChangeCatownerListAdapter(){ return changeCatownerListAdapter; }

    public CatownerListAdapter getCatownerListAdapter(){ return catownerListAdapter; }

    public CatListImageAdapter getCatListImageAdapter(){
        return catListImageAdapter;
    }

    public PersonelListAdapter getPersonelListAdapter(){ return personelListAdapter; }



    private void invalidateAdapter(){
        //catListImageAdapter.setCatsId(catListIdArray);
        catListImageAdapter.setCatsInfo(catsInfo);
        catListImageAdapter.setUrlDataList(urlImageList);
        catListImageAdapter.setCatsInfoCopy(catsInfo);
        catListImageAdapter.setUrlDataListCopy(urlImageList);
        catListImageAdapter.notifyDataSetChanged();
    }

}
