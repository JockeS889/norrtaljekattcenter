package kattcenter.norrtljekattcenter;

/**
 * Created by Joakim on 2017-11-09.
 */

public class Variable {

    private static boolean formOptions = false;

    public static void setFormOptions(boolean formOptions){
        Variable.formOptions = formOptions;
        System.out.println("SET VALUE OF FORMOPTIONS: "+Variable.formOptions);
    }

    public static boolean getFormOptions(){
        System.out.println("RETURN VALUE OF FORMOPTIONS: "+Variable.formOptions);
        return Variable.formOptions;
    }
}
