package kattcenter.norrtljekattcenter;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.widget.ArrayAdapter;

/**
 * Created by Jocke on 2017-05-31.
 */
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class HamburgerListAdapter extends ArrayAdapter<HamburgerListItem> {

    Context context;
    List<HamburgerListItem> drawerItemList;
    int layoutResID;
    private int selectedItem = 0;

    public HamburgerListAdapter(Context context, int layoutResourceID,
                               List<HamburgerListItem> listItems) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;

    }

    public void setSelectedItem(int selectedItem){
        this.selectedItem = selectedItem;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub


        HamburgerListItemHolder hamburgerListItemHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            hamburgerListItemHolder = new HamburgerListItemHolder();

            view = inflater.inflate(layoutResID, parent, false);
            hamburgerListItemHolder.ItemName = (TextView) view
                    .findViewById(R.id.drawer_itemName);
            hamburgerListItemHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);

            view.setTag(hamburgerListItemHolder);

        } else {
            hamburgerListItemHolder = (HamburgerListItemHolder) view.getTag();

        }

        HamburgerListItem dItem = this.drawerItemList.get(position);
        Drawable gray_circle = context.getResources().getDrawable(R.drawable.circular_border, null);
        Drawable white_circle = context.getResources().getDrawable(R.drawable.circular_border_white, null);
        if(position == selectedItem) {
            hamburgerListItemHolder.icon.setBackground(white_circle);
            hamburgerListItemHolder.icon.setColorFilter(Color.argb(255, 255, 255, 255));
            hamburgerListItemHolder.ItemName.setTextColor(Color.argb(255, 255, 255, 255));
        }
        else {
            hamburgerListItemHolder.icon.setBackground(gray_circle);
            hamburgerListItemHolder.icon.setColorFilter(Color.argb(255, 170, 170, 170));
            hamburgerListItemHolder.ItemName.setTextColor(Color.argb(255, 170, 170, 170));
        }
        hamburgerListItemHolder.icon.setImageDrawable(view.getResources().getDrawable(
                dItem.getImgResID()));
        hamburgerListItemHolder.ItemName.setText(dItem.getItemName());

        return view;
    }

    private static class HamburgerListItemHolder {
        TextView ItemName;
        ImageView icon;
    }
}