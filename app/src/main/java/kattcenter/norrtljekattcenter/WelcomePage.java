package kattcenter.norrtljekattcenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.HashMap;

import kattcenter.norrtljekattcenter.network.DataPacket;
import kattcenter.norrtljekattcenter.network.DatabaseConnection;
import kattcenter.norrtljekattcenter.security.RSA;

/**
 * Author: Joakim Sandberg
 */
public class WelcomePage extends AppCompatActivity {

    private View view;
    private Handler processHandler = new Handler();
    private ProgressBar progressBar;
    private TextView textView;
    private Type privilige;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);

        view = findViewById(R.id.background_image);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        textView = (TextView) findViewById(R.id.state);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.hide();
        }

        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        processHandler.postDelayed(processRunnable, 3000);


    }

    private final Runnable processRunnable = new Runnable() {
        @Override
        public void run() {
            //progressBar.setVisibility(View.VISIBLE);
            //textView.setVisibility(View.VISIBLE);
            loginScreen();
        }
    };

    public void loginResult(boolean success){
        if(success){
            PriviligeHolder.setType(privilige);
            Intent homepage = new Intent(WelcomePage.this, HomePage.class);
            homepage.putExtra("PRIVILEGED", privilige);
            startActivity(homepage);
        }
        else{
            Intent loginIntent = new Intent(WelcomePage.this, LoginPage.class);
            startActivity(loginIntent);
        }
    }

    private void loginScreen(){
        //CHECK ALREADY LOGIN SETTINGS
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        String email = settings.getString("Email", null);
        String password = settings.getString("Password", null);
        String priv = settings.getString("Privilige", null);
        System.out.println("WELCOME-------------   "+email+"  "+password+"  "+priv);
        if(email != null && password != null && priv != null){
            //Type privilige = null;
            switch (priv){
                case "ADMIN": privilige = Type.ADMIN; break;
                case "PERSONAL": privilige = Type.PERSONAL; break;
                case "CATOWNER": privilige = Type.CATOWNER; break;
                default:break;
            }

            String blub = email+":"+password;
            blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
            HashMap<String, String> data = new HashMap<>();
            data.put("blub", blub);
            data.put("logintype", "login");
            new DatabaseConnection(this, new DataPacket(data, Type.LOGIN)).execute();

            //RSA rsa = new RSA(this);
            //byte[] encryptedPassword = rsa.encrypt(password);
            //rsa.decrypt(encryptedPassword);
            /*
            PriviligeHolder.setType(privilige);
            Intent homepage = new Intent(WelcomePage.this, HomePage.class);
            homepage.putExtra("PRIVILEGED", privilige);
            startActivity(homepage);
            */
        }
        else {
            Intent loginIntent = new Intent(WelcomePage.this, LoginPage.class);
            startActivity(loginIntent);
        }
    }
}
