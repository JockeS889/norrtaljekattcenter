package kattcenter.norrtljekattcenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RunnableFuture;
import android.os.Handler;
import android.widget.Toast;

import kattcenter.norrtljekattcenter.catform.AutoCompleteAdapter;
import kattcenter.norrtljekattcenter.catform.IndexHolder;
import kattcenter.norrtljekattcenter.catform.MedicalInfo;
import kattcenter.norrtljekattcenter.catform.ResourceMapper;

/**
 * Created by Jocke on 2017-06-10.
 */

public class LoadFormHandler {

    private Context context;
    private HomePage homePage;

    private ArrayList<String> catvetdoc_forms = new ArrayList<>();
    private HashMap<String, String> rMapper = new HashMap<>();

    public LoadFormHandler(Context context){
        this.context = context;
        homePage = ((HomePage) context);

        String[] parents = context.getResources().getStringArray(R.array.catvet_items);

        catvetdoc_forms.add("catform_vetdoc_generalstate");
        catvetdoc_forms.add("catform_vetdoc_lynne");
        catvetdoc_forms.add("catform_vetdoc_skinpaws");
        catvetdoc_forms.add("catform_vetdoc_lymf");
        catvetdoc_forms.add("catform_vetdoc_eyes");
        catvetdoc_forms.add("catform_vetdoc_ears");
        catvetdoc_forms.add("catform_vetdoc_mouth");
        catvetdoc_forms.add("catform_vetdoc_abdomen");
        catvetdoc_forms.add("catform_vetdoc_circulationorgan");
        catvetdoc_forms.add("catform_vetdoc_respirationorgan");
        catvetdoc_forms.add("catform_vetdoc_outergenitalias");
        catvetdoc_forms.add("catform_vetdoc_locomotiveorgan");


        for(int i = 0; i < catvetdoc_forms.size(); i++){
            rMapper.put(catvetdoc_forms.get(i), parents[i]);
        }
    }

    public void loadForm(String form_table, String content){
        ResourceMapper resourceMapper = new ResourceMapper();
        switch (form_table){
            case "catform_basic":
                resourceMapper.initBasic();
                break;
            case "catform_medical":
                //resourceMapper.initMedical();
                break;
            case "catform_vaccination":
                //resourceMapper.initVaccination();
                break;
            case "catform_parasites":
                //resourceMapper.initParasites();
            /*
            case "catform_deworming":
                resourceMapper.initDeworming();
                break;
            case "catform_verm":
                resourceMapper.initVermin();
                break;
                */
            case "catform_claws":
                //resourceMapper.initClaw();
                break;
            case "catform_weight":
                //resourceMapper.initWeight();
                break;
            case "catform_regskk":
                //resourceMapper.initRegSKK();
                break;
            case "catform_vet":
                //resourceMapper.initVet();
                break;
            case "catform_insurance":
                //resourceMapper.initInsurance();
                break;
            case "catform_background":
                //resourceMapper.initBackground();
                break;
            case "catform_prevowner":

                break;
            case "catform_status":
                //resourceMapper.initStatus();
                break;
            case "catform_connection":
                resourceMapper.initConnection();
            default: break;
        }
        boolean isCatVetDoc = false;
        try {
            for (String catvetdoc : catvetdoc_forms) {
                if (catvetdoc.equals(form_table)) {
                    String json = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
                    //Log.i(TagHolder.JSON_TAG, "json = "+json);
                    if(json != null && !json.equals("") && !json.equals("null")) {
                        JSONObject jsonObject = new JSONObject(json);
                        Iterator<String> keys = jsonObject.keys();
                        System.out.println("TABLE: " + form_table);
                        while (keys.hasNext()) {
                            String key = keys.next();
                            String value = jsonObject.getString(key);
                            //Log.e("LOADFORMHANDLER", "[JSON] KEY: " + key + " VALUE: " + value);
                            if (key.equals("date")) {
                                ((TextView) homePage.findViewById(R.id.catvet_reg_date_text)).setText(value);
                            } else if (key.equals("note")) {
                                homePage.updateCatvetregComment(rMapper.get(form_table), value);
                            } else if (!(key.equals("id") || key.equals("catcenter_id"))) {
                                if (key.equals("without_remark")) {
                                    key = "U.a.";
                                } else if (key.equals("general_without_remark")) {
                                    key = "A.T. u.a.";
                                } else if (key.equals("genererally_low")) {
                                    key = "AT. Nedsatt";
                                } else {
                                    if (form_table.equals("catform_vetdoc_skinpaws"))
                                        key = key.replace("_", "/");
                                    else
                                        key = key.replace("_", " ");
                                }
                                String spinnerParent = rMapper.get(form_table);
                                boolean isChecked = (value.equals("1")) ? true : false;
                                homePage.updateRecord(spinnerParent, key, isChecked);
                            }
                        }
                        //System.out.println("[JSON]: " + json);

                    }
                    isCatVetDoc = true;
                }
            }
            if(homePage.getPrivileged() != Type.CATOWNER)
                homePage.refreshSpinnerClick();
        } catch (JSONException jsone){
            jsone.printStackTrace();
        }
        if(!isCatVetDoc) {
            load(form_table, content, resourceMapper);
        }
        //homePage.closeLoadingDialog();
        homePage.getHomePageHandler().closeStandardDialog();
    }


    private void load(String form_type, String content, ResourceMapper resourceMapper){
        String json = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
        System.out.println("JSON: "+json+" LENGTH: "+json.length());
        String name = "Okänd";
        if (!json.equals("null")) {
            try {
                if(form_type.equals("catform_connection")){
                }
                else {

                    JSONObject jsonObject = new JSONObject(json);
                    Iterator<String> iterator = jsonObject.keys();
                    //String name = "Okänd";
                    while (iterator.hasNext()) {
                        String key = iterator.next();
                        if (key.equals("id") || key.equals("table_name") || key.equals("start_date_reminded") || key.equals("repeat_date_reminded"))
                            continue;
                        if (!form_type.equals("catform_basic") && key.equals("catcenter_id"))
                            continue;
                        if (form_type.equals("catform_medical") && key.equals("repeat_date"))
                            continue;
                        Log.i("LOADFORMHANDLER", "LOAD VIEW BY KEY: "+key);
                        if (key.equals("reg_date") || key.equals("owner")) continue;
                        View rightView = null;
                        if(form_type.equals("catform_basic"))
                            rightView = homePage.getCatInfoBasic().findViewById(resourceMapper.getResource(key));
                        else if(form_type.equals("catform_regskk"))
                            rightView = homePage.getCatInfoRegSKK().findViewById(resourceMapper.getResource(key));
                        //final View view = homePage.getCatInfoBasic().findViewById(resourceMapper.getResource(key));
                        final View view = rightView;
                        Log.i("LOADFORMHANDLER", "ID: " + resourceMapper.getResource(key) + "  VIEW: " + view);
                        final String value = jsonObject.getString(key);
                        if (key.equals("name")) name = value;
                        //Log.i("LOADFORMHANDLER", "Value: "+value);
                        if (view instanceof AutoCompleteTextView) {

                        }
                        else if (view instanceof EditText) {
                            System.out.println("[EDITTEXT] KEY: " + key + "   VALUE: " + value);
                            ((EditText) view).setText(value);
                        } else if (view instanceof CheckBox) {

                            switch (value) {
                                case "1":
                                    ((CheckBox) view).setChecked(true);
                                    break;
                                case "2":
                                    ((CheckBox) view).setChecked(false);
                                    break;
                                case "true":
                                case "false":
                                    //System.out.println("CHECKBOX BOOLEAN: " + Boolean.parseBoolean(value));
                                    ((CheckBox) view).setChecked(Boolean.parseBoolean(value));
                                    break;
                                default:
                                    //Log.i("LOADFORMHANDLER", "VALUE IS: "+value);
                                    if (value != null && !value.equals("")) {
                                        if (form_type.equals("catform_status")) {
                                            String[] splitted = value.split(" ");
                                            if (splitted.length >= 2 && splitted[splitted.length - 1].substring(0, 1).equals("(")) {
                                                ((CheckBox) view).setChecked(true);
                                            }
                                        }

                                        ((CheckBox) view).setText(value);
                                    }

                                    break;
                            }
                        } else if (view instanceof RadioButton) {
                            if (form_type.equals("catform_status")) {
                                System.out.println("RB STATUS  " + value.split(" ").length);
                                if (value.split(" ").length == 2) {
                                    ((RadioButton) view).setChecked(true);
                                } else {
                                    ((RadioButton) view).setChecked(false);
                                }
                                ((RadioButton) view).setText(value);
                            }
                        } else if (view instanceof RadioGroup) {
                            switch (value) {
                                case "Nej":
                                case "Ja":
                                    break;
                                default:
                                    if (!value.equals("") && !value.split(" ")[0].equals("Ja")) {
                                        int intValue = Integer.parseInt(value);
                                        if (intValue > 0) {
                                            ((RadioGroup) view).check(intValue);
                                        }
                                    }
                                    break;
                            }

                        } else if (view instanceof TextView) {
                            System.out.println("[TEXTVIEW] KEY: " + key + "   VALUE: " + value);
                            ((TextView) view).setText(value);
                        } else if (view instanceof ImageView && key.equals("photo_src")) {
                            File currentCatImageFile;
                            if (homePage.getNewCat()) {
                                currentCatImageFile = homePage.getFileManager().getLatestTakenPicture();
                            } else {
                                currentCatImageFile = homePage.getFileManager().getCurrentViewedCatPicture();
                            }
                            //File currentCatImageFile = ((HomePage)context).getFileManager().getCurrentViewedCatPicture();
                            Log.i("LOADFORMHANDLER", "CurrentCatImageFile: " + currentCatImageFile);
                            Log.i("LOADFORMHANDLER", "PATH: " + currentCatImageFile.getAbsolutePath());
                            try {
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                                ((ImageView) view).setImageBitmap(BitmapFactory.decodeFile(currentCatImageFile.getAbsolutePath(), options));
                                view.setTag("SELECTED_PHOTO");
                                System.out.println("[ELSE] KEY: " + key + "   VALUE: " + value + "   VIEW: " + view);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Kunde inte ladda bild till formulär!", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                    if (form_type.equals("catform_basic")) {

                        //homePage.setView(5, name, homePage.getCatformtitles()[0], R.color.catform, R.color.catformDark);
                    }
                }
            } catch (JSONException jsone) {
                jsone.printStackTrace();
            }
            /*catch (Exception exp){

                exp.printStackTrace();
                Toast.makeText(context, "Fel", Toast.LENGTH_SHORT).show();
            }
            */

        }

        if (form_type.equals("catform_basic")) {
            //homePage.isNewCat(false);
            //homePage.setView(IndexHolder.FIRST_FORMPAGE_INDEX, name, homePage.getCatformtitles()[0], R.color.catform, R.color.catformDark);

        }

    }


    private void addCatToConnectionParent(TextView textView, String name, String id){
        textView.setText(name+"   "+id);
    }

    private void addCatToConnectionList(final LinearLayout list, String name, String id){
        final RelativeLayout entry = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.chosen_connect_cat_list_entry, null);
        ((TextView)entry.findViewById(R.id.chosenCatName)).setText(name);
        ((TextView)entry.findViewById(R.id.chosenCatID)).setText(id);
        list.addView(entry);
        ((Button)entry.findViewById(R.id.removeCat)).setText("Ta bort");
        (entry.findViewById(R.id.removeCat)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.removeView(entry);
            }
        });
    }



    /*

    private void loadBasicInfoForm(String content){
        String json = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
        try{
            JSONObject jsonObject = new JSONObject(json);
            ResourceMapper resourceMapper = new ResourceMapper();
            resourceMapper.initBasic();
            Iterator<String> iterator = jsonObject.keys();
            String name = "Okänd";
            while(iterator.hasNext()){
                String key = iterator.next();
                View view = homePage.findViewById(resourceMapper.getResource(key));
                String value = jsonObject.getString(key);
                if(view instanceof EditText){
                    System.out.println("[EDITTEXT] KEY: "+key+"   VALUE: "+value);
                    ((EditText)view).setText(value);
                    if(key.equals("name")) name = value;
                }
                else if(view instanceof CheckBox){
                    System.out.println("[CHECKBOX] KEY: "+key+"   VALUE: "+value);
                    switch (value){
                        case "true":
                        case "false":
                            System.out.println("CHECKBOX BOOLEAN: "+Boolean.parseBoolean(value));
                            ((CheckBox)view).setChecked(Boolean.parseBoolean(value));
                            break;
                        default:
                            ((CheckBox)view).setText(value);
                            break;
                    }
                }
                else if(view instanceof RadioGroup){
                    int intValue = Integer.parseInt(value);
                    if(intValue > 0){
                        ((RadioGroup)view).check(intValue);
                    }
                }
                else if(view instanceof TextView){
                    System.out.println("[TEXTVIEW] KEY: "+key+"   VALUE: "+value);
                    ((TextView)view).setText(value);
                }
                else{
                    System.out.println("[ELSE] KEY: "+key+"   VALUE: "+value);
                }
            }
            homePage.setView(5, name, homePage.getCatformtitles()[0], R.color.catform, R.color.catformDark);

        } catch (JSONException jsone){
            jsone.printStackTrace();
        }
    }

    private void loadMedicalInfoForm(String content){
        String json = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
        try{
            JSONObject jsonObject = new JSONObject(json);
            ResourceMapper resourceMapper = new ResourceMapper();
            resourceMapper.initMedical();
            Iterator<String> iterator = jsonObject.keys();
            String name = "Okänd";
            while(iterator.hasNext()){
                String key = iterator.next();
                if(key.equals("catcenter_id") || key.equals("id")) continue;
                View view = homePage.findViewById(resourceMapper.getResource(key));
                String value = jsonObject.getString(key);
                if(key.equals("name")) name = value;
                if(view instanceof EditText){
                    System.out.println("[EDITTEXT] KEY: "+key+"   VALUE: "+value);
                    ((EditText)view).setText(value);
                }
                else if(view instanceof CheckBox){
                    System.out.println("[CHECKBOX] KEY: "+key+"   VALUE: "+value);
                    switch (value){
                        case "true":
                        case "false":
                            System.out.println("CHECKBOX BOOLEAN: "+Boolean.parseBoolean(value));
                            ((CheckBox)view).setChecked(Boolean.parseBoolean(value));
                            break;
                        default:
                            ((CheckBox)view).setText(value);
                            break;
                    }
                }
                else if(view instanceof RadioGroup){
                    int intValue = Integer.parseInt(value);
                    if(intValue > 0){
                        ((RadioGroup)view).check(intValue);
                    }
                }
                else if(view instanceof TextView){
                    System.out.println("[TEXTVIEW] KEY: "+key+"   VALUE: "+value);
                    ((TextView)view).setText(value);
                }
                else{
                    System.out.println("[ELSE] KEY: "+key+"   VALUE: "+value);
                }
            }
            //homePage.setView(6, name, homePage.getCatformtitles()[0], R.color.catform, R.color.catformDark);
            //homePage.closeLoadingDialog();
        } catch (JSONException jsone){
            jsone.printStackTrace();
        }
    }

    private void loadVaccinationInfoForm(String content){
        String json = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
    }

    private void setValue(View view, String value){
        if(view instanceof EditText){
            ((EditText)view).setText(value);
        }
        else if(view instanceof CheckBox){
            switch (value){
                case "true":
                case "false":
                    System.out.println("CHECKBOX BOOLEAN: "+Boolean.parseBoolean(value));
                    ((CheckBox)view).setChecked(Boolean.parseBoolean(value));
                    break;
                default:
                    ((CheckBox)view).setText(value);
                    break;
            }
        }
        else if(view instanceof RadioGroup){
            int intValue = Integer.parseInt(value);
            if(intValue > 0){
                ((RadioGroup)view).check(intValue);
            }
        }
        else if(view instanceof TextView){
            ((TextView)view).setText(value);
        }
        else{
        }
    }


/*
    private void loadBasicInfoForm(String content){
        System.out.println("CONTENT: "+content);
        content = new String(Base64.decode(content, Base64.NO_WRAP));
        System.out.println("DECODED CONTENT: "+content);
        String[] info = content.split(":");
        HashMap<String, String> receivedFormData = new HashMap<>();
        String name = null;
        ResourceMapper resourceMapper = new ResourceMapper();
        resourceMapper.init();

        for(String s: info){
            String fieldData = new String(Base64.decode(s.getBytes(), Base64.NO_WRAP));
            String fieldTitle = fieldData.split(":")[0];
            if(fieldData.split(":").length > 1) {
                String fieldValue = fieldData.split(":")[1];
                System.out.println("L: "+fieldValue.length());
                if (fieldValue.equals("false") || fieldValue.equals("true")) {
                    Boolean checkBoxValue = Boolean.valueOf(fieldValue);
                    CheckBox checkBox = (CheckBox) homePage.findViewById(resourceMapper.getResource(fieldTitle));
                    checkBox.setChecked(checkBoxValue);
                } else {
                    try {
                        int val = Integer.parseInt(fieldValue);
                        System.out.println("INTEGER VALUE: " + val);
                        continue;
                    } catch (Exception e) {
                    }
                    System.out.println("STRING VALUE: " + fieldValue);

                    View view = homePage.findViewById(resourceMapper.getResource(fieldTitle));
                    if(view instanceof EditText){
                        ((EditText)view).setText(fieldValue);
                        if(fieldTitle.equals("name")){
                            name = fieldValue;
                        }
                    }
                    else if(view instanceof TextView){
                        ((TextView)view).setText(fieldValue);
                    }
                    else if(view instanceof CheckBox){
                        ((CheckBox)view).setText(fieldValue);
                    }

                }
            }
            System.out.println("DATA: " + fieldData);
        }
        if(name == null) name = "Okänd";
        homePage.setView(5, name, homePage.getCatformtitles()[0], R.color.catform, R.color.catformDark);
        homePage.closeLoadingDialog();

    }
    */
}
