package kattcenter.norrtljekattcenter.catowner;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.PriviligeHolder;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.catform.IndexHolder;
import kattcenter.norrtljekattcenter.catlist.CatListDialog;
import kattcenter.norrtljekattcenter.catlist.ChosenCatFromListAdapter;
import kattcenter.norrtljekattcenter.dialog.BottomDialog;
import kattcenter.norrtljekattcenter.network.DataPacket;
import kattcenter.norrtljekattcenter.network.Headers;

/**
 * Created by Joakim on 2017-06-25.
 */

public class CatownerManager {

    private Context context;
    private HomePage homePage;
    private Type priv;
    private String mail;
    private CatListDialog catListDialog;
    private ChosenCatFromListAdapter chosenCatFromListAdapter, confirmChosenCatFromListAdapter;
    private RelativeLayout newUserFormContent;

    public CatownerManager(Context context){
        this.context = context;
        homePage = (HomePage) this.context;
        mail = null;
        initCatListDialog();
    }

    public CatownerManager(Context context, Type priv){
        this.context = context;
        homePage = (HomePage) this.context;
        this.priv = priv;
        mail = null;
    }


    public void initCatListDialog(){
        //catListDialog = new CatListDialog(this, (ListView) catOwnerCatsInfoPage.findViewById(R.id.catlist), (ListView)catOwnerSavePage.findViewById(R.id.pickedCatList), chosenCatFromListAdapter, confirmChosenCatFromListAdapter);
        RelativeLayout catOwnerCatsInfoPage = homePage.getCatOwnerCatsInfoPage();
        RelativeLayout catOwnerSavePage = homePage.getCatOwnerSavePage();
        initAdapters();
        catListDialog = new CatListDialog(context, (ListView) catOwnerCatsInfoPage.findViewById(R.id.catlist), (ListView)catOwnerSavePage.findViewById(R.id.pickedCatList), chosenCatFromListAdapter, confirmChosenCatFromListAdapter);
        homePage.setCatListDialog(catListDialog);
    }

    public void initAdapters(){
        chosenCatFromListAdapter = new ChosenCatFromListAdapter(context, new ArrayList<String[]>(), true);
        confirmChosenCatFromListAdapter = new ChosenCatFromListAdapter(context, new ArrayList<String[]>(), false);
    }

    public ChosenCatFromListAdapter getChosenCatFromListAdapter(){
        return chosenCatFromListAdapter;
    }


    public void selectCatOwner(String owner){
        homePage.findViewById(R.id.confirmSaveField).setVisibility(View.GONE);
        LinearLayout holder = ((LinearLayout)homePage.getCatOwnerInfo().findViewById(R.id.catowner_catlist_holder));
        holder.removeAllViews();
        initCatListDialog();
        String formTable = "catowner";
        String blub = formTable + ":" + new String(Base64.decode(owner.getBytes(), Base64.NO_WRAP)).split(":")[1];
        String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("form_action", "DOWNLOAD");
        data.put("blub", encoded_blub);
        HashMap<String, String> data2 = new HashMap<>();
        String blub2 = "CATOWNER_CATS:" + owner;
        String encoded_blub2 = Base64.encodeToString(blub2.getBytes(), Base64.NO_WRAP);
        data2.put("blub", encoded_blub2);
        try {
            homePage.setLoadCatownerCats(true);
            homePage.addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_LIST));
            homePage.addDatapacketToNetworkThread(new DataPacket(data2, Type.DOWNLOAD_LIST));
            homePage.signalQueueToSend();
            //((HomePage)context).showLoadingDialog("Laddar information...");
            homePage.getHomePageHandler().showStandardDialog("Laddar information...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /*
    public void selectCatOwner(String name, String email){
        if(email.equals("")){
            new BottomDialog(context).popup("Kan ej ladda information om: 'Kattcenter'");
        }
        else {
            homePage.findViewById(R.id.confirmSaveField).setVisibility(View.GONE);
            initCatListDialog();
            String formTable = "catowner";
            String blub = formTable + ":" + email;
            String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
            HashMap<String, String> data = new HashMap<>();
            data.put("form_action", "DOWNLOAD");
            data.put("blub", encoded_blub);
            HashMap<String, String> data2 = new HashMap<>();
            String blub2 = "CATOWNER_CATS:" + Base64.encodeToString((name+":"+email).getBytes(), Base64.NO_WRAP);
            String encoded_blub2 = Base64.encodeToString(blub2.getBytes(), Base64.NO_WRAP);
            data2.put("blub", encoded_blub2);
            try {
                homePage.addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_LIST));
                homePage.addDatapacketToNetworkThread(new DataPacket(data2, Type.DOWNLOAD_LIST));
                homePage.signalQueueToSend();
                //((HomePage)context).showLoadingDialog("Laddar information...");
                homePage.getHomePageHandler().showStandardDialog("Laddar information...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    */
    /*
    public void selectCatOwnerFromMail(String email){
        if(email.equals("")){
            new BottomDialog(context).popup("Kan ej ladda information om: 'Kattcenter'");
        }
        else {
            homePage.findViewById(R.id.confirmSaveField).setVisibility(View.GONE);
            initCatListDialog();
            String formTable = "catowner";
            String blub = formTable + ":" + email;
            String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
            HashMap<String, String> data = new HashMap<>();
            data.put("form_action", "DOWNLOAD");
            data.put("blub", encoded_blub);
            HashMap<String, String> data2 = new HashMap<>();
            String blub2 = "CATOWNER_CATS:" + email;
            String encoded_blub2 = Base64.encodeToString(blub2.getBytes(), Base64.NO_WRAP);
            data2.put("blub", encoded_blub2);
            try {
                homePage.addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_LIST));
                homePage.addDatapacketToNetworkThread(new DataPacket(data2, Type.DOWNLOAD_LIST));
                homePage.signalQueueToSend();
                //((HomePage)context).showLoadingDialog("Laddar information...");
                homePage.getHomePageHandler().showStandardDialog("Laddar information...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    */
    private void setAutoZipCodeField(final EditText editText){

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 3){
                    if(before == 0)
                        editText.setText(s+" ");
                    else
                        editText.setText(s.subSequence(0, 2));
                    editText.setSelection(editText.getText().length());
                }
                /*
                if(s.toString().length() == 3){
                    Log.i("HOMEPAGE", "APPENDING TO ZIP");
                    editTextZip.setText(editTextZip.getText()+" ");
                    editTextZip.append("");
                }
                Log.i("HOMEPAGE", "ZIP CODE COUNT TEXT: "+s.toString().length()+"  "+s.toString());
                */
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setAutoSSNField(final EditText editText){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //System.out.println("CHANGED: "+s+" "+start+" "+before+" "+count);

                if(s.length() == 6){
                    if(before == 0)
                        editText.setText(s+"-");
                    else
                        editText.setText(s.subSequence(0, 5));
                    editText.setSelection(editText.getText().length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void newCatownerForm(){
        final Dialog dialog = new Dialog(context);
        //RelativeLayout content = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.catowner_infopage, null);
        newUserFormContent = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.new_user_form, null);
        ((TextView)newUserFormContent.findViewById(R.id.new_user_title)).setText("Ny kattägare");
        dialog.setContentView(newUserFormContent);

        setAutoSSNField((EditText)newUserFormContent.findViewById(R.id.new_user_ssn_text));
        setAutoZipCodeField((EditText)newUserFormContent.findViewById(R.id.new_user_zipcode_text));

        Button sendLogin = (Button)newUserFormContent.findViewById(R.id.sendLogin);
        sendLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePage.sendLogin(dialog, "catowner");
            }
        });


        Button closeForm = (Button)newUserFormContent.findViewById(R.id.closeNewUserForm);
        closeForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }



    public void handleCatownerCats(){
        //catListDialog = new CatListDialog(this, (ListView) catOwnerCatsInfoPage.findViewById(R.id.catlist), (ListView)catOwnerSavePage.findViewById(R.id.pickedCatList), chosenCatFromListAdapter, confirmChosenCatFromListAdapter);
        //catListDialog = new CatListDialog(this, (ListView)catOwnerInfo.findViewById(R.id.catlist));
        //catListDialog = new CatListDialog(this, chosenCatownerCats, chosenCatFromListAdapter);
        /*
        (catOwnerCatsInfoPage.findViewById(R.id.pickCat)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catListDialog.viewPickerDialog();
                HashMap<String, String> data = new HashMap<>();
                //data.put("misc_action", "names_and_id");
                data.put("misc_action", "names_id_catowner");
                try {
                    networkThread.addDataToQueue(new DataPacket(data, Type.DOWNLOAD_MISC));
                    networkThread.nextInQueue();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        */
    }

    public void clearCatownerInfo(){
        ((EditText)homePage.getCatOwnerInfo().findViewById(R.id.catowner_ssn_text)).setText("");
        ((EditText)homePage.getCatOwnerInfo().findViewById(R.id.catowner_surname_text)).setText("");
        ((EditText)homePage.getCatOwnerInfo().findViewById(R.id.catowner_lastname_text)).setText("");
        ((EditText)homePage.getCatOwnerInfo().findViewById(R.id.catowner_address_text)).setText("");
        ((EditText)homePage.getCatOwnerInfo().findViewById(R.id.catowner_zipcode_text)).setText("");
        ((EditText)homePage.getCatOwnerInfo().findViewById(R.id.catowner_city_text)).setText("");
        ((EditText)homePage.getCatOwnerInfo().findViewById(R.id.catowner_cell_text)).setText("");
        ((EditText)homePage.getCatOwnerInfo().findViewById(R.id.catowner_mail_text)).setText("");
        (homePage.getCatOwnerInfo().findViewById(R.id.catowner_ssn_text)).setEnabled(true);
        (homePage.getCatOwnerInfo().findViewById(R.id.catowner_surname_text)).setEnabled(true);
        (homePage.getCatOwnerInfo().findViewById(R.id.catowner_lastname_text)).setEnabled(true);
        (homePage.getCatOwnerInfo().findViewById(R.id.catowner_address_text)).setEnabled(true);
        (homePage.getCatOwnerInfo().findViewById(R.id.catowner_zipcode_text)).setEnabled(true);
        (homePage.getCatOwnerInfo().findViewById(R.id.catowner_city_text)).setEnabled(true);
        (homePage.getCatOwnerInfo().findViewById(R.id.catowner_cell_text)).setEnabled(true);
        (homePage.getCatOwnerInfo().findViewById(R.id.catowner_mail_text)).setEnabled(true);
    }

    public void loadCatownerInfo(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            String id = jsonObject.getString("id");
            String ssn = new String(Base64.decode(jsonObject.getString("ssn").getBytes(), Base64.NO_WRAP));
            String surName = jsonObject.getString("surname");
            String lastName = jsonObject.getString("lastname");
            String address = jsonObject.getString("address");
            String zipcode = jsonObject.getString("zipcode");
            String city = jsonObject.getString("city");
            String cell = jsonObject.getString("cell");
            mail = jsonObject.getString("email");
            priv = homePage.getPrivileged();
            if(priv == Type.PERSONAL || priv == Type.ADMIN) {

                //System.out.println(((TextView)catowner_management_page.findViewById(R.id.catowner_name_text)));
                /*
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.catowner_ssn_text)).setText(ssn);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.catowner_surname_text)).setText(surName);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.catowner_lastname_text)).setText(lastName);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.catowner_address_text)).setText(address);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.catowner_zipcode_text)).setText(zipcode);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.catowner_city_text)).setText(city);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.catowner_cell_text)).setText(cell);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.catowner_mail_text)).setText(mail);
                (homePage.getCatOwnerInfo().findViewById(R.id.catowner_ssn_text)).setEnabled(false);
                (homePage.getCatOwnerInfo().findViewById(R.id.catowner_surname_text)).setEnabled(false);
                (homePage.getCatOwnerInfo().findViewById(R.id.catowner_lastname_text)).setEnabled(false);
                (homePage.getCatOwnerInfo().findViewById(R.id.catowner_address_text)).setEnabled(false);
                (homePage.getCatOwnerInfo().findViewById(R.id.catowner_zipcode_text)).setEnabled(false);
                (homePage.getCatOwnerInfo().findViewById(R.id.catowner_city_text)).setEnabled(false);
                (homePage.getCatOwnerInfo().findViewById(R.id.catowner_cell_text)).setEnabled(false);
                (homePage.getCatOwnerInfo().findViewById(R.id.catowner_mail_text)).setEnabled(false);
                */

                if(ssn.length() == 10 && ssn.charAt(6) != '-'){
                    ssn = ssn.substring(0, 6)+"-"+ssn.substring(6, ssn.length());
                }else if (ssn.length() != 11){
                    new BottomDialog(context).popup("Fel format av personnummer");
                }

                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_name)).setText(surName+" "+lastName);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_ssn)).setText(ssn);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_cell)).setText(cell);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_cell2)).setText(cell);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_mail)).setText(mail);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_street)).setText(address);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_zipcode)).setText(zipcode);
                ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_city)).setText(city);

                homePage.setView(IndexHolder.OWNER_INFO_PAGE, "Kattägare");

                //LinearLayout linearLayout = (LinearLayout) homePage.getCatOwnerInfo().findViewById(R.id.catowner_info_buttons);
                //linearLayout.setVisibility(View.VISIBLE);
                //homePage.getCatOwnerInfo().findViewById(R.id.catowner_info_buttons).setVisibility(View.VISIBLE);
                //System.out.println("CO: " + PriviligeHolder.getType());
                /*
                if (PriviligeHolder.getType() == Type.ADMIN) {
                    homePage.getCatOwnerInfo().findViewById(R.id.catowner_info_buttons).findViewById(R.id.manageCatownerCats).setVisibility(View.VISIBLE);
                } else if (PriviligeHolder.getType() == Type.PERSONAL) {
                    homePage.getCatOwnerInfo().findViewById(R.id.catowner_info_buttons).findViewById(R.id.manageCatownerCats).setVisibility(View.GONE);
                }
                */
                //homePage.getCatOwnerInfo().findViewById(R.id.catowners_cats).setVisibility(View.GONE);
                //homePage.getCatOwnerInfo().findViewById(R.id.catowner_info_buttons).setVisibility(View.VISIBLE);
                //homePage.setView(IndexHolder.OWNER_INFO_PAGE, "Kattägare", null, R.color.newColorPrimary, R.color.newColorPrimary);
            }
            else if(priv == Type.CATOWNER){
                HashMap<String, String> catOwnerInfoData = new HashMap<>();
                catOwnerInfoData.put("id", id);
                catOwnerInfoData.put("surname", surName);
                catOwnerInfoData.put("lastname", lastName);
                catOwnerInfoData.put("ssn", ssn);
                catOwnerInfoData.put("cell", cell);
                catOwnerInfoData.put("address", address);
                catOwnerInfoData.put("zipcode", zipcode);
                catOwnerInfoData.put("city", city);
                homePage.getHomePageHandler().updateSettingsPage(catOwnerInfoData);
                homePage.settingsLoaded();
            }
        } catch (JSONException jsone){
            jsone.printStackTrace();
        }
    }

    public void updateNumber(String cell){
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_cell)).setText(cell);
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_cell2)).setText(cell);
    }

    public void updateEmail(String email){
        mail = email;
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_mail)).setText(mail);
    }

    public void updateAddressInfo(String address, String zipcode, String city){
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_street)).setText(address);
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_zipcode)).setText(zipcode);
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_city)).setText(city);
    }

    public String getAddressInfo(){
        String address = "";
        address += ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_street)).getText().toString()+", ";
        address += ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_zipcode)).getText().toString()+", ";
        address += ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_city)).getText().toString();
        return address;
    }

    public void loadCatOwnerCatsInfo(String json_array){
        System.out.println("LOADING CATOWNER CATS......");
        String[] jsonRows = json_array.split("\n");
        LinearLayout holder = ((LinearLayout)homePage.getCatOwnerInfo().findViewById(R.id.catowner_catlist_holder));
        holder.removeAllViews();
        for(String json: jsonRows){
            try{
                if(json == null || json.isEmpty() || json.equals("null")) continue;
                JSONObject jsonObject = new JSONObject(json);
                String catcenter_id = jsonObject.getString("catcenter_id");
                String catcenter_name = jsonObject.getString("catcenter_name");
                String img_src = context.getString(R.string.serverimagesrc) + jsonObject.getString("img_src");

                LinearLayout list_entry = (LinearLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.new_catowner_catlist_entry, null);
                ((TextView)list_entry.findViewById(R.id.new_catowner_cat_name)).setText(catcenter_name);
                ((TextView)list_entry.findViewById(R.id.new_catowner_cat_id)).setText(catcenter_id);
                loadStatusText((TextView)list_entry.findViewById(R.id.new_catowner_cat_status), jsonObject);
                ImageView imageView = (ImageView)list_entry.findViewById(R.id.new_catowner_cat_thumbnail);
                loadSrcToImageView(imageView, img_src);

                holder.addView(list_entry);
            } catch (JSONException jsone){
                jsone.printStackTrace();
            }

        }
    }

    private void loadStatusText(TextView textView, JSONObject jsonObject) throws JSONException{
        String isPrivate = jsonObject.getString("isPrivateCat");
        String onCatHome = jsonObject.getString("onCatHome");
        String onCallCenter = jsonObject.getString("onCallCenter");
        String onTNR = jsonObject.getString("onTNR");
        String adopted = jsonObject.getString("adopted");
        String deceased = jsonObject.getString("deceased");
        String returnedOwner = jsonObject.getString("returnedOwner");
        String missing = jsonObject.getString("missing");
        String mother = jsonObject.getString("mother");
        String father = jsonObject.getString("father");
        String children = jsonObject.getString("children");
        String siblings = jsonObject.getString("siblings");

        String statusText = "";
        if(!isPrivate.isEmpty() && isPrivate.equals("yes")){
            statusText += "Privat katt | ";
        }
        if(isActiveStatus(onCatHome)) statusText += "Katthemskatt | ";
        if(isActiveStatus(onCallCenter)) statusText += "Jourhemskatt | ";
        if(isActiveStatus(onTNR)) statusText += "TNR | ";
        if(isActiveStatus(adopted)) statusText += "Adopterad | ";
        if(isActiveStatus(deceased)) statusText += "Avliden | ";
        if(isActiveStatus(returnedOwner)) statusText += "Åter ägaren | ";
        textView.setText(statusText);

    }
    private boolean isActiveStatus(String text){
        return (!text.isEmpty() && text.equals("1")) ? true : false;
    }

    private void loadSrcToImageView(final ImageView imageView, String url){
        Glide.with(context)
                .load(Headers.getUrlWithHeaders(context, url))
                .apply(RequestOptions.circleCropTransform())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.error_loading_image));
                        return false;
                    }


                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);
    }


    /*
    public void loadCatOwnerCatsInfo(String json_array){
        String[] jsonRows = json_array.split("\n");
        ArrayList<String[]> cats = new ArrayList<>();
        //Log.i("CATOWNERMANAGER", "JSON RECEIVED: "+json);
        try{
            for(String json: jsonRows){
                if(json != null && !json.isEmpty() && !json.equals("") && !json.equals("null")) {
                    Log.i("CATOWNERMANAGER", "ROW: "+json);
                    JSONObject jsonObject = new JSONObject(json);
                    String id = jsonObject.getString("catcenter_id");
                    String name = jsonObject.getString("name");
                    String img_src = jsonObject.getString("img_src");
                    String serverImgURL = homePage.getString(R.string.serverimagesrc);
                    cats.add(new String[]{name, id, serverImgURL+img_src});
                }
            }


        } catch (JSONException jsone){
            jsone.printStackTrace();
        }
        chosenCatFromListAdapter.setCatInfoList(cats);
        chosenCatFromListAdapter.notifyDataSetChanged();
        //homePage.getChosenCatFromListAdapter().setCatInfoList(cats);
        //homePage.getChosenCatFromListAdapter().notifyDataSetChanged();
        //RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, cats.size()*52);
        //layoutParams.addRule(RelativeLayout.BELOW, R.id.catlist_title);
        //homePage.getChosenCatownerCats().setLayoutParams(layoutParams);
    }
    */

    public String getMail(){
        return mail;
    }
}
