package kattcenter.norrtljekattcenter.catowner;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DataPacket;

/**
 * Created by Joakim on 2017-07-14.
 */

public class ChangeCatownerListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> catownerNames, catownerEmails, catownerStatuses;
    public ChangeCatownerListAdapter(Context context){
        this.context = context;
    }

    public void setCatownerNames(ArrayList<String> catownerNames){
        this.catownerNames = catownerNames;
    }

    public void setCatownerEmails(ArrayList<String> catownerEmails){
        this.catownerEmails = catownerEmails;
    }

    public void setCatownerStatuses(ArrayList<String> catownerStatuses){
        this.catownerStatuses = catownerStatuses;
    }

    public ArrayList<String> getCatOwnerNames(){
        return catownerNames;
    }

    public ArrayList<String> getCatOwnerEmails(){
        return catownerEmails;
    }

    public ArrayList<String> getCatOwnerStatuses(){
        return catownerStatuses;
    }
    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return catownerEmails.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return null;
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view;
        if(convertView == null){
            //view = ((Activity) context).getLayoutInflater().inflate(R.layout.catlist_item, null);
           // view = ((HomePage)context).getLayoutInflater().inflate(R.layout.catowner_listentry, null);
            System.out.println("THIS VIEW");
            view = ((HomePage)context).getLayoutInflater().inflate(R.layout.new_catowner_listentry, null);
        }
        else{
            view = convertView;
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Log.i("CATOWNERLISTADAPTER", "BACKGROUND ALREADY SET");
        }
        else{
            Log.i("CATOWNERLISTADAPTER", "RESETTING BACKGROUND");
            view.setBackground(ContextCompat.getDrawable(context, android.R.drawable.dialog_holo_light_frame));
        }
        TextView nameView = (TextView) view.findViewById(R.id.short_catowner_name);
        nameView.setText(catownerNames.get(position));
        ((TextView)view.findViewById(R.id.short_catowner_email)).setText(catownerEmails.get(position));
        ((TextView)view.findViewById(R.id.short_catowner_status)).setText(catownerStatuses.get(position));
        if(parent instanceof ListView){
            //Log.i("CHANGECATOWNER", "PARENT IS LISTVIEW");
            ((ListView) parent).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String chosenEmail = catownerEmails.get(position);
                    String chosenCatid = ((HomePage)context).getChosenCatFromListAdapter().getChosenCatId();
                    Dialog chooseCatownerFromListDialog = ((HomePage)context).getChosenCatFromListAdapter().getChooseCatownerDialog();
                    chooseCatownerFromListDialog.dismiss();
                    HashMap<String, String> data = new HashMap<>();
                    data.put("misc_action", "set_cat_owner");
                    String blub = Base64.encodeToString(new String(chosenEmail+":"+chosenCatid).getBytes(), Base64.NO_WRAP);
                    data.put("blub", blub);
                    try {
                        ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.UPLOAD_MISC));
                        ((HomePage)context).signalQueueToSend();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    /*
                    RelativeLayout relativeLayout = (RelativeLayout) parent.getParent();
                    Dialog dialogParent = (Dialog) relativeLayout.getParent();
                    dialogParent.dismiss();
                    */
                }
            });
        }
        else{
            Log.i("CHANGECATOWNER", "PARENT IS "+parent);
        }
        /*
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("CHANGECATOWNER", "CLICKED ITEM");
                //selectCatowner(catOwnerEmails.get(position));
            }
        });
        */
        return view;
    }
}
