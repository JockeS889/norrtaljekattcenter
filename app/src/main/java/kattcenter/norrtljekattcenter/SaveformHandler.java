package kattcenter.norrtljekattcenter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.BarcodeEAN;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.documentation.DocumentGenerator;

/**
 * Created by Jocke on 2017-06-08.
 */

public class SaveformHandler {

    private HashMap<String, Boolean> checkedForms = new HashMap<>();
    private HashMap<String, Boolean> checkedMailForms = new HashMap<>();
    private Context context;
    private HomePage activity;
    private DocumentGenerator documentGenerator;
    private Uri documentURI;
    public SaveformHandler(Context context){
        this.context = context;
        activity = (HomePage) context;
    }

    public void setCheckedForms(HashMap<String, Boolean> checkedForms){
        this.checkedForms = checkedForms;
    }

    public HashMap<String, Boolean> getCheckedForms(){
        return checkedForms;
    }

    public void setCheckedMailForms(HashMap<String, Boolean> checkedMailForms){
        this.checkedMailForms = checkedMailForms;
    }

    public HashMap<String, Boolean> getCheckedMailForms(){ return checkedMailForms;}


    /*
    public void mailForm() throws IOException {
        Uri fileURI = new DocumentGenerator().generate4(saveMedicationFormDocument());
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
// set the type to 'email'
        emailIntent.setType("vnd.android.cursor.dir/email");
        String to[] = {"jockesandberg21@gmail.com"};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
// the attachment
        emailIntent.putExtra(Intent.EXTRA_STREAM, fileURI);
// the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Cat medical info");
        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));


    }
    */


    public void initPDFDocumentGenerator(){
        documentGenerator = new DocumentGenerator(context);
        //documentURI = documentGenerator.createDocument();
    }

    public void addContentToPdfGenerator(String form_table, String content){
        documentGenerator.addContent(form_table, content);
    }

    public Uri pdf(HashMap<String, Boolean> checkedMailForms){
        //Uri fileURI = new DocumentGenerator(context).generatePDF(checkedMailForms);
        //return fileURI;
        return null;
    }



    public void mail2(){
        //documentGenerator.closeDocument();


        RelativeLayout settingsPage = ((HomePage)context).getSettingsPage();
        documentURI = documentGenerator.createDocument(settingsPage);
        ((HomePage)context).getHomePageHandler().closeStandardDialog();
        if(documentURI != null) {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            // set the type to 'email'
            emailIntent.setType("vnd.android.cursor.dir/email");
            String to[] = {"jockesandberg21@gmail.com"};    //CHANGE MAIL
            emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
            // the attachment
            emailIntent.putExtra(Intent.EXTRA_STREAM, documentURI);
            // the mail subject
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Test pdf info");
            context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }
        else{
            Log.i("SAVEFORMHANDLER", "NO DOCUMENT TO SEND");
        }

    }


    /*
    public void mail(HashMap<String, Boolean> checkedMailForms){
        //Uri fileURI = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/" + directory + "/" + fileName));
        //Uri fileURI = new DocumentGenerator(context).generatePDF(checkedMailForms);

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
// set the type to 'email'
        emailIntent.setType("vnd.android.cursor.dir/email");
        String to[] = {"jockesandberg21@gmail.com"};    //CHANGE MAIL
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
// the attachment
        emailIntent.putExtra(Intent.EXTRA_STREAM, fileURI);
// the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Test pdf info");
        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));

    }
    */


    public void mailForm2(){

        String fileName = "testPdf.pdf";
        String directory = "Test";
        Document document = new Document();

        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + directory;

        File dir = new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }

        File pdfFile = new File(dir, fileName);
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(pdfFile);
            PdfWriter pdfWriter = PdfWriter.getInstance(document, fileOutputStream);
            document.open();
            Paragraph paragraph = new Paragraph("Detta är en test rad");
            Font font = new Font(Font.FontFamily.COURIER);
            paragraph.setAlignment(Paragraph.ALIGN_CENTER);
            paragraph.setFont(font);
            PdfPTable pdfPTable = new PdfPTable(2);
            pdfPTable.setWidthPercentage(100);
            BarcodeEAN barcodeEAN = new BarcodeEAN();
            barcodeEAN.setCode("5809487595");
            PdfPCell cell = new PdfPCell(barcodeEAN.createImageWithBarcode(pdfWriter.getDirectContent(), BaseColor.BLACK, BaseColor.GRAY));
            pdfPTable.addCell(new PdfPCell(paragraph));
            pdfPTable.addCell(cell);
            document.add(pdfPTable);
            //mail(fileName, directory);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } finally {
            document.close();
        }




        /*
        Intent mailDoc = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        mailDoc.setType("text/plain");
        mailDoc.addCategory(Intent.CATEGORY_OPENABLE);
        mailDoc.putExtra(Intent.EXTRA_TITLE, "testCreated.txt");
        ((HomePage)context).startActivityForResult(mailDoc, 21);
        */
    }

    /*
    private Bundle saveMedicationFormDocument(){
        Bundle bundle = new Bundle();
        bundle.putString("Startdatum", ((TextView)activity.findViewById(R.id.catmed_start_date_text)).getText().toString());
        bundle.putString("Slutdatum", ((TextView)activity.findViewById(R.id.catmed_end_date_text)).getText().toString());
        bundle.putString("Läkemedel", ((TextView)activity.findViewById(R.id.catmed_type_text)).getText().toString());
        bundle.putString("Antal doser", ((TextView)activity.findViewById(R.id.catmed_dose_amount_text)).getText().toString());
        bundle.putString("Administrering", ((TextView)activity.findViewById(R.id.catmed_administration_text)).getText().toString());
        return bundle;
    }
    */
}
