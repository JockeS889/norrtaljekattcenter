package kattcenter.norrtljekattcenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.catform.AutoCompleteAdapter;
import kattcenter.norrtljekattcenter.catform.IndexHolder;
import kattcenter.norrtljekattcenter.dialog.BottomDialog;
import kattcenter.norrtljekattcenter.network.DataPacket;
import kattcenter.norrtljekattcenter.network.Headers;

/**
 * Created by Jocke on 2017-05-22.
 */

public class CatListImageAdapter extends BaseAdapter{
    private Context context;
    //private ArrayList<Bitmap> bitmapArrayList;
    //private HashMap<String, Bitmap> bitmapHashMap = new HashMap<>();
    private ArrayList<String> urlDataList;
    private ArrayList<String[]> catsInfo = new ArrayList<>();
    private ArrayList<String> urlDataListCopy;
    private ArrayList<String[]> catsInfoCopy = new ArrayList<>();
    private Type privileged;
    //private ImageLoader imageLoader;
    /*
    public ImageAdapter(Context context, ConnectionType connectionType){
        this.context = context;
        this.connectionType = connectionType;
    }
    */

    public void setCatsInfoCopy(ArrayList<String[]> catsInfoCopy){
        this.catsInfoCopy = catsInfoCopy;
    }

    public void setUrlDataListCopy(ArrayList<String> urlDataListCopy){
        this.urlDataListCopy = urlDataListCopy;
    }

    public ArrayList<String> getUrlDataListCopy(){
        return urlDataListCopy;
    }

    public ArrayList<String[]> getCatsInfoCopy(){
        return catsInfoCopy;
    }

    public CatListImageAdapter(Context context, Type privileged){
        this.context = context;
        this.privileged = privileged;
        //imageLoader = new ImageLoader(context);
    }

    public void setCatsInfo(ArrayList<String[]> catsInfo){
        this.catsInfo = catsInfo;
    }

    public ArrayList<String[]> getCatsInfo(){
        return catsInfo;
    }
    /*
    public void setBitmapList(ArrayList<Bitmap> bitmapList){
        this.bitmapArrayList = bitmapList;
    }

    public ArrayList<Bitmap> getBitmapArrayList(){
        return bitmapArrayList;
    }
    public void setCatsId(ArrayList<String> catsId){
        this.catsId = catsId;
    }

    public void setBitmapHashMap(String catid, Bitmap bitmap){
         bitmapHashMap.put(catid, bitmap);
    }
    */
    public int getTotalCats(){
        return catsInfo.size();
    }

    public void setUrlDataList(ArrayList<String> urlDataList){
        this.urlDataList = urlDataList;
    }

    public ArrayList<String> getUrlDataList(){
        return urlDataList;
    }

    @Override
    public int getCount() {
        return urlDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        //ViewHolderItem viewHolderItem;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //GridView.LayoutParams layoutParams = new GridView.LayoutParams(200,200);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(250, 250+200);

        View view;
        if(convertView == null){
            view = layoutInflater.inflate(R.layout.new_catlist_entry, null);
            //view = ((HomePage) context).getLayoutInflater().inflate(R.layout.catlist_item, null);
            //view = layoutInflater.inflate(R.layout.catlist_item, null);
            //viewHolderItem = new ViewHolderItem();
            //viewHolderItem.imageView = (ImageView) convertView.findViewById(R.id.short_catinfo_photo);
            //viewHolderItem.textView = (TextView) convertView.findViewById(R.id.short_catinfo_id);
            //onvertView.setTag(viewHolderItem);
        }
        else{
            view = convertView;
            //viewHolderItem = (ViewHolderItem) convertView.getTag();
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){

        }
        else{
            view.setBackground(ContextCompat.getDrawable(context, android.R.drawable.dialog_holo_light_frame));
        }

        Log.i("CATLISTIMAGEADAPTER", ((HomePage)context).getCurrentFlippedViewIndex()+" "+IndexHolder.CATLIST_PAGE);

        if(((HomePage)context).getCurrentFlippedViewIndex() == IndexHolder.CATLIST_PAGE || (privileged == Type.CATOWNER && ((HomePage)context).getCurrentFlippedViewIndex() == IndexHolder.HOMEPAGE_INDEX)) {
            final ImageView imageView = (ImageView) view.findViewById(R.id.short_catinfo_photo);
            imageView.setDrawingCacheEnabled(true);
            System.out.println(urlDataList.get(position));
            Glide.with(context)
                    .load(Headers.getUrlWithHeaders(context, urlDataList.get(position)))
                    .apply(RequestOptions.circleCropTransform())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            System.out.println("MY_GLIDEXCEPTION: "+e.getMessage());
                            imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.error_loading_image));
                            return false;
                        }


                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(imageView);

            String[] catInfo = catsInfo.get(position);
            final String catId = catInfo[0];
            final String catName = catInfo[1];
            System.out.println("CATLISTADAPTER:    CATID: "+catId+"    CATNAME: "+catName);

            ((TextView)view.findViewById(R.id.short_catinfo_id)).setText(catId);
            ((TextView)view.findViewById(R.id.short_catinfo_name)).setText(catName);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    switch (privileged) {
                        case ADMIN:
                        case PERSONAL:
                            //System.out.println("CAT BITMAP CACHE" + imageView.getDrawingCache());
                            //System.out.println("CATS: "+catsId);
                            System.out.println(position + " " + urlDataList.get(position) + " " + catId);
                            selectCat(catId, urlDataList.get(position), imageView.getDrawingCache(), false, false);
                            break;
                        case CATOWNER:
                            if(catId.equals("Privat katt")){
                                selectCat(catName, urlDataList.get(position), imageView.getDrawingCache(), true, true);
                            }
                            else{
                                selectCat(catId, urlDataList.get(position), imageView.getDrawingCache(), true, false);
                            }


                            break;
                        default:
                            break;
                    }
                }
            });
        }

        return view;
    }

    private void selectCat(String id, String fileName, Bitmap bitmap, boolean catowner, boolean privateCat){
        String formTable = "catform_basic";     /* ALWAYS INITIAL FORM TO LOAD WHEN CLICKED ON CAT IN LIST */
        String blub;
        String form_action;
        if(privateCat){
            String owner_name = ((TextView)((HomePage)context).getSettingsPage().findViewById(R.id.user_name_text)).getText().toString();
            String owner_mail = ((HomePage)context).getUserEmail();
            String owner = Base64.encodeToString((owner_name+":"+owner_mail).getBytes(), Base64.NO_WRAP);
            blub = formTable+":"+id+":"+owner;
            form_action = "DOWNLOAD_PRIVATE_CAT";
        }
        else{
            blub = formTable+":"+id;
            form_action = "DOWNLOAD";
        }
        String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("form_action", form_action);
        data.put("blub", encoded_blub);
        ((HomePage)context).setCurrentCatId(id);
        try {
            if(catowner){

            }
            else{
                ((HomePage)context).setNewCat(false);
                ((AutoCompleteTextView)((HomePage)context).findViewById(R.id.catbasic_catname_autocomplete_text)).setAdapter((AutoCompleteAdapter)null);
                //((HomePage)context).lockAllForms();


            }
            boolean isSaved = ((HomePage) context).getFileManager().saveImageToLocalStorage("current_cat.jpg", bitmap, true);
            Log.i("CATLISTADAPTER", "BITMAP: "+bitmap.toString()+" SAVED: "+isSaved);
            if(!isSaved){
                new BottomDialog(context).popup("Ett fel uppstod vid nerladdning av bild");
            }
            ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_FORM));
            ((HomePage)context).signalQueueToSend();
            //((HomePage)context).showLoadingDialog("Laddar formulär...");
            ((HomePage)context).getHomePageHandler().showStandardDialog("Laddar formulär...");
            ((HomePage)context).setCurrentViewedCatImageFileName(fileName.split("/")[fileName.split("/").length-1]);
            if(((HomePage)context).getPrivileged() != Type.CATOWNER)
                ((HomePage)context).resetFormIndex();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Toast.makeText(context, "[INTERRUPTED]", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context, "[I/O]", Toast.LENGTH_SHORT).show();
        }
        //new DatabaseConnection(context, data, Type.DOWNLOAD_FORM).execute();

    }
}
