package kattcenter.norrtljekattcenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import kattcenter.norrtljekattcenter.catform.AutoCompleteAdapter;
import kattcenter.norrtljekattcenter.catform.BundleResourceMapping;
import kattcenter.norrtljekattcenter.catform.FormFieldMapper;
import kattcenter.norrtljekattcenter.catform.IndexHolder;
import kattcenter.norrtljekattcenter.catform.ResourceMapper;
import kattcenter.norrtljekattcenter.catform.TimeLine;
import kattcenter.norrtljekattcenter.catlist.CatListDialog;
import kattcenter.norrtljekattcenter.dialog.BottomDialog;
import kattcenter.norrtljekattcenter.network.Headers;

/**
 * Created by Joakim on 2017-07-02.
 */

public class LoadPreviewHandler {

    private Context context;
    private HomePage homePage;

    private ArrayList<String> catvetdoc_forms = new ArrayList<>();
    private HashMap<String, String> rMapper = new HashMap<>();


    /* VETDOC VARIABLES */
    String date = null;

    public LoadPreviewHandler(Context context){
        this.context = context;
        homePage = ((HomePage) context);

        /*
        String[] parents = context.getResources().getStringArray(R.array.catvet_items);

        catvetdoc_forms.add("catform_vetdoc_generalstate");
        catvetdoc_forms.add("catform_vetdoc_lynne");
        catvetdoc_forms.add("catform_vetdoc_skinpaws");
        catvetdoc_forms.add("catform_vetdoc_lymf");
        catvetdoc_forms.add("catform_vetdoc_eyes");
        catvetdoc_forms.add("catform_vetdoc_ears");
        catvetdoc_forms.add("catform_vetdoc_mouth");
        catvetdoc_forms.add("catform_vetdoc_abdomen");
        catvetdoc_forms.add("catform_vetdoc_circulationorgan");
        catvetdoc_forms.add("catform_vetdoc_respirationorgan");
        catvetdoc_forms.add("catform_vetdoc_outergenitalias");
        catvetdoc_forms.add("catform_vetdoc_locomotiveorgan");


        for(int i = 0; i < catvetdoc_forms.size(); i++){
            rMapper.put(catvetdoc_forms.get(i), parents[i]);
        }
        */
    }

    /*

    public void loadForm(String form_table, String content){
        ResourceMapper resourceMapper = new ResourceMapper();
        switch (form_table){
            case "catform_basic":
                resourceMapper.initBasic();
                break;
            case "catform_medical":
                resourceMapper.initMedical();
                break;
            case "catform_vaccination":
                resourceMapper.initVaccination();
                break;
            case "catform_deworming":
                resourceMapper.initDeworming();
                break;
            case "catform_verm":
                resourceMapper.initVermin();
                break;
            case "catform_claws":
                resourceMapper.initClaw();
                break;
            case "catform_weight":
                resourceMapper.initWeight();
                break;
            case "catform_regskk":
                resourceMapper.initRegSKK();
                break;
            case "catform_vet":
                resourceMapper.initVet();
                break;
            case "catform_insurance":
                resourceMapper.initInsurance();
                break;
            case "catform_background":
                resourceMapper.initBackground();
                break;
            case "catform_prevowner":

                break;
            case "catform_status":
                resourceMapper.initStatus();
                break;
            default: break;
        }
        boolean isCatVetDoc = false;
        try {
            for (String catvetdoc : catvetdoc_forms) {
                if (catvetdoc.equals(form_table)) {
                    String json = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
                    Log.i(TagHolder.JSON_TAG, "json = "+json);
                    if(json != null && !json.equals("") && !json.equals("null")) {
                        JSONObject jsonObject = new JSONObject(json);
                        Iterator<String> keys = jsonObject.keys();
                        System.out.println("TABLE: " + form_table);
                        while (keys.hasNext()) {
                            String key = keys.next();
                            String value = jsonObject.getString(key);
                            System.out.println("[JSON] KEY: " + key + " VALUE: " + value);
                            if (key.equals("date")) {
                                ((TextView) homePage.findViewById(R.id.catvet_reg_date_text)).setText(value);
                            } else if (key.equals("note")) {
                                homePage.updateCatvetregComment(rMapper.get(form_table), value);
                            } else if (!(key.equals("id") || key.equals("catcenter_id"))) {
                                if (key.equals("without_remark")) {
                                    key = "U.a.";
                                } else if (key.equals("general_without_remark")) {
                                    key = "A.T. u.a.";
                                } else if (key.equals("genererally_low")) {
                                    key = "AT. Nedsatt";
                                } else {
                                    if (form_table.equals("catform_vetdoc_skinpaws"))
                                        key = key.replace("_", "/");
                                    else
                                        key = key.replace("_", " ");
                                }
                                String spinnerParent = rMapper.get(form_table);
                                boolean isChecked = (value.equals("1")) ? true : false;
                                homePage.updateRecord(spinnerParent, key, isChecked);
                            }
                        }
                        //System.out.println("[JSON]: " + json);

                    }
                    isCatVetDoc = true;
                }
            }
            homePage.refreshSpinnerClick();
        } catch (JSONException jsone){
            jsone.printStackTrace();
        }
        if(!isCatVetDoc) {
            load(form_table, content, resourceMapper);
        }
        //homePage.closeLoadingDialog();
        homePage.getHomePageHandler().closeStandardDialog();
    }

*/

    public void clearPreview(){
        ResourceMapper resourceMapper = new ResourceMapper();
        resourceMapper.initPreview();
        homePage.findViewById(R.id.preview_status_text).setVisibility(View.VISIBLE);
        LinearLayout content_holder = (LinearLayout)homePage.findViewById(resourceMapper.getResource("content_holder"));
        content_holder.removeAllViews();
        Log.i("LOADPREVIEW", "CLEARING CONTENT");
    }

    public void loadPreview(String form_type, String content){
        if(!homePage.getLoadPreview()) return;
        ResourceMapper resourceMapper = new ResourceMapper();
        resourceMapper.initPreview();
        if(homePage.getPrivileged() != Type.CATOWNER) {
            homePage.findViewById(R.id.preview_status_text).setVisibility(View.GONE);
        }
        LinearLayout content_holder = (LinearLayout) homePage.findViewById(resourceMapper.getResource("content_holder"));
        if (!homePage.getLoadCatVetItems())
            content_holder.removeAllViews();

        if(form_type.equals("cats")){
            Log.i("PREVIEWHANDLER", "LENGTHH: "+content.length());
            for(String row: content.split("\n")){
                //Log.i("PREVIEWHANDLER", "CONTENTROW: "+row);
            }
            loadCatownerHistoryPreview(content);
        }
        else if(form_type.equals("catform_connection")){
            Log.i("LOADPREVIEWHANDLER", "CONTENT: "+content);
            String decoded = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
            String[] parts = decoded.split(":", -1);

            RelativeLayout connectionPreviewForm = (RelativeLayout) homePage.getLayoutInflater().inflate(R.layout.new_catconnection_page, null);
            loadCatconnectionPreview(connectionPreviewForm, parts);

            /*
            RelativeLayout connectionPreviewForm = (RelativeLayout) homePage.getLayoutInflater().inflate(R.layout.cat_connection_information, null);
            if(!mother.equals(""))
                ((TextView)connectionPreviewForm.findViewById(R.id.catconnection_preview_mother_text)).setText(new String(Base64.decode(mother.getBytes(), Base64.NO_WRAP)));
            if(!father.equals(""))
                ((TextView)connectionPreviewForm.findViewById(R.id.catconnection_preview_father_text)).setText(new String(Base64.decode(father.getBytes(), Base64.NO_WRAP)));
            siblings = new String(Base64.decode(siblings.getBytes(), Base64.NO_WRAP));
            Log.i("LOADPREVIEW", "SIBLINGS: "+siblings);
            if(!siblings.equals("[]") && !siblings.equals("")) {
                Log.i("LOADPREVIEW", "SETTING SIBLINGS TEXT");
                siblings = siblings.replaceAll("\\[", "");
                siblings = siblings.replaceAll("]", "");
                String[] allSiblings = siblings.split(",");
                String textSibling = "";
                for(String sibling: allSiblings){
                    textSibling += sibling + "\n";
                }
                ((TextView) connectionPreviewForm.findViewById(R.id.catconnection_siblings_text)).setText(textSibling);
            }
            children = new String(Base64.decode(children.getBytes(), Base64.NO_WRAP));
            Log.i("LOADPREVIEW", "CHILDREN: "+children);
            if(!children.equals("[]") && !children.equals("")) {
                Log.i("LOADPREVIEW", "SETTING CHILDREN TEXT");
                children = children.replaceAll("\\[", "");
                children = children.replaceAll("]", "");
                String[] allChildren = children.split(",");
                String text = "";
                for(String child: allChildren){
                    text += child + "\n";
                }
                ((TextView) connectionPreviewForm.findViewById(R.id.catconnection_children_text)).setText(text);
            }
            */
            content_holder.addView(connectionPreviewForm);
        }
        else {
            String json = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
            String name = null;
            if (!json.equals("null")) {
                if (form_type.equals("catform_basic")) {
                    resourceMapper.initBasicPreview();
                    LinearLayout basicPreviewForm = (LinearLayout) homePage.getLayoutInflater().inflate(R.layout.catform_basic_preview_content, null);
                    content_holder.addView(basicPreviewForm);
                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        Iterator<String> iterator = jsonObject.keys();
                        while (iterator.hasNext()) {
                            String key = iterator.next();
                            final String value = jsonObject.getString(key);
                            if (key.equals("photo_src")) {
                                if(value.isEmpty()){
                                    ImageView imageView = (ImageView) content_holder.findViewById(R.id.catpreview_cat_image);
                                    imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.error_loading_image));
                                }
                                else {
                                    File currentCatImageFile;
                                    if (homePage.getNewCat()) {
                                        currentCatImageFile = homePage.getFileManager().getLatestTakenPicture();
                                    } else {
                                        currentCatImageFile = homePage.getFileManager().getCurrentViewedCatPicture();
                                    }
                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                                    try {
                                        Bitmap imageBitmap = BitmapFactory.decodeFile(currentCatImageFile.getAbsolutePath(), options);
                                        Log.i("PREVIEWHANDLER", "BITMAP: " + imageBitmap);
                                        final ImageView imageView = (ImageView) content_holder.findViewById(R.id.catpreview_cat_image);
                                        String url = context.getString(R.string.serverimagesrc) + "" + value;
                                        homePage.setCurrentCatImgSrc(value);

                                        imageView.setImageBitmap(imageBitmap);
                                        imageView.setDrawingCacheEnabled(true);
                                        imageView.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Log.i("LOADPREVIEW", "DOWNLOADING IMAGE AGAIN");
                                                String url = context.getString(R.string.serverimagesrc) + "" + value;
                                                reloadImage(imageView, url);
                                            }
                                        });
                                        //imageView.setBackground(context.getResources().getDrawable(R.drawable.circular_border, null));
                                        reloadImage(imageView, url);
                                    } catch (Exception exp) {
                                        Toast.makeText(context, "Fel vid inladdning av bild", Toast.LENGTH_SHORT).show();
                                        exp.printStackTrace();
                                    }
                                }
                            }
                            else{
                                if(resourceMapper.hasResource(key)){
                                    ((TextView)basicPreviewForm.findViewById(resourceMapper.getResource(key))).setText(value);
                                }
                            }
                            if (key.equals("catcenter_name")) homePage.setCurrentViewedCatname(value);
                        }
                    } catch (JSONException jsone) {
                        jsone.printStackTrace();
                    }
                    int index = homePage.getPrivileged() == Type.CATOWNER ? IndexHolder.CATOWNER_CAT_PREVIEW_PAGE : IndexHolder.FORM_PREVIEW_PAGE;
                    homePage.setView(index, homePage.getCatformtitles()[0]);
                    //homePage.setView(IndexHolder.FORM_PREVIEW_PAGE, name, homePage.getCatformtitles()[0], R.color.newColorPrimary, R.color.newColorPrimary);
                    homePage.setNewCat(false);
                }
                else {
                    RelativeLayout timeLineHolder = (RelativeLayout) homePage.getLayoutInflater().inflate(R.layout.catpreview_updated_history_list, null);
                    LinearLayout timeLine;
                    if(homePage.getLoadCatVetItems()){
                        Log.i("LOADPREVIEWHANDLER", "CONTENT HOLDER HOLDS: "+content_holder.getChildCount()+" ITEMS");
                        //ArrayList<View> timeLines = new TimeLine(context).createTimeLineFromHistory(content, form_type);
                        if(content_holder.getChildCount() == 0){
                            timeLine = (LinearLayout) timeLineHolder.findViewById(R.id.timeLine);
                            new TimeLine(context).createTimeLineCatVetItems(content, form_type, timeLine);
                            /*
                            for (View view : timeLines) {
                                Log.i("PREVIEWHANDLER", "VIEW: " + view);
                                timeLine.addView(view);
                            }
                            */
                            content_holder.addView(timeLineHolder);
                        }
                        else{
                            timeLineHolder = (RelativeLayout)content_holder.getChildAt(0);
                            timeLine = (LinearLayout) timeLineHolder.findViewById(R.id.timeLine);
                            new TimeLine(context).createTimeLineCatVetItems(content, form_type, timeLine);
                        }
                    }
                    /*
                    HashMap<String, String> catVetItems = new BundleResourceMapping().getCatVetItemTable();
                    if(catVetItems.containsKey(form_type)){
                        

                    }
                    */
                    else{
                        timeLine = (LinearLayout) timeLineHolder.findViewById(R.id.timeLine);
                        ArrayList<View> timeLines = new TimeLine(context).createTimeLineFromHistory(content, form_type);
                        for (View view : timeLines) {
                            //Log.i("PREVIEWHANDLER", "VIEW: " + view);
                            timeLine.addView(view);
                        }
                        content_holder.addView(timeLineHolder);
                    }


                }
            }
        }
    }

    private void activeCat(TextView nameView, TextView idView, String name, String id){
        nameView.setVisibility(View.VISIBLE);
        nameView.setText(name);
        idView.setVisibility(View.VISIBLE);
        idView.setText(id);
    }

    private void inactiveCat(TextView nameView, TextView idView){
        nameView.setVisibility(View.GONE);
        idView.setVisibility(View.GONE);
    }

    public void loadCatconnectionPreview(RelativeLayout contentHolder, String[] family_members){
        String mother = family_members[0];
        String father = family_members[1];
        String siblings = family_members[2];
        String children = family_members[3];
        String url = context.getString(R.string.serverimagesrc);
        reloadImage((ImageView)contentHolder.findViewById(R.id.this_cat), url+homePage.getCurrentCatImgSrc());
        ImageView mother_imageview = (ImageView)contentHolder.findViewById(R.id.cat_mother);
        ImageView father_imageview = (ImageView)contentHolder.findViewById(R.id.cat_father);
        ImageView sibling_imageview = (ImageView)contentHolder.findViewById(R.id.add_sibling).findViewById(R.id.sibling_cat);
        ImageView child_imageview = (ImageView)contentHolder.findViewById(R.id.add_child).findViewById(R.id.child_cat);
        if(mother.isEmpty()){
            mother_imageview.setOnClickListener(new MyAddCatConnectionListener(mother_imageview, contentHolder, "mother"));
            inactiveCat((TextView)contentHolder.findViewById(R.id.mother_cat_name), (TextView)contentHolder.findViewById(R.id.mother_cat_id));
        }
        else{
            mother = new String(Base64.decode(mother.getBytes(), Base64.NO_WRAP));
            String[] mother_info = mother.split(":");
            String[] mother_name_id = (new String(Base64.decode(mother_info[0].getBytes(), Base64.NO_WRAP))).split(":");
            String mother_name = mother_name_id[0];
            String mother_id = mother_name_id[1];
            String mother_url = mother_info[1];
            activeCat((TextView)contentHolder.findViewById(R.id.mother_cat_name),
                    (TextView)contentHolder.findViewById(R.id.mother_cat_id),
                    mother_name,
                    mother_id);
            reloadImage(mother_imageview, url+mother_url);
            mother_imageview.setOnClickListener(new MySelectCatConnectionListener(contentHolder));
        }
        if(father.isEmpty()) {
            father_imageview.setOnClickListener(new MyAddCatConnectionListener(father_imageview, contentHolder, "father"));
        }
        else{
            father = new String(Base64.decode(father.getBytes(), Base64.NO_WRAP));
            String[] father_info = father.split(":");
            String[] father_name_id = (new String(Base64.decode(father_info[0].getBytes(), Base64.NO_WRAP))).split(":");
            String father_name = father_name_id[0];
            String father_id = father_name_id[1];
            String father_url = father_info[1];
            activeCat((TextView)contentHolder.findViewById(R.id.father_cat_name),
                    (TextView)contentHolder.findViewById(R.id.father_cat_id),
                    father_name,
                    father_id);
            reloadImage(father_imageview, url+father_url);

        }
        if(siblings.isEmpty()){

        }
        else{
            siblings = new String(Base64.decode(siblings.getBytes(), Base64.NO_WRAP));
            String[] sibling_array = siblings.split(":");
            for(String sibling: sibling_array){
                String[] kitten_info = (new String(Base64.decode(sibling.getBytes(), Base64.NO_WRAP))).split(":");
                addCatconnectionSibling(contentHolder, kitten_info, url);
            }
        }
        if(children.isEmpty()){
            //child_imageview.setOnClickListener(new MyAddCatConnectionListener(father_imageview, contentHolder));
        }
        else{
            children = new String(Base64.decode(children.getBytes(), Base64.NO_WRAP));
            String[] kittens = children.split(":");
            for(String kitten: kittens){
                String[] kitten_info = (new String(Base64.decode(kitten.getBytes(), Base64.NO_WRAP))).split(":");
                addCatconnectionKitten(contentHolder, kitten_info, url);
            }
        }
        sibling_imageview.setOnClickListener(new MyAddCatConnectionListener(contentHolder, "sibling"));
        child_imageview.setOnClickListener(new MyAddCatConnectionListener(contentHolder, "child"));
    }

    private class MyAddCatConnectionListener implements View.OnClickListener{

        private ImageView selectedView;
        private RelativeLayout contentHolder;
        private String type;

        public MyAddCatConnectionListener(ImageView selectedView, RelativeLayout contentHolder, String type){
            this.selectedView = selectedView;
            this.contentHolder = contentHolder;
            this.type = type;
        }
        public MyAddCatConnectionListener(RelativeLayout contentHolder, String type){
            this.contentHolder = contentHolder;
            this.type = type;
        }
        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            CatListDialog catListDialog = new CatListDialog(context);
            homePage.setCatListDialog(catListDialog);
            homePage.getCatListDialog().viewDialog(selectedView, contentHolder, type);
        }
    }

    private class MySelectCatConnectionListener implements View.OnClickListener{

        private RelativeLayout contentHolder;

        public MySelectCatConnectionListener(RelativeLayout contentHolder){
            this.contentHolder = contentHolder;
        }
        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {

        }
    }

    private void addCatconnectionKitten(RelativeLayout contentHolder, String[] kitten_info, String url){
        String kitten_name = kitten_info[0];
        String kitten_id = kitten_info[1];
        String kitten_url = kitten_info[2];
        LinearLayout new_kitten_entry = (LinearLayout) homePage.getLayoutInflater().inflate(R.layout.new_catconnection_child_entry, null);
        reloadImage((ImageView)new_kitten_entry.findViewById(R.id.child_cat), url+kitten_url);
        LinearLayout kitten_holder = (LinearLayout) contentHolder.findViewById(R.id.child_holder);

        activeCat((TextView)new_kitten_entry.findViewById(R.id.child_cat_name),
                (TextView)new_kitten_entry.findViewById(R.id.child_cat_id),
                kitten_name,
                kitten_id);
        kitten_holder.addView(new_kitten_entry, 0);
    }

    private void addCatconnectionSibling(RelativeLayout contentHolder, String[] sibling_info, String url){
        String sibling_name = sibling_info[0];
        String sibling_id = sibling_info[1];
        String sibling_url = sibling_info[2];
        LinearLayout new_sibling_entry = (LinearLayout) homePage.getLayoutInflater().inflate(R.layout.new_catconnection_sibling_entry, null);
        reloadImage((ImageView)new_sibling_entry.findViewById(R.id.sibling_cat), url+sibling_url);
        LinearLayout sibling_holder = (LinearLayout) contentHolder.findViewById(R.id.sibling_holder);

        activeCat((TextView)new_sibling_entry.findViewById(R.id.sibling_cat_name),
                (TextView)new_sibling_entry.findViewById(R.id.sibling_cat_id),
                sibling_name,
                sibling_id);
        sibling_holder.addView(new_sibling_entry, 0);
        LinearLayout childConnectorHolder = (LinearLayout) contentHolder.findViewById(R.id.child_connector_holder);
        View childConnector = homePage.getLayoutInflater().inflate(R.layout.new_child_connector, null);
        childConnectorHolder.addView(childConnector);
    }


    private void reloadImage(final ImageView imageView, String url){
        Glide.with(context)
                .load(Headers.getUrlWithHeaders(context, url))
                .apply(RequestOptions.circleCropTransform())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.error_loading_image));
                        new BottomDialog(context).popup("Ett fel uppstod vid nerladdning av bild");
                        return false;
                    }


                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);

    }

    private void loadCatownerHistoryPreview(String json_decoded){

        ResourceMapper resourceMapper = new ResourceMapper();
        resourceMapper.initPreview();
        LinearLayout content_holder = (LinearLayout)homePage.findViewById(resourceMapper.getResource("content_holder"));
        //LinearLayout content_holder2 = new LinearLayout(context);
        //content_holder2.setBackgroundColor(Color.argb(255, 200, 100, 100));
        //content_holder2.setBackground(ContextCompat.getDrawable(context, R.drawable.background_gradient_2));
        //RelativeLayout background = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.catowner_timeline_background, null);

        content_holder.removeAllViews();
        String[] json_rows = json_decoded.split("\n");
        for(String json: json_rows){
            if(!json.equals("null")) {
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    String catcenter_id = jsonObject.getString("catcenter_id");
                    if (catcenter_id.equals(homePage.getCurrentCatId())) {
                        String current_owner = jsonObject.getString("owner");
                        String current_owner_date_time = jsonObject.getString("owner_date");
                        String previous_owner = jsonObject.getString("previous_owner");

                        RelativeLayout timeLineHolder = (RelativeLayout) homePage.getLayoutInflater().inflate(R.layout.catpreview_updated_history_list, null);
                        LinearLayout timeLine = (LinearLayout) timeLineHolder.findViewById(R.id.timeLine);
                        //timeLine.removeAllViews();
                        ArrayList<View> timeLines = new TimeLine(context).createTimeLineFromPreviousCatowners(current_owner, current_owner_date_time, previous_owner);
                        Log.i("PREVIEWHANDLER", current_owner+" "+current_owner_date_time+" "+previous_owner + "  TOTAL VIEWS: "+timeLines.size());
                        for (View view : timeLines) {
                            Log.i("PREVIEWHANDLER", "VIEW: " + view);
                            timeLine.addView(view);
                        }
                        content_holder.addView(timeLineHolder);
                    }
                } catch (JSONException jsone) {
                    jsone.printStackTrace();
                }
            }
        }
        //content_holder2.addView(background);
        //background.addView(content_holder2);
        //content_holder.addView(content_holder2);

    }


    public void loadPreview2(String form_type, String content){
        if(!homePage.getLoadPreview()) return;
        ResourceMapper resourceMapper = new ResourceMapper();
        resourceMapper.initPreview();
        LinearLayout content_holder = (LinearLayout)homePage.findViewById(resourceMapper.getResource("content_holder"));
        LinearLayout timeLine;
        if(form_type.equals("catform_vetdoc_generalstate") || !isVetdocForm(form_type))
                content_holder.removeAllViews();
        if(form_type.equals("catform_basic")){
            LinearLayout basicPreviewForm = (LinearLayout) homePage.getLayoutInflater().inflate(R.layout.catform_basic_preview_content, null);
            content_holder.addView(basicPreviewForm);

        }
        else{
            timeLine = (LinearLayout) homePage.getLayoutInflater().inflate(R.layout.catpreview_updated_history_list, null);
            content_holder.addView(timeLine);
        }
        //(homePage.findViewById(R.id.preview_status_text)).setVisibility(View.VISIBLE);
        String json = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
        String name = "Okänd";
        if(!json.equals("null")){
            if(form_type.equals("catform_basic")){
                try{
                    JSONObject jsonObject = new JSONObject(json);
                    Iterator<String> iterator = jsonObject.keys();
                    while(iterator.hasNext()){
                        String key = iterator.next();
                        if(key.equals("photo_src")){
                            File currentCatImageFile;
                            if (homePage.getNewCat()) {
                                currentCatImageFile = homePage.getFileManager().getLatestTakenPicture();
                            } else {
                                currentCatImageFile = homePage.getFileManager().getCurrentViewedCatPicture();
                            }
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                            Bitmap imageBitmap = BitmapFactory.decodeFile(currentCatImageFile.getAbsolutePath(), options);
                            Log.i("PREVIEWHANDLER", "BITMAP: "+imageBitmap);
                            ImageView imageView = (ImageView) content_holder.findViewById(R.id.catpreview_cat_image);
                            imageView.setBackground(context.getResources().getDrawable(R.drawable.circular_border_white, null));
                            imageView.setImageBitmap(imageBitmap);
                        }
                    }
                } catch (JSONException jsone){
                    jsone.printStackTrace();
                }
            }
            else {
                (homePage.findViewById(R.id.preview_status_text)).setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    Iterator<String> iterator = jsonObject.keys();

                    BundleResourceMapping bundleResourceMapping = new BundleResourceMapping();
                    FormFieldMapper formFieldMapper = new FormFieldMapper();
                    HashMap<String, String> translationTable = bundleResourceMapping.getFormTableTranslated(form_type);
                    String birthday_component = null;
                    String neuter_component = null;
                    String tatoo_component = null;
                    String chip_component = null;
                    while (iterator.hasNext()) {
                        String key = iterator.next();
                        if (key.equals("id") || key.equals("table_name") || key.equals("start_date_reminded") || key.equals("repeat_date_reminded"))
                            continue;
                        if (!form_type.equals("catform_basic") && key.equals("catcenter_id"))
                            continue;
                        if (form_type.equals("catform_medical") && key.equals("repeat_date"))
                            continue;
                        String value = jsonObject.getString(key);
                        //System.out.println("KEY: "+key+"   VALUE: "+jsonObject.getString(key));
                        if (key.equals("name")) name = value;
                        if (key.equals("photo_src")) {
                            File currentCatImageFile;
                            if (homePage.getNewCat()) {
                                currentCatImageFile = homePage.getFileManager().getLatestTakenPicture();
                            } else {
                                currentCatImageFile = homePage.getFileManager().getCurrentViewedCatPicture();
                            }
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                            ImageView imageView = new ImageView(context);
                            try {
                                imageView.setImageBitmap(BitmapFactory.decodeFile(currentCatImageFile.getAbsolutePath(), options));
                                //if(homePage.getNewCat()){
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(500, 500);
                                layoutParams.gravity = Gravity.CENTER;
                                //imageView.setLayoutParams(new LinearLayout.LayoutParams(250, 250));
                                imageView.setLayoutParams(layoutParams);
                                //}
                                imageView.setTag("SELECTED_PHOTO");
                                content_holder.addView(imageView);
                                System.out.println("[ELSE] KEY: " + key + "   VALUE: " + value + "   VIEW: " + imageView);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Kunde inte ladda bild till förhandsvy!", Toast.LENGTH_SHORT).show();
                            }

                        } else {

                            String text;
                            if (isVetdocForm(form_type)) {
                                if (key.equals("date")) {
                                    TextView textView = new TextView(context);
                                    textView.setText(bundleResourceMapping.getCatVetItemTable().get(form_type));
                                    textView.setTextSize(22);
                                    textView.setTextColor(Color.parseColor("#000000"));
                                    content_holder.addView(textView);
                                    if (date == null) {
                                        date = value;
                                        text = date;
                                    } else {
                                        continue;
                                    }

                                } else if (key.equals("note")) {
                                    text = "Kommentar: " + value;
                                } else {
                                    String newKey;
                                    if (key.equals("without_remark")) newKey = "Utan anmärkning";
                                    else if (key.equals("generally_low")) newKey = "AT. Nedsatt";
                                    else if (key.equals("general_without_remark"))
                                        newKey = "AT. u.a.";
                                    else newKey = key.replaceAll("_", " ");
                                    text = newKey + ((value.equals("1")) ? ": Ja" : ": Nej");
                                }
                                //Log.i("LOADPREVIEWHANDLER", "VETDOC: " + key + " " + value);
                                //text = value;
                            } else {
                                if (translationTable != null && translationTable.containsKey(key)) {
                                    String translation_key = translationTable.get(key);
                                    if (formFieldMapper.containsTranslation(translation_key))
                                        text = formFieldMapper.getTranslation(translation_key) + ": " + value;
                                    else
                                        text = value;
                                } else
                                    text = value;
                                //Log.e("LOADPREVIEWHANLDER", "Key: " + key + "  TEXT: " + text);
                            }


                            if (key.equals("birthdate")) {
                                System.out.println("FIRST PART OF BIRTHDATE COMP");
                                birthday_component = text;
                                continue;
                            } else if (key.equals("birthdate_guess")) {
                                System.out.println("COMP: " + birthday_component);
                                birthday_component += " (" + text + ")";
                                text = birthday_component;
                            }
                            if (key.equals("neuter")) {
                                neuter_component = "Kastrerad: ";
                                continue;
                            } else if (key.equals("neuter_date")) {
                                String[] strings = text.split(" ");
                                System.out.println("L: " + strings.length);
                                if (strings.length == 3)
                                    if (!text.equals("Kastrerad"))
                                        neuter_component += text.split(" ")[2];
                                text = neuter_component;
                            }
                            if (key.equals("tatoo")) {
                                continue;
                            }
                            if (key.equals("tatoo_date")) {
                                if (text.split(" ").length == 3)
                                    tatoo_component = text.split(" ")[2];
                                continue;
                            } else if (key.equals("tatoo_string")) {
                                String[] splitted = text.split(" ");
                                String tatooString = "";
                                for (int i = 1; i < splitted.length; i++)
                                    tatooString += splitted[i] + " ";
                                text = "Tatuering: " + tatooString + " (" + tatoo_component + ")";
                            }
                            if (key.equals("chip")) {
                                continue;
                            } else if (key.equals("chip_date")) {
                                if (text.split(" ").length == 2)
                                    chip_component = text.split(" ")[1];
                                continue;
                            } else if (key.equals("chip_code")) {
                                chip_component = "Chip: " + text + " (" + chip_component + ")";
                                text = chip_component;
                            }
                            TextView textView = new TextView(context);
                            textView.setText(text);
                            textView.setTextColor(Color.parseColor("#000000"));
                            content_holder.addView(textView);
                        }
                    }

                } catch (JSONException je) {
                    je.printStackTrace();
                }
            }
        }

        if (form_type.equals("catform_basic")) {
            homePage.setView(IndexHolder.FORM_PREVIEW_PAGE, name);
           // homePage.setView(IndexHolder.FORM_PREVIEW_PAGE, name, homePage.getCatformtitles()[0], R.color.newColorPrimary, R.color.newColorPrimary);
            homePage.setNewCat(false);
        }

        //homePage.setView(IndexHolder.FORM_PREVIEW_PAGE, name, homePage.getCatformtitles()[0], R.color.newColorPrimary, R.color.newColorPrimary);

        homePage.getHomePageHandler().closeStandardDialog();
    }

    private boolean isVetdocForm(String form_type){
        boolean isVetDoc = false;
        switch (form_type){
            case "catform_vetdoc_generalstate":
            case "catform_vetdoc_lynne":
            case "catform_vetdoc_skinpaws":
            case "catform_vetdoc_lymf":
            case "catform_vetdoc_eyes":
            case "catform_vetdoc_ears":
            case "catform_vetdoc_mouth":
            case "catform_vetdoc_abdomen":
            case "catform_vetdoc_circulationorgan":
            case "catform_vetdoc_respirationorgan":
            case "catform_vetdoc_outergenitalias":
            case "catform_vetdoc_locomotiveorgan":
                isVetDoc = true;
                break;
            default:break;
        }
        return isVetDoc;
    }


    /*


    private void load(String form_type, String content, ResourceMapper resourceMapper){
        String json = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
        System.out.println("JSON: "+json+" LENGTH: "+json.length());
        String name = "Okänd";
        if (!json.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(json);
                Iterator<String> iterator = jsonObject.keys();
                //String name = "Okänd";
                while (iterator.hasNext()) {
                    String key = iterator.next();
                    if (key.equals("id") || key.equals("table_name") || key.equals("start_date_reminded") || key.equals("repeat_date_reminded")) continue;
                    if (!form_type.equals("catform_basic") && key.equals("catcenter_id"))
                        continue;
                    if(form_type.equals("catform_medical") && key.equals("repeat_date"))
                        continue;
                    System.out.println("LOAD VIEW BY KEY: "+key);
                    View view = homePage.findViewById(resourceMapper.getResource(key));
                    String value = jsonObject.getString(key);
                    if (key.equals("name")) name = value;
                    if(view instanceof AutoCompleteTextView){
                        AutoCompleteAdapter autoCompleteAdapter = (AutoCompleteAdapter) ((AutoCompleteTextView)view).getAdapter();
                        ((AutoCompleteTextView)view).setAdapter(null);
                        ((AutoCompleteTextView) view).setText(value);
                        ((AutoCompleteTextView)view).setAdapter(autoCompleteAdapter);
                        //((AutoCompleteTextView) view).dismissDropDown();
                    }
                    else if (view instanceof EditText) {
                        System.out.println("[EDITTEXT] KEY: " + key + "   VALUE: " + value);
                        ((EditText) view).setText(value);
                    } else if (view instanceof CheckBox) {
                        System.out.println("[CHECKBOX] KEY: " + key + "   VALUE: " + value);
                        switch (value) {
                            case "1":
                                ((CheckBox) view).setChecked(true);
                                break;
                            case "2":
                                ((CheckBox) view).setChecked(false);
                                break;
                            case "true":
                            case "false":
                                System.out.println("CHECKBOX BOOLEAN: " + Boolean.parseBoolean(value));
                                ((CheckBox) view).setChecked(Boolean.parseBoolean(value));
                                break;
                            default:
                                if(value != null && !value.equals("")){
                                    if(form_type.equals("catform_status")){
                                        String[] splitted = value.split(" ");
                                        if(splitted.length >= 2 && splitted[splitted.length-1].substring(0, 1).equals("(")){
                                            ((CheckBox)view).setChecked(true);
                                        }
                                    }

                                    ((CheckBox) view).setText(value);
                                }

                                break;
                        }
                    }
                    else if (view instanceof RadioButton){
                        if(form_type.equals("catform_status")){
                            System.out.println("RB STATUS  "+value.split(" ").length);
                            if(value.split(" ").length == 2) {
                                ((RadioButton)view).setChecked(true);
                            }
                            else{
                                ((RadioButton)view).setChecked(false);
                            }
                            ((RadioButton)view).setText(value);
                        }
                    }
                    else if (view instanceof RadioGroup) {
                        switch (value){
                            case "Nej":
                            case "Ja":
                                break;
                            default:
                                if(!value.equals("") && !value.split(" ")[0].equals("Ja")) {
                                    int intValue = Integer.parseInt(value);
                                    if (intValue > 0) {
                                        ((RadioGroup) view).check(intValue);
                                    }
                                }
                                break;
                        }

                    }

                    else if (view instanceof TextView) {
                        System.out.println("[TEXTVIEW] KEY: " + key + "   VALUE: " + value);
                        ((TextView) view).setText(value);
                    } else if (view instanceof ImageView && key.equals("photo_src")){

                        File currentCatImageFile = ((HomePage)context).getFileManager().getCurrentViewedCatPicture();
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        ((ImageView)view).setImageBitmap(BitmapFactory.decodeFile(currentCatImageFile.getAbsolutePath(), options));
                        view.setTag("SELECTED_PHOTO");
                        System.out.println("[ELSE] KEY: " + key + "   VALUE: " + value + "   VIEW: "+view);
                    }
                }
                if (form_type.equals("catform_basic")) {

                    //homePage.setView(5, name, homePage.getCatformtitles()[0], R.color.catform, R.color.catformDark);
                }
            } catch (JSONException jsone) {
                jsone.printStackTrace();
            }

        }
        if (form_type.equals("catform_basic")) {
            //homePage.isNewCat(false);
            homePage.setView(IndexHolder.FIRST_FORMPAGE_INDEX, name, homePage.getCatformtitles()[0], R.color.catform, R.color.catformDark);

        }
    }
    */



}

