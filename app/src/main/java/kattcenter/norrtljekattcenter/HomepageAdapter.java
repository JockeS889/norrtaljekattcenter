package kattcenter.norrtljekattcenter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kattcenter.norrtljekattcenter.catform.IndexHolder;

/**
 * Created by Jocke on 2017-06-01.
 */

public class HomepageAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Category> categories;
    public HomepageAdapter(Context context){
        this.context = context;
    }

    public void setCategories(ArrayList<Category> categories){
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        if(convertView == null){
            //view = ((Activity) context).getLayoutInflater().inflate(R.layout.catlist_item, null);
            view = layoutInflater.inflate(R.layout.homescreen_category, null);
        }
        else{
            view = convertView;
        }
        /*
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){

        }
        else{
            view.setBackground(ContextCompat.getDrawable(context, android.R.drawable.dialog_holo_light_frame));
        }
        */

        view.setMinimumHeight(((HomePage)context).getHomepageHeight()/3);
        //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT/3, ViewGroup.LayoutParams.MATCH_PARENT/3);
        //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(150, 460);
        //view.setLayoutParams(params);
        switch (position){
            case 0: view.setBackground(ContextCompat.getDrawable(context, R.color.cat1)); break;
            case 1: view.setBackground(ContextCompat.getDrawable(context, R.color.cat2)); break;
            case 2: view.setBackground(ContextCompat.getDrawable(context, R.color.cat3)); break;
            case 3: view.setBackground(ContextCompat.getDrawable(context, R.color.cat4)); break;
            case 4: view.setBackground(ContextCompat.getDrawable(context, R.color.cat5)); break;
            case 5: view.setBackground(ContextCompat.getDrawable(context, R.color.cat6)); break;
            case 6: view.setBackground(ContextCompat.getDrawable(context, R.color.cat7)); break;
            case 7: view.setBackground(ContextCompat.getDrawable(context, R.color.cat8)); break;
            case 8: view.setBackground(ContextCompat.getDrawable(context, R.color.cat9)); break;
            default:break;
        }
        TextView title = (TextView) view.findViewById(R.id.title);
        TextView amount = (TextView) view.findViewById(R.id.amount);
        final Category currentCategory = categories.get(position);
        title.setText(currentCategory.getTitle());
        if(currentCategory.getAmount() != -1)
            amount.setText(Integer.toString(currentCategory.getAmount()));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentCategory.getAmount() > 0) {
                    //((HomePage) context).viewList(currentCategory.getType());
                    ((HomePage)context).getHomePageHandler().getListHandler().viewList(currentCategory.getType());
                    ((HomePage)context).setView(IndexHolder.CATLIST_PAGE, currentCategory.getType());
                }
                else{
                    Toast.makeText(context, "Inget innehåll just nu", Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }
}
