package kattcenter.norrtljekattcenter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.opengl.EGLDisplay;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.HashMap;

import kattcenter.norrtljekattcenter.network.DataPacket;
import kattcenter.norrtljekattcenter.network.DatabaseConnection;

public class LoginPage extends AppCompatActivity {

    private View view;
    private ViewFlipper viewFlipper;
    private RadioGroup newAccountRadioGroup;
    private Type privilige;
    private String email, password, priv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        view = findViewById(R.id.login_fullscreen_content);
        RelativeLayout headerLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.header, null);
        ((TextView)headerLayout.findViewById(R.id.headerTitle)).setText("Norrtälje Kattcenter");
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(headerLayout);
        viewFlipper = (ViewFlipper) findViewById(R.id.loginFlipper);
        /*
        newAccountRadioGroup = (RadioGroup) findViewById(R.id.priviligeModeNewAccount);
        (findViewById(R.id.loginFragment)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide();
            }
        });
        (findViewById(R.id.newUserFragment)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide();
            }
        });
        */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorLoginScreen, null)));
        }
        else{
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.colorLoginScreen)));
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
    }



    public void flipView(View view){
        /*
        switch (view.getId()){
            case R.id.saveNewUserButton:break;
            default:viewFlipper.showNext();
                break;
        }
        */
        switch (viewFlipper.getDisplayedChild()){
            case 0:
                viewFlipper.showNext();
                break;
            case 1:
                viewFlipper.showPrevious();
                break;
            default:break;
        }


    }

    public void register(View view){
        (findViewById(R.id.registerButton)).setEnabled(false);
        email = ((EditText)findViewById(R.id.new_user_email)).getText().toString();
        password = ((EditText)findViewById(R.id.new_user_temp_pass)).getText().toString();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        String blub = email+":"+password+":"+Type.CATOWNER.toString();
        blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("blub", blub);
        data.put("logintype", "register");
        new DatabaseConnection(this, new DataPacket(data, Type.LOGIN)).execute();
        (findViewById(R.id.registerprogressbar)).setVisibility(View.VISIBLE);
    }

    public void registerSuccess(){
        viewFlipper.showNext();
    }

    public void registerFailed(){
        (findViewById(R.id.registerButton)).setEnabled(true);
        (findViewById(R.id.registerprogressbar)).setVisibility(View.INVISIBLE);
    }

    public void login(View view){
        //System.out.println("------------------------------------------------------------ID: "+newAccountRadioGroup.getCheckedRadioButtonId());
        //Type priviliged;
        switch (view.getId()){
            case R.id.loginButton:
                view.setEnabled(false);
                (findViewById(R.id.loginProgressbar)).setVisibility(View.VISIBLE);
                loginUser();
                break;
            case R.id.createUserButton:
                createUser();
                break;
        }

        /*
        Intent homepage = new Intent(LoginPage.this, HomePage.class);
        switch (newAccountRadioGroup.getCheckedRadioButtonId()){
            case -1: Toast.makeText(this, "Kontotyp måste väljas", Toast.LENGTH_SHORT).show();
                break;
            case R.id.newPersonal:
                homepage.putExtra("PRIVILEGED", Type.ADMIN);
                startActivity(homepage);
                break;
            case R.id.newCatowner:
                homepage.putExtra("PRIVILEGED", Type.CATOWNER);
                startActivity(homepage);
                break;
            default:break;
        }

        */




    }

    private void loginUser(){
        email = ((EditText)findViewById(R.id.loginEmail)).getText().toString();
        password = ((EditText)findViewById(R.id.loginPassword)).getText().toString();
        password = Base64.encodeToString(password.getBytes(), Base64.NO_WRAP);
        /*
        int rbid = ((RadioGroup)findViewById(R.id.priviligeModeLogin)).getCheckedRadioButtonId();
        switch (rbid){
            case R.id.loginPersonal:
                privilige = Type.ADMIN;
                break;
            case R.id.loginCatowner:
                privilige = Type.CATOWNER;
                //Toast.makeText(this, "Kattägare kan inte läggas till just nu", Toast.LENGTH_LONG).show();
                //return;
                break;
            case -1:
                Toast.makeText(this, "Kontotyp måste väljas", Toast.LENGTH_SHORT).show();
                (findViewById(R.id.loginButton)).setEnabled(true);
                return;
            default:break;
        }
        priv = privilige.toString();
        */

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        //String blub = email+":"+password+":"+priv;
        String blub = email+":"+password;
        blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("blub", blub);
        data.put("logintype", "login");
        new DatabaseConnection(this, new DataPacket(data, Type.LOGIN)).execute();
    }

    public void loginSuccess(){
        //password = Base64.encodeToString(password.getBytes(), Base64.NO_WRAP);
        privilige = PriviligeHolder.getType();
        priv = privilige.toString();
        SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Email", email);
        editor.putString("CalendarEmail", email);
        editor.putString("Password", password);
        editor.putString("Privilige", priv);
        editor.commit();
        Intent homepage = new Intent(LoginPage.this, HomePage.class);
        homepage.putExtra("PRIVILEGED", privilige);
        startActivity(homepage);
    }

    public void loginFailed(){
        (findViewById(R.id.loginButton)).setEnabled(true);
        (findViewById(R.id.loginProgressbar)).setVisibility(View.INVISIBLE);
    }

    private void createUser(){
        //DatabaseConnection connection = new DatabaseConnection(this);
        //String surName = ((EditText)findViewById(R.id.new_user_surname)).getText().toString();
        //String lastName = ((EditText)findViewById(R.id.new_user_surname)).getText().toString();
        //String ssn = ((EditText)findViewById(R.id.new_user_ssn)).getText().toString();
        //String address = ((EditText)findViewById(R.id.new_user_address)).getText().toString();
        //String cell = ((EditText)findViewById(R.id.new_user_cell)).getText().toString();
        //email = ((EditText)findViewById(R.id.new_user_email)).getText().toString();
        //String userName = ((EditText)findViewById(R.id.new_user_user_name)).getText().toString();
        password = ((EditText)findViewById(R.id.new_user_pass)).getText().toString();
        String repeatPassword = ((EditText)findViewById(R.id.new_user_repeat_pass)).getText().toString();
        if(!password.equals(repeatPassword)) {
            Dialog errorDialog = new Dialog(this);
            errorDialog.setTitle("Lösenord matchar ej");
            errorDialog.show();
        }
        else {
            privilige = Type.CATOWNER;

        /*
        int rbid = ((RadioGroup)findViewById(R.id.priviligeModeNewAccount)).getCheckedRadioButtonId();
        switch (rbid){
            case R.id.newPersonal:
                privilige = Type.ADMIN;
                break;
            case R.id.newCatowner:
                //privilige = Type.CATOWNER;
                Toast.makeText(this, "Kattägare kan inte läggas till just nu", Toast.LENGTH_LONG).show();
                return;
            case -1:
                Toast.makeText(this, "Kontotyp måste väljas", Toast.LENGTH_SHORT).show();
                //(findViewById(R.id.loginButton)).setEnabled(true);
                return;
            default:break;
        }
        */
            priv = privilige.toString();
            password = Base64.encodeToString(password.getBytes(), Base64.NO_WRAP);
            //ssn = Base64.encodeToString(ssn.getBytes(), Base64.NO_WRAP);
            /*
            SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("Email", email);
            editor.putString("CalendarEmail", email);
            editor.putString("Password", password);
            editor.putString("Privilige", priv);
            editor.commit();
            */
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            //String blub = name + ":" + ssn + ":" + address + ":" + cell + ":" + email + ":" + password + ":" + priv;
            String blub = email + ":" + password + ":" + priv;
            blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
            HashMap<String, String> data = new HashMap<>();
            data.put("blub", blub);
            data.put("logintype", "create");
            new DatabaseConnection(this, new DataPacket(data, Type.LOGIN)).execute();
            (findViewById(R.id.createUserProgressbar)).setVisibility(View.VISIBLE);
        }
    }
}
