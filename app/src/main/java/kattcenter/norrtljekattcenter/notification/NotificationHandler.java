package kattcenter.norrtljekattcenter.notification;



import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
/**
 * Created by Joakim on 2017-06-27.
 */

public class NotificationHandler {

    private Context context;
    private int NOTIFICATION_ID = 1;
    private int DOWNLOAD_ID = 2;
    //private Notification.Builder mNotification;
    private NotificationCompat.Builder mNotification;
    private NotificationManager mNotificationManager;
    private PendingIntent mContentIntent;
    private CharSequence mContentTitle;
    private int totalReminderSent = 0;
    private int totalFilesDownloaded = 0;
    private int totalFilesToDownload = 0;


    public NotificationHandler(Context context){
        this.context = context;
    }

    /**
     * Put the notification into the status bar
     */
    public void createNotification() {
        //get the notification manager
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        //create the notification
        int icon = android.R.drawable.stat_notify_sync;

        CharSequence tickerText = "Kontrollerar påminnelser"; //Initial text that appears in the status bar
        long now = System.currentTimeMillis();
        //mNotification = new Notification(icon, tickerText, now);



        //create the content which is shown in the notification pulldown
        mContentTitle = "Kontrollerar påminnelser"; //Full title of the notification in the pull down
        CharSequence contentText = totalReminderSent+" skickade"; //Text of the notification in the pull down

        //you have to set a PendingIntent on a notification to tell the system what you want it to do when the notification is selected
        //I don't want to use this here so I'm just creating a blank one
        Intent notificationIntent = new Intent();
        mContentIntent = PendingIntent.getActivity(context, totalReminderSent, notificationIntent, 0);

        //add the additional content and intent to the notification
        //mNotification.setLatestEventInfo(context, mContentTitle, contentText, mContentIntent);

        //make this notification appear in the 'Ongoing events' section
        //mNotification.flags = Notification.FLAG_ONGOING_EVENT;


        mNotification = new NotificationCompat.Builder(context)
                .setTicker(tickerText)
                .setContentTitle(mContentTitle)
                .setContentText(contentText)
                .setSmallIcon(icon)
                .setContentIntent(mContentIntent);



        //show the notification
        mNotificationManager.notify(NOTIFICATION_ID, mNotification.build());
    }

    public void createMultiDownloadNotification(int length){
        //get the notification manager
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        totalFilesToDownload = length;
        totalFilesDownloaded = 0;
        //create the notification
        int icon = android.R.drawable.stat_sys_download;
        CharSequence tickerText = "Hämtar"; //Initial text that appears in the status bar
        long now = System.currentTimeMillis();
        //mNotification = new Notification(icon, tickerText, now);



        //create the content which is shown in the notification pulldown
        mContentTitle = "Hämtar "+length+" filer från server"; //Full title of the notification in the pull down
        CharSequence contentText = "0/"+length+" Hämtade"; //Text of the notification in the pull down

        //you have to set a PendingIntent on a notification to tell the system what you want it to do when the notification is selected
        //I don't want to use this here so I'm just creating a blank one
        Intent notificationIntent = new Intent();
        mContentIntent = PendingIntent.getActivity(context, totalReminderSent, notificationIntent, 0);

        //add the additional content and intent to the notification
        //mNotification.setLatestEventInfo(context, mContentTitle, contentText, mContentIntent);

        //make this notification appear in the 'Ongoing events' section
        //mNotification.flags = Notification.FLAG_ONGOING_EVENT;


        mNotification = new NotificationCompat.Builder(context)
                .setTicker(tickerText)
                .setContentTitle(mContentTitle)
                .setContentText(contentText)
                .setSmallIcon(icon)
                .setContentIntent(mContentIntent);



        //show the notification
        mNotificationManager.notify(DOWNLOAD_ID, mNotification.build());
    }

    public void createDownloadNotification(String file_name, String text){
        //get the notification manager
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        //create the notification
        int icon = android.R.drawable.stat_sys_download;
        CharSequence tickerText = "Hämtar"; //Initial text that appears in the status bar
        long now = System.currentTimeMillis();
        //mNotification = new Notification(icon, tickerText, now);



        //create the content which is shown in the notification pulldown
        mContentTitle = "Hämtar fil från server"; //Full title of the notification in the pull down
        CharSequence contentText = text + file_name; //Text of the notification in the pull down

        //you have to set a PendingIntent on a notification to tell the system what you want it to do when the notification is selected
        //I don't want to use this here so I'm just creating a blank one
        Intent notificationIntent = new Intent();
        mContentIntent = PendingIntent.getActivity(context, totalReminderSent, notificationIntent, 0);

        //add the additional content and intent to the notification
        //mNotification.setLatestEventInfo(context, mContentTitle, contentText, mContentIntent);

        //make this notification appear in the 'Ongoing events' section
        //mNotification.flags = Notification.FLAG_ONGOING_EVENT;


        mNotification = new NotificationCompat.Builder(context)
                .setTicker(tickerText)
                .setContentTitle(mContentTitle)
                .setContentText(contentText)
                .setSmallIcon(icon)
                .setContentIntent(mContentIntent);



        //show the notification
        mNotificationManager.notify(DOWNLOAD_ID, mNotification.build());
    }

    public void multiDownloadComplete(){
        mNotification.setSmallIcon(android.R.drawable.stat_sys_download_done);
        mNotification.setContentTitle(totalFilesDownloaded+" dokument hämtade");
        mNotification.setContentText("Sparat i telefonens nerladdningsmapp");
        //PendingIntent pendingIntent = ((HomePage)context).openPDFIntent();
        PendingIntent pendingIntent = ((HomePage)context).openDownloadFolderIntent();
        mNotification.setContentIntent(pendingIntent);
        mNotificationManager.notify(DOWNLOAD_ID, mNotification.build());

        /*
        mNotifyManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(Welcome.this);
        mBuilder.setContentTitle("Download")
                .setContentText("Download in progress")
                .setSmallIcon(R.drawable.icon)
                .setContentIntent(p);
        mBuilder.setProgress(100, 0, false);
        mNotifyManager.notify(id, mBuilder.build());
        */
    }

    public void downloadComplete(String file_name){
        mNotification.setSmallIcon(android.R.drawable.stat_sys_download_done);
        mNotification.setContentTitle(file_name+" nerladdad");
        mNotification.setContentText("Sparat i telefonens nerladdningsmapp");
        PendingIntent pendingIntent = ((HomePage)context).openPDFIntent();
        mNotification.setContentIntent(pendingIntent);
        mNotificationManager.notify(DOWNLOAD_ID, mNotification.build());

        /*
        mNotifyManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(Welcome.this);
        mBuilder.setContentTitle("Download")
                .setContentText("Download in progress")
                .setSmallIcon(R.drawable.icon)
                .setContentIntent(p);
        mBuilder.setProgress(100, 0, false);
        mNotifyManager.notify(id, mBuilder.build());
        */
    }

    /**
     * Receives progress updates from the background task and updates the status bar notification appropriately
     * @return true if complete, false if there are remaining files
     */
    public boolean updateMultiDownloadProgress() {
        totalFilesDownloaded++;
        //build up the new status message
        CharSequence contentText = totalFilesDownloaded+"/"+totalFilesToDownload +" Hämtade"; //Text of the notification in the pull down
        //totalReminderSent += sentReminder;
        //CharSequence contentText = totalReminderSent+" skickade";
        System.out.println(contentText);
        //publish it to the status bar
        //mNotification.setLatestEventInfo(mContext, mContentTitle, contentText, mContentIntent);
        mNotification.setContentText(contentText);
        mNotification.setSmallIcon(android.R.drawable.stat_sys_download);
        mNotificationManager.notify(DOWNLOAD_ID, mNotification.build());
        return totalFilesToDownload == totalFilesDownloaded;
    }

    /**
     * Receives progress updates from the background task and updates the status bar notification appropriately
     * @param sentReminder
     */
    public void progressUpdate(int sentReminder) {
        //build up the new status message
        totalReminderSent += sentReminder;
        CharSequence contentText = totalReminderSent+" skickade";
        System.out.println(contentText);
        //publish it to the status bar
        //mNotification.setLatestEventInfo(mContext, mContentTitle, contentText, mContentIntent);
        mNotification.setContentText(contentText);
        mNotification.setSmallIcon(android.R.drawable.stat_sys_upload);
        mNotificationManager.notify(NOTIFICATION_ID, mNotification.build());
    }

    /**
     * called when the background task is complete, this removes the notification from the status bar.
     * We could also use this to add a new ‘task complete’ notification
     */
    public void completed()    {
        //remove the notification from the status bar
        //mNotificationManager.cancel(NOTIFICATION_ID);
        //mNotification.setSmallIcon(android.R.drawable.stat_sys_upload_done);
        mNotification.setSmallIcon(R.drawable.ic_assignment_turned_in_black_24dp);
        mNotification.setContentTitle("Påminnelser kontrollerat");
        mNotificationManager.notify(NOTIFICATION_ID, mNotification.build());
    }
}