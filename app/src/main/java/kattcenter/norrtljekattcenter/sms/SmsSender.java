package kattcenter.norrtljekattcenter.sms;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.telephony.SmsManager;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.calendar.ReminderHandler;

import java.util.ArrayList;

/**
 * Created by Jocke on 2017-06-19.
 */

public class SmsSender extends BroadcastReceiver {

    private Context context;
    //private ReminderHandler reminderHandler;
    static String SMS_SENT = "sms_constant";
    static String SMS_DELIVERED = "sms_second_constant";
    static int MAX_SMS_MESSAGE_LENGTH = 160;
    public SmsSender(Context context){
        this.context = context;
    }

    /*
    public void setReminderHandler(ReminderHandler reminderHandler){
        this.reminderHandler = reminderHandler;
    }
    */

    public void sendSms(String phoneNumber, String message){
        SmsManager manager = SmsManager.getDefault();
        PendingIntent pendingSent = PendingIntent.getBroadcast(context, 0, new Intent(SMS_SENT), 0);
        PendingIntent pendingDelivered = PendingIntent.getBroadcast(context, 0, new Intent(SMS_DELIVERED), 0);
        int length = message.length();

        if(length > MAX_SMS_MESSAGE_LENGTH)
        {
            ArrayList<String> messagelist = manager.divideMessage(message);

            manager.sendMultipartTextMessage(phoneNumber, null, messagelist, null, null);
        }
        else
        {
            manager.sendTextMessage(phoneNumber, null, message, pendingSent, pendingDelivered);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String message = null;

        switch (getResultCode()) {
            case Activity.RESULT_OK:
                message = "SMS skickat!";
                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                message = "SMS skickades inte.";
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                message = "SMS skickades inte: [NO SERVICE]";
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
                message = "SMS skickades inte: [Null PDU]";
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                message = "SMS skickades inte: [Radio off]";
                break;
        }

        //reminderHandler.nextReminder(message);

        //Toast.makeText(context, message, Toast.LENGTH_LONG).show();

        /*
        final Dialog bottomDialog = new Dialog(context, R.style.MaterialDialogSheet);
        RelativeLayout relativeLayout = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.list_entry_textview_1, null);
        relativeLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
        TextView textView = (TextView) relativeLayout.findViewById(R.id.listEntry);
        textView.setText(message);
        textView.setGravity(Gravity.CENTER);
        bottomDialog.setContentView(relativeLayout);
        //bottomDialog.setCancelable(true);
        bottomDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
        bottomDialog.show();


        final Handler dialogHandler = new Handler();
        final Runnable dialogRunnable = new Runnable() {
            @Override
            public void run() {
                if(bottomDialog.isShowing()){
                    bottomDialog.dismiss();
                }
            }
        };

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialogHandler.removeCallbacks(dialogRunnable);
            }
        });

        dialogHandler.postDelayed(dialogRunnable, 4000);
        */
        /*
        AppMsg.makeText(SendMessagesWindow.this, message,
                AppMsg.STYLE_CONFIRM).setLayoutGravity(Gravity.BOTTOM)
                .show();
                */
    }

    /*
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };
    */
}
