package kattcenter.norrtljekattcenter.personel;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;


import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.catform.IndexHolder;

/**
 * Created by Joakim on 2017-07-17.
 */

public class PersonelManager {

    private Context context;
    private HomePage homePage;
    private String mail;
    private Type priv;
    private RelativeLayout newUserFormContent;

    public PersonelManager(Context context){
        this.context = context;
        homePage = (HomePage) context;
        priv = Type.ADMIN;
    }

    private void setAutoZipCodeField(final EditText editText){

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 3){
                    if(before == 0)
                        editText.setText(s+" ");
                    else
                        editText.setText(s.subSequence(0, 2));
                    editText.setSelection(editText.getText().length());
                }
                /*
                if(s.toString().length() == 3){
                    Log.i("HOMEPAGE", "APPENDING TO ZIP");
                    editTextZip.setText(editTextZip.getText()+" ");
                    editTextZip.append("");
                }
                Log.i("HOMEPAGE", "ZIP CODE COUNT TEXT: "+s.toString().length()+"  "+s.toString());
                */
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setAutoSSNField(final EditText editText){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //System.out.println("CHANGED: "+s+" "+start+" "+before+" "+count);

                if(s.length() == 6){
                    if(before == 0)
                        editText.setText(s+"-");
                    else
                        editText.setText(s.subSequence(0, 5));
                    editText.setSelection(editText.getText().length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    public void newPersonelForm(){
        final Dialog dialog = new Dialog(context);
        //RelativeLayout content = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.catowner_infopage, null);
        newUserFormContent = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.new_user_form, null);
        ((TextView)newUserFormContent.findViewById(R.id.new_user_title)).setText("Ny personal");
        dialog.setContentView(newUserFormContent);

        setAutoSSNField((EditText)newUserFormContent.findViewById(R.id.new_user_ssn_text));
        setAutoZipCodeField((EditText)newUserFormContent.findViewById(R.id.new_user_zipcode_text));

        Button sendLogin = (Button)newUserFormContent.findViewById(R.id.sendLogin);
        sendLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePage.sendLogin(dialog, "personel");
            }
        });



        Button closeForm = (Button)newUserFormContent.findViewById(R.id.closeNewUserForm);
        closeForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }



    public void clearPersonelInfo(){
        /*
        ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_ssn_text)).setText("");
        ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_surname_text)).setText("");
        ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_lastname_text)).setText("");
        ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_address_text)).setText("");
        ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_zipcode_text)).setText("");
        ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_city_text)).setText("");
        ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_cell_text)).setText("");
        ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_mail_text)).setText("");
        homePage.getPersonalInfo().findViewById(R.id.personal_ssn_text).setEnabled(true);
        homePage.getPersonalInfo().findViewById(R.id.personal_surname_text).setEnabled(true);
        homePage.getPersonalInfo().findViewById(R.id.personal_lastname_text).setEnabled(true);
        homePage.getPersonalInfo().findViewById(R.id.personal_address_text).setEnabled(true);
        homePage.getPersonalInfo().findViewById(R.id.personal_zipcode_text).setEnabled(true);
        homePage.getPersonalInfo().findViewById(R.id.personal_city_text).setEnabled(true);
        homePage.getPersonalInfo().findViewById(R.id.personal_cell_text).setEnabled(true);
        homePage.getPersonalInfo().findViewById(R.id.personal_mail_text).setEnabled(true);
        */
    }

    public void loadPersonelInfo(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            String id = jsonObject.getString("id");
            String ssn = new String(Base64.decode(jsonObject.getString("ssn").getBytes(), Base64.NO_WRAP));
            String surName = jsonObject.getString("surname");
            String lastName = jsonObject.getString("lastname");
            String address = jsonObject.getString("address");
            String zipcode = jsonObject.getString("zipcode");
            String city = jsonObject.getString("city");
            String cell = jsonObject.getString("cell");
            mail = jsonObject.getString("email");
            if(priv == Type.ADMIN) {

                //System.out.println(((TextView)catowner_management_page.findViewById(R.id.catowner_name_text)));
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_name)).setText(surName+" "+lastName);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_ssn)).setText(ssn);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_cell)).setText(cell);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_cell2)).setText(cell);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_mail)).setText(mail);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_address_street)).setText(address);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_address_zipcode)).setText(zipcode);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_address_city)).setText(city);

                /*
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_ssn_text)).setText(ssn);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_surname_text)).setText(surName);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_lastname_text)).setText(lastName);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_address_text)).setText(address);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_zipcode_text)).setText(zipcode);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_city_text)).setText(city);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_cell_text)).setText(cell);
                ((TextView) homePage.getPersonalInfo().findViewById(R.id.personal_mail_text)).setText(mail);
                homePage.getPersonalInfo().findViewById(R.id.personal_ssn_text).setEnabled(false);
                homePage.getPersonalInfo().findViewById(R.id.personal_surname_text).setEnabled(false);
                homePage.getPersonalInfo().findViewById(R.id.personal_lastname_text).setEnabled(false);
                homePage.getPersonalInfo().findViewById(R.id.personal_address_text).setEnabled(false);
                homePage.getPersonalInfo().findViewById(R.id.personal_zipcode_text).setEnabled(false);
                homePage.getPersonalInfo().findViewById(R.id.personal_city_text).setEnabled(false);
                homePage.getPersonalInfo().findViewById(R.id.personal_cell_text).setEnabled(false);
                homePage.getPersonalInfo().findViewById(R.id.personal_mail_text).setEnabled(false);
                */

                //homePage.setView(IndexHolder.PERSONAL_INFO_PAGE, null, null, R.color.newColorPrimary, R.color.newColorPrimary);
                homePage.setView(IndexHolder.PERSONAL_INFO_PAGE, "PERSONAL");
            }
        } catch (JSONException jsone){
            jsone.printStackTrace();
        }
    }

    public void updateNumber(String cell){
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_cell)).setText(cell);
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_cell2)).setText(cell);
    }

    public void updateEmail(String email){
        mail = email;
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_mail)).setText(mail);
    }


    public void updateAddressInfo(String address, String zipcode, String city){
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_street)).setText(address);
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_zipcode)).setText(zipcode);
        ((TextView) homePage.getCatOwnerInfo().findViewById(R.id.profile_address_city)).setText(city);
    }

    public String getAddressInfo(){
        String address = "";
        address += ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_address_street)).getText().toString()+", ";
        address += ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_address_zipcode)).getText().toString()+", ";
        address += ((TextView) homePage.getPersonalInfo().findViewById(R.id.profile_address_city)).getText().toString();
        return address;
    }

    public String getMail(){
        return mail;
    }
}
