package kattcenter.norrtljekattcenter.personel;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DataPacket;

/**
 * Created by Joakim on 2017-07-16.
 */

public class PersonelListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> personelNames, personelEmails, personelStatuses;
    private ArrayList<String> personelNamesCopy, personelEmailsCopy, personelStatusesCopy;
    public PersonelListAdapter(Context context){
        this.context = context;
    }

    public void setPersonelNames(ArrayList<String> personelNames){
        this.personelNames = personelNames;
    }

    public void setPersonelEmails(ArrayList<String> personelEmails){
        this.personelEmails = personelEmails;
    }

    public void setPersonelStatuses(ArrayList<String> personelStatuses){
        this.personelStatuses = personelStatuses;
    }

    public ArrayList<String> getPersonelNames(){
        return personelNames;
    }

    public ArrayList<String> getPersonelEmails(){
        return personelEmails;
    }

    public ArrayList<String> getPersonelStatuses(){
        return personelStatuses;
    }


    public void setPersonelNamesCopy(ArrayList<String> personelNamesCopy){
        this.personelNamesCopy = personelNamesCopy;
    }

    public void setPersonelEmailsCopy(ArrayList<String> personelEmailsCopy){
        this.personelEmailsCopy = personelEmailsCopy;
    }

    public void setPersonelStatusesCopy(ArrayList<String> personelStatusesCopy){
        this.personelStatusesCopy = personelStatusesCopy;
    }

    public ArrayList<String> getPersonelNamesCopy(){
        return personelNamesCopy;
    }

    public ArrayList<String> getPersonelEmailsCopy(){
        return personelEmailsCopy;
    }

    public ArrayList<String> getPersonelStatusesCopy(){
        return personelStatusesCopy;
    }


    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return personelEmails.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return null;
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //GridView.LayoutParams layoutParams = new GridView.LayoutParams(200,200);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(250, 250+200);

        View view;
        if(convertView == null){
            //view = ((Activity) context).getLayoutInflater().inflate(R.layout.catlist_item, null);
            //view = layoutInflater.inflate(R.layout.personal_list_entry, null);
            view = layoutInflater.inflate(R.layout.new_personal_listentry, null);
        }
        else{
            view = convertView;
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Log.i("CATOWNERLISTADAPTER", "BACKGROUND ALREADY SET");
        }
        else{
            Log.i("CATOWNERLISTADAPTER", "RESETTING BACKGROUND");
            view.setBackground(ContextCompat.getDrawable(context, android.R.drawable.dialog_holo_light_frame));
        }
        TextView nameView = (TextView) view.findViewById(R.id.short_personal_name);
        nameView.setText(personelNames.get(position));
        ((TextView)view.findViewById(R.id.short_personal_email)).setText(personelEmails.get(position));
        ((TextView)view.findViewById(R.id.short_personal_status)).setText(personelStatuses.get(position));
        /*
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCatowner(catOwnerEmails.get(position));
            }
        });
        */
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPersonel(personelEmails.get(position));
            }
        });
        return view;
    }

    private void selectPersonel(String email){
        ((HomePage)context).getHomePageHandler().setAdminLoadPersonalInfo(true);
        ((HomePage) context).setAddNewPersonal(false);
        String formTable = "personel";
        String blub = formTable+":"+email;
        String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("form_action", "DOWNLOAD");
        data.put("blub", encoded_blub);
        try {
            ((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_LIST));
            ((HomePage)context).signalQueueToSend();
            //((HomePage)context).showLoadingDialog("Laddar information...");
            ((HomePage)context).getHomePageHandler().showStandardDialog("Laddar information...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
