package kattcenter.norrtljekattcenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.opengl.EGLDisplay;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Base64;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import kattcenter.norrtljekattcenter.catform.BundleResourceMapping;
import okhttp3.OkHttpClient;

/**
 * Created by Jocke on 2017-06-08.
 */

public class SaveFormInstanceHandler {

    private Context context;
    private HomePage activity;
    public SaveFormInstanceHandler(HomePage activity){
        this.activity = activity;
    }

    public String getCatID(){
        //return ((TextView)activity.findViewById(R.id.catbasic_cathome_id_text)).getText().toString();
        return activity.getCurrentCatId();
    }

    public Bundle saveBasicFormInstance(){
        Bundle basicInfoBundle = new Bundle();


        ImageView imageView = (ImageView)(activity.findViewById(R.id.info_cat_photo));
        String image_tag = null;
        if(imageView != null)
            image_tag = (String)imageView.getTag();
        if(image_tag != null)
            basicInfoBundle.putBoolean("CATPHOTO", (image_tag.equals("TAKEN_PHOTO") || (image_tag.equals("SELECTED_PHOTO"))));
        if(activity.getPrivileged() == Type.CATOWNER){
            basicInfoBundle.putString("CATNAME", ((EditText)activity.findViewById(R.id.catowner_editable_catname)).getText().toString());
        }
        else{
            basicInfoBundle.putString("CATNAME", ((TextView)activity.findViewById(R.id.catbasic_cathome_name_text)).getText().toString());
            basicInfoBundle.putString("CATHOME_NAME", ((AutoCompleteTextView)activity.findViewById(R.id.catbasic_catname_autocomplete_text)).getText().toString());
            basicInfoBundle.putString("ARRIVED_DATE", ((TextView)activity.findViewById(R.id.catbasic_arrived_date_text)).getText().toString());
            basicInfoBundle.putString("MOVED", ((TextView)activity.findViewById(R.id.catbasic_moved_date_text)).getText().toString());
            basicInfoBundle.putString("TATOO_STRING", ((TextView)activity.findViewById(R.id.catbasic_tatoo_text)).getText().toString());
            basicInfoBundle.putString("TATOO_DATE", ((TextView)activity.findViewById(R.id.catbasic_tatoo_date_text)).getText().toString());
            basicInfoBundle.putString("CHIP_CODE", ((TextView)activity.findViewById(R.id.catbasic_chip_code_text)).getText().toString());
            basicInfoBundle.putString("CHIP_DATE", ((TextView)activity.findViewById(R.id.catbasic_chip_date_text)).getText().toString());
            basicInfoBundle.putString("CATHOME_ID", ((TextView)activity.findViewById(R.id.catbasic_cathome_id_text)).getText().toString());
        }




        basicInfoBundle.putString("NEUTER_DATE", ((TextView)activity.findViewById(R.id.catbasic_neuter_date_text)).getText().toString());
        basicInfoBundle.putString("BIRTHDAY", ((TextView)activity.findViewById(R.id.catbasic_birthday_date_text)).getText().toString());
        basicInfoBundle.putString("BIRTHDAY_GUESS", ((TextView)activity.findViewById(R.id.catbasic_birthday_guess)).getText().toString());
        basicInfoBundle.putString("SEX", ((TextView)activity.findViewById(R.id.catbasic_sex_type_text)).getText().toString());
        basicInfoBundle.putString("RACE", ((TextView)activity.findViewById(R.id.catbasic_race_text)).getText().toString());
        basicInfoBundle.putString("COLOR", ((TextView)activity.findViewById(R.id.catbasic_color_text)).getText().toString());
        basicInfoBundle.putString("HAIR", ((TextView)activity.findViewById(R.id.catbasic_hair_type)).getText().toString());

        /*
        if(activity.getPrivileged() == Type.CATOWNER){
            basicInfoBundle.putString("CATHOME_NAME", ((TextView)activity.findViewById(R.id.catbasic_catcenter_name_text)).getText().toString());
        }else{

        }
        */



        return basicInfoBundle;
    }

    public Bundle saveTemperFormInstance(){
        Bundle temperInfoBundle = new Bundle();
        String general = "";
        if(((RadioButton)activity.findViewById(R.id.catform_temperament_shy)).isChecked()){
            general = "SKYGG";
        }
        else if(((RadioButton)activity.findViewById(R.id.catform_temperament_tame)).isChecked()){
            general = "TAM";
        }
        temperInfoBundle.putString("GENERAL", general);
        temperInfoBundle.putString("TYPE", ((TextView)activity.findViewById(R.id.catform_temperament_temper_text)).getText().toString());
        temperInfoBundle.putString("ADAPTABLE_SKILL", ((TextView)activity.findViewById(R.id.catform_temperament_adaptable_skills_text)).getText().toString());
        temperInfoBundle.putString("USED_TO_CHILDREN", ((TextView)activity.findViewById(R.id.catform_temperament_used_to_children_text)).getText().toString());
        temperInfoBundle.putString("USED_TO_DOGS", ((TextView)activity.findViewById(R.id.catform_temperament_used_to_dogs_text)).getText().toString());
        temperInfoBundle.putString("CAR_SICK", ((TextView)activity.findViewById(R.id.catform_temperament_carsick_text)).getText().toString());
        temperInfoBundle.putString("TALKS", ((TextView)activity.findViewById(R.id.catform_temperament_talking_text)).getText().toString());
        temperInfoBundle.putString("COMMENT", ((TextView)activity.findViewById(R.id.catform_temperament_comment_text)).getText().toString());
        return temperInfoBundle;
    }


    public Bundle saveMedicationFormInstance(){
        Bundle medicationInfoBundle = new Bundle();
        medicationInfoBundle.putString("START_DATE", ((TextView)activity.findViewById(R.id.catmed_start_date_text)).getText().toString());
        medicationInfoBundle.putString("END_DATE", ((TextView)activity.findViewById(R.id.catmed_end_date_text)).getText().toString());
        LinearLayout typeList = (LinearLayout)activity.findViewById(R.id.catmed_type_list);
        String types = "";
        for(int i = 0; i < typeList.getChildCount(); i++){
            RelativeLayout entry = (RelativeLayout) typeList.getChildAt(i);
            types += ((TextView)entry.findViewById(R.id.first_object_text)).getText().toString() + "\n";
        }
        medicationInfoBundle.putString("MEDICATION", types);
        medicationInfoBundle.putString("DOSE", ((TextView)activity.findViewById(R.id.catmed_dose_amount_text)).getText().toString());
        //medicationInfoBundle.putString("ADMINISTRATION", ((TextView)activity.findViewById(R.id.catmed_administration_text)).getText().toString());
        medicationInfoBundle.putString("TREATMENT_TYPE", ((TextView)activity.findViewById(R.id.catmed_treatment_type_text)).getText().toString());
        medicationInfoBundle.putString("COMMENT", ((TextView)activity.findViewById(R.id.catmed_comment_text)).getText().toString());
        medicationInfoBundle.putString("REPEAT_DATE", ((TextView)activity.findViewById(R.id.catmed_repeat_date_text)).getText().toString());
        return medicationInfoBundle;
    }

    public Bundle saveVaccinFormInstance(){
        Bundle vaccinInfoBundle = new Bundle();
        vaccinInfoBundle.putString("START_DATE", ((TextView)activity.findViewById(R.id.catvaccin_start_date_text)).getText().toString());
        vaccinInfoBundle.putString("VACCIN_TYPE", ((TextView)activity.findViewById(R.id.catvaccin_type_text)).getText().toString());
        vaccinInfoBundle.putInt("TIME", ((RadioGroup)activity.findViewById(R.id.catvaccin_vaccinTime)).getCheckedRadioButtonId());
        vaccinInfoBundle.putString("COMMENT", ((TextView)activity.findViewById(R.id.catvaccin_comment_text)).getText().toString());
        vaccinInfoBundle.putString("REPEAT_DATE", ((TextView)activity.findViewById(R.id.catvaccin_repeat_date_text)).getText().toString());
        return vaccinInfoBundle;
    }

    public Bundle saveParasitesFormInstance(){
        Bundle parasiteBundle = new Bundle();
        parasiteBundle.putString("ADMINISTRATION_DATE", ((TextView)activity.findViewById(R.id.catparasite_admininstration_date_text)).getText().toString());
        parasiteBundle.putString("PARASITE_TYPE", ((TextView)activity.findViewById(R.id.catparasite_type_text)).getText().toString());
        LinearLayout typeList = (LinearLayout)activity.findViewById(R.id.catparasite_med_type_list);
        String types = "";
        for(int i = 0; i < typeList.getChildCount(); i++){
            RelativeLayout entry = (RelativeLayout) typeList.getChildAt(i);
            types += ((TextView)entry.findViewById(R.id.first_object_text)).getText().toString() + "\n";
        }
        parasiteBundle.putString("MEDICAL", types);
        parasiteBundle.putString("DOSE", ((EditText)activity.findViewById(R.id.catparasite_dose_text)).getText().toString());
        parasiteBundle.putString("ADMINISTRATION_TYPE", ((TextView)activity.findViewById(R.id.catparasite_administration_text)).getText().toString());
        parasiteBundle.putString("COMMENT", ((TextView)activity.findViewById(R.id.catparasite_comment_text)).getText().toString());
        parasiteBundle.putString("REPEAT_DATE", ((TextView)activity.findViewById(R.id.catparasite_repeat_date_text)).getText().toString());
        return parasiteBundle;
    }

    /*
    public Bundle saveDewormingFormInstance(){
        Bundle dewormingBundle = new Bundle();
        dewormingBundle.putString("START_DATE", ((TextView)activity.findViewById(R.id.catdeworming_start_date_text)).getText().toString());
        dewormingBundle.putString("DEWORMING_TYPE", ((TextView)activity.findViewById(R.id.catdeworming_type_text)).getText().toString());
        dewormingBundle.putString("DOSE", ((TextView)activity.findViewById(R.id.catdeworming_dose_text)).getText().toString());
        dewormingBundle.putString("COMMENT", ((TextView)activity.findViewById(R.id.catdeworming_comment_text)).getText().toString());
        dewormingBundle.putString("REPEAT_DATE", ((TextView)activity.findViewById(R.id.catdeworming_repeat_date_text)).getText().toString());
        return dewormingBundle;
    }

    public Bundle saveVerminFormInstance(){
        Bundle verminInfoBundle = new Bundle();
        verminInfoBundle.putString("START_DATE", ((TextView)activity.findViewById(R.id.catvermin_start_date_text)).getText().toString());
        verminInfoBundle.putString("VERMIN_TYPE", ((TextView)activity.findViewById(R.id.catvermin_type_text)).getText().toString());
        verminInfoBundle.putString("TREATMENT_TYPE", ((TextView)activity.findViewById(R.id.catvermin_treatment_text)).getText().toString());
        verminInfoBundle.putString("COMMENT", ((TextView)activity.findViewById(R.id.catvermin_comment_text)).getText().toString());
        verminInfoBundle.putString("REPEAT_DATE", ((TextView)activity.findViewById(R.id.catvermin_repeat_date_text)).getText().toString());
        return verminInfoBundle;
    }
    */

    public Bundle saveClawFormInstance(){
        Bundle clawInfoBundle = new Bundle();
        clawInfoBundle.putString("START_DATE", ((TextView)activity.findViewById(R.id.catclaw_start_date_text)).getText().toString());
        clawInfoBundle.putString("COMMENT", ((TextView)activity.findViewById(R.id.catclaw_comment_text)).getText().toString());
        clawInfoBundle.putString("REPEAT_DATE", ((TextView)activity.findViewById(R.id.catclaw_repeat_date_text)).getText().toString());
        return clawInfoBundle;
    }

    public Bundle saveWeightFormInstance(){
        Bundle weightInfoBundle = new Bundle();
        weightInfoBundle.putString("START_DATE", ((TextView)activity.findViewById(R.id.catweight_start_date_text)).getText().toString());
        weightInfoBundle.putString("WEIGHT", ((TextView)activity.findViewById(R.id.catweight_weight_text)).getText().toString());
        weightInfoBundle.putString("COMMENT", ((TextView)activity.findViewById(R.id.catweight_comment_text)).getText().toString());
        weightInfoBundle.putString("CHECK_WEIGHT", ((TextView)activity.findViewById(R.id.catweight_check_weight_date_text)).getText().toString());
        return weightInfoBundle;
    }

    public Bundle saveVetDocFormInstance(){
        Bundle vetDocBundle = new Bundle();
        vetDocBundle.putString("DATE", ((TextView)activity.findViewById(R.id.catvet_reg_date_text)).getText().toString());
        vetDocBundle.putSerializable("VETDOCCHECKBOXES", activity.getRecord());
        vetDocBundle.putSerializable("VETDOCNOTES", activity.getCatvetreg_comments());
        ListView listView = (ListView) activity.findViewById(R.id.catvetreg_documentList);
        String docNames = "";
        for(int child = 0; child < listView.getChildCount(); child++){
            TextView textView = (TextView)listView.getChildAt(child);
            String text = textView.getText().toString();
            Log.i("SAVEFORMINSTANCEHANDLER", "TEXT: "+text);
            docNames+= text+":";
        }
        vetDocBundle.putString("DOCUMENTS", docNames);
        return vetDocBundle;
    }

    public Bundle saveRegSKKFormInstance(){
        Bundle regSKKBundle = new Bundle();
        regSKKBundle.putInt("REG_SKK", ((RadioGroup)activity.findViewById(R.id.catreg_skk_rg)).getCheckedRadioButtonId());
        //String skk_text = ((RadioButton)activity.findViewById(R.id.catreg_skk_yes_rb)).getText().toString();
        regSKKBundle.putString("REG_SKK_DATE", ((RadioButton)activity.findViewById(R.id.catreg_skk_yes_rb)).getText().toString());
        regSKKBundle.putInt("REG_SVE", ((RadioGroup)activity.findViewById(R.id.catreg_svekatt_rg)).getCheckedRadioButtonId());
        regSKKBundle.putString("REG_SVE_DATE", ((RadioButton)activity.findViewById(R.id.catreg_svekatt_yes_rb)).getText().toString());
        ListView listView = (ListView) activity.findViewById(R.id.catskkreg_documentList);
        String docNames = "";
        for(int child = 0; child < listView.getChildCount(); child++){
            TextView textView = (TextView)listView.getChildAt(child);
            docNames+= textView.getText().toString()+":";
        }
        regSKKBundle.putString("DOCUMENTS", docNames);
        return regSKKBundle;
    }

    public Bundle saveVetFormInstance(){
        Bundle vetBundle = new Bundle();
        vetBundle.putBoolean("NO_KNOWN", ((CheckBox)activity.findViewById(R.id.catvet_no_known)).isChecked());
        vetBundle.putString("NO_KNOWN_DATE", ((CheckBox)activity.findViewById(R.id.catvet_no_known)).getText().toString());
        vetBundle.putString("SAMPLE_DATE", ((TextView)activity.findViewById(R.id.catvet_bloodsample_date_text)).getText().toString());
        vetBundle.putString("SAMPLE_RESULT", ((TextView)activity.findViewById(R.id.catvet_bloodsample_result_text)).getText().toString());
        vetBundle.putString("KNOWN_EVENTS", ((TextView)activity.findViewById(R.id.catvet_known_events_text)).getText().toString());
        vetBundle.putString("COMMENT", ((TextView)activity.findViewById(R.id.catvet_comment_text)).getText().toString());
        return vetBundle;
    }

    public Bundle saveInsuranceFormInstance(){
        Bundle insuranceBundle = new Bundle();
        insuranceBundle.putString("TAKEN_DATE", ((TextView)activity.findViewById(R.id.catinsurance_taken_date_text)).getText().toString());
        insuranceBundle.putString("OLD_COMPANY", ((TextView)activity.findViewById(R.id.catinsurance_taken_company_text)).getText().toString());
        insuranceBundle.putString("OLD_NUMBER", ((EditText)activity.findViewById(R.id.catinsurance_old_number)).getText().toString());
        insuranceBundle.putString("SIGNED_DATE", ((TextView)activity.findViewById(R.id.catinsurance_signed_date_text)).getText().toString());
        insuranceBundle.putString("NEW_COMPANY", ((TextView)activity.findViewById(R.id.catinsurance_signed_company_text)).getText().toString());
        insuranceBundle.putString("NEW_NUMBER", ((EditText)activity.findViewById(R.id.catinsurance_new_number)).getText().toString());
        insuranceBundle.putString("TRANSFER_DATE", ((TextView)activity.findViewById(R.id.catinsurance_transfered_date_text)).getText().toString());
        return insuranceBundle;
    }

    public Bundle saveBackgroundFormInstance(){
        Bundle backgroundBundle = new Bundle();
        backgroundBundle.putString("BORN_OUTSIDE", ((CheckBox)activity.findViewById(R.id.catbackground_born_outside)).getText().toString());
        backgroundBundle.putString("HOMELESS", ((CheckBox)activity.findViewById(R.id.catbackground_homeless)).getText().toString());
        backgroundBundle.putString("DUMPED", ((CheckBox)activity.findViewById(R.id.catbackground_dumped)).getText().toString());
        backgroundBundle.putString("LOST", ((CheckBox)activity.findViewById(R.id.catbackground_lost)).getText().toString());
        backgroundBundle.putString("LEFT_BEHIND", ((CheckBox)activity.findViewById(R.id.catbackground_left)).getText().toString());
        backgroundBundle.putString("ESTATE", ((CheckBox)activity.findViewById(R.id.catbackground_estate)).getText().toString());
        backgroundBundle.putString("RELOCATE", ((CheckBox)activity.findViewById(R.id.catbackground_replacing)).getText().toString());
        backgroundBundle.putString("VIA_VET", ((CheckBox)activity.findViewById(R.id.catbackground_via_vet)).getText().toString());
        backgroundBundle.putString("COMMENT", ((TextView)activity.findViewById(R.id.catbackground_comment_text)).getText().toString());
        return backgroundBundle;
    }

    public Bundle savePrevOwnersFormInstance(){
        return null;
    }

    public Bundle saveStatusFormInstance(){
        Bundle statusBundle = new Bundle();
        //statusBundle.putInt("STATUS_RG", ((RadioGroup)activity.findViewById(R.id.catstatus_status_rg)).getCheckedRadioButtonId());

        statusBundle.putString("NOT_BOUGHT", ((RadioButton)activity.findViewById(R.id.catstatus_not_bought)).getText().toString());
        statusBundle.putString("NEWLY_ARRIVED", ((RadioButton)activity.findViewById(R.id.catstatus_new_arrived)).getText().toString());
        statusBundle.putString("ADOPTABLE", ((RadioButton)activity.findViewById(R.id.catstatus_adoptable)).getText().toString());
        statusBundle.putString("CATHOME", ((CheckBox)activity.findViewById(R.id.catstatus_cathome)).getText().toString());
        statusBundle.putString("BOOKED", ((CheckBox)activity.findViewById(R.id.catstatus_booked)).getText().toString());
        statusBundle.putString("ADOPTED", ((CheckBox)activity.findViewById(R.id.catstatus_adopted)).getText().toString());
        statusBundle.putString("CALLCENTER", ((CheckBox)activity.findViewById(R.id.catstatus_callcenter)).getText().toString());
        statusBundle.putString("TNR", ((CheckBox)activity.findViewById(R.id.catstatus_tnr)).getText().toString());
        statusBundle.putString("MOVED", ((CheckBox)activity.findViewById(R.id.catstatus_external_cathome)).getText().toString());
        statusBundle.putString("EXTERNAL_CATHOME", ((EditText)activity.findViewById(R.id.catstatus_external_cathome_text)).getText().toString());
        statusBundle.putString("MISSING", ((CheckBox)activity.findViewById(R.id.catstatus_missing)).getText().toString());
        statusBundle.putString("RETURNEDOWNER", ((CheckBox)activity.findViewById(R.id.catstatus_returned)).getText().toString());
        statusBundle.putString("DECEASED", ((CheckBox)activity.findViewById(R.id.catstatus_deceased)).getText().toString());
        /*
        statusBundle.putBoolean("BOOKED", ((CheckBox)activity.findViewById(R.id.catstatus_booked)).isChecked());
        statusBundle.putBoolean("ADOPTED", ((CheckBox)activity.findViewById(R.id.catstatus_adopted)).isChecked());
        statusBundle.putBoolean("CALLCENTER", ((CheckBox)activity.findViewById(R.id.catstatus_callcenter)).isChecked());
        statusBundle.putBoolean("TNR", ((CheckBox)activity.findViewById(R.id.catstatus_tnr)).isChecked());
        statusBundle.putBoolean("MOVED", ((CheckBox)activity.findViewById(R.id.catstatus_external_cathome)).isChecked());
        statusBundle.putString("EXTERNAL_CATHOME", ((EditText)activity.findViewById(R.id.catstatus_external_cathome_text)).getText().toString());
        statusBundle.putBoolean("MISSING", ((CheckBox)activity.findViewById(R.id.catstatus_missing)).isChecked());
        statusBundle.putBoolean("RETURNEDOWNER", ((CheckBox)activity.findViewById(R.id.catstatus_returned)).isChecked());
        statusBundle.putBoolean("DECEASED", ((CheckBox)activity.findViewById(R.id.catstatus_deceased)).isChecked());
        */

        return statusBundle;
    }

    public Bundle saveConnectionFormInstance(){
        Bundle connectionBundle = new Bundle();
        connectionBundle.putString("MOTHER", ((TextView)activity.findViewById(R.id.catconnection_mother_text)).getText().toString());
        connectionBundle.putString("FATHER", ((TextView)activity.findViewById(R.id.catconnection_father_text)).getText().toString());
        ArrayList<CharSequence> arrayList_siblings = new ArrayList<>();
        LinearLayout linearLayout_siblings = (LinearLayout) activity.findViewById(R.id.catconnection_siblings_list);
        for(int i = 0; i < linearLayout_siblings.getChildCount(); i++){
            RelativeLayout entry = (RelativeLayout) linearLayout_siblings.getChildAt(i);
            CharSequence charSequence = ((TextView)entry.findViewById(R.id.chosenCatName)).getText() + ":" + ((TextView)entry.findViewById(R.id.chosenCatID)).getText();
            arrayList_siblings.add(charSequence);
        }
        connectionBundle.putCharSequenceArrayList("SIBLINGS", arrayList_siblings);
        ArrayList<CharSequence> arrayList_children = new ArrayList<>();
        LinearLayout linearLayout_children = (LinearLayout) activity.findViewById(R.id.catconnection_children_list);
        for(int i = 0; i < linearLayout_children.getChildCount(); i++){
            RelativeLayout entry = (RelativeLayout) linearLayout_children.getChildAt(i);
            CharSequence charSequence = ((TextView)entry.findViewById(R.id.chosenCatName)).getText() + ":" + ((TextView)entry.findViewById(R.id.chosenCatID)).getText();
            arrayList_children.add(charSequence);
        }
        connectionBundle.putCharSequenceArrayList("CHILDREN", arrayList_children);
        return connectionBundle;
    }


    public Bundle saveCatsNewOwnerFormInstance(){
        Bundle ownerBundle = new Bundle();
        ownerBundle.putString("DATE", ((TextView)activity.findViewById(R.id.catform_catowner_date_text)).getText().toString());
        ownerBundle.putString("TIME", ((TextView)activity.findViewById(R.id.catform_catowner_time_text)).getText().toString());
        ArrayList<CharSequence> selectedOwners = new ArrayList<>();
        LinearLayout list = (LinearLayout)activity.findViewById(R.id.catform_new_catowner_list);
        int totalSelected = list.getChildCount();
        for(int i = 0; i < totalSelected; i++){
            RelativeLayout relativeLayout = (RelativeLayout) list.getChildAt(i);
            String element = ((TextView)relativeLayout.findViewById(R.id.chosenCatOwnerName)).getText().toString();
            element += ":"+((TextView)relativeLayout.findViewById(R.id.chosenCatownerEmail)).getText().toString();
            element = Base64.encodeToString(element.getBytes(), Base64.NO_WRAP);
            selectedOwners.add(element);
        }
        ownerBundle.putCharSequenceArrayList("SELECTED_OWNERS", selectedOwners);

        return ownerBundle;
    }



}
