package kattcenter.norrtljekattcenter.layout.personal_homepage;

import android.app.Dialog;
import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import kattcenter.norrtljekattcenter.Category;
import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.HomepageAdapter;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.catlist.ListHandler;
import kattcenter.norrtljekattcenter.catowner.CatownerManager;
import kattcenter.norrtljekattcenter.dialog.MessageDialog;
import kattcenter.norrtljekattcenter.dialog.SaveFormDialogUpdated;
import kattcenter.norrtljekattcenter.dialog.ShareDialog;
import kattcenter.norrtljekattcenter.dialog.StandardDialog;
import kattcenter.norrtljekattcenter.network.DataPacket;
import kattcenter.norrtljekattcenter.personel.PersonelManager;

import static kattcenter.norrtljekattcenter.TagHolder.JSON_TAG;

/**
 * Created by Joakim on 2017-06-25.
 */

public class HomePageHandler {

    private Context context;
    private HomePage homePage;
    private MessageDialog messageDialog;
    private StandardDialog standardDialog;
    private ShareDialog shareDialog;
    //private SaveFormDialog saveFormDialog;
    private SaveFormDialogUpdated saveFormDialogUpdated;
    private CatownerManager catownerManager;
    private PersonelManager personelManager;
    private ListHandler listHandler;
    private Type priv;
    private boolean adminLoadPersonalInfo = false;

    public HomePageHandler(Context context){
        this.context = context;
        homePage = (HomePage)this.context;
        messageDialog = new MessageDialog(context);
        standardDialog = new StandardDialog(context);
        shareDialog = new ShareDialog(context);
        //saveFormDialog = new SaveFormDialog(context);
        saveFormDialogUpdated = new SaveFormDialogUpdated(context);
        catownerManager = new CatownerManager(context);
        personelManager = new PersonelManager(context);
        listHandler = new ListHandler(context);
    }

    /**
     * CALLED BY CATOWNER - LESS AUTHORIZED AND FUNCTIONS-RELIABLE
     * @param context
     * @param priv
     */
    public HomePageHandler(Context context, Type priv){
        this.context = context;
        homePage = (HomePage)this.context;
        this.priv = priv;
        standardDialog = new StandardDialog(context);
        saveFormDialogUpdated = new SaveFormDialogUpdated(context);
        catownerManager = new CatownerManager(context, priv);
        listHandler = new ListHandler(context, true);
    }

    public void initPersonalHomepage() {

        ArrayList<Category> categories = new ArrayList<>();
        categories.add(new Category("Alla katter", 0, Type.ALL_CATS));
        categories.add(new Category("Katter på katthemmet", 0, Type.CATS_HOME));
        categories.add(new Category("Katter i jourhem", 0, Type.CATS_CENTER));
        categories.add(new Category("TNR", 0, Type.TNR));
        categories.add(new Category("Adopterade", 0, Type.ADOPTED));
        categories.add(new Category("Avlidna", 0, Type.DECEASED));
        categories.add(new Category("Åter ägaren", 0, Type.RETURNED_TO_OWNER));
        categories.add(new Category("Saknade", 0, Type.MISSING));
        categories.add(new Category("Väntelista", 0, Type.WAITING_LIST));
        //categories.add(new Category("Kattägare", 0, Type.CATOWNER));
        homePage.getHomepageAdapter().setCategories(categories);
        //refreshPersonalHomepage();
    }

    public void reloadPersonalHomepage(String[] blub) {
        ArrayList<Category> categories = new ArrayList<>();
        int totalCats = 0;
        int onHome = 0;
        int onCallcenter = 0;
        int onTNR = 0;
        int adopted = 0;
        int deceased = 0;
        int returnedOwner = 0;
        int missing = 0;
        int waiting = 0;
        //int catowners = 0;
        String cats_table = blub[1];
        String catowners_table = blub[2];
        String content = new String(Base64.decode(cats_table.getBytes(), Base64.NO_WRAP));
        String content2 = new String(Base64.decode(catowners_table.getBytes(), Base64.NO_WRAP));
        String[] contentArray = content.split(":");
        String[] content2Array = content2.split(":");
        if (contentArray.length == 2) {
            content = contentArray[1];
            content = new String(Base64.decode(content.getBytes(), Base64.NO_WRAP));
            String[] jsonRows = content.split("\n");
            try {
                Log.e(JSON_TAG, "Total json rows: " + jsonRows.length);
                for (String json : jsonRows) {
                    if(json != null && !json.isEmpty() && !json.equals("null")) {
                        Log.e(JSON_TAG, "JSON: "+json);
                        JSONObject jsonObject = new JSONObject(json);

                        if (jsonObject.getInt("onCatHome") == 1) onHome++;
                        if (jsonObject.getInt("onCallCenter") == 1) onCallcenter++;
                        if (jsonObject.getInt("onTNR") == 1) onTNR++;
                        if (jsonObject.getInt("adopted") == 1) adopted++;
                        if (jsonObject.getInt("deceased") == 1) deceased++;
                        if (jsonObject.getInt("returnedOwner") == 1) returnedOwner++;
                        if (jsonObject.getInt("missing") == 1) missing++;
                        if (jsonObject.getInt("waiting") == 1) waiting++;
                        totalCats++;
                    }
                }
            } catch (JSONException jsonE) {
                jsonE.printStackTrace();
            }
        }
        if (content2Array.length == 2) {
            content2 = content2Array[1];
            content2 = new String(Base64.decode(content2.getBytes(), Base64.NO_WRAP));
            String[] jsonRows2 = content2.split("\n");
            //catowners = jsonRows2.length;
        }
        categories.add(new Category("Alla katter", totalCats, Type.ALL_CATS));
        categories.add(new Category("Katter på katthemmet", onHome, Type.CATS_HOME));
        categories.add(new Category("Katter i jourhem", onCallcenter, Type.CATS_CENTER));
        categories.add(new Category("TNR", onTNR, Type.TNR));
        categories.add(new Category("Adopterade", adopted, Type.ADOPTED));
        categories.add(new Category("Avlidna", deceased, Type.DECEASED));
        categories.add(new Category("Åter ägaren", returnedOwner, Type.RETURNED_TO_OWNER));
        categories.add(new Category("Saknade", missing, Type.MISSING));
        categories.add(new Category("Väntelista", waiting, Type.WAITING_LIST));
        //categories.add(new Category("Kattägare", catowners, Type.CATOWNER));
        homePage.getHomepageAdapter().setCategories(categories);
        homePage.getHomepageAdapter().notifyDataSetChanged();
        closeStandardDialog();
    }

    // REFRESH HOMEPAGE
    public void refreshPersonalHomepage() {
        HashMap<String, String> data = new HashMap<>();
        String blub = Base64.encodeToString("DOWNLOAD_CATEGORIES".getBytes(), Base64.NO_WRAP);
        data.put("blub", blub);

        try {
            homePage.addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_ALL));
            homePage.signalQueueToSend();
            //networkThread.addDataToQueue(new DataPacket(data, Type.DOWNLOAD_ALL));
            //networkThread.nextInQueue();
            standardDialog.showLoadingDialog("Hämtar kategorier...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // new DatabaseConnection(this, data, Type.DOWNLOAD_ALL).execute();

    }

    public void loadCatownerHomepage(String email){
        HashMap<String, String> data = new HashMap<>(), data2 = new HashMap<>();
        String owner_name = ((TextView)homePage.getSettingsPage().findViewById(R.id.user_name_text)).getText().toString();
        String owner = Base64.encodeToString((owner_name+":"+email).getBytes(), Base64.NO_WRAP);
        String blub = "CATOWNER_CATS:"+owner;
        String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        data.put("blub", encoded_blub);

        blub = "CATOWNER_PRIVATE_CATS:"+owner;
        String encoded_blub2 = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        data2.put("blub", encoded_blub2);
        homePage.prepareAndSendData(data, Type.DOWNLOAD_LIST);
        homePage.prepareAndSendData(data2, Type.DOWNLOAD_LIST);
        standardDialog.showLoadingDialog("Hämtar katter...");
        /*
        try {
            homePage.addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_LIST));
            homePage.signalQueueToSend();
            //networkThread.addDataToQueue(new DataPacket(data, Type.DOWNLOAD_ALL));
            //networkThread.nextInQueue();
            standardDialog.showLoadingDialog("Hämtar katter...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        */
    }

    public void refreshSettingsPage(String user, String email){
        //String formTable = "catowner";
        String blub = user+":"+email;
        String encoded_blub = Base64.encodeToString(blub.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> data = new HashMap<>();
        data.put("form_action", "DOWNLOAD");
        data.put("blub", encoded_blub);
        try {
            homePage.addDatapacketToNetworkThread(new DataPacket(data, Type.DOWNLOAD_LIST));
            homePage.signalQueueToSend();
            //((HomePage)context).showLoadingDialog("Laddar information...");
            //homePage.getHomePageHandler().showStandardDialog("Laddar information...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setAdminLoadPersonalInfo(boolean adminLoadPersonalInfo){
        this.adminLoadPersonalInfo = adminLoadPersonalInfo;
    }

    private boolean getAdminLoadPersonalInfo(){
        return adminLoadPersonalInfo;
    }

    public void recievedPersonelInfo(String json){
        priv = homePage.getPrivileged();
        Log.i("HOMEPAGEHANDLER", "PRIV: "+priv);
        if(priv == Type.PERSONAL){
            receivedPersonelSettings(json);
        }
        else if(priv == Type.ADMIN){
            if(getAdminLoadPersonalInfo()){
                personelManager.loadPersonelInfo(json);
                setAdminLoadPersonalInfo(false);
                standardDialog.closeLoadingDialog();
            }
            else{
                receivedPersonelSettings(json);
            }
        }
    }

    public void receivedPersonelSettings(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            //String ssn = new String(Base64.decode(jsonObject.getString("ssn").getBytes(), Base64.NO_WRAP));
            homePage.setCurrentUserName(jsonObject.getString("surname")); //NECESSARY?
            String id = jsonObject.getString("id");
            String surName = jsonObject.getString("surname");
            String lastName = jsonObject.getString("lastname");
            String address = jsonObject.getString("address");
            String zipcode = jsonObject.getString("zipcode");
            String city = jsonObject.getString("city");
            String cell = jsonObject.getString("cell");
            //String mail = jsonObject.getString("email");
            HashMap<String, String> catOwnerInfoData = new HashMap<>();
            catOwnerInfoData.put("id", id);
            catOwnerInfoData.put("surname", surName);
            catOwnerInfoData.put("lastname", lastName);
            catOwnerInfoData.put("cell", cell);
            catOwnerInfoData.put("address", address);
            catOwnerInfoData.put("zipcode", zipcode);
            catOwnerInfoData.put("city", city);
            updateSettingsPage(catOwnerInfoData);

        } catch (JSONException jsone){
            jsone.printStackTrace();
        }
    }

    public void loadAboutUsPage(){
        HashMap<String, String> data = new HashMap<>();
        data.put("misc_action", "get_open_hours");
        homePage.prepareAndSendData(data, Type.DOWNLOAD_MISC);
    }

    public void updateSettingsPage(HashMap<String, String> account_info){
        RelativeLayout settingsPage = homePage.getSettingsPage();
        if(account_info.containsKey("id")){
            homePage.setCurrentUserId(account_info.get("id"));
        }
        if(account_info.containsKey("surname") && account_info.containsKey("lastname")){
            ((TextView)settingsPage.findViewById(R.id.user_name_text)).setText(account_info.get("surname")+" "+account_info.get("lastname"));

        }
        if(account_info.containsKey("ssn")){
            ((TextView)settingsPage.findViewById(R.id.user_ssn_text)).setText(account_info.get("ssn"));
        }
        if(account_info.containsKey("cell")){
            ((TextView)settingsPage.findViewById(R.id.user_cell_text)).setText(account_info.get("cell"));
        }
        if(account_info.containsKey("address")){
            ((TextView)settingsPage.findViewById(R.id.user_street_info_text)).setText(account_info.get("address"));
        }
        if(account_info.containsKey("zipcode")){
            ((TextView)settingsPage.findViewById(R.id.user_zipcode_info_text)).setText(account_info.get("zipcode"));
        }
        if(account_info.containsKey("city")){
            ((TextView)settingsPage.findViewById(R.id.user_city_info_text)).setText(account_info.get("city"));
        }
    }

    /*
    public void handleDownloadedCatownerCatsInfo(String decoded_cat_info){
        Log.i("HOMEPAGEHANDLER", "DOWNLOADED CATOWNER CATS");
        if(priv != null && priv == Type.CATOWNER){
            //CATOWNER
            listHandler.addCatsToList(decoded_cat_info);
        }
        else{
            //ADMIN OR PERSONAL
            catownerManager.loadCatOwnerCatsInfo(decoded_cat_info);
        }
    }
    */


    public CatownerManager getCatownerManager(){
        return catownerManager;
    }

    public PersonelManager getPersonelManager() { return personelManager; }

    public ListHandler getListHandler(){
        return listHandler;
    }

    public void showStandardDialog(String title){
        standardDialog.showLoadingDialog(title);
    }

    public void closeStandardDialog(){
        if(standardDialog != null)
            standardDialog.closeLoadingDialog();
    }

    public void showSaveformDialog(){
        //saveFormDialog.show();
        saveFormDialogUpdated.showDialog();
    }

    public void closeSaveformDialog(){
        //saveFormDialog.close();
        saveFormDialogUpdated.closeDialog();
    }

    public boolean isSaveformShowing(){
        //return saveFormDialog.isDialogShowing();
        return saveFormDialogUpdated.isShowing();
    }

    public void showMessageDialog(String title, String message){
        messageDialog.showDialog(title, message);
    }

    public void closeMessageDialog(){
        messageDialog.closeDialog();
    }

    public void showShareOptionsDialog(){
        final Dialog dialog = new Dialog(context);
        LinearLayout content = (LinearLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.new_dialog_share_options, null);
        dialog.setContentView(content);
        content.findViewById(R.id.mail_form_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showShareOptionsDialog("MAIL");
            }
        });
        /*
        content.findViewById(R.id.pdf_form_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showShareOptionsDialog("PDF");
            }
        });
        */
        dialog.show();
    }

    public void showShareOptionsDialog(String option) {
        shareDialog.showShareOptionDialog(option);
    }

    public void initPDFGenerator(){
        shareDialog.initPDFDocumentGenerator();
    }

    public void addContentToPDfGenerator(String table, String content){
        shareDialog.addContentToPDF(table, content);
    }

    public void closeShareLoadingDialog(){
        shareDialog.closeLoadingDialog();
    }

    /*
    public Dialog getSaveFormsDialog(){
        //return saveFormDialog.getSaveFormsDialog();
    }
    */
}
