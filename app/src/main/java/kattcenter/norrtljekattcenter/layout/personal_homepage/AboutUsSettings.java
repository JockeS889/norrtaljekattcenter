package kattcenter.norrtljekattcenter.layout.personal_homepage;

import android.app.Dialog;
import android.content.Context;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.R;
import kattcenter.norrtljekattcenter.Type;
import kattcenter.norrtljekattcenter.network.DataPacket;

/**
 * Created by Joakim on 2017-07-26.
 */

public class AboutUsSettings {

    private Context context;
    private RelativeLayout openHoursContent;
    private HashMap<String, Integer> textToInt = new HashMap<>();
    public AboutUsSettings(Context context){
        this.context = context;
        textToInt.put("10:00", 0);
        textToInt.put("11:00", 1);
        textToInt.put("12:00", 2);
        textToInt.put("13:00", 3);
        textToInt.put("14:00", 4);
        textToInt.put("15:00", 5);
        textToInt.put("16:00", 6);
        textToInt.put("17:00", 7);
        textToInt.put("18:00", 8);
    }

    public void showDialog(JSONObject jsonObject){
        try{
            String mon_open = jsonObject.getString("monday_open");
            String mon_close = jsonObject.getString("monday_close");
            String mon_is_closed = jsonObject.getString("monday_is_closed");
            String tue_open = jsonObject.getString("tuesday_open");
            String tue_close = jsonObject.getString("tuesday_close");
            String tue_is_closed = jsonObject.getString("tuesday_is_closed");
            String wed_open = jsonObject.getString("wednesday_open");
            String wed_close = jsonObject.getString("wednesday_close");
            String wed_is_closed = jsonObject.getString("wednesday_is_closed");
            String thu_open = jsonObject.getString("thursday_open");
            String thu_close = jsonObject.getString("thursday_close");
            String thu_is_closed = jsonObject.getString("thursday_is_closed");
            String fri_open = jsonObject.getString("friday_open");
            String fri_close = jsonObject.getString("friday_close");
            String fri_is_closed = jsonObject.getString("friday_is_closed");
            String sat_open = jsonObject.getString("saturday_open");
            String sat_close = jsonObject.getString("saturday_close");
            String sat_is_closed = jsonObject.getString("saturday_is_closed");
            String sun_open = jsonObject.getString("sunday_open");
            String sun_close = jsonObject.getString("sunday_close");
            String sun_is_closed = jsonObject.getString("sunday_is_closed");

            final Dialog setOpeningHoursDialog = new Dialog(context);
            setOpeningHoursDialog.setTitle("Öppettider");
            openHoursContent = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.set_aboutus, null);
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.monday_start_hour), textToInt.get(mon_open));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.monday_end_hour), textToInt.get(mon_close));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.tuesday_start_hour), textToInt.get(tue_open));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.tuesday_end_hour), textToInt.get(tue_close));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.wednesday_start_hour), textToInt.get(wed_open));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.wednesday_end_hour), textToInt.get(wed_close));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.thursday_start_hour), textToInt.get(thu_open));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.thursday_end_hour), textToInt.get(thu_close));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.friday_start_hour), textToInt.get(fri_open));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.friday_end_hour), textToInt.get(fri_close));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.saturday_start_hour), textToInt.get(sat_open));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.saturday_end_hour), textToInt.get(sat_close));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.sunday_start_hour), textToInt.get(sun_open));
            initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.sunday_end_hour), textToInt.get(sun_close));
            ((CheckBox)openHoursContent.findViewById(R.id.monday_closed)).setChecked(Boolean.parseBoolean(mon_is_closed));
            ((CheckBox)openHoursContent.findViewById(R.id.tuesday_closed)).setChecked(Boolean.parseBoolean(tue_is_closed));
            ((CheckBox)openHoursContent.findViewById(R.id.wednesday_closed)).setChecked(Boolean.parseBoolean(wed_is_closed));
            ((CheckBox)openHoursContent.findViewById(R.id.thursday_closed)).setChecked(Boolean.parseBoolean(thu_is_closed));
            ((CheckBox)openHoursContent.findViewById(R.id.friday_closed)).setChecked(Boolean.parseBoolean(fri_is_closed));
            ((CheckBox)openHoursContent.findViewById(R.id.saturday_closed)).setChecked(Boolean.parseBoolean(sat_is_closed));
            ((CheckBox)openHoursContent.findViewById(R.id.sunday_closed)).setChecked(Boolean.parseBoolean(sun_is_closed));
            CheckBox abnormalOpenTimes = (CheckBox) openHoursContent.findViewById(R.id.enableAbnormalTimes);
            abnormalOpenTimes.setChecked(jsonObject.getBoolean("divergent"));
            abnormalOpenTimes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    //footerBar.findViewById(R.id.alert_pic_image).setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
                }
            });

            ((EditText)openHoursContent.findViewById(R.id.commentFieldOpenHours)).setText(jsonObject.getString("comment"));

            Button saveOpenHours = (Button) openHoursContent.findViewById(R.id.saveOpenHours);
            saveOpenHours.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        saveSettings();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    setOpeningHoursDialog.dismiss();
                }
            });

            setOpeningHoursDialog.setContentView(openHoursContent);
            setOpeningHoursDialog.show();

            /*

            String tue_text = jsonObject.getString("tuesday_open")+"-"+jsonObject.getString("tuesday_close");
            String wed_text = jsonObject.getString("wednesday_open")+"-"+jsonObject.getString("wednesday_close");
            String thu_text = jsonObject.getString("thursday_open")+"-"+jsonObject.getString("thursday_close");
            String fri_text = jsonObject.getString("friday_open")+"-"+jsonObject.getString("friday_close");
            String sat_text = jsonObject.getString("saturday_open")+"-"+jsonObject.getString("saturday_close");
            String sun_text = jsonObject.getString("sunday_open")+"-"+jsonObject.getString("sunday_close");
            mon_text = (jsonObject.getString("monday_is_closed").equals("1")) ? "Stängt" : mon_text;
            tue_text = (jsonObject.getString("tuesday_is_closed").equals("1")) ? "Stängt" : tue_text;
            wed_text = (jsonObject.getString("wednesday_is_closed").equals("1")) ? "Stängt" : wed_text;
            thu_text = (jsonObject.getString("thursday_is_closed").equals("1")) ? "Stängt" : thu_text;
            fri_text = (jsonObject.getString("friday_is_closed").equals("1")) ? "Stängt" : fri_text;
            sat_text = (jsonObject.getString("saturday_is_closed").equals("1")) ? "Stängt" : sat_text;
            sun_text = (jsonObject.getString("sunday_is_closed").equals("1")) ? "Stängt" : sun_text;
            */
        } catch (JSONException jsone){
            jsone.printStackTrace();
        }
        /*
        final Dialog setOpeningHoursDialog = new Dialog(context);
        setOpeningHoursDialog.setTitle("Öppettider");
        openHoursContent = (RelativeLayout) ((HomePage)context).getLayoutInflater().inflate(R.layout.set_aboutus, null);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.monday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.monday_end_hour), 8);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.tuesday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.tuesday_end_hour), 8);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.wednesday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.wednesday_end_hour), 8);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.thursday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.thursday_end_hour), 8);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.friday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.friday_end_hour), 6);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.saturday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.saturday_end_hour), 1);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.sunday_start_hour), 0);
        initOpenHourSpinner((Spinner)openHoursContent.findViewById(R.id.sunday_end_hour), 1);
        CheckBox abnormalOpenTimes = (CheckBox) openHoursContent.findViewById(R.id.enableAbnormalTimes);
        //abnormalOpenTimes.setChecked(footerBar.findViewById(R.id.alert_pic_image).getVisibility() == View.VISIBLE);
        abnormalOpenTimes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                //footerBar.findViewById(R.id.alert_pic_image).setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
            }
        });

        Button saveOpenHours = (Button) openHoursContent.findViewById(R.id.saveOpenHours);
        saveOpenHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    saveSettings();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setOpeningHoursDialog.dismiss();
            }
        });

        setOpeningHoursDialog.setContentView(openHoursContent);
        setOpeningHoursDialog.show();
        */
    }

    private void initOpenHourSpinner(Spinner spinner, int position){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.open_hours, R.layout.dropdown_textview_2);
        spinner.setAdapter(adapter);
        spinner.setSelection(position);
    }

    private void saveSettings() throws InterruptedException, JSONException {
        String selectedTime_openMonday = (String)((Spinner)(openHoursContent.findViewById(R.id.monday_start_hour))).getSelectedItem();
        String selectedTime_closeMonday = (String)((Spinner)(openHoursContent.findViewById(R.id.monday_end_hour))).getSelectedItem();
        String selectedTime_openTuesday = (String)((Spinner)(openHoursContent.findViewById(R.id.tuesday_start_hour))).getSelectedItem();
        String selectedTime_closeTuesday = (String)((Spinner)(openHoursContent.findViewById(R.id.tuesday_end_hour))).getSelectedItem();
        String selectedTime_openWednesday = (String)((Spinner)(openHoursContent.findViewById(R.id.wednesday_start_hour))).getSelectedItem();
        String selectedTime_closeWednesday = (String)((Spinner)(openHoursContent.findViewById(R.id.wednesday_end_hour))).getSelectedItem();
        String selectedTime_openThursday = (String)((Spinner)(openHoursContent.findViewById(R.id.thursday_start_hour))).getSelectedItem();
        String selectedTime_closeThursday = (String)((Spinner)(openHoursContent.findViewById(R.id.thursday_end_hour))).getSelectedItem();
        String selectedTime_openFriday = (String)((Spinner)(openHoursContent.findViewById(R.id.friday_start_hour))).getSelectedItem();
        String selectedTime_closeFriday = (String)((Spinner)(openHoursContent.findViewById(R.id.friday_end_hour))).getSelectedItem();
        String selectedTime_openSaturday = (String)((Spinner)(openHoursContent.findViewById(R.id.saturday_start_hour))).getSelectedItem();
        String selectedTime_closeSaturday = (String)((Spinner)(openHoursContent.findViewById(R.id.saturday_end_hour))).getSelectedItem();
        String selectedTime_openSunday = (String)((Spinner)(openHoursContent.findViewById(R.id.sunday_start_hour))).getSelectedItem();
        String selectedTime_closeSunday = (String)((Spinner)(openHoursContent.findViewById(R.id.sunday_end_hour))).getSelectedItem();
        boolean mondayClosed = ((CheckBox)openHoursContent.findViewById(R.id.monday_closed)).isChecked();
        boolean tuesdayClosed = ((CheckBox)openHoursContent.findViewById(R.id.tuesday_closed)).isChecked();
        boolean wednesdayClosed = ((CheckBox)openHoursContent.findViewById(R.id.wednesday_closed)).isChecked();
        boolean thursdayClosed = ((CheckBox)openHoursContent.findViewById(R.id.thursday_closed)).isChecked();
        boolean fridayClosed = ((CheckBox)openHoursContent.findViewById(R.id.friday_closed)).isChecked();
        boolean saturdayClosed = ((CheckBox)openHoursContent.findViewById(R.id.saturday_closed)).isChecked();
        boolean sundayClosed = ((CheckBox)openHoursContent.findViewById(R.id.sunday_closed)).isChecked();

        boolean divergent = ((CheckBox)openHoursContent.findViewById(R.id.enableAbnormalTimes)).isChecked();
        String comment = ((EditText)openHoursContent.findViewById(R.id.commentFieldOpenHours)).getText().toString();

        HashMap<String, String> data = new HashMap<>();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("monday_open", selectedTime_openMonday);
        jsonObject.put("monday_close", selectedTime_closeMonday);
        jsonObject.put("monday_is_closed", mondayClosed);
        jsonObject.put("tuesday_open", selectedTime_openTuesday);
        jsonObject.put("tuesday_close", selectedTime_closeTuesday);
        jsonObject.put("tuesday_is_closed", tuesdayClosed);
        jsonObject.put("wednesday_open", selectedTime_openWednesday);
        jsonObject.put("wednesday_close", selectedTime_closeWednesday);
        jsonObject.put("wednesday_is_closed", wednesdayClosed);
        jsonObject.put("thursday_open", selectedTime_openThursday);
        jsonObject.put("thursday_close", selectedTime_closeThursday);
        jsonObject.put("thursday_is_closed", thursdayClosed);
        jsonObject.put("friday_open", selectedTime_openFriday);
        jsonObject.put("friday_close", selectedTime_closeFriday);
        jsonObject.put("friday_is_closed", fridayClosed);
        jsonObject.put("saturday_open", selectedTime_openSaturday);
        jsonObject.put("saturday_close", selectedTime_closeSaturday);
        jsonObject.put("saturday_is_closed", saturdayClosed);
        jsonObject.put("sunday_open", selectedTime_openSunday);
        jsonObject.put("sunday_close", selectedTime_closeSunday);
        jsonObject.put("sunday_is_closed", sundayClosed);

        jsonObject.put("divergent_hours", divergent ? "true" : "false");
        jsonObject.put("comment", comment);

        String blub = Base64.encodeToString(jsonObject.toString().getBytes(), Base64.NO_WRAP);
        data.put("misc_action", "update_open_hours");
        data.put("blub", blub);
        //((HomePage)context).addDatapacketToNetworkThread(new DataPacket(data, Type.UPLOAD_MISC));
        ((HomePage)context).prepareAndSendData(data, Type.UPLOAD_MISC);
        ((HomePage)context).updateOpenHours(jsonObject);

    }
}
