package kattcenter.norrtljekattcenter;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntRange;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.*;

import java.util.List;

/**
 * Created by Joakim on 2017-11-08.
 */

public class AppMenuListAdapter extends ArrayAdapter<HamburgerListItem>{


    private Context context;
    private List<HamburgerListItem> drawerItemList;
    private int layoutResID;
    private int selectedItem = 0;

    public AppMenuListAdapter(Context context, int layoutResourceID,
                                List<HamburgerListItem> listItems) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;

    }

    public void setSelectedItem(int selectedItem){
        this.selectedItem = selectedItem;
    }

    public void setDrawerItemList(List<HamburgerListItem> drawerItemList){
        this.drawerItemList = drawerItemList;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view = convertView;
        AppMenuListItemHolder appMenuListItemHolder;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            appMenuListItemHolder = new AppMenuListItemHolder();
            view = inflater.inflate(R.layout.app_menu_list_item, parent, false);
            appMenuListItemHolder.ItemName = (TextView) view
                    .findViewById(R.id.drawer_itemName);
            appMenuListItemHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);



            /*
            appMenuListItemHolder.circularImageView = (CircularImageView) view.findViewById(R.id.circular_imageview);

            appMenuListItemHolder.circularImageView.setBorderColor(Color.WHITE);
            appMenuListItemHolder.circularImageView.setBorderWidth(1);

            appMenuListItemHolder.circularImageView.addShadow();
            */





            view.setTag(appMenuListItemHolder);
        }
        else{
            appMenuListItemHolder = (AppMenuListItemHolder) view.getTag();
        }
        HamburgerListItem dItem = this.drawerItemList.get(position);
        Drawable gray_circle = context.getResources().getDrawable(R.drawable.circular_border, null);
        Drawable white_circle = context.getResources().getDrawable(R.drawable.circular_border_white, null);
        if(position == selectedItem) {
            appMenuListItemHolder.icon.setBackground(white_circle);
            appMenuListItemHolder.icon.setColorFilter(Color.argb(255, 255, 255, 255));
            appMenuListItemHolder.ItemName.setTextColor(Color.argb(255, 255, 255, 255));
        }
        else {
            appMenuListItemHolder.icon.setBackground(gray_circle);
            appMenuListItemHolder.icon.setColorFilter(Color.argb(255, 170, 170, 170));
            appMenuListItemHolder.ItemName.setTextColor(Color.argb(255, 170, 170, 170));
        }
        appMenuListItemHolder.icon.setImageDrawable(view.getResources().getDrawable(dItem.getImgResID(), null));
       // appMenuListItemHolder.circularImageView.setImageDrawable(view.getResources().getDrawable(dItem.getImgResID(), null));
        ((TextView)view.findViewById(R.id.drawer_itemName)).setText(dItem.getItemName());

        return view;
    }

    private static class AppMenuListItemHolder {
        TextView ItemName;
        ImageView icon;
        CircularImageView circularImageView;
    }
}
