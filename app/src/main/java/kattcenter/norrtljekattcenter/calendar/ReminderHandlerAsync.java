package kattcenter.norrtljekattcenter.calendar;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableFuture;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.sms.SmsSender;

/**
 * Created by Joakim on 2017-06-27.
 */

public class ReminderHandlerAsync extends AsyncTask<Void, Void, String> {

    private Context context;
    private SmsSender smsSender;
    private String timeStamp;
    private Date current_date;
    private long current_date_long;
    private LinkedBlockingQueue<String[]> smsMessages = new LinkedBlockingQueue<>();
    private LinkedBlockingQueue<String[]> reminderMessages = new LinkedBlockingQueue<>();


    public ReminderHandlerAsync(Context context, SmsSender smsSender, String reminders){
        this.context = context;
        this.smsSender = smsSender;
        addReminder(reminders);
    }

    private void addReminder(String reminders){
        String[] reminder = reminders.split(":");
        if(reminder.length == 2) {
            switch (reminder[0]) {
                case "yearly_reminder":
                    sendYearlyReminders(new String(Base64.decode(reminder[1].getBytes(), Base64.NO_WRAP)));
                    break;
                case "monthly_reminders":
                    sendMontlyReminders(new String(Base64.decode(reminder[1].getBytes(), Base64.NO_WRAP)));
                    break;
                default:
                    break;
            }
        }
    }


    private void sendYearlyReminders(String allReminders){
        try{
            String[] rows = allReminders.split("\n");
            for(String json: rows){
                JSONObject jsonObject = new JSONObject(json);
                String cell = jsonObject.getString("cell");
                String last_login = jsonObject.getString("last_login");
                String yearly_reminded = jsonObject.getString("yearly_reminded");
                if(!Boolean.parseBoolean(yearly_reminded) && last_login != null && !last_login.equals("")){
                    Date last_login_date = new SimpleDateFormat("yyyy-MM-dd_HH-mm").parse(last_login);

                    long last_login_date_long = last_login_date.getTime();
                    long year_reminder_limit = 28512000000L;    // ~ 11 months
                    if(current_date_long - last_login_date_long > year_reminder_limit){
                        reminderMessages.put(new String[]{cell, "Det har snart gått sedan du checkade in på katthemmet. Logga in för att berätta hur XX mår och hur det går för er, samt kolla att dina kontaktuppgifter stämmer"});
                    }
                    else{
                        System.out.println("CELL "+cell);
                    }

                }
            }
            //((HomePage)context).getHomePageHandler().closeStandardDialog();
            //System.out.println("CLOSE DIALOG REMINDERS");

        } catch ( JSONException jsone){
            jsone.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void sendMontlyReminders(String allReminders){
        try{
            if(allReminders != null && !allReminders.isEmpty() && !allReminders.equals("") && !allReminders.equals("[]") && !allReminders.equals("null")) {
                JSONObject jsonObject = new JSONObject(allReminders);
                Iterator<String> keys = jsonObject.keys();
                while (keys.hasNext()) {
                    String mail = keys.next();
                    JSONObject value = jsonObject.getJSONObject(mail);
                    String catId = value.getString("catid");
                    JSONArray reminders = value.getJSONArray("reminders");
                    String forms = "";
                    for (int i = 0; i < reminders.length(); i++) {
                        forms += reminders.getString(i) + ", ";
                    }
                    forms = forms.substring(0, forms.length() - 2);
                    System.out.println("Skickar mail till [" + mail + "] om påminnelse för " + forms + " till katt " + catId);
                    reminderMessages.put(new String[]{mail, "Välkommen in till Kattcentret för " + forms + " denna månad"});
                }
            }
            //((HomePage)context).closeLoadingDialog();
            //((HomePage)context).getHomePageHandler().closeStandardDialog();
           // System.out.println("CLOSE DIALOG REMINDERS");

        } catch ( JSONException jsone){
            jsone.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }




    @Override
    protected void onPreExecute(){
        timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(Calendar.getInstance().getTime());
        try{
            current_date = new SimpleDateFormat("yyyy-MM-dd_HH-mm").parse(timeStamp);
            current_date_long = current_date.getTime();
        } catch (ParseException pe){
            pe.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(Void... params){
        while(!reminderMessages.isEmpty())
            try {
                String[] reminderMessage = reminderMessages.take();
                //System.out.println("[SENDING SMS]\nNr: "+smsMessage[0]+"\nMessage:"+smsMessage[1]);
                System.out.println("Skickar meddelande till "+reminderMessage[0]+": "+reminderMessage[1]);
                ((HomePage)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((HomePage)context).incrementReminderStatusBar();
                    }
                });
                //Thread.sleep(2000);
                //smsSender.sendSms(smsMessage[0], smsMessage[1]);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        ((HomePage)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((HomePage)context).reminderCompleted();
            }
        });
        return null;
    }

    @Override
    protected void onPostExecute(String result){

    }

    @Override
    protected void onCancelled(){

    }
}
