package kattcenter.norrtljekattcenter.calendar;

import android.content.Context;
import android.icu.util.DateInterval;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.sms.SmsSender;

/**
 * Created by Jocke on 2017-06-20.
 */

public class ReminderHandler extends Thread{

    /*
    private Context context;
    private SmsSender smsSender;
    private LinkedBlockingQueue<String[]> linkedBlockingQueue = new LinkedBlockingQueue<>();
    private LinkedBlockingQueue<String[]> smsMessages = new LinkedBlockingQueue<>();
    private String timeStamp;
    private Date current_date;
    private long current_date_long;

    public ReminderHandler(Context context, SmsSender smsSender){
        this.context = context;
        this.smsSender = smsSender;
        //this.smsSender.setReminderHandler(this);
        timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(Calendar.getInstance().getTime());
        //String currentDate = timeStamp.split("_")[0];
        try {
            current_date = new SimpleDateFormat("yyyy-MM-dd_HH-mm").parse(timeStamp);
            current_date_long = current_date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void addReminder(String result){
        String[] reminder = result.split(":");
        if(reminder.length == 2) {
            switch (reminder[0]) {
                case "yearly_reminder":
                    sendYearlyReminders(new String(Base64.decode(reminder[1].getBytes(), Base64.NO_WRAP)));
                    break;
                case "monthly_reminders":
                    sendMontlyReminders(new String(Base64.decode(reminder[1].getBytes(), Base64.NO_WRAP)));
                    break;
                default:
                    break;
            }
        }
    }

    public void nextReminder(String lastResult){

    }

    private void sendYearlyReminders(String allReminders){
        try{
            String[] rows = allReminders.split("\n");
            for(String json: rows){
                JSONObject jsonObject = new JSONObject(json);
                String cell = jsonObject.getString("cell");
                String last_login = jsonObject.getString("last_login");
                String yearly_reminded = jsonObject.getString("yearly_reminded");
                if(!Boolean.parseBoolean(yearly_reminded) && last_login != null && !last_login.equals("")){

                    //System.out.println("LAST LOGIN: "+last_login+"      TIMESTAMP: "+timeStamp);

                    //String[] currentTime = timeStamp.split("_")[1].split("-");
                    String lastLoginDate = last_login.split("_")[0];
                    Date last_login_date = new SimpleDateFormat("yyyy-MM-dd_HH-mm").parse(last_login);

                    long last_login_date_long = last_login_date.getTime();
                    long year_reminder_limit = 28512000000L;    // ~ 11 months
                    if(current_date_long - last_login_date_long > year_reminder_limit){
                        smsMessages.put(new String[]{cell, "Det har snart gått sedan du checkade in på katthemmet. Logga in för att berätta hur XX mår och hur det går för er, samt kolla att dina kontaktuppgifter stämmer"});
                    }
                    else{
                        System.out.println("CELL "+cell);
                    }




                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        DateInterval dateInterval = new DateInterval(last_login_date_long, current_date_long);
                    }
                    else{

                    }

                }
            }
            //((HomePage)context).closeLoadingDialog();
            ((HomePage)context).getHomePageHandler().closeStandardDialog();
            System.out.println("CLOSE DIALOG REMINDERS");

        } catch ( JSONException jsone){
            jsone.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void sendMontlyReminders(String allReminders){
        try{
            JSONObject jsonObject = new JSONObject(allReminders);
            Iterator<String> keys = jsonObject.keys();
            while(keys.hasNext()){
                String mail = keys.next();
                JSONObject value = jsonObject.getJSONObject(mail);
                String catId = value.getString("catid");
                JSONArray reminders = value.getJSONArray("reminders");
                String forms = "";
                for(int i = 0; i < reminders.length(); i++){
                    forms += reminders.getString(i)+", ";
                }
                forms = forms.substring(0, forms.length()-2);
                //System.out.println("Skickar mail till ["+mail+"] om påminnelse för "+forms+" till katt " + catId);

            }
            //((HomePage)context).closeLoadingDialog();
            ((HomePage)context).getHomePageHandler().closeStandardDialog();
            //System.out.println("CLOSE DIALOG REMINDERS");

        } catch ( JSONException jsone){
            jsone.printStackTrace();
        }
    }


    @Override
    public void run(){


        while(true){
            try {
                String[] smsMessage = smsMessages.take();
                System.out.println("[SENDING SMS]\nNr: "+smsMessage[0]+"\nMessage:"+smsMessage[1]);
                //Thread.sleep(2000);
                //smsSender.sendSms(smsMessage[0], smsMessage[1]);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
    */
}
