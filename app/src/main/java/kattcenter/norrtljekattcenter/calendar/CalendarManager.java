package kattcenter.norrtljekattcenter.calendar;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Jocke on 2017-06-18.
 */

public class CalendarManager {

    private Context context;
    public CalendarManager(Context context){
        this.context = context;
    }


    public void addEvent(String title, String description, String startDate, String endDate){
        addToCalendar(title, description, startDate, endDate);
    }

    private void addToCalendar(String title, String description, String startDateString, String endDateString) {
        SharedPreferences settings = context.getSharedPreferences("UserInfo", 0);
        String email = settings.getString("CalendarEmail", null);
        int calendarID = -1;
        String calendarEmail = email;

        String[] projection = new String[]{CalendarContract.Calendars._ID, CalendarContract.Calendars.ACCOUNT_NAME};
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(Uri.parse("content://com.android.calendar/calendars"), projection,
                CalendarContract.Calendars.ACCOUNT_NAME + "=? and (" +
                        CalendarContract.Calendars.NAME + "=? or " +
                        CalendarContract.Calendars.CALENDAR_DISPLAY_NAME + "=?)",
                new String[]{calendarEmail, calendarEmail, calendarEmail}, null);
        if (cursor.moveToFirst()) {
            if (cursor.getString(1).equals(calendarEmail)) {
                calendarID = cursor.getInt(0);
            }
        }
        if(calendarID != -1) {
            long startMillis = 0;
            long endMillis = 0;

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date startDate = null, endDate = null;
            try {
                startDate = simpleDateFormat.parse(startDateString + " 08:00:00");
                startMillis = startDate.getTime();
                endDate = simpleDateFormat.parse(endDateString + " 17:00:00");
                endMillis = endDate.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            TimeZone timeZone = TimeZone.getDefault();
            values.put(CalendarContract.Events.DTSTART, startMillis);
            values.put(CalendarContract.Events.DTEND, endMillis);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
            values.put(CalendarContract.Events.TITLE, title);
            values.put(CalendarContract.Events.DESCRIPTION, description);
            values.put(CalendarContract.Events.CALENDAR_ID, calendarID);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
            requestCalendarSync();
        }
        else{
            System.out.println("PLEASE CHANGE CALENDAR EMAIL");
        }
    }

    private void requestCalendarSync() {
        AccountManager aM = AccountManager.get(context);
        Account[] accounts = aM.getAccounts();

        for (Account account : accounts)
        {
            int isSyncable = ContentResolver.getIsSyncable(account,  CalendarContract.AUTHORITY);

            if (isSyncable > 0)
            {
                Bundle extras = new Bundle();
                extras.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                ContentResolver.requestSync(accounts[0], CalendarContract.AUTHORITY, extras);
            }
        }
    }
}
