package kattcenter.norrtljekattcenter;

/**
 * Created by Jocke on 2017-05-31.
 */

public class HamburgerListItem {

    String ItemName;
    int imgResID;

    public HamburgerListItem(String itemName, int imgResID) {
        super();
        ItemName = itemName;
        this.imgResID = imgResID;
    }

    public String getItemName() {
        return ItemName;
    }
    public void setItemName(String itemName) {
        ItemName = itemName;
    }
    public int getImgResID() {
        return imgResID;
    }
    public void setImgResID(int imgResID) {
        this.imgResID = imgResID;
    }
}
