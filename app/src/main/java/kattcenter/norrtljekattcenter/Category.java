package kattcenter.norrtljekattcenter;

/**
 * Created by Jocke on 2017-06-01.
 */

public class Category {
    private String title;
    private int amount;
    private Type type;

    public Category(String title, int amount, Type type){
        this.title = title;
        this.amount = amount;
        this.type = type;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    public int getAmount(){
        return amount;
    }

    public void setType(Type type){
        this.type = type;
    }

    public Type getType(){
        return type;
    }
}
