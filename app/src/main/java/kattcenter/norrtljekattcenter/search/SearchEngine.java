package kattcenter.norrtljekattcenter.search;

import android.content.Context;
import android.util.Log;

import com.itextpdf.text.pdf.languages.ArabicLigaturizer;

import java.lang.reflect.Array;
import java.util.ArrayList;

import kattcenter.norrtljekattcenter.CatListImageAdapter;
import kattcenter.norrtljekattcenter.CatownerListAdapter;
import kattcenter.norrtljekattcenter.HomePage;
import kattcenter.norrtljekattcenter.personel.PersonelListAdapter;

/**
 * Created by Joakim on 2017-07-27.
 */

public class SearchEngine {

    private Context context;
    private HomePage homePage;

    public SearchEngine(Context context){
        this.context = context;
        homePage = (HomePage) context;
    }

    public synchronized void query(CharSequence charsequence){
        String searchType = homePage.getCurrentSearchType();
        //CatListImageAdapter catListImageAdapter = homePage.getHomePageHandler().getListHandler().getCatListImageAdapter();

        //ArrayList<String[]> catsInfo = catListImageAdapter.getCatsInfo();

        switch (searchType){
            case "PERSONAL":
                searchPersonel(charsequence);
                break;
            case "KATTÄGARE":
                searchCatowner(charsequence);
                break;
            case "VÄNTELISTA":

                break;
            default:
                searchCat(charsequence);
                break;
        }
    }

    private void searchCat(CharSequence charsequence){
        CatListImageAdapter catListImageAdapter = homePage.getHomePageHandler().getListHandler().getCatListImageAdapter();
        catListImageAdapter.setUrlDataList(catListImageAdapter.getUrlDataListCopy());
        catListImageAdapter.setCatsInfo(catListImageAdapter.getCatsInfoCopy());
        catListImageAdapter.notifyDataSetChanged();
        ArrayList<String[]> catsInfo = catListImageAdapter.getCatsInfo();
        ArrayList<String[]> newCatInfo = new ArrayList<>();
        ArrayList<String> catURL = catListImageAdapter.getUrlDataList();
        ArrayList<String> newCatURL = new ArrayList<>();
        int totalCats = catsInfo.size();
        for(int i = 0; i < totalCats; i++){
            String[] catInfo = catsInfo.get(i);
            String catId = catInfo[0].toLowerCase();
            String catName = catInfo[1].toLowerCase();
            String lowerCaseCharsequence = charsequence.toString().toLowerCase();
            charsequence = (CharSequence) lowerCaseCharsequence;
            if(catId.contains(charsequence)){
                Log.i("SEARCHENGINE", "Found a match by ID: "+catId);
                newCatInfo.add(catInfo);
                newCatURL.add(catURL.get(i));
            }
            else if(catName.contains(charsequence)){
                Log.i("SEARCHENGINE", "Found a match by Name: "+catName);
                newCatInfo.add(catInfo);
                newCatURL.add(catURL.get(i));
            }
        }
        catListImageAdapter.setCatsInfo(newCatInfo);
        catListImageAdapter.setUrlDataList(newCatURL);
        catListImageAdapter.notifyDataSetChanged();
    }

    private void searchCatowner(CharSequence charSequence){
        CatownerListAdapter catownerListAdapter = homePage.getHomePageHandler().getListHandler().getCatownerListAdapter();
        catownerListAdapter.setCatOwnerNames(catownerListAdapter.getCatOwnerNamesCopy());
        catownerListAdapter.setCatOwnerEmails(catownerListAdapter.getCatOwnerEmailsCopy());
        catownerListAdapter.setCatOwnerStatuses(catownerListAdapter.getCatOwnerStatusesCopy());
        catownerListAdapter.notifyDataSetChanged();
        ArrayList<String> newCatOwnerNames = new ArrayList<>(), newCatOwnerEmails = new ArrayList<>(), newCatOwnerStatuses = new ArrayList<>();
        ArrayList<String> catownerNames = catownerListAdapter.getCatOwnerNames(), catownerEmails = catownerListAdapter.getCatOwnerEmails(),
                catownerStatuses = catownerListAdapter.getCatOwnerStatuses();
        int totalCatowners = catownerNames.size();
        for(int i = 0; i < totalCatowners; i++){
            String name = catownerNames.get(i).toLowerCase();
            String email = catownerEmails.get(i).toLowerCase();
            String lowerCaseCharsequence = charSequence.toString().toLowerCase();
            charSequence = lowerCaseCharsequence;
            if(name.contains(charSequence) || email.contains(charSequence)){
                newCatOwnerNames.add(catownerNames.get(i));
                newCatOwnerEmails.add(catownerEmails.get(i));
                newCatOwnerStatuses.add(catownerStatuses.get(i));
            }
        }
        catownerListAdapter.setCatOwnerNames(newCatOwnerNames);
        catownerListAdapter.setCatOwnerEmails(newCatOwnerEmails);
        catownerListAdapter.setCatOwnerStatuses(newCatOwnerStatuses);
        catownerListAdapter.notifyDataSetChanged();
    }

    private void searchPersonel(CharSequence charSequence){
        PersonelListAdapter personelListAdapter = homePage.getHomePageHandler().getListHandler().getPersonelListAdapter();
        personelListAdapter.setPersonelNames(personelListAdapter.getPersonelNamesCopy());
        personelListAdapter.setPersonelEmails(personelListAdapter.getPersonelEmailsCopy());
        personelListAdapter.setPersonelStatuses(personelListAdapter.getPersonelStatusesCopy());
        personelListAdapter.notifyDataSetChanged();
        ArrayList<String> newPersonelNames = new ArrayList<>(), newPersonelEmails = new ArrayList<>(), newPersonelStatuses = new ArrayList<>();
        ArrayList<String> personelNames = personelListAdapter.getPersonelNames(), personelEmails = personelListAdapter.getPersonelEmails(),
                personelStatuses = personelListAdapter.getPersonelStatuses();
        int totalPersonels = personelNames.size();
        for(int i = 0; i < totalPersonels; i++){
            String name = personelNames.get(i).toLowerCase();
            String email = personelEmails.get(i).toLowerCase();
            String lowerCaseCharsequence = charSequence.toString().toLowerCase();
            charSequence = lowerCaseCharsequence;
            if(name.contains(charSequence) || email.contains(charSequence)){
                newPersonelNames.add(personelNames.get(i));
                newPersonelEmails.add(personelEmails.get(i));
                newPersonelStatuses.add(personelStatuses.get(i));
            }
        }
        personelListAdapter.setPersonelNames(newPersonelNames);
        personelListAdapter.setPersonelEmails(newPersonelEmails);
        personelListAdapter.setPersonelStatuses(newPersonelStatuses);
        personelListAdapter.notifyDataSetChanged();
    }
}
