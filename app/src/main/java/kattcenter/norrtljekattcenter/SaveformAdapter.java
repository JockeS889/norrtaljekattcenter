package kattcenter.norrtljekattcenter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Jocke on 2017-06-08.
 */

public class SaveformAdapter extends BaseAdapter{

    private Context context;
    private SaveformHandler saveformHandler;
    private HashMap<String, Boolean> checkedLockedForms = new HashMap<>();
    private HashMap<String, Boolean> checkedMailForms = new HashMap<>();
    private HashMap<String, View> rows = new HashMap<>();
    private HashMap<String, Boolean> lockedForms = null;
    private ArrayList<String> forms;
    private String[] dbTables;
    private View parentView;

    public SaveformAdapter(Context context, View parentView, ArrayList<String> forms, String[] dbTables, SaveformHandler saveformHandler){
        this.context = context;
        this.parentView = parentView;
        this.forms = forms;
        this.saveformHandler = saveformHandler;
        this.dbTables = dbTables;
        lockedForms = new HashMap<>();
        for(String s: forms){
            checkedLockedForms.put(s, false);
            checkedMailForms.put(s, false);
        }
        for(String s: dbTables){
            lockedForms.put(s, false);
        }

        this.saveformHandler.setCheckedForms(checkedLockedForms);
    }

    public View getRowByFormTitle(String title){
        return rows.get(title);
    }

    public void setLockedForms(HashMap<String, Boolean> lockedForms){
        this.lockedForms = lockedForms;
    }

    @Override
    public int getCount() {
        return forms.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent){
        final String form = forms.get(position);
        if(convertView == null){
            convertView = ((HomePage) context).getLayoutInflater().inflate(R.layout.lockform_dialog_row_item, null);
        }
        String dbTable = dbTables[position];

        /*
        CheckBox lock_checkBox = (CheckBox) convertView.findViewById(R.id.form_locker_checkbox);
        if(!((HomePage)context).isFormEnabled(dbTable)){
            lock_checkBox.setEnabled(false);
        }
        */

        ((TextView)convertView.findViewById(R.id.lockform_row_title)).setText(form);
        final LinearLayout row = (LinearLayout) convertView.findViewById(R.id.rowLinearLayout);
        final TextView lockStatus = (TextView) convertView.findViewById(R.id.form_locker_text_status);

        /*
        lock_checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkedLockedForms.put(form, isChecked);
                int amount_checked = 0;

                Set<String> keys = checkedLockedForms.keySet();
                HashMap<String, String> titlesToDBTables = ((HomePage)context).getTitlesToDbtables();
                for(String s: keys){
                    if(checkedLockedForms.get(s)){
                        amount_checked++;
                    }
                }
                if(amount_checked > 0){
                    (parentView.findViewById(R.id.lockForms)).setBackgroundColor(Color.parseColor("#555555"));
                    (parentView.findViewById(R.id.lockForms)).setEnabled(true);
                    //(parentView.findViewById(R.id.mailForms)).setBackgroundColor(Color.parseColor("#555555"));
                    //(parentView.findViewById(R.id.mailForms)).setEnabled(true);
                }
                else{
                    (parentView.findViewById(R.id.lockForms)).setBackgroundColor(Color.parseColor("#AAAAAA"));
                    (parentView.findViewById(R.id.lockForms)).setEnabled(false);
                    //(parentView.findViewById(R.id.mailForms)).setBackgroundColor(Color.parseColor("#AAAAAA"));
                    //(parentView.findViewById(R.id.mailForms)).setEnabled(false);
                }
                rows.put(form, row);
                saveformHandler.setCheckedForms(checkedLockedForms);
            }
        });
        */
        ((CheckBox)convertView.findViewById(R.id.form_mail_checkbox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkedMailForms.put(form, isChecked);
                int amount_checked = 0;
                String dbTable = dbTables[position];
                Set<String> keys = checkedMailForms.keySet();
                HashMap<String, String> titlesToDBTables = ((HomePage)context).getTitlesToDbtables();
                for(String s: keys){
                    if(checkedMailForms.get(s)){
                        amount_checked++;
                    }
                }
                if(amount_checked > 0){
                    (parentView.findViewById(R.id.mailForms)).setBackgroundColor(Color.parseColor("#555555"));
                    (parentView.findViewById(R.id.mailForms)).setEnabled(true);
                }
                else{
                    (parentView.findViewById(R.id.mailForms)).setBackgroundColor(Color.parseColor("#AAAAAA"));
                    (parentView.findViewById(R.id.mailForms)).setEnabled(false);
                }
                saveformHandler.setCheckedMailForms(checkedMailForms);
            }
        });
        return convertView;
    }
}
